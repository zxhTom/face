package com.bshinfo.web.model.dataTableUtil;

import java.io.Serializable;

/**
 * datatable接收参数的实体类<br/>
 * 
 * 功能描述: 接收前台表格datatable发送的参数实体<BR/>
 * 
 * @author 	[jixingwang]
 * 
 * @see 	[DataTableParam]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-2-17 16:20:25]
 */
public class DataTableParam implements Serializable 
{
	private static final long serialVersionUID = 6003791544948183660L;
	private String name;
	private String value;
	
	public DataTableParam(String name, String value) 
	{
		super();
		this.name = name;
		this.value = value;
	}

	public DataTableParam() 
	{
		super();
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
