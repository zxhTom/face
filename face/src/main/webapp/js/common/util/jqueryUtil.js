jQuery.extend(jQuery, {
	getBasePath : function() 
	{
		var curWwwPath = window.document.location.href;// 获取当前网址，如：
		// http://localhost:8083/uimcardprj/share/meun.jsp
		var pathName = window.document.location.pathname;// 获取主机地址之后的目录，如：
		// uimcardprj/share/meun.jsp
		var pos = curWwwPath.indexOf(pathName);
		var localhostPaht = curWwwPath.substring(0, pos);// 获取主机地址，如：
		// http://localhost:8083
		var projectName = pathName.substring(0, pathName.substr(1).indexOf("/") + 1);// 获取带"/"的项目名，如：/uimcardprj
		var basePath = localhostPaht + projectName;
		return basePath;
	}
});