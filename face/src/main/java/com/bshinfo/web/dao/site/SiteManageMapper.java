package com.bshinfo.web.dao.site;

import java.util.List;
import java.util.Map;

import com.bshinfo.web.base.model.Page;

public interface SiteManageMapper
{
	public List<Map<String, Object>> selectAllArea();

	/***
	 * 带条件分页查询所有的站点
	 * 
	 * @return [java.util.List<Map<String, Object>>]
	 * 
	 * @author [wudongwei]
	 * 
	 * @Date [2017-2-17 16:29:26]
	 */
	public List<Map<String, Object>> selectSiteListByConditionAndPage(Page<Map<String, Object>> page);

	/***
	 * 带条件查询所有的站点数量
	 * 
	 * @return [java.util.List<Map<String, Object>>]
	 * 
	 * @author [wudongwei]
	 * 
	 * @Date [2017-2-17 16:29:26]
	 */
	long countAllSiteByConditionMap(Map<String, String> conditionMap);
	
	/**
	 * 编辑站点信息
	 * @param parameterMap
	 * @return
	 */
	int modifySiteInfoByInfo(Map<String,Object> parameterMap);
	/**
	 * 新增一个站点
	 * 
	 * @param parameter
	 * @return
	 */
	public int addNewSite(Map<String, Object> parameter);
	
	/**
	 * 删除某个站点的所有覆盖区域
	 * @param siteId
	 * @return
	 */
	public int deleteAllAreaBySite(String siteId);
	/**
	 * 查询一个站点信息
	 * @param parameter
	 * @return
	 */
	public Object selecOneSiteInfo(String siteId);
	/**
	 * 新增站点区域联系
	 * 
	 * @param parameter
	 * @return
	 */
	public int addNewSiteArea(Map<String, Object> parameter);

	/**
	 * 启用网站
	 * 
	 * @param siteId
	 * @return
	 */
	public int startSite(String siteId);

	/**
	 * 暂停网站
	 * 
	 * @param siteId
	 * @return
	 */
	public int pauseSite(String siteId);

	/**
	 * 逻辑删除站点
	 * 
	 * @param siteId
	 * @return
	 */
	public int deleteSite(String siteId);
	
	/**
	 * 根据站点统计该站点用户
	 * @param siteId
	 * @return
	 */
	public Long countUsersBySite(String siteId);
}
