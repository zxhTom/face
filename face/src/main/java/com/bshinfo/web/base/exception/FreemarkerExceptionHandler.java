package com.bshinfo.web.base.exception;

import java.io.Writer;

import org.apache.log4j.Logger;

import freemarker.core.Environment;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;


public class FreemarkerExceptionHandler implements TemplateExceptionHandler
{
	/**日志工具*/
	public static Logger logger = Logger.getLogger(FreemarkerExceptionHandler.class);
	
	@Override
	public void handleTemplateException(TemplateException te, Environment env,Writer out) throws TemplateException
	{
		logger.error("[Freemarker Error,ErrorMsg]:",te);
		
		throw new FreemarkerException("freemarker error",te);
		
	}

}
