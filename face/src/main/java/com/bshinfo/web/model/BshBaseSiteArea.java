package com.bshinfo.web.model;

import java.io.Serializable;

/**
 * 站点-区域联系 实体类<br/>
 * 
 * 功能描述: 站点所覆盖的区域<BR/>
 * 
 * @author [wudongwei]
 * 
 * @see [MainMenu]
 * 
 * @since [JDK 1.7]
 * 
 * @version [1.0]
 * 
 * @Date [2017-2-17 13:21:31]
 */
public class BshBaseSiteArea implements Serializable
{
	/**
	 *  java 序列化实现 (实体类必须添加)
	 */
	private static final long serialVersionUID = 8433902852061991523L;

	// 站点ID:bsh_base_site.id
	private String siteId;

	// 区域ID:bsh_base_area.id
	private String areaId;

	// 创建人ID:bsh_base_user_info.id
	private String createUserId;

	// 创建时间
	private java.util.Date createTime;

	// 更新时间
	private java.util.Date updateTime;

	public String getSiteId()
	{

		return this.siteId;
	}

	public void setSiteId(String siteId)
	{

		this.siteId = siteId;
	}

	public String getAreaId()
	{

		return this.areaId;
	}

	public void setAreaId(String areaId)
	{

		this.areaId = areaId;
	}

	public String getCreateUserId()
	{

		return this.createUserId;
	}

	public void setCreateUserId(String createUserId)
	{

		this.createUserId = createUserId;
	}

	public java.util.Date getCreateTime()
	{

		return this.createTime;
	}

	public void setCreateTime(java.util.Date createTime)
	{

		this.createTime = createTime;
	}

	public java.util.Date getUpdateTime()
	{

		return this.updateTime;
	}

	public void setUpdateTime(java.util.Date updateTime)
	{

		this.updateTime = updateTime;
	}
}
