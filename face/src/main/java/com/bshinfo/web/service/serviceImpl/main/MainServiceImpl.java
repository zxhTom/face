package com.bshinfo.web.service.serviceImpl.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.bshinfo.web.dao.main.MainMenuMapper;
import com.bshinfo.web.service.serviceI.main.MainServiceI;

/**
 * MainMenu业务逻辑层实现类 <br/>
 * 
 * 功能描述: 对MainMenu的各种业务处理 <BR/>
 * 
 * @author 	[jixingwang]
 * 
 * @see 	[MainServiceImpl]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-2-17 16:20:25]
 */
@Service
public class MainServiceImpl implements MainServiceI 
{
	@Override
	public List<Map<String, Object>> selectMenu(String userId) 
	{
		//获取一级菜单
		List<Map<String, Object>> parentMenu = mainMenuMapper.selectParentMenu(userId);
		//一级菜单是空直接退出方法
		if(parentMenu == null || parentMenu.size() <= 0)
		{
			return null;
		}
		
		//一级菜单的id（即是二级菜单的父级id）
		List<String> parentIds = new ArrayList<String>();
		for (Map<String, Object> map : parentMenu) 
		{
			parentIds.add(map.get("id").toString());
		}
		//获取二级菜单
		List<Map<String, Object>> subMenu = mainMenuMapper.selectSubMenu(parentIds,userId);
		for (Map<String, Object> parent : parentMenu) 
		{
			List<Map<String,Object>> childrenList = new ArrayList<Map<String,Object>>();
			for (int i = 0; i < subMenu.size(); i++) 
			{
				if (parent.get("id").toString().equals(subMenu.get(i).get("parentId").toString())) 
				{	
					childrenList.add(subMenu.get(i));
					subMenu.remove(i);
					i --;
				}
			}
			parent.put("subMenus", childrenList);
		}
		
		return parentMenu;
	}

	@Resource
	MainMenuMapper mainMenuMapper;
}
