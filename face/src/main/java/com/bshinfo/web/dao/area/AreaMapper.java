package com.bshinfo.web.dao.area;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

/**
 * Area数据访问层接口 <br/>
 * 
 * 功能描述: 对Area的原子性操作 <BR/>
 * 
 * @author 	[hezhijian]
 * 
 * @see 	[com.bshinfo.web.model.AreaInfo]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-3-1 11:33:14]
 */
public interface AreaMapper 
{

	/**
	 * 查询区域
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author 	[hezhijian]
     * 
     * @Date    [2017-3-1 11:38:47]
	 */
	List<Map<String, Object>> getAreaTree();

	/***
	 * 获取区域名称和编码信息
	 * 
	 * @param [java.lang.String] areaId 区域id
	 * 
	 * @return [java.util.Map<String, Object>] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-3-1 14:15:58]
	 */
	Map<String, Object> getAreaInfoByAreaId(String areaId);
	
	/***
	 * 获取区域所有信息
	 * 
	 * @param [java.lang.String] areaId 区域id
	 * 
	 * @return [java.util.Map<String, Object>] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-3-1 15:32:20]
	 */
	Map<String, Object> getAreaAllInfoByAreaId(String areaId);

	/***
	 * 新增的时候检测名字是否重复
	 * 
	 * @param [java.lang.String] areaName 区域名称
	 * 
	 * @param [java.lang.String] parentId 父级Id
	 * 
	 * @return [java.lang.Integer] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-3-1 16:24:22]
	 */
	Integer checkNameAdd(@Param("areaName")String areaName, @Param("parentId")String parentId);

	/***
	 * 新增子区域
	 * 
	 * @param [java.util.Map<String, Object>] areaMap 区域信息的集合
	 * 
	 * @return [] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-3-1 16:33:22]
	 */
	void addArea(Map<String, Object> areaMap);

	/***
	 * 修改的时候检测名字是否重复
	 * 
	 * @param [java.lang.String] areaName 区域名称
	 * 
	 * @param [java.lang.String] areaId 区域id
	 * 
	 * @return [java.lang.Integer] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-3-1 16:44:22]
	 */
	Integer checkNameEdit(@Param("areaName")String areaName, @Param("areaId")String areaId);

	/***
	 * 修改子区域
	 * 
	 * @param [java.util.Map<String, Object>] areaMap 区域信息的集合
	 * 
	 * @return [] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-3-1 17:05:00]
	 */
	void editArea(Map<String, Object> areaMap);

	/***
	 * 检测是否含有子区域
	 * 
	 * @param [java.lang.String] areaId 区域id
	 * 
	 * @return [java.lang.Integer] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-3-2 09:41:42]
	 */
	Integer checkAreaChild(String areaId);

	/***
	 * 修改子区域
	 * 
	 * @param [java.lang.String] areaId 区域id
	 * 
	 * @return [] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-3-2 09:53:11]
	 */
	void deleteArea(String areaId);

}
