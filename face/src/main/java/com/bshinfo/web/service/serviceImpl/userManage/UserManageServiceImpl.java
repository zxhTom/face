package com.bshinfo.web.service.serviceImpl.userManage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Service;

import com.bshinfo.web.base.model.Page;
import com.bshinfo.web.base.util.ConstantUtil;
import com.bshinfo.web.base.util.PaginationUtil;
import com.bshinfo.web.dao.userManage.UserManageMapper;
import com.bshinfo.web.model.dataTableUtil.DataTableModel;
import com.bshinfo.web.service.serviceI.userManage.UserManageService;

/**
 * UserManage业务逻辑层实现类 <br/>
 * 
 * 功能描述: 对UserManage的各种业务处理 <BR/>
 * 
 * @author 	[donghang]
 * 
 * @see 	[UserManageServiceImpl]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-2-20]
 */
@Service
public class UserManageServiceImpl implements UserManageService
{

	@Override
	public DataTableModel getUserTable(Map<String, String> dataTableMap,String id) 
	{
		Map<String,Object> paramMap = new HashMap<String,Object>();
		
		DataTableModel dataTableModel = new DataTableModel();
		
		List<List<Object>> data=new ArrayList<List<Object>>();
		
		String sEcho = dataTableMap.get("sEcho");
		int start = Integer.parseInt(dataTableMap.get("iDisplayStart"));
		int length = Integer.parseInt(dataTableMap.get("iDisplayLength"));
		int currentPage = start/length + 1;
		//此id为当前登陆用户id
		paramMap.put("id", id);
		paramMap.put("admin", ConstantUtil.ADMIN_ID);
		paramMap.put("userName",dataTableMap.get("userName"));
		paramMap.put("start",dataTableMap.get("start")+ ("".equals(dataTableMap.get("start")) || dataTableMap.get("start") == null ? "" : " 00:00:00"));
		paramMap.put("end",dataTableMap.get("end")+ ("".equals(dataTableMap.get("end")) || dataTableMap.get("end") == null ? "" : " 23:59:59"));
		paramMap.put("site",dataTableMap.get("site"));
		paramMap.put("realName",dataTableMap.get("realName"));
		paramMap.put("status",dataTableMap.get("status"));
		Page<Map<String, Object>> page = PaginationUtil.setPageInfoStart(paramMap,currentPage,length);
		
		List<Map<String, Object>> resList = this.userManage.getUserTable(page);
		
		Integer count = userManage.getUserTableCount(paramMap);
		
		for (Map<String, Object> map : resList) 
		{
			List<Object> rowList = new ArrayList<Object>();
			rowList.add(map.get("id"));
			rowList.add(map.get(""));
			rowList.add(map.get("user_name"));
			rowList.add(map.get("verify_status"));
			rowList.add(map.get("site_name"));
			rowList.add(map.get("create_time"));
			rowList.add(map.get("user_status"));
			rowList.add("");
			rowList.add(map.get("verify_img_front_url")+"?t="+ System.currentTimeMillis());
			rowList.add(map.get("verify_img_back_url")+"?t="+ System.currentTimeMillis());
			data.add(rowList);
		}
		dataTableModel.setiTotalDisplayRecords(count);
		dataTableModel.setiTotalRecords(count);
		dataTableModel.setsEcho(Integer.valueOf(sEcho));
		dataTableModel.setAaData(data);
		return dataTableModel;
	}
	
	@Override
	public Map<String, Object> getUserById(String id) 
	{
		return userManage.getUserById(id);
	}
	
	@Override
	public boolean resetPassword(String id) 
	{
		userManage.resetPassword(id);
		return true;
	}
	
	@Override
	public boolean deleteUserById(String id) 
	{
		userManage.deleteUserById(id);
		userManage.deleteUserRelation(id);
		return true;
	}
	
	@Override
	public boolean changeStatusById(String id, String status) 
	{
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("id",id);
		paramMap.put("status",status);
		if(status.equals("0"))
		{
			userManage.changeStatusById(paramMap);
		}else
		{
			userManage.changeStatusById(paramMap);
		}	
		return true;
	}
	
	@Override
	public boolean apprUserVerifyById(String id, String message, String status) 
	{
		
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("id",id);
		paramMap.put("status",status);
		paramMap.put("message", message);
		if("1".equals(status))
		{
			userManage.changeUserVerifyById(paramMap);
		}
			
		userManage.apprUserVerifyById(paramMap);
		
		return true;
	}

	@Override
	public void addUser(Map<String, Object> paramMap) 
	{
		userManage.addUserToInfo(paramMap);
		userManage.addUserToAttr(paramMap);
		userManage.addUserToLogin(paramMap);
	}

	@Override
	public void editUser(Map<String, Object> paramMap) 
	{
		userManage.editUserToInfo(paramMap);
		userManage.editUserToAttr(paramMap);
		userManage.editUserToLogin(paramMap);
	}

	@Override
	public void changeUsersStatus(String ids,String status) 
	{
		Map<String,Object> paramMap = new HashMap<String,Object>();
		ArrayList<String>  list = new ArrayList<String>();
		JSONObject userIds = JSONObject.fromObject(ids);
	    if(userIds!=null)
	    {
	        @SuppressWarnings("unchecked")
			Iterator<String> iterator = userIds.keys();
	        while(iterator.hasNext())
	        {
	          String key = (String) iterator.next();
              String value = userIds.getString(key);
              list.add(value);      
	        }
	    }
		paramMap.put("list", list);
		paramMap.put("status", status);
		userManage.changeUsersStatus(paramMap);
	}
	
	@Override
	public List<Map<String, Object>> getSelectList(String id) 
	{
		return userManage.getRuleSite(id);
	}

	@Override
	public List<Map<String, Object>> getCity() 
	{
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("type", "2");
		return userManage.getCity(paramMap);
	}
	
	@Override
	public List<Map<String, Object>> getArea(String id) 
	{
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("type", "3");
		paramMap.put("id", id);
		return userManage.getCity(paramMap);
	}
	
	@Override
	public List<Map<String, Object>> getSiteTree(String id) {
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("id", id);
		return userManage.getSiteTree(paramMap);
	}

	@Override
	public boolean checkUser(Map<String, Object> map) {
		int i = userManage.checkUser(map);
		if(i>0)
		{
			return false;
		}else
		{
			return true;
		}
	}

	@Override
	public String queryByName(String siteName) {
		String siteId="";
		Map<String,Object> map =userManage.querySiteByName(siteName);
		if(map!=null)
		{
			siteId = map.get("id").toString();
		}
		return siteId;
	}
	
	@Override
	public Map<String, Object> loginCheck(String username, String password) {
		
		return userManage.loginCheck(username, password);
	}

	@Resource
	private UserManageMapper userManage;

	@Override
	public Map<String, Object> getUserRole(String userId,String menuId) {
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("userId",userId);
		paramMap.put("menuId",menuId);
		List<Map<String,Object>> list = userManage.getUserRole(paramMap);
		Map<String,Object> resultMap = new HashMap<String,Object>();
		for(Map<String,Object> map:list)
		{
			resultMap.put(map.get("menu_code").toString(),true);
		}
		return resultMap;
	}

}
