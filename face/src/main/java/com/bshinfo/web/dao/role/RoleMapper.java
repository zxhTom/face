package com.bshinfo.web.dao.role;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.bshinfo.web.base.model.Page;

/**
 * Role数据访问层接口 <br/>
 * 
 * 功能描述: 对Role的原子性操作 <BR/>
 * 
 * @author 	[hezhijian]
 * 
 * @see 	[com.bshinfo.web.model.RoleInfo]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-2-20 11:16:00]
 */
public interface RoleMapper 
{
	/**
	 * 查询所有角色
	 * 
	 * @param [com.bshinfo.web.base.model.Page<Map<String, Object>>] page 前台传过来的参数
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author 	[hezhijian]
     * 
     * @Date    [2017-2-20 11:28:49]
	 */
	List<Map<String, Object>> roleList(Page<Map<String, Object>> page);

	/**
	 * 查询角色总数
	 * 
	 * @param [java.util.Map<String, Object>] param 前台传过来的参数
	 * 
	 * @return [java.lang.Integer] 
	 * 
     * @author 	[hezhijian]
     * 
     * @Date    [2017-2-20 11:28:49]
	 */
	Integer findRoleCount(Map<String, Object> param);

	/**
	 * 查询权限
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author 	[hezhijian]
     * 
     * @Date    [2017-2-21 14:37:10]
	 */
	List<Map<String, Object>> getPowerTree();

	/**
	 * 新增角色
	 * 
	 * @param [java.util.Map<String, Object>] roleMap 新增角色的数据
	 * 
	 * @return [java.lang.Integer] 
	 * 
     * @author 	[hezhijian]
     * 
     * @Date    [2017-2-21 14:37:10]
	 */
	Integer addRole(Map<String, Object> roleMap);

	/***
	 * 检测角色名称是否唯一
	 * 
	 * @Param [java.util.Map<String, Object>] roleMap 角色信息集合
	 * 
	 * @return [int] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-2-22 09:57:38]
	 */
	int checkName(Map<String, Object> roleMap);

	/***
	 * 给对应得资源id添上类型
	 * 
	 * @Param [java.util.List<String>] power 资源id集合
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-2-22 10:56:38]
	 */
	List<Map<String, Object>> getPowerType(@Param("power")List<String> power);

	/***
	 * 新增角色权限关联
	 * 
	 * @Param [java.util.List<Map<String, Object>>] powerList 角色和权限数据的集合
	 * 
	 * @return [] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-2-22 11:28:25]
	 */
	void addRoleAndPower(@Param("powerList")List<Map<String, Object>> powerList);

	/***
	 * 根据id查询角色信息
	 * 
	 * @Param [java.lang.String] id 角色id
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-2-22 13:18:40]
	 */
	List<Map<String, Object>> selectRoleById(String id);

	/**
	 * 修改角色
	 * 
	 * @param [java.util.Map<String, Object>] roleMap 修改角色的数据
	 * 
	 * @return [java.lang.Integer] 
	 * 
     * @author 	[hezhijian]
     * 
     * @Date    [2017-2-22 15:28:41]
	 */
	int editRole(Map<String, Object> roleMap);

	/***
	 * 根据角色Id删除角色权限关联
	 * 
	 * @Param [java.util.List<String>] roleIdList 角色ids集合
	 * 
	 * @return [] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-2-22 15:40:40]
	 */
	void deleteRoleAndPower(@Param("roleIdList")List<String> roleIdList);

	/***
	 * 根据角色id查询角色被哪些人使用
	 * 
	 * @Param [java.lang.String] roleId 角色id
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-2-22 16:05:24]
	 */
	List<Map<String, Object>> checkRoleById(String roleId);

	/***
	 * 根据角色Id删除角色信息
	 * 
	 * @Param [java.util.List<String>] roleIdList 角色ids集合
	 * 
	 * @return [] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-2-22 16:41:59]
	 */
	void deleteRole(@Param("roleIdList")List<String> roleIdList);
	
	/***
	 * 根据角色ids查询哪些角色被使用
	 * 
	 * @Param [java.util.List<String>] roleIdList 角色ids集合
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-2-22 17:07:06]
	 */
	List<Map<String, Object>> checkRoleByIds(@Param("roleIdList")List<String> roleIdList);

	/***
	 * 根据角色id查询该角色拥有的权限
	 * 
	 * @Param [java.lang.String] roleId 角色id
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-2-23 09:39:21]
	 */
	List<Map<String, Object>> getPowerTreeById(String roleId);

	/***
	 * 根据角色id查询拥有该角色用户的id
	 * 
	 * @Param [java.lang.String] roleId 角色id
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-3-6 09:30:39]
	 */
	List<Map<String, Object>> getUserIdById(String roleId);
}
