<#assign base=request.contextPath />
<!DOCTYPE>
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta content="width=device-width, initial-scale=1" name="viewport" />

		<meta content="" name="description" />

		<meta content="" name="author" />

		<title>课程中心</title>

		<!-- BEGIN GLOBAL MANDATORY STYLES -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
		<!-- END GLOBAL MANDATORY STYLES -->
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
		<!-- END PAGE LEVEL PLUGINS -->
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

		<!-- BEGIN THEME GLOBAL STYLES -->
		<link href="${base}/js/library/metronic-4.7/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
		<!-- END THEME GLOBAL STYLES -->
		<!-- BEGIN THEME LAYOUT STYLES -->
		<link href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
		<link href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
		<!-- 弹出框 -->
		<link href="${base}/js/library/sweetalert/css/sweetalert.css" rel="stylesheet" type="text/css" />
		<!-- END THEME LAYOUT STYLES -->

		<!-- BEGIN CORE PLUGINS -->
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
		<!-- END CORE PLUGINS -->
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/moment.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-CN.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL PLUGINS -->
		<script src="${base}/js/library/metronic-4.7/assets/global/scripts/datatable.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jstree/dist/jstree.min.js" type="text/javascript"></script>
		<!-- BEGIN THEME GLOBAL SCRIPTS -->
		<script src="${base}/js/library/metronic-4.7/assets/global/scripts/app.min.js" type="text/javascript"></script>
		<!-- END THEME GLOBAL SCRIPTS -->
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/ui-tree.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>

		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->
		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->
		<!-- BEGIN THEME LAYOUT SCRIPTS -->
		<script src="${base}/js/library/metronic-4.7/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
		<!-- BEGIN 表单判空 -->
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery-validation/js/jquery.validate.js" type="text/javascript"></script>
		<!-- END 表单判空 -->
		<!-- 弹出框 -->
		<script src="${base}/js/library/sweetalert/js/sweetalert.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/sweetalert/js/showAlert.js" type="text/javascript"></script>
		<!-- END THEME LAYOUT SCRIPTS -->
	</head>

	<body id="CourseAdmin" style="background-color:#FFFFFF;">
		<!-- BEGIN PAGE BAR -->
		<div class="page-bar">
			<div class="page-toolbar">
				<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
					<i class="icon-calendar"></i>&nbsp;
					<span class="thin uppercase hidden-xs"></span>&nbsp;
					<i class="fa fa-angle-down"></i>
				</div>
			</div>
		</div>
		<!-- END PAGE BAR -->

		<!-- BEGIN PAGE TITLE-->
		<h3 class="page-title">
			课程中心 <small>分类管理</small><small id="TitleInfo"></small>
		</h3>
		<!-- END PAGE TITLE-->
		<!-- END PAGE HEADER-->
		<div class="row ">
			<div class="col-md-3 col-sm-3 col-xs-3">
				<div class="portlet light bordered">
					<div style="height:550px;overflow-y:scroll;">
						<div id="areaTree" class="tree-demo"> </div>
					</div>
				</div>
			</div>
			<div class="col-md-8 col-sm-8 col-xs-8">
				<div>
					<div style="flex: 1;">
						<div class="modal-dialog" role="document" style="height: 200px;">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title" id="myModalLabel">详细信息</h4>
								</div>
								<div style="width: 100%;height: 100%;display: flex;flex-direction: column;">
									<div style="height: 50px;display: flex; align-items: center;justify-content: center;"> 分类名称：<input id="SortName" class="form-control input-inline input-medium" type="text" readonly="readonly"></input>
									</div>
									<div style="height: 50px; display: flex; align-items: center;justify-content: center;"> 上级分类：<input id="UpSort" class="form-control input-inline input-medium" type="text" readonly="readonly"></input>
									</div>
									<div style="display: flex; align-items: center;justify-content: center;">
										<button type="button" id="btnAdd" class="btn green" style="margin-right: 20px;" data-toggle="modal" data-target="#windownAdd">新增</button>
										<button type="button" id="btnEdit" class="btn green" style="margin-right: 20px;" data-toggle="modal" data-target="#windowChange">修改</button>
										<button type="button" class="btn green" style="margin-right: 20px;" onclick="Del()">删除</button></div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>

		</div>
		<input type="hidden" id="hideUpSort" />
		<input type="hidden" id="hidelocalId" />
	</body>
	<!--
    	作者：tuxiaokang
    	时间：2017-02-23
    	描述：新增分类模态窗
    -->
	<div class="modal fade" id="windownAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document" style="height: 200px;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">新增分类</h4>
				</div>
				<form action="javascript:;" id="addvalidate" class="form-horizontal form-row-seperated">
					<div style="width: 100%;height: 100%;display: flex;flex-direction: column;">
						<div style="height: 100px;display: flex; align-items: center;justify-content: flex-start;"> <label class="col-sm-3 control-label">
								分类名称
								<span class="required"> * </span> </label><input id="MSortName" name="name" maxlength="10" minlength="2" class="form-control input-inline input-medium required " type="text"></input>
						</div>
						<div style="display: flex; align-items: center;justify-content: center;">
							<button type="submit" class="btn green" style="margin-right: 30px;">完成</button>
							<button type="button" class="btn green" class="btn btn-primary" data-dismiss="modal" style="margin-right: 40px;">取消</button></div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!--
    	作者：tuxiaokang
    	时间：2017-02-23
    	描述：修改分类模态窗
    -->
	<div class="modal fade" id="windowChange" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document" style="height: 200px;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">修改分类</h4>
				</div>
				<form action="javascript:;" id="editvalidate" class="form-horizontal form-row-seperated">
					<div style="width: 100%;height: 100%;display: flex;flex-direction: column;">
						<div style="height: 100px;display: flex; align-items: center;justify-content: flex-start;"> <label class="col-sm-3 control-label">
								分类名称
								<span class="required"> * </span> </label><input id="MSortName1" maxlength="10" minlength="2" class="form-control input-inline input-medium required" type="text"></input>
						</div>
						<div style="height: 50px;display: flex; align-items: center;justify-content: flex-start;"> <label class="col-sm-3 control-label">
								分类名称
								<span class="required"> * </span></label>
							<select id="MUpSortName1" class="form-control input-medium input-inline">
								<option id="selectUpSort"></option>
							</select>
						</div>
						<div style="display: flex; align-items: center;justify-content: center;">
							<button type="submit" class="btn green" style="margin-right: 30px;">完成</button>
							<button type="button" class="btn green" class="btn btn-primary" data-dismiss="modal" style="margin-right: 40px;">取消</button></div>
					</div>
				</form>
			</div>
		</div>

	</div>
	<script>
		jQuery.extend(jQuery, {
			getBasePath: function() {
				var curWwwPath = window.document.location.href; // 获取当前网址，如：
				// http://localhost:8083/uimcardprj/share/meun.jsp
				var pathName = window.document.location.pathname; // 获取主机地址之后的目录，如：
				// uimcardprj/share/meun.jsp
				var pos = curWwwPath.indexOf(pathName);
				var localhostPaht = curWwwPath.substring(0, pos); // 获取主机地址，如：
				// http://localhost:8083
				var projectName = pathName.substring(0, pathName.substr(1).indexOf("/") + 1); // 获取带"/"的项目名，如：/uimcardprj
				var basePath = localhostPaht + projectName;
				return basePath;
			}
		});
	</script>
	<script>
		function initjstree() 
		{
			//获取区域树
			//获取区域树
			$.ajax({
				type: "POST",
				dataType: 'json',
				url: jQuery.getBasePath() + "/category/getAllCategory.bsh",
				success: function(data) {
					$('#areaTree').jstree({
						'plugins': ["wholerow", "types"],
						'core': {
							"themes": {
								"responsive": false
							},
							'data': data.tree
						},
						"types": {
							"default": {
								"icon": "fa fa-folder icon-state-warning icon-lg"
							},
							"file": {
								"icon": "fa fa-file icon-state-warning icon-lg"
							}
						}
					}).bind("select_node.jstree", function(e, data) {
						$("#hideUpSort").val("");
						//获取分类名称
						var sortManageName = data.node.text;
						//获取上级分类名称
						var upsortName = data.node.data;
						//获取当前分类上级分类名称id
						var hideupsortID = data.node.parent;
						//获取当前分类名称id
						var hidelocalId = data.node.id;
						$('#TitleInfo').html('→' + sortManageName);
						$("#SortName").val(sortManageName);
						$("#hidelocalId").val(hidelocalId);
						$("#hideUpSort").val(hideupsortID);

						if(upsortName == null) {
							$("#UpSort").val("无");
						} else {
							$("#UpSort").val(upsortName);
						}
					});
				},
				error: function(data) {
					showSuccessOrErrorModal("请求出错了", "error");
				}

			});

		}
		//初始化页面
		$(function() 
		{
			$("#addvalidate").validate({
				submitHandler: function() {
					ischeck();
					AddSave();
				}

			});

			$("#editvalidate").validate({
				submitHandler: function() {
					EditSave();
				}
			});

			initjstree();
			$(".close").remove();
			$('#TitleInfo').html('');
			$("#SortName").val("课程分类");
			$("#UpSort").val("无");
		});

		//新增分类页面
		$("#btnAdd").click(function(e) 
		{
			var sortname = $("#SortName").val();
			if(sortname == "" || sortname == null) {
				showInfoModal("请选择分类！");
				e.stopPropagation();
			}
			$("#MSortName").val("");
			var UpSortName = $("#UpSortName").val();
		});

		//修改数据页面
		$("#btnEdit").click(function(e) 
		{
			var sortname = $("#SortName").val();
			var UpSort = $("#UpSort").val();
			if(sortname == "" || sortname == null || UpSort == "" || UpSort == null || UpSort == "无") {
				showInfoModal("请选择根节点以外的分类！");
				e.stopPropagation();
			}
			$("#MUpSortName1").html("");
			$("#MSortName1").val($("#SortName").val());
			var parentId = $("#hideUpSort").val();

			var selectsort = new Array();
			var ids = new Array();
			$.ajax({
				"type": "POST",
				"url": jQuery.getBasePath() + "/category/getUpCategory.bsh",
				"dataType": "json",
				"data": {
					id: parentId
				},
				"success": function(data) {
					var datas = data.tree;
					for(i = 0; i < datas.length; i++) {
						selectsort.push(datas[i].category_name);
						ids.push(datas[i].id);
					}
					var option = $("<option></option>");
					for(i = 0; i < selectsort.length; i++) {
						var option = $("<option></option>");
						option.text(selectsort[i]);
						option.attr("value", ids[i]);
						$("#MUpSortName1").append(option);
					}
					$("#MUpSortName1").val(parentId);

				},
				error: function(data) {
					showSuccessOrErrorModal("请求出错了", "error");
				}
			});
		});

		//保存新增数据
		function AddSave() 
		{
			var categoryname = $("#MSortName").val();
			var parentId = $("#hidelocalId").val();
			//var parentId = $('#MUpSortName option:selected').val();
			$.ajax({
				"type": "POST",
				"url": jQuery.getBasePath() + "/category/addCategory.bsh",
				"dataType": "json",
				"data": {
					categoryname: categoryname,
					parent_id: parentId
				},
				"success": function(data) {
					showSuccessOrErrorModal("新增成功!", "success");
					$("#windownAdd").modal("hide");
					refreshjstree();
				},
				error: function(data) {
					showSuccessOrErrorModal("请求出错了", "error");
				}

			});
		}

		//编辑数据
		function EditSave() 
		{
			var id = $("#hidelocalId").val();
			var categoryname = $("#MSortName1").val();
			var upsortId = $("#MUpSortName1").val();

			$.ajax({
				"type": "POST",
				"url": jQuery.getBasePath() + "/category/editCategory.bsh",
				"dataType": "json",
				"data": {
					id: id,
					categoryname: categoryname,
					parent_id: upsortId
				},
				"success": function(data) {
					if(data.status == "success") {
						showSuccessOrErrorModal("修改成功!", "success");
						$("#windowChange").modal("hide");
						refreshjstree();
					} else {
						showSuccessOrErrorModal("修改失败!", "error");
						$("#windowChange").modal("hide");
						refreshjstree();
					}

				},
				error: function(data) {
					showSuccessOrErrorModal("请求出错了", "error");
				}

			});
		}
		
		
		function Del(){
			var SortName = $("#SortName").val();
			var UpSortName = $("#UpSort").val();
			if(UpSortName == "无") {
				showInfoModal("根节点无法删除！");
				return;
			}

			if(UpSortName == "" || UpSortName == null || SortName == "" || SortName == null) {
				showInfoModal("请选择分类后删除！");
				return;
			}

				var id = $("#hidelocalId").val();
				var msg = "确定删除该分类？";
				
				showConfirmModal(msg,Deldata,id);
		
		}
		
		
		

		//删除数据
		function Deldata(id) 
		{
		
			$.ajax({
				"type": "POST",
				"url": "http://192.168.1.192:8080/course/CheckCategory/CheckCategoryById.bsh",
				"dataType": "json",
				"data": {
					id: id
				},
				"success": function(data) 
				{
					var info = "确定删除此分类？该分类已被引用！";
					if(data.status == true) 
					{
						showConfirmModal(info,realDel,id);
					}else
					{
					realDel(id);
					}
					
					
				},
				error: function(data) {
					showSuccessOrErrorModal("请求出错了", "error");
				}

			});
			
			
		function realDel(id){
		$.ajax({
				"type": "POST",
				"url": jQuery.getBasePath() + "/category/delCategory.bsh",
				"dataType": "json",
				"data": {
					id: id
				},
				"success": function(data) {
					if(data.status == "success") {
						refreshjstree();
						showSuccessOrErrorModal("删除成功!", "success");
						$("#SortName").val("");
						$("#UpSort").val("");

					} else {
						refreshjstree();
						showSuccessOrErrorModal("删除失败!", "error");
					}
				},
				error: function(data) {
					showSuccessOrErrorModal("请求出错了", "error");
				}

			});
		
		}	
			
			
		}

		//动态刷新树
		function refreshjstree() 
		{
			$.ajax({
				type: "POST",
				dataType: 'json',
				url: jQuery.getBasePath() + "/category/getAllCategory.bsh",
				success: function(data) {
					$('#areaTree').jstree(true).settings.core.data = data.tree;
					$('#areaTree').jstree(true).refresh();
				},
				error: function(data) {
					showSuccessOrErrorModal("请求出错了", "error");
				}
			});
		}

		//验证是否重名
		function ischeck() 
		{
			var MSortName = $("#MSortName").val();
			$.ajax({
				type: "POST",
				dataType: 'json',
				data: {
					categoryname: MSortName
				},
				url: jQuery.getBasePath() + "/category/ischeck.bsh",
				success: function(data) {
					if(data.status != "successs") {
						$("#MSortName").text("该分类名称已被占用");
						return;
					}
				},
				error: function(data) {
					showSuccessOrErrorModal("请求出错了", "error");
				}
			});

		}
	</script>

</html>