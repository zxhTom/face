package com.bshinfo.web.base.util;

import java.util.HashMap;

import com.bshinfo.web.model.dataTableUtil.DataTableParam;

/**
 * 处理DataTable的工具类<br/>
 * 
 * 功能描述: 主要用于处理DataTable的实体类的工具类<BR/>
 * 
 * @author 	[jixingwang]
 * 
 * @see 	[com.bshinfo.web.model.dataTableUtil.DataTableParam]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-2-17 16:25:23]
 */
public class DatatableUtil 
{
	/***
	 * 将DataTableParam数组转化成HashMap
	 * 
	 * @param  [DataTableParam[]] dataTableParams datatable数组
	 * 
	 * @return [java.util.HashMap<String, String>] 
	 * 
     * @author 	[jixingwang]
     * 
     * @Date    [2017-2-17 16:29:26]
	 */
	public static HashMap<String, String> convertToMap(DataTableParam[] dataTableParams) 
	{
		HashMap<String, String> paramsMap = new HashMap<>();
		for (DataTableParam dataTableParam : dataTableParams) 
		{
			paramsMap.put(dataTableParam.getName(), dataTableParam.getValue());
		}

		return paramsMap;
	}
}
