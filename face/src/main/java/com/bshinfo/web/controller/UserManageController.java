package com.bshinfo.web.controller;

import java.io.File;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.bshinfo.web.base.util.ConstantUtil;
import com.bshinfo.web.base.util.DatatableUtil;
import com.bshinfo.web.base.util.DateUtil;
import com.bshinfo.web.base.util.ExcelUtil;
import com.bshinfo.web.base.util.FileUtil;
import com.bshinfo.web.base.util.UUIDTool;
import com.bshinfo.web.model.UserInfo;
import com.bshinfo.web.model.dataTableUtil.DataTableModel;
import com.bshinfo.web.model.dataTableUtil.DataTableParam;
import com.bshinfo.web.service.serviceI.userManage.UserManageService;

/**
 * UserManage控制层 <br/>
 * 
 * 功能描述: 对UserManage做数据获取封装与页面控制跳转 <BR/>
 * 
 * @author 	[donghang]
 * 
 * @see 	[UserManageController]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-2-20]
 */
@Controller
@RequestMapping("/userManage")
public class UserManageController {
	
	public static Logger logger = Logger.getLogger(UserManageController.class);
	/**
	 * 跳转到用户管理页面userManage.ftl
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [String] 
	 * 
     * @author [Donghang]
     * 
     * @Date   [2017-2-20]
	 */
	@RequestMapping("/userManage.bsh")
	public String userManage(HttpServletRequest request,HttpServletResponse response) 
	{
		return "baseData/systemManage/user/userMange.ftl";
	}

	/**
	 * 返回用户列表信息
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [Object] 
	 * 
     * @author [Donghang]
     * 
     * @Date   [2017-2-20]
	 */
	@RequestMapping("/getUserTable.bsh")
	@ResponseBody
	public Object getUserTable(HttpServletRequest request,HttpServletResponse response, @RequestBody DataTableParam[] dataTableParams)
	{	
		Map<String,Object> map = new HashMap<String,Object>();
		DataTableModel data;
		try 
		{
			Map<String, String> dataTableMap = DatatableUtil.convertToMap(dataTableParams);
			//此id为当前登陆用户id
			UserInfo user = (UserInfo) request.getSession().getAttribute("userInfo");
			String id=user.getId(); 
			data = userManage.getUserTable(dataTableMap,id);
			map.put("data",data);
			map.put("flag", true);
		} catch (Exception e) 
		{
			e.printStackTrace();
			map.put("flag", false);
		}
		return map;
	}
	
	/**
	 * 检验用户信息是否重复
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [Object] 
	 * 
     * @author [Donghang]
     * 
     * @Date   [2017-2-23]
	 */
	@RequestMapping("/checkUser.bsh")
	@ResponseBody
	public Object checkUser(HttpServletRequest request,HttpServletResponse response)
	{	
		
		Map<String,Object> map = new HashMap<String,Object>();
		try 
		{
			Map<String,Object> paramMap = new HashMap<String,Object>();
			String userName = request.getParameter("userName");
			String email = request.getParameter("email");
			String phone = request.getParameter("phone");
			String idCard = request.getParameter("idCard");
			String id = request.getParameter("id");
			paramMap.put("userName",userName);
			paramMap.put("email",email);
			paramMap.put("id",id);
			paramMap.put("phone",phone);
			paramMap.put("idCard",idCard);
			Boolean isUsing =userManage.checkUser(paramMap);
			map.put("isUsing", isUsing);
			map.put("flag", true);
		} catch (Exception e) 
		{
		 
			e.printStackTrace();
			map.put("flag", false);
		}
		return map;
	}
	
	/**
	 * 根据id返回用户信息
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [Object] 
	 * 
     * @author [Donghang]
     * 
     * @Date   [2017-2-20]
	 */
	@RequestMapping("/getUserById.bsh")
	@ResponseBody
	public Object getUserById(HttpServletRequest request,HttpServletResponse response)
	{	
		Map<String,Object> map = new HashMap<String,Object>();
		Map<String, Object> user;
		try 
		{
			String id = request.getParameter("id"); 
			user = userManage.getUserById(id);
			map.put("user", user);
			map.put("flag", true);
		} catch (Exception e) 
		{
			e.printStackTrace();
			map.put("flag", false);
		}
		return map;
	}
	
	/**
	 * 根据id重置用户密码
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [Object] 
	 * 
     * @author [Donghang]
     * 
     * @Date   [2017-2-20]
	 */
	@RequestMapping("/resetPassword.bsh")
	@ResponseBody
	public Object resetPassword(HttpServletRequest request,HttpServletResponse response)
	{	
		Map<String,Object> map = new HashMap<String,Object>();
		try 
		{
			String id = request.getParameter("id"); 
			userManage.resetPassword(id);
			map.put("flag", true);
		} catch (Exception e) 
		{
			e.printStackTrace();
			map.put("flag", false);
		}
		return map;
	}
	
	/**
	 * 根据id删除用户
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [boolean] 
	 * 
     * @author [Donghang]
     * 
     * @Date   [2017-2-21]
	 */
	@RequestMapping("/deleteUserById.bsh")
	@ResponseBody
	public Object deleteUserById(HttpServletRequest request,HttpServletResponse response)
	{	
		Map<String,Object> map = new HashMap<String,Object>();
		try 
		{
			//删除用户时，用户表做逻辑删除，其他的用户关联表做物理删除
			String id = request.getParameter("id"); 
			userManage.deleteUserById(id);
			map.put("flag", true);
		} catch (Exception e) 
		{
			e.printStackTrace();
			map.put("flag", false);
		}
		return map;
	}
	
	/**
	 * 根据id更改用户状态
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [boolean] 
	 * 
     * @author [Donghang]
     * 
     * @Date   [2017-2-21]
	 */
	@RequestMapping("/changeStatusById.bsh")
	@ResponseBody
	public Object changeStatusById(HttpServletRequest request,HttpServletResponse response)
	{	
		Map<String,Object> map = new HashMap<String,Object>();
		try 
		{
			String id = request.getParameter("id");
			String status = request.getParameter("status");
			userManage.changeStatusById(id,status);
			map.put("flag", true);
		} catch (Exception e) 
		{
			e.printStackTrace();
			map.put("flag", false);
		}
		return map;
	}
	
	/**
	 * 根据id更改用户状态
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [boolean] 
	 * 
     * @author [Donghang]
     * 
     * @Date   [2017-2-21]
	 */
	@RequestMapping("/apprUserVerify.bsh")
	@ResponseBody
	public Object apprUserVerify(HttpServletRequest request,HttpServletResponse response)
	{	
		Map<String,Object> map = new HashMap<String,Object>();
		try 
		{
			String id = request.getParameter("id");
			String message = request.getParameter("message");
			String status = request.getParameter("appr"); 
			userManage.apprUserVerifyById(id,message,status);
			map.put("flag", true);
		} catch (Exception e) 
		{
			e.printStackTrace();
			map.put("flag", false);
		}
		return map;
	}
	
	/**
	 * 根据id查询用户管辖站点
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [List<Map<String,Object>>] 
	 * 
     * @author [Donghang]
     * 
     * @Date   [2017-2-23]
	 */
	@RequestMapping("/getSelectList.bsh")
	@ResponseBody
	public Object getSelectList(HttpServletRequest request,HttpServletResponse response)
	{	
		Map<String,Object> map = new HashMap<String,Object>();
		List<Map<String, Object>> list;
		try 
		{	
			UserInfo user = (UserInfo) request.getSession().getAttribute("userInfo");
			String id = user.getId();
			list = userManage.getSelectList(id);
			map.put("list", list);
			map.put("flag", true);
		} catch (Exception e) 
		{
			e.printStackTrace();
			map.put("flag", false);
		}
		return map;
	}
	
	/**
	 * 获取城市列表
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [List<Map<String,Object>>] 
	 * 
     * @author [Donghang]
     * 
     * @Date   [2017-2-23]
	 */
	@RequestMapping("/getCity.bsh")
	@ResponseBody
	public Object getCity(HttpServletRequest request,HttpServletResponse response)
	{	
		Map<String,Object> map = new HashMap<String,Object>();
		try 
		{
			List<Map<String,Object>> list = userManage.getCity();
			map.put("list", list);
			map.put("flag", true);
		} catch (Exception e) 
		{
			e.printStackTrace();
			map.put("flag", false);
		}
		return map;
	}
	
	/**
	 * 获取区/县列表
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [List<Map<String,Object>>] 
	 * 
     * @author [Donghang]
     * 
     * @Date   [2017-2-23]
	 */
	@RequestMapping("/getArea.bsh")
	@ResponseBody
	public Object getArea(HttpServletRequest request,HttpServletResponse response)
	{	
		Map<String,Object> map = new HashMap<String,Object>();
		List<Map<String, Object>> list;
		try 
		{
			String id = request.getParameter("city");
			list = userManage.getArea(id);
			map.put("list", list);
			map.put("flag", true);
		} catch (Exception e) 
		{
			e.printStackTrace();
			map.put("flag", false);
		}
		return map;
	}
	
	/**
	 * 根据id批量更改用户状态
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [boolean] 
	 * 
     * @author [Donghang]
     * 
     * @Date   [2017-2-21]
	 */
	@RequestMapping("/changeUsersStatus.bsh")
	@ResponseBody
	public Object changeUsersStatus(HttpServletRequest request,HttpServletResponse response)
	{	
		Map<String,Object> map = new HashMap<String,Object>();
		
		try 
		{
			String ids = request.getParameter("ids");
			String status = request.getParameter("status");
			userManage.changeUsersStatus(ids,status);
			map.put("flag", true);
		} catch (Exception e) 
		{
			e.printStackTrace();
			map.put("flag", false);
		}
		return map;
	}
	
	/**
	 * 新增用户
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [boolean] 
	 * 
     * @author [Donghang]
     * 
     * @Date   [2017-2-21]
	 */
	@RequestMapping("/addUser.bsh")
	@ResponseBody
	public Object addUser(HttpServletRequest request,HttpServletResponse response,@RequestParam("files") MultipartFile[] files)
	{	
		Map<String,Object> map = new HashMap<String,Object>();
		try 
		{
			String id = UUIDTool.randomUUID();
			String time = DateUtil.getDateString(null);
			Map<String,Object> paramMap = new HashMap<String,Object>();
			paramMap.put("id",id);
			paramMap.put("time",time);
			paramMap.put("createUserId", ConstantUtil.ADMIN_ID);
			paramMap.put("siteId",request.getParameter("siteId").trim());
			paramMap.put("userName",request.getParameter("userName").trim());
			paramMap.put("userNick",request.getParameter("userNick").trim());
			paramMap.put("isVerify",request.getParameter("identity").trim());
			paramMap.put("realName",request.getParameter("realName").trim());
			paramMap.put("sex",request.getParameter("sex").trim());
			paramMap.put("addrCity",request.getParameter("addrCity").trim());
			paramMap.put("addrArea",request.getParameter("addrArea").trim());
			paramMap.put("jobAddr",request.getParameter("jobAddr").trim());
			paramMap.put("email",request.getParameter("email").trim());
			paramMap.put("eduDegree",request.getParameter("eduDegree").trim());
			paramMap.put("graduation",request.getParameter("graduation").trim());
			paramMap.put("idCard",request.getParameter("idCard").trim());
			paramMap.put("phone",request.getParameter("phone").trim());
			paramMap.put("fontUrl","");
			paramMap.put("backUrl","");
			if("0".equals(request.getParameter("identity")))
			{
				paramMap.put("verifyStatus","2");
			}else
			{
				paramMap.put("verifyStatus","1");
			}
			
			String path=request.getSession().getServletContext().getRealPath("image/user/");
			if(files!=null&&files.length>0)
			{  
			    //循环获取file数组中得文件  
			    for(int i = 0;i<files.length;i++)
			    {  
			        MultipartFile file = files[i]; 
			        String fileName = file.getOriginalFilename();  
			        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);  
			        if(i==0)
			        {	if(!file.isEmpty())
			        	{
			        		paramMap.put("fontUrl",id+"_font."+suffix);
			        	};
			        }else if(i==1)
			        {
			        	if(!file.isEmpty())
			        	{
			        		paramMap.put("backUrl",id+"_back."+suffix);
			        	};
			        }
			        //保存文件  
			        saveFile(file,i,id,path);  
			    }  
			}  

			userManage.addUser(paramMap);
			map.put("flag", true);
		} catch (Exception e) 
		{
			e.printStackTrace();
			map.put("flag", false);
		}
		
		return map;
	}
	
	/**
	 * 根据id修改用户
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [boolean] 
	 * 
     * @author [Donghang]
     * 
     * @Date   [2017-2-21]
	 */
	@RequestMapping("/eidtUser.bsh")
	@ResponseBody
	public Object eidtUser(HttpServletRequest request,HttpServletResponse response,@RequestParam("files") MultipartFile[] files)
	{	
		Map<String,Object> map = new HashMap<String,Object>();
		
		try 
		{
			String id = request.getParameter("id");
			String time = DateUtil.getDateString(null);
			Map<String,Object> paramMap = new HashMap<String,Object>();
			paramMap.put("id",id);
			paramMap.put("time",time);
			paramMap.put("siteId",request.getParameter("siteId").trim());
			paramMap.put("userName",request.getParameter("userName").trim());
			paramMap.put("userNick",request.getParameter("userNick").trim());
			paramMap.put("verifyStatus",request.getParameter("verifyStatus"));
			paramMap.put("realName",request.getParameter("realName").trim());
			paramMap.put("sex",request.getParameter("sex"));
			paramMap.put("addrCity",request.getParameter("addrCity"));
			paramMap.put("addrArea",request.getParameter("addrArea"));
			paramMap.put("jobAddr",request.getParameter("jobAddr").trim());
			paramMap.put("email",request.getParameter("email").trim());
			paramMap.put("eduDegree",request.getParameter("eduDegree").trim());
			paramMap.put("graduation",request.getParameter("graduation").trim());
			paramMap.put("idCard",request.getParameter("idCard").trim());
			paramMap.put("phone",request.getParameter("phone").trim());
			paramMap.put("fontUrl","");
			paramMap.put("backUrl","");
			if("1".equals(request.getParameter("verifyStatus")))
			{
				paramMap.put("isVerify","1");
			}
			else
			{
				paramMap.put("isVerify","0");
			}
			
			String path=request.getSession().getServletContext().getRealPath("image/user/");
			if(files!=null&&files.length>0)
			{  
			    //循环获取file数组中得文件  
			    for(int i = 0;i<files.length;i++)
			    {  
			    	MultipartFile file = files[i]; 
			        String fileName = file.getOriginalFilename();  
			        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);  
			        if(!file.isEmpty())
			        {
				        if(i==0)
				        {	
				        	paramMap.put("fontUrl",id+"_font."+suffix);
				        }else if(i==1)
				        {
				        	paramMap.put("backUrl",id+"_back."+suffix);
				        }
			        }   
			        //保存文件  
			        saveFile(file,i,id,path);  
			    }  
			}

			userManage.editUser(paramMap);
			map.put("flag", true);
		} catch (Exception e) 
		{
			e.printStackTrace();
			map.put("flag", false);
		}
		
		return map;
	}
	
	/**
	 * 根据管理员id获取站点树
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [Object] 
	 * 
     * @author [Donghang]
     * 
     * @Date   [2017-2-23]
	 */
	@RequestMapping("/getSiteTree.bsh")
	@ResponseBody
	public Object getSiteTree(HttpServletRequest request,HttpServletResponse response) 
	{	
		Map<String,Object> map = new HashMap<String,Object>();
		List<Map<String,Object>> tree;
		try 
		{
			UserInfo user = (UserInfo) request.getSession().getAttribute("userInfo");
			String id = user.getId();	
			tree = userManage.getSiteTree(id);
			map.put("tree", tree);
			map.put("flag", true);
		} catch (Exception e) 
		{
			map.put("flag", false);
			return map;
		}
		return map;
	}
	
	/**
	 * 下载用户模板
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [Object] 
	 * 
     * @author [Donghang]
     * 
     * @Date   [2017-2-23]
	 */
	@RequestMapping(value = "/downloadtemplate.bsh")
	public void downloadtemplate(HttpServletRequest request,HttpServletResponse response) 
	{
		try 
		{
			String fileNames = URLDecoder.decode(request.getParameter("fileName"),"UTF-8");
			String path = request.getSession().getServletContext().getRealPath("template/");
			String templateFilePath = path+File.separator+fileNames;
			File tpFIle=new File(templateFilePath.replace("\\", "//"));
			FileUtil.downloadFile(tpFIle, fileNames, response);
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * 获取用户权限
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [Object] 
	 * 
     * @author [Donghang]
     * 
     * @Date   [2017-3-3]
	 */
	@RequestMapping(value = "/getUserRole.bsh")
	@ResponseBody
	public Object getUserRole(HttpServletRequest request,HttpServletResponse response) 
	{
		Map<String,Object> map = new HashMap<String,Object>();
		try 
		{
			//此id为当前登陆用户id
			UserInfo user = (UserInfo) request.getSession().getAttribute("userInfo");
			String menuId = request.getParameter("menuId");
			String id=user.getId(); 
			map = userManage.getUserRole(id,menuId);
			return map;
		} catch (Exception e) 
		{
			e.printStackTrace();
			map.put("flag", "false");
			return map;
		}
	}
	
	/**
	 * 批量导入用户信息
	 */
	@RequestMapping(value = "/importUserInfo.bsh")
	@ResponseBody
	public Object importUserInfo(HttpServletRequest request,@RequestParam(value = "file", required = false) MultipartFile file,
			HttpServletResponse response) 
	{
		    String rows ="";
		    Boolean flag =true;
		    Map<String,Object> map = new HashMap<String,Object>();
		    response.setContentType("text/html; charset=UTF-8");
		    try 
		    {
		    	String path=request.getSession().getServletContext().getRealPath("upload/");
				File f = new File(path);
				if (!f.exists())// 如果上传的路径不存在就创建
				{
					f.mkdirs();
				}
				
				String newName =UUIDTool.randomUUID().toString()+file.getOriginalFilename();
				
				File newFile = new File(path,newName);
				if(file !=null){
					file.transferTo(newFile);
				}
				String excelPath = path+"/"+newName;
				
				Workbook web=null;
				Sheet sheet=null;
				web =ExcelUtil.getVersionWorkbook(excelPath.replace("\\", "//"), "read");
				sheet =web.getSheet("record");
				int n = sheet.getPhysicalNumberOfRows();
				
				//必填项判断
				for(int i=1;i<n;i++)
				{
					Row row = sheet.getRow(i);
					if(row == null)
					{
						flag = false;
						int j=i+1;
						rows =j+"行数据必填项不可为空";
						map.put("sucess", true);
						map.put("msg", rows);
						return map;
					}
					
					String userName =ExcelUtil.getCellValue(row.getCell(0), "String");
					String siteName =ExcelUtil.getCellValue(row.getCell(2), "String");
					String sex =ExcelUtil.getCellValue(row.getCell(4), "String");

					if("".equals(userName) || "".equals(siteName) || "".equals(sex))
					{
						flag = false;
						int j=i+1;
						rows =j+"行数据必填项不可为空";	
						map.put("sucess", true);
						map.put("msg", rows);
						return map;
					}
					
				}
				if(flag)
				{
					for(int i=1;i<n;i++)
					{
						String userName =ExcelUtil.getCellValue(sheet.getRow(i).getCell(0), "String");
						String userNick =ExcelUtil.getCellValue(sheet.getRow(i).getCell(1), "String");
						String siteName =ExcelUtil.getCellValue(sheet.getRow(i).getCell(2), "String");
						String realName =ExcelUtil.getCellValue(sheet.getRow(i).getCell(3), "String");
						String sex =ExcelUtil.getCellValue(sheet.getRow(i).getCell(4), "String");
						String idCard =ExcelUtil.getCellValue(sheet.getRow(i).getCell(5), "String");
						String phone =ExcelUtil.getCellValue(sheet.getRow(i).getCell(6), "String");
						String eduDegree=ExcelUtil.getCellValue(sheet.getRow(i).getCell(7), "String");
						String jobAddr =ExcelUtil.getCellValue(sheet.getRow(i).getCell(8), "String");
						String email =ExcelUtil.getCellValue(sheet.getRow(i).getCell(9), "String");
						String graduation =ExcelUtil.getCellValue(sheet.getRow(i).getCell(10), "String");
						
						
						Map<String,Object> paramMap = new HashMap<String,Object>();
						String id =UUIDTool.randomUUID();
						String time = DateUtil.getDateString(null);
						
						paramMap.put("userName", userName);
						if(!userManage.checkUser(paramMap))
						{
							rows = i+"行用户名重复";
							map.put("sucess", false);
							map.put("msg", rows);
							return map;
						}
						paramMap.put("userNick", userNick);
						paramMap.put("siteName", siteName);
						String siteId=userManage.queryByName(siteName);
						if("".equals(siteId))
						{
							paramMap.put("siteId", ConstantUtil.ADMIN_SITE_ID);
						}else
						{
							paramMap.put("siteId", siteId);
						}
						paramMap.put("realName", realName);
						if("女".equals(sex))
						{	
							paramMap.put("sex", "2");
						}else
						{
							paramMap.put("sex", "1");
						}
						paramMap.put("idCard", idCard);
						paramMap.put("phone", phone);
						switch(eduDegree){
						case "初中" :paramMap.put("eduDegree", "1");break;
						case "高中" :paramMap.put("eduDegree", "2");break;
						case "大专/大学" :paramMap.put("eduDegree", "3");break;
						case "硕士" :paramMap.put("eduDegree", "4");break;
						case "博士" :paramMap.put("eduDegree", "5");break;
						default : paramMap.put("eduDegree", "");
						}
						paramMap.put("jobAddr", jobAddr);
						paramMap.put("email", email);
						paramMap.put("graduation", graduation);
						paramMap.put("isVerify", "0");
						paramMap.put("time", time);
						paramMap.put("verifyStatus","2");
						paramMap.put("createUserId",ConstantUtil.ADMIN_ID);
						paramMap.put("id", id);
						userManage.addUser(paramMap);
					}  
				}
				map.put("sucess", true);
				map.put("msg", rows);
		} catch (Exception e) 
		{
			e.printStackTrace();
			try
			{
				map.put("sucess", false);
				map.put("msg", "新增出错了 "+rows);
				return map;
			}catch(Exception e1)
			{
				e1.printStackTrace();	
			}
		}
			return map;
	}
	
	/*** 
     * 保存文件 
     * @param file 
     * @return 
     */  
    private boolean saveFile(MultipartFile file,int i,String id,String path) 
    {  
         String fileName = file.getOriginalFilename();  
         String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);  
        // 判断文件是否为空  
        if (!file.isEmpty()) 
        {  
            try {  
            	String fp;
            	if(i==0)
            	{
            		fp= id+"_font."+suffix;
            	}else
            	{
            		fp= id+"_back."+suffix;
            	}
                // 文件保存路径  
                String filePath = path+"/"+fp;  
                // 转存文件  
                file.transferTo(new File(filePath));  
                return true;  
            } catch (Exception e) 
            {  
                e.printStackTrace();  
            }  
        }  
        return false;  
    }    
	@Resource
	private UserManageService userManage;
}
