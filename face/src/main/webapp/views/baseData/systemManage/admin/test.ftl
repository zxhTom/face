<#assign base=request.contextPath />
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta content="width=device-width, initial-scale=1" name="viewport" />

<meta content="" name="description" />

<meta content="" name="author" />

<title>管理员管理</title>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/select2/css/select2.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/select2/css/select2-bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/morris/morris.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/fullcalendar/fullcalendar.min.css"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/jstree/dist/themes/default/style.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/datatables.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css"
	rel="stylesheet" type="text/css" />

<!-- BEGIN THEME GLOBAL STYLES -->
<link
	href="${base}/js/library/metronic-4.7/assets/global/css/components.min.css"
	rel="stylesheet" id="style_components" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/css/plugins.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link
	href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/layout.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/themes/darkblue.min.css"
	rel="stylesheet" type="text/css" id="style_color" />
<link
	href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/custom.min.css"
	rel="stylesheet" type="text/css" />
<!-- 弹出框 -->
<link href="${base}/js/library/sweetalert/css/sweetalert.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
</head>

<body style="background-color:#FFFFFF;" id="manager">
<!-- BEGIN PAGE TITLE-->
		<h3 class="page-title">
			后台管理 <small>获取站点</small>
		</h3>
		<!-- END PAGE TITLE-->
	<div style="height:100px; overflow-y:scroll">
		<div class="form-group">
			<div class="row">
				<div class="col-md-5">
					<div class="form-group">
						<label class="control-label col-md-3"> 站点名称 </label>
						<div class="col-md-9">
							<input type="text" id="site_name" name="name" onfocus="havaFocus()" onblur="leaveFocus()"
								class="form-control" />
						</div>
					</div>
				</div>
				<div class="col-md-5">
					<div class="form-group">
						<label class="control-label col-md-3"> 站点类型 </label>
						<div class="col-md-9">
							<select id="site_type" class="form-control">
								<option value="">请选择...</option>
								<option value="1">总站</option>
								<option value="2">子站</option>
								<option value="3">联盟网站</option>
							</select>
						</div>
					</div>
				</div>
				
			</div>
			<div class="row" style="margin-top: 30px;margin-bottom: 30px;">
				<div class="row"
					style="margin-top:20px; margin-right:30px; float: right;">
					<div class="col-md-6">
						<button type="button" class="btn green" onclick="doSearch()">查询</button>
					</div>
					<div class="col-md-6">
						<button type="button" class="btn green" onclick="clearSearch()">清空</button>
					</div>
				</div>
			</div>
			<table class="table table-striped table-hover table-bordered"
				id="ChooseSiteTable">
			</table>
			</div></div>
			<button class='btn green' style='float:right;margin-left:15px;' onclick='selectSite()'>选择</button>
<!-- BEGIN CORE PLUGINS -->
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-CN.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="${base}/js/library/metronic-4.7/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jstree/dist/jstree.min.js" type="text/javascript"></script>
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/scripts/app.min.js" type="text/javascript"></script>

<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/ui-tree.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="${base}/js/library/metronic-4.7/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<!-- 弹出框 -->
<script src="${base}/js/library/sweetalert/js/sweetalert.min.js" type="text/javascript"></script>
<script src="${base}/js/library/sweetalert/js/showAlert.js" type="text/javascript"></script>
<!-- 获取根路径 -->
<script src="${base}/js/common/util/jqueryUtil.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script type="text/javascript">
var siteDataTable=null;
$(document).ready(function(){
	initSiteTable();
	//$("#ChooseSiteTable").before("<button class='btn green' style='float:left;margin-left:15px;' onclick='selectSite()'>选择</button>");
});

function havaFocus()
{
	document.onkeyup=keyup;	
}
function keyup(e)
{
	siteDataTable.draw();
}
function leaveFocus()
{
	document.onkeyup=null;		
}
//初始化站点表格
function initSiteTable()
{
	siteDataTable=$('#ChooseSiteTable').DataTable({
		"bSort" : true,
		"bProcessing" : true,
		"bServerSide" : true,
		"async" : false,
		//"retrieve" : true,//保证只有一个table实例  
		"bStateSave" : false,
		"sAjaxSource" : jQuery.getBasePath()
				+ "/adminManage/getSite.bsh",
		"sDom" : '<"top"l<"clear">>rt<"bottom"ip<"clear">>',
		"lengthMenu" : [ 3, 25, 75, 100 ],
		"pageLength" : 3,
		"oLanguage" : 
		{
			"sProcessing" : "正在加载数据...",
			"sSearch" : "搜索:",
			"sLengthMenu" : "_MENU_条记录",
			"sZeroRecords" : "没有查到记录",
			"sInfo" : "第  _START_ 条到第  _END_ 条记录,一共  _TOTAL_ 条记录",
			"sInfoEmpty" : "0条记录",
			"oPaginate" : 
			{
				"sPrevious" : "上一页 ",
				"sNext" : " 下一页",
			}
		},
		"aoColumnDefs" : [
			{
				"bSearchable" : true,
				"bVisible" : false,
				"aTargets" : [ 0 ]
			}],
		"aoColumns":[{
			"sTitle" : "id",
			"bSortable" : false
		},
		{
			"sTitle" : '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" id="checkAll" onclick="siteChooseOrNoChoose(this)" /><span></span></label>',
			"bSortable" : false,
			"sClass" : "text-center",
			"fnCreatedCell" : function(nTd, sData, oData,iRow, iCol) 
			{
				var value = oData[0];
				var content = '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" class="testCheckbox" value="'+value+'" /><span></span></label>';
				$(nTd).html(content);
			}
		}, {
			"sTitle" : "站点名称",
			"bSortable" : true,
			"sClass" : "text-center"
		}, {
			"sTitle" : "站点类型",
			"bSortable" : true,
			"bAutoWidth" : true,
			"sClass" : "text-center"
		}],
		"fnServerData" : function retrieveData(sSource, aoData, fnCallback)
		{
			aoData.push({"name":"site_name","value":$('#site_name').val()});
			aoData.push({"name":"site_type","value":$('#site_type').val()});
			$.ajax({
				"type" : "POST",
				"url" : sSource,
				"dataType" : "json",
				"contentType" : "application/json",
				"data" : JSON.stringify(aoData),
				"success" : function(data) 
				{
					fnCallback(data);
				}
			});
		}
	});
}

//全选反选
function siteChooseOrNoChoose(obj)
{
	if ($(obj).is(":checked")) 
	{
		$(".testCheckbox").prop("checked", true);
	}
	else
	{
		$(".testCheckbox").prop("checked", false);
	}
}
//选择站点
function selectSite()
{
	var siteIds={};
	var flag=0;
	$(".testCheckbox").each(function(){
		if($(this).is(":checked"))
		{
			siteIds[$(this).val()]=$(this).val();
		}
	});
	if(JSON.stringify(siteIds).length==2)
	{
		showInfoModal("没有选择任何站点");
	}
	else
	{
		console.log(JSON.stringify(siteIds));
	}
}
//查询
function doSearch()
{
	siteDataTable.draw();	
}
//清空查询状态栏
function clearSearch()
{
	$('#site_name').val("");
	$('#site_type').val("");
	siteDataTable.draw();
}
</script>			
</body>
</html>