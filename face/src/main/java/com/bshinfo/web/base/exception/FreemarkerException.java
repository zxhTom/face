package com.bshinfo.web.base.exception;

import freemarker.template.TemplateException;

public class FreemarkerException extends RuntimeException
{
	/**
	 *  序列号
	 */
	private static final long serialVersionUID = 1L;
	
	// 异常对应的返回码
	private String exceptionCode;  
	// 异常对应的描述信息
    private String exceptionMsg;  
    
    private TemplateException te;
    
    public FreemarkerException()
    {
    	super();
    }
    
    public FreemarkerException(String msg)
    {
    	super(msg);
    	exceptionMsg = msg;
    }
    
    public FreemarkerException(String msg,TemplateException e)
    {
    	super();
    	exceptionMsg = msg;
    	te = e;
    }
    
    public TemplateException getTe() {
		return te;
	}

	public void setTe(TemplateException te) {
		this.te = te;
	}

	public FreemarkerException(String msg,String code)
    {
    	super();
    	exceptionMsg = msg;
    	exceptionCode = code;
    }

	public String getExceptionCode() {
		return exceptionCode;
	}

	public void setExceptionCode(String exceptionCode) {
		this.exceptionCode = exceptionCode;
	}

	public String getExceptionMsg() {
		return exceptionMsg;
	}

	public void setExceptionMsg(String exceptionMsg) {
		this.exceptionMsg = exceptionMsg;
	}

}
