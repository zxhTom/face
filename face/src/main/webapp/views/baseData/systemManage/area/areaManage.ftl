<#assign base=request.contextPath />
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta content="width=device-width, initial-scale=1" name="viewport" />

		<meta content="" name="description" />

		<meta content="" name="author" />

		<title>后台管理</title>

		<!-- BEGIN GLOBAL MANDATORY STYLES -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
		<!-- END GLOBAL MANDATORY STYLES -->
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
		<!-- END PAGE LEVEL PLUGINS -->
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

		<!-- BEGIN THEME GLOBAL STYLES -->
		<link href="${base}/js/library/metronic-4.7/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
		<!-- END THEME GLOBAL STYLES -->
		<!-- BEGIN THEME LAYOUT STYLES -->
		<link href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
		<link href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
		
		<!-- 弹出框 -->
		<link href="${base}/js/library/sweetalert/css/sweetalert.css" rel="stylesheet" type="text/css" />
		<!-- END THEME LAYOUT STYLES -->

	</head>

	<body id="areaBody" style="background-color:#FFFFFF;">
		<!-- BEGIN PAGE BAR -->
		<div class="page-bar">
			<div class="page-toolbar">
				<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body"
					data-placement="bottom" data-original-title="Change dashboard date range">
					<i class="icon-calendar"></i>&nbsp; 
					<span class="thin uppercase hidden-xs"></span>&nbsp; 
					<i class="fa fa-angle-down"></i>
				</div>
			</div>
		</div>
		<!-- END PAGE BAR -->

		<!-- BEGIN PAGE TITLE-->
		<h3 class="page-title">
			后台管理 <small>区域管理</small>
		</h3>
		<!-- END PAGE TITLE-->
		<!-- END PAGE HEADER-->
		<div class="row ">
			<div class="col-md-3 ">
				<div class="portlet light bordered" >
					<div style="height:500px;overflow-y:scroll;">
						<div id="areaTree" class="tree-demo" > </div>
					</div>
				</div>
			</div>
			<div class="col-md-9 ">
				<div class="portlet light bordered">
					<div class="row">
						<table class="table table-striped table-hover table-bordered" >
							<tr>
								<th>区域名称</th>
								<th>区域编码</th>
							</tr>
							<tbody id="areaTable">
							</tbody>
						</table>
						<button class='btn green' type='button' style='float:right;margin-right:15px;' onclick='showConfirmModal("确定删除此区域？",delArea)'>删除</button>
						<button class='btn green' type='button' data-toggle='modal' data-target='#editArea' style='float:right;margin-right:15px;' onclick='getAreaAllInfo()'>修改</button>
						<button class='btn green' type='button' style='float:right;margin-right:15px;' onclick='addArea()'>新增子区域</button>
					</div>
				</div>
			</div>
		</div>
		
		<!-- 新增/编辑模态框 -->
		<div id="editArea" class="modal fade" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title" id="modalTitle"></h4>
					</div>
					<div class="modal-body form">
						<form action="javascript:;" id="addAreaInfo" class="form-horizontal form-row-seperated">
							<input type="hidden" id="areaId" name="areaId" value="0">
							<div class="form-group">
								<label class="col-sm-3 control-label">
									区域名称
									<span class="required"> * </span>
								</label>
								<div class="col-sm-8">
									<div class="input-group">
										<input type="text" id="areaName" name="areaName" class="form-control required" maxlength="20" style="width:300px;" />
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">
									区域编码&nbsp;&nbsp;&nbsp;
								</label>
								<div class="col-sm-8">
									<div class="input-group">
										<input type="text" id="areaCode" name="areaCode" class="form-control" maxlength="64" style="width:300px;" />
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">
									区域顺序
									<span class="required"> * </span>
								</label>
								<div class="col-sm-8">
									<div class="input-group">
										<input type="text" id="areaOrder" name="areaOrder" class="form-control required number" maxlength="20" style="width:300px;" />
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">
									区域邮编&nbsp;&nbsp;&nbsp;
								</label>
								<div class="col-sm-8">
									<div class="input-group">
										<input type="text" id="areaZipCode" name="areaZipCode" class="form-control number" maxlength="6" style="width:300px;" />
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn green" data-dismiss="modal">关闭</button>
								<button type="sumbit" class="btn green">提交</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>			
	
		<!-- BEGIN CORE PLUGINS -->
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
		<!-- END CORE PLUGINS -->
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/moment.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-CN.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL PLUGINS -->
		<script src="${base}/js/library/metronic-4.7/assets/global/scripts/datatable.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jstree/dist/jstree.min.js" type="text/javascript"></script>
		<!-- BEGIN THEME GLOBAL SCRIPTS -->
		<script src="${base}/js/library/metronic-4.7/assets/global/scripts/app.min.js" type="text/javascript"></script>
		<!-- END THEME GLOBAL SCRIPTS -->
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/ui-tree.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>

		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->
		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->
		<!-- BEGIN THEME LAYOUT SCRIPTS -->
		<script src="${base}/js/library/metronic-4.7/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>

		<!-- BEGIN 表单判空 -->
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery-validation/js/jquery.validate.js" type="text/javascript"></script>
		
		<!-- 弹出框 -->
		<script src="${base}/js/library/sweetalert/js/sweetalert.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/sweetalert/js/showAlert.js" type="text/javascript"></script>
		
		<!-- 获取根路径 -->
		<script src="${base}/js/common/util/jqueryUtil.js" type="text/javascript"></script>
	
	<script>
		
		//当前区域id
		var areaId = "";
		//区域类型：1-省，2-市，3-区或者县
		var areaType = "";
		
		function getAreaTree()
		{
			//获取区域树
			$.ajax({
        		type 	 : "POST",
        		async    :  false,  
        		dataType : 'json',  
		        url : jQuery.getBasePath() + "/area/doGetAreaTree.bsh",  
		        error : function(data) 
		        {  
		        	showSuccessOrErrorModal("出错了！！:" + data,"error");
		        },  
		        success : function(data) 
		        { 
		        	if(areaId == "")
		        	{
		        		areaId = data.tree[0].id;
		        	}
		        	if(areaType == "")
		        	{
		        		areaType = data.tree[0].area_type;
		        	}
		        	data.state = {
						opened: true
					};
		        	$('#areaTree').jstree(
		        	{
		        		'plugins': ["wholerow", "types"],
		        		'core': 
		        		{
		        			"themes" : 
		        			{
		        				"responsive": false
		        			},
		        			'data':data.tree
		        		},
		        		"types" :
		        		{
		        			"default" : 
		        			{
		        				"icon" : "fa fa-folder icon-state-warning icon-lg"
		        			},
		        			"file" : 
		        			{
		        				"icon" : "fa fa-file icon-state-warning icon-lg"
		        			}
		        		}
		        	}).bind("select_node.jstree", function(e, data) 
		            {
			        	areaId = data.node.original.id;
			            areaType = data.node.original.area_type;
			            getAreaInfo();
		            });
		        }
		    });
		    $("#areaTree").on("loaded.jstree", function(e, data) 
		    {
		        var inst = data.instance;
		        var obj = inst.get_node(e.target.firstChild.firstChild.lastChild);
		        inst.open_node(obj);
			})
		    
		}
		
		    
		$(document).ready(function()
		{
			getAreaTree();
			getAreaInfo();
			
			/* 验证表单 */
			$("#addAreaInfo").validate({
				submitHandler : function() 
				{
					
					var id = $("#areaId").val();
					var areaName = $.trim($("#areaName").val());
					var areaOrder = $.trim($("#areaOrder").val());
					var areaCode = $.trim($("#areaCode").val());
					var areaZipCode = $.trim($("#areaZipCode").val());
					
					if(areaName == "" || areaName == null)
					{
						showInfoModal("区域名称不能为空！");
						return false;
					}
					
					if(areaOrder == "" || areaOrder == null)
					{
						showInfoModal("区域顺序不能为空！");
						return false;
					}
					
					$.ajax(
					{
					 	url      : jQuery.getBasePath() + "/area/doAddArea.bsh",
	                 	dataType : "json",
	                 	async    :  true,
	                 	type     : "post",
	                 	data     :  {"id":id,"areaName":areaName,
	                 				 "areaOrder":areaOrder,
	                 				 "areaCode":areaCode,
	                 				 "areaZipCode":areaZipCode,
	                 				 "areaId":areaId,
	                 				 "areaType":areaType}, // 参数对象
	                	success:function(data)
	                 	{
	                 		if(data.status)
	                 		{
	                 			if(id != 0)
	                     		{
	                     			$("#showname").text(areaName);
	                     			$("#showcode").text(areaCode);
	                     		}
	                 			showSuccessOrErrorModal(data.msg,"success");
	                     		$("#editArea").modal("hide");
	                     		//重新加载有变动的树
	                     		$('#areaTree').jstree(true).settings.core.data=data.tree;
	                     		$('#areaTree').jstree(true).refresh();
	                 		}else
	                 		{
	                 			showSuccessOrErrorModal(data.msg,"error");
	                 		}
	                     	
	                 	},
	                 	error:  function(data)
	                 	{
	                     	showSuccessOrErrorModal(data.msg,"error");

	                 	}
					});
					
				}
			});
			
		});
		
		$("#areaBody").bind("DOMNodeInserted",function()
		{
			window.top.resizeParentHeightFun();
		});
		
		//获取区域名称和编码信息
		function getAreaInfo()
		{
			$.ajax(
			{
				url      : jQuery.getBasePath() + "/area/doGetAreaInfo.bsh",
		        dataType : "json",
		        async    :  false,
		        type     : "post",
		        data     :  {"areaId":areaId}, // 参数对象
		        success:function(data)
		        {
		        	if(data.status)
		            {
		            	var areaInfo = "<tr><td id='showname'>" + data.area_name + "</td><td id='showcode'>" + data.area_code +"</td></tr>";
		            	$("#areaTable").empty();
		            	$("#areaTable").append(areaInfo);
		            }
		        },
		        error:  function(data)
		        {
		            showSuccessOrErrorModal(data.msg,"error");
		        }
			});
		}
		
		//获取区域所有信息进行修改
		function getAreaAllInfo()
		{
			$(".input-group label").hide();
			$.ajax(
			{
				url      : jQuery.getBasePath() + "/area/doGetAreaAllInfo.bsh",
		        dataType : "json",
		        async    :  false,
		        type     : "post",
		        data     :  {"areaId":areaId}, // 参数对象
		        success:function(data)
		        {
		        	if(data.status)
		            {
		            	$("#areaId").val(data.id);
		            	$("#areaName").val(data.area_name);
		            	$("#areaCode").val(data.area_code);
		            	$("#areaOrder").val(data.area_order);
		            	$("#areaZipCode").val(data.area_zip_code);
		            }
		        },
		        error:  function(data)
		        {
		            showSuccessOrErrorModal(data.msg,"error");
		        }
			});
		}
		
		//新增子区域
		function addArea()
		{
			//类型3下不能添加子区域
			if(areaType == 3)
			{
				showInfoModal("该区域下不能添加子区域！");
			}else
			{
				$(".input-group label").hide();
				$("#editArea").modal("show");
				$("#areaId").val("0");
			    $("#areaName").val("");
			    $("#areaCode").val("");
			    $("#areaOrder").val("");
			    $("#areaZipCode").val("");
			}
			
		}
		
		function delArea()
		{
			$.ajax(
			{
				url      : jQuery.getBasePath() + "/area/doDeleteArea.bsh",
		        dataType : "json",
		        async    :  false,
		        type     : "post",
		        data     :  {"areaId":areaId}, // 参数对象
		        success:function(data)
		        {
		        	if(data.status)
		            {
		            	showSuccessOrErrorModal(data.msg,"success");
		            	areaId = data.tree[0].id;
		        		areaType = data.tree[0].area_type;
		            	getAreaInfo();
		            	//重新加载有变动的树
	                    $('#areaTree').jstree(true).settings.core.data=data.tree;
	                    $('#areaTree').jstree(true).refresh();
		            }else
		            {
		            	showSuccessOrErrorModal(data.msg,"error");
		            }
		        },
		        error:  function(data)
		        {
		            showSuccessOrErrorModal(data.msg,"error");
		        }
			});
		}
	</script>
	</body>
</html>