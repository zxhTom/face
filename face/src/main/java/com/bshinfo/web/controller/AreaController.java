package com.bshinfo.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bshinfo.web.base.controller.BaseController;
import com.bshinfo.web.base.util.DateUtil;
import com.bshinfo.web.base.util.UUIDTool;
import com.bshinfo.web.model.UserInfo;
import com.bshinfo.web.service.serviceI.area.AreaServiceI;

/**
 * Area控制层 <br/>
 * 
 * 功能描述: 对Area做数据获取封装与页面控制跳转 <BR/>
 * 
 * @author 	[hezhijian]
 * 
 * @see 	[AreaController]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-3-1 11:10:05]
 */
@Controller
@RequestMapping("/area")
public class AreaController extends BaseController
{
	public static Logger logger = Logger.getLogger(AreaController.class);

	/**
	 * 跳转到主页面areaManage.ftl
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [String] 
	 * 
     * @author [hezhijian]
     * 
     * @Date   [2017-3-1 10:20:17]
	 */
	@RequestMapping("/toAreaManage.bsh")
	public String toAreaManage(HttpServletRequest request,HttpServletResponse response) 
	{
		return "baseData/systemManage/area/areaManage.ftl";
	}
	
	/**
	 * 获取区域树数据
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [net.sf.json.JSONObject] 
	 * 
     * @author [hezhijian]
     * 
     * @Date   [2017-3-1 11:11:00]
	 */
	@RequestMapping("/doGetAreaTree.bsh")
	@ResponseBody
	public JSONObject doGetAreaTree(HttpServletRequest request,HttpServletResponse response) 
	{
		List<Map<String,Object>> tree;
		
		JSONObject json = new JSONObject();
		
		try
		{
			tree = areaServiceI.getAreaTree();
			json.put("tree", tree);
			json.put("status", "success");
			json.put("msg", "请求区域树信息成功!");
		}
		catch(Exception e)
		{
			logger.error("[请求区域树异常-ErrorMsg:]", e);
			json.put("status", "error");
			json.put("msg", "请求区域树信息异常!");
		}
		
		return json;
	}
	
	/**
	 * 获取区域名称和编码信息
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [java.util.Map<String, Object>] 
	 * 
     * @author [hezhijian]
     * 
     * @Date   [2017-3-1 14:10:13]
	 */
	@RequestMapping("/doGetAreaInfo.bsh")
	@ResponseBody
	public Map<String, Object> doGetAreaInfo(HttpServletRequest request,HttpServletResponse response) 
	{
		Map<String, Object> areaInfoMap = new HashMap<String, Object>();
		
		String areaId = request.getParameter("areaId");
		
		try
		{
			areaInfoMap = areaServiceI.getAreaInfoByAreaId(areaId);
			areaInfoMap.put("status", true);
			areaInfoMap.put("msg", "请求区域名称和编码信息成功！");
		}
		catch(Exception e)
		{
			logger.error("[请求区域名称和编码信息异常-ErrorMsg:]", e);
			areaInfoMap.put("status", "error");
			areaInfoMap.put("msg", "请求区域名称和编码信息异常！");
		}
		
		return areaInfoMap;
	}
	
	/**
	 * 获取区域所有信息
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [java.util.Map<String, Object>] 
	 * 
     * @author [hezhijian]
     * 
     * @Date   [2017-3-1 15:32:04]
	 */
	@RequestMapping("/doGetAreaAllInfo.bsh")
	@ResponseBody
	public Map<String, Object> doGetAreaAllInfo(HttpServletRequest request,HttpServletResponse response) 
	{
		Map<String, Object> areaInfoMap = new HashMap<String, Object>();
		
		String areaId = request.getParameter("areaId");
		
		try
		{
			areaInfoMap = areaServiceI.getAreaAllInfoByAreaId(areaId);
			areaInfoMap.put("status", true);
			areaInfoMap.put("msg", "请求区域所有信息成功！");
		}
		catch(Exception e)
		{
			logger.error("[请求区域所有信息异常-ErrorMsg:]", e);
			areaInfoMap.put("status", "error");
			areaInfoMap.put("msg", "请求区域所有信息异常！");
		}
		
		return areaInfoMap;
	}
	
	/**
	 * 新增编辑区域所有信息
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @Param [javax.servlet.http.HttpSession] session 会话对象
	 * 
	 * @return [java.util.Map<String, Object>] 
	 * 
     * @author [hezhijian]
     * 
     * @Date   [2017-3-1 15:32:04]
	 */
	@RequestMapping("/doAddArea.bsh")
	@ResponseBody
	public Map<String, Object> doAddArea(HttpServletRequest request,HttpServletResponse response,HttpSession session) 
	{
		Map<String, Object> map = new HashMap<String, Object>();
		
		String id = request.getParameter("id");//表单中的id
		String areaName = request.getParameter("areaName");//区域的名称
		String areaOrder = request.getParameter("areaOrder");//区域的顺便
		String areaCode = request.getParameter("areaCode");//区域的编码
		String areaZipCode = request.getParameter("areaZipCode");//区域的邮编
		String areaId = request.getParameter("areaId");//当前区域的id
		String areaType = request.getParameter("areaType");//区域的类型
		String time = DateUtil.getDateString(null);//当前时间
		UserInfo userInfo = (UserInfo) session.getAttribute("userInfo");//当前登录的用户
		String userId = userInfo.getId();//当前登录的用户Id
		
		Map<String, Object> areaMap = new HashMap<String, Object>();
		
		areaMap.put("areaName", areaName);
		areaMap.put("areaOrder", areaOrder);
		areaMap.put("areaCode", areaCode);
		areaMap.put("areaZipCode", areaZipCode);
		areaMap.put("time", time);
		
		try
		{
			int i = 0;
			
			if("0".equals(id))
			{
				//新增
				id = UUIDTool.randomUUID();
				areaMap.put("areaId",id);
				areaMap.put("parentId", areaId);
				areaMap.put("areaType", new Integer(areaType)+1);
				areaMap.put("userId", userId);
				
				//新增的时候检测名称是否重复
				i = areaServiceI.checkNameAdd(areaName,areaId);
				
				if(i > 0)
				{
					map.put("status", false);
					map.put("msg", "区域名称重复！");
				}else 
				{
					areaServiceI.addArea(areaMap);
					
					List<Map<String,Object>> tree = areaServiceI.getAreaTree();
					
					map.put("tree", tree);
					map.put("status", true);
					map.put("msg", "区域新增成功！");
				}
				
			}else 
			{
				//修改
				areaMap.put("areaId",id);
				
				//修改的时候检测名称是否重复
				i = areaServiceI.checkNameEdit(areaName,areaId);
				
				if(i > 0)
				{
					map.put("status", false);
					map.put("msg", "区域名称重复！");
				}else 
				{
					areaServiceI.editArea(areaMap);
					
					List<Map<String,Object>> tree = areaServiceI.getAreaTree();
					
					map.put("tree", tree);
					map.put("status", true);
					map.put("msg", "区域修改成功！");
				}
			}
			
		}
		catch(Exception e)
		{
			logger.error("[请求新增/编辑区域信息异常-ErrorMsg:]", e);
			map.put("status", "error");
			map.put("msg", "请求新增/编辑区域信息异常！");
		}
		
		return map;
	}
	
	/**
	 * 删除区域
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [java.util.Map<String, Object>] 
	 * 
     * @author [hezhijian]
     * 
     * @Date   [2017-3-2 09:39:04]
	 */
	@RequestMapping("/doDeleteArea.bsh")
	@ResponseBody
	public Map<String, Object> doDeleteArea(HttpServletRequest request,HttpServletResponse response) 
	{
		Map<String, Object> map = new HashMap<String, Object>();
		
		String areaId = request.getParameter("areaId");
		
		try
		{
			//检测是否有子区域
			int i = areaServiceI.checkAreaChild(areaId);
			
			if(i > 0)
			{
				map.put("status", false);
				map.put("msg", "该区域下有子区域，不能删除！");
			}else 
			{
				areaServiceI.deleteArea(areaId);
				
				List<Map<String,Object>> tree = areaServiceI.getAreaTree();
				
				map.put("tree", tree);
				map.put("status", true);
				map.put("msg", "删除成功！");
			}
			
		}
		catch(Exception e)
		{
			logger.error("[请求删除区域异常-ErrorMsg:]", e);
			map.put("status", "error");
			map.put("msg", "请求删除区域异常！");
		}
		
		return map;
	}
	
	@Resource
	private AreaServiceI areaServiceI;
	
}
