package com.bshinfo.web.base.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

public class CORSFilter implements Filter 
{
 
    @Override
    public void init(FilterConfig filterConfig) throws ServletException 
    {
 
    }
 
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException 
    {
	        HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
	        
	        //The HTTP response headers
	        httpResponse.addHeader("Access-Control-Allow-Origin", "*");
	        httpResponse.addHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
	        httpResponse.addHeader("Access-Control-Allow-Headers", "Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With");
	        httpResponse.addHeader("Access-Control-Max-Age","3600");
	        httpResponse.addHeader("Access-Control-Expose-Headers","Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With");
	
	        //The HTTP request headers
	        httpResponse.addHeader("Origin", "*");
	        httpResponse.addHeader("Access-Control-Request-Method", "GET, POST, OPTIONS");     
	        httpResponse.addHeader("Access-Control-Request-Headers","Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With");
	        filterChain.doFilter(servletRequest, servletResponse);
    }
 
    @Override
    public void destroy() 
    {
 
    }
}