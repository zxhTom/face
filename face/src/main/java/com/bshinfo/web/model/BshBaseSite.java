package com.bshinfo.web.model;

import java.io.Serializable;

/**
 * 站点实体类<br/>
 * 
 * 功能描述: 站点信息<BR/>
 * 
 * @author [wudongwei]
 * 
 * @see [MainMenu]
 * 
 * @since [JDK 1.7]
 * 
 * @version [1.0]
 * 
 * @Date [2017-2-17 13:21:31]
 */
public class BshBaseSite implements Serializable
{
	/**
	 *  java 序列化实现 (实体类必须添加)
	 */
	private static final long serialVersionUID = 5347782053002592142L;

	// 主键
	private String id;

	// 站点名称
	private String siteName;

	// 站点简称
	private String siteAbbreviation;

	// 站点类型,1-总站，2-子站，3-联盟站
	private short siteType;

	// 站点logo地址
	private String siteLogoUrl;

	// 站点描述
	private String siteDesc;

	// 站点模板ID:bsh_base_template.id
	private String siteTemplateId;

	// 站点地址
	private String siteUrl;

	// 站点是否启用：0-未启用，1-已启用
	private short isEnabled;

	// 创建人ID：bsh_base_user.id
	private String createUserId;

	// 创建时间
	private java.util.Date createTime;

	// 更新时间
	private java.util.Date updateTime;

	// 是否删除标识：0-未删除，1-已删除,默认值:0
	private short isDel;

	public String getId()
	{

		return this.id;
	}

	public void setId(String id)
	{

		this.id = id;
	}

	public String getSiteName()
	{

		return this.siteName;
	}

	public void setSiteName(String siteName)
	{

		this.siteName = siteName;
	}

	public String getSiteAbbreviation()
	{

		return this.siteAbbreviation;
	}

	public void setSiteAbbreviation(String siteAbbreviation)
	{

		this.siteAbbreviation = siteAbbreviation;
	}

	public short getSiteType()
	{

		return this.siteType;
	}

	public void setSiteType(short siteType)
	{

		this.siteType = siteType;
	}

	public String getSiteLogoUrl()
	{

		return this.siteLogoUrl;
	}

	public void setSiteLogoUrl(String siteLogoUrl)
	{

		this.siteLogoUrl = siteLogoUrl;
	}

	public String getSiteDesc()
	{

		return this.siteDesc;
	}

	public void setSiteDesc(String siteDesc)
	{

		this.siteDesc = siteDesc;
	}

	public String getSiteTemplateId()
	{

		return this.siteTemplateId;
	}

	public void setSiteTemplateId(String siteTemplateId)
	{

		this.siteTemplateId = siteTemplateId;
	}

	public String getSiteUrl()
	{

		return this.siteUrl;
	}

	public void setSiteUrl(String siteUrl)
	{

		this.siteUrl = siteUrl;
	}

	public short getIsEnabled()
	{

		return this.isEnabled;
	}

	public void setIsEnabled(short isEnabled)
	{

		this.isEnabled = isEnabled;
	}

	public String getCreateUserId()
	{

		return this.createUserId;
	}

	public void setCreateUserId(String createUserId)
	{

		this.createUserId = createUserId;
	}

	public java.util.Date getCreateTime()
	{

		return this.createTime;
	}

	public void setCreateTime(java.util.Date createTime)
	{

		this.createTime = createTime;
	}

	public java.util.Date getUpdateTime()
	{

		return this.updateTime;
	}

	public void setUpdateTime(java.util.Date updateTime)
	{

		this.updateTime = updateTime;
	}

	public short getIsDel()
	{

		return this.isDel;
	}

	public void setIsDel(short isDel)
	{

		this.isDel = isDel;
	}
}
