package com.bshinfo.web.service.serviceImpl.navigation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.bshinfo.web.dao.main.MainMenuMapper;
import com.bshinfo.web.dao.navigation.NavigationMapper;
import com.bshinfo.web.model.MainMenu;
import com.bshinfo.web.model.ResourceMenu;
import com.bshinfo.web.service.serviceI.navigation.NavigationServiceI;

/**
 * 导航菜单管理的业务逻辑层实现类 <br/>
 * 
 * 功能描述: 对导航菜单管理模块的各种业务处理 <BR/>
 * 
 * @author 	[jixingwang]
 * 
 * @see 	[NavigationServiceImpl]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-3-1 10:51:41]
 */
@Service
public class NavigationServiceImpl implements NavigationServiceI 
{

	@Override
	public List<Map<String, Object>> getPowerTree() 
	{
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		
		List<Map<String, Object>> tree = navigationMapper.getPowerTree();
		
		if(tree != null && tree.size() > 0)
		{
			for(Map<String,Object> child : tree)
			{
				if(child.get("children").toString().equals("0"))
				{
					child.put("icon","fa fa-file icon-state-warning icon-lg");
				}
				child.remove("children");
				list.add(child);
			}
		}
		
		return list;
	}
	
	@Override
	public Map<String, Object> getMenuInfo(String menuId, String menuType) 
	{
		Map<String, Object> menuInfo = new HashMap<String, Object>();
		if("3".equals(menuType))
		{
			menuInfo = navigationMapper.getMenuInfo(menuId);
			menuInfo.put("menuIcon", "");
			menuInfo.put("menuTpye", "按钮");
			menuInfo.put("menuURL", "");
			menuInfo.put("menuOrder", "");
		}
		else
		{
			menuInfo = mainMenuMapper.getMenuInfo(menuId);
			if("1".equals(menuType))
			{
				menuInfo.put("menuTpye", "一级菜单");
			}
			else
			{
				menuInfo.put("menuTpye", "二级菜单");
			}
		}
		
		return menuInfo;
	}
	
	@Override
	public void addMenu(MainMenu mainMenu, ResourceMenu resourceMenu) 
	{
		if(mainMenu == null)
		{
			navigationMapper.addMenu(resourceMenu);
		}
		else
		{
			mainMenuMapper.addMenu(mainMenu);
		}
	}
	
	@Override
	public void updateMenu(MainMenu mainMenu, ResourceMenu resourceMenu) 
	{
		if(mainMenu == null)
		{
			navigationMapper.updateMenu(resourceMenu);
		}
		else
		{
			mainMenuMapper.updateMenu(mainMenu);
		}
	}
	
	@Override
	public void delMenu(String menuId, String menuType) 
	{
		if("3".equals(menuType))
		{
			navigationMapper.delMenu(menuId);
		}
		else
		{
			mainMenuMapper.delMenu(menuId);
		}
	}

	@Override
	public Integer checkCodeByParentId(String parentId, String menuCode,
			int menuType, String menuId) 
	{
		int result = -1;
		//menuType 1.代表按钮    2.一二级菜单
		if(menuType == 1)
		{
			result = navigationMapper.checkCodeByParentId(parentId, menuCode, menuId);
		}
		else
		{
			result = mainMenuMapper.checkCodeByParentId(parentId, menuCode, menuId);
		}
		return result;
	}
	
	@Override
	public List<Map<String,Object>> getMenuByPower(String resourceId, String userId) {
		
		return navigationMapper.getMenuByPower(resourceId, userId);
	}
	
	@Resource
	private NavigationMapper navigationMapper;
	
	@Resource
	private MainMenuMapper mainMenuMapper;

}
