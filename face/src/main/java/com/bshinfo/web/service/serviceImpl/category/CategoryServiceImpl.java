package com.bshinfo.web.service.serviceImpl.category;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.bshinfo.web.base.util.ConstantUtil;
import com.bshinfo.web.dao.category.CategoryMapper;
import com.bshinfo.web.service.serviceI.category.CategoryServiceI;


/**
 * 
* @Title: CategoryServiceImpl 
* @Description: (获取分类业务逻辑处理) 
* @throws

 */
@Service
public class CategoryServiceImpl implements CategoryServiceI
{

	@Resource
	private CategoryMapper categoryMapper;
	
	/**
	 * 
	* @Title: CategoryServiceImpl 
	* @Description: (给查询出来的信息归类) 
	* @throws

	 */
	@Override
	public List<Map<String, Object>> getAllCategory() 
	{
	List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> tree = categoryMapper.getAllCategory();
		if(tree!=null && !"".equals(tree))
		{
			for(Map<String,Object> child : tree)
			{
				
				if(child.get("children").toString().equals("0"))
				{
					child.put("icon","fa fa-file icon-state-warning icon-lg");
				}
				child.remove("children");
				list.add(child);
			}
		}
		return list;
	}

	/**
	 * 
	* @Title: insertAddCategory 
	* @Description: (新增分类信息) 
	* @throws

	 */
	@Override
	public int insertAddCategory(Map<String, Object> map) 
	{
		int i = 0;
		//超管id
		String userId = ConstantUtil.ADMIN_ID;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String code  ="CA"+ sdf.format(new Date()).toString();
		String id = UUID.randomUUID().toString().replace("-", "");
		map.put("id", id);
		map.put("order_number", categoryMapper.querySeq()+1);
		map.put("category_code", code);
		map.put("create_user_id", userId);
		i = categoryMapper.insertAddCategory(map);
		return i;
	}

	@Override
	public List<Map<String, Object>> getUpCategory(String id)
	{
		return categoryMapper.getUpCategory(id);
	}

	@Override
	public Integer delCategory(String id) 
	{
		return categoryMapper.delCategory(id);
	}

	@Override
	public int editCategory(Map<String, Object> editSort)
	{
		return categoryMapper.editCategory(editSort);
	}

	@Override
	public Integer selectupId(Map<String, Object> selectupId)
	{
		return  categoryMapper.selectupId(selectupId);
	}

	@Override
	public int delAllupCategory(Map<String, Object> selectupId) 
	{
		return categoryMapper.delAllupCategory(selectupId);
	}

	@Override
	public int ischeck(Map<String, Object> categoryname) 
	{
		return categoryMapper.ischeck(categoryname);
	}

	@Override
	public int querySeq() 
	{
		return categoryMapper.querySeq();
	}

	@Override
	public String queryId() {
		return categoryMapper.queryId();
	}
	
	public  void del(){
		
	}
	
	
}
