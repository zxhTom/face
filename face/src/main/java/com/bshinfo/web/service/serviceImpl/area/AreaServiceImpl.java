package com.bshinfo.web.service.serviceImpl.area;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.bshinfo.web.dao.area.AreaMapper;
import com.bshinfo.web.service.serviceI.area.AreaServiceI;

/**
 * area业务逻辑层实现类 <br/>
 * 
 * 功能描述: 对area的各种业务处理 <BR/>
 * 
 * @author 	[hezhijian]
 * 
 * @see 	[AreaServiceImpl]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-3-1 11:33:07]
 */
@Service
public class AreaServiceImpl implements AreaServiceI 
{

	@Override
	public List<Map<String, Object>> getAreaTree() 
	{
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		
		List<Map<String, Object>> tree = areaMapper.getAreaTree();
		
		if(tree!=null && !"".equals(tree))
		{
			for(Map<String,Object> child : tree)
			{
				if(child.get("children").toString().equals("0"))
				{
					child.put("icon","fa fa-file icon-state-warning icon-lg");
				}
				child.remove("children");
				list.add(child);
			}
		}
		
		return list;
	}
	
	@Override
	public Map<String, Object> getAreaInfoByAreaId(String areaId) 
	{
		Map<String, Object> areaInfoMap = new HashMap<String, Object>();
		
		areaInfoMap = areaMapper.getAreaInfoByAreaId(areaId);
		if(areaInfoMap.get("area_code") == null)
		{
			areaInfoMap.put("area_code", "");
		}
		
		return areaInfoMap;
	}
	
	@Override
	public Map<String, Object> getAreaAllInfoByAreaId(String areaId) 
	{
		Map<String, Object> areaInfoMap = new HashMap<String, Object>();
		
		areaInfoMap = areaMapper.getAreaAllInfoByAreaId(areaId);
		
		return areaInfoMap;
	}
	
	@Override
	public Integer checkNameAdd(String areaName, String parentId) {
		Integer i = areaMapper.checkNameAdd(areaName, parentId);
		return i;
	}
	
	@Override
	public void addArea(Map<String, Object> areaMap) {
		areaMapper.addArea(areaMap);
	}
	
	@Override
	public Integer checkNameEdit(String areaName, String areaId) {
		Integer i = areaMapper.checkNameEdit(areaName, areaId);
		return i;
	}
	
	@Override
	public void editArea(Map<String, Object> areaMap) {
		areaMapper.editArea(areaMap);
	}
	
	@Override
	public Integer checkAreaChild(String areaId) {
		Integer i = areaMapper.checkAreaChild(areaId);
		return i;
	}
	
	@Override
	public void deleteArea(String areaId) {
		areaMapper.deleteArea(areaId);
	}
	
	@Resource
	private AreaMapper areaMapper;

}
