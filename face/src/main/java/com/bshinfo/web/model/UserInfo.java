package com.bshinfo.web.model;

/**
 * 用户基础信息类<br/>
 * 
 * 功能描述: 后台管理用户基础信息<BR/>
 * 
 * @author 	[donghang]
 * 
 * @see 	[UserInfo]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-2-20]
 */
public class UserInfo implements java.io.Serializable
{
	/** 
	 * java 序列化实现 (实体类必须添加)
	 */ 
	private static final long serialVersionUID = -3700410201580804028L;

	// 主键id
	private String id;
	// 真实姓名
	private String realName;
	// 性别
	private Integer userSex;
	
	// 用户头像
	private String userAvatarUrl;
	// 用户昵称
	private String userNick;
	// 用户状态
	private Integer userStatus;
	// 实名认证状态
	private Integer isVerify;
	// 所属站点id
	private String siteId;
	// 创建时间
	private String createTime;
	// 更新时间
	private String updateTime;
	// 是否删除标识：0-未删除，1-已删除,：默认未删除 0
	private Integer isDel;
	//创建人id
	private String createUserId;
	

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}
	public Integer getUserSex() {
		return userSex;
	}
	public void setUserSex(Integer userSex) {
		this.userSex = userSex;
	}
	public String getUserAvatarUrl() {
		return userAvatarUrl;
	}
	public void setUserAvatarUrl(String userAvatarUrl) {
		this.userAvatarUrl = userAvatarUrl;
	}
	public String getUserNick() {
		return userNick;
	}
	public void setUserNick(String userNick) {
		this.userNick = userNick;
	}
	public Integer getUserStatus() {
		return userStatus;
	}
	public void setUserStatus(Integer userStatus) {
		this.userStatus = userStatus;
	}
	public Integer getIsVerify() {
		return isVerify;
	}
	public void setIsVerify(Integer isVerify) {
		this.isVerify = isVerify;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getIsDel() {
		return isDel;
	}
	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
}
