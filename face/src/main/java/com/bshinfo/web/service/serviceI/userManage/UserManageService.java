package com.bshinfo.web.service.serviceI.userManage;

import java.util.List;
import java.util.Map;

import com.bshinfo.web.model.dataTableUtil.DataTableModel;

/**
 * UserManage业务逻辑层接口 <br/>
 * 
 * 功能描述: 对UserManage的各种业务处理 <BR/>
 * 
 * @author 	[donghang]
 * 
 * @see 	[UserManageService]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-2-20]
 */
public interface UserManageService {

	/***
	 * 查询用户列表
	 * 
	 * @return [DataTableModel] 
	 * 
     * @author 	[donghang]
	 * @param id 
     * 
     * @Date    [2017-2-20]
	 */
	DataTableModel getUserTable(Map<String, String> dataTableMap, String id);

	/***
	 * 根据用户id查询用户信息
	 * 
	 * @return [Map] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-20]
	 */
	Map<String, Object> getUserById(String id);

	/***
	 * 根据用户id重置用户密码
	 * 
	 * @return [boolean] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-20]
	 */
	boolean resetPassword(String id);

	/***
	 * 根据用户id重置用户密码
	 * 
	 * @return [boolean] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-21]
	 */	
	boolean deleteUserById(String id);

	/***
	 * 根据用户id更改用户状态
	 * 
	 * @return [boolean] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-21]
	 */	
	boolean changeStatusById(String id, String status);

	/***
	 * 根据用户id更改审核状态
	 * 
	 * @return [boolean] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-21]
	 */	
	boolean apprUserVerifyById(String id, String message, String status);

	/***
	 * 新增用户
	 * 
	 * @return [void] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-21]
	 */	
	void addUser(Map<String, Object> paramMap);

	/***
	 * 根据id修改用户
	 * 
	 * @return [void] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-22]
	 */	
	void editUser(Map<String, Object> paramMap);

	/***
	 * 根据id批量修改用户状态
	 * 
	 * @return [boolean] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-22]
	 */	
	void changeUsersStatus(String ids,String status);

	/***
	 * 根据id查询用户管辖站点
	 * 
	 * @return [List<Map<String, Object>>] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-23]
	 */	
	List<Map<String, Object>> getSelectList(String id);

	/***
	 * 获取城市列表
	 * 
	 * @return [List<Map<String, Object>>] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-23]
	 */	
	List<Map<String, Object>> getCity();

	/***
	 * 获取区/县列表
	 * 
	 * @return [List<Map<String, Object>>] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-23]
	 */	
	List<Map<String, Object>> getArea(String id);

	/***
	 * 根据管理员id 展示站点树
	 * 
	 * @return [List<Map<String, Object>>] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-23]
	 */	
	List<Map<String, Object>> getSiteTree(String id);

	/***
	 * 查询用户名，手机号，邮箱是否唯一
	 * 
	 * @return [boolean] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-23]
	 */	
	boolean checkUser(Map<String, Object> map);

	/***
	 * 根据站名查询站点id
	 * 
	 * @return [boolean] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-23]
	 */	
	String queryByName(String siteName);
	
	/**
	 * 登录验证
	 * 
	 * @param  [java.lang.String] username 用户登录名称
	 * 
	 * @param  [java.lang.String] password 用户密码
	 * 
	 * @return [java.util.Map<String,Object>] 
	 * 
     * @author 	[jixingwang]
     * 
     * @Date    [2017-2-27 11:10:11]
	 */
	Map<String,Object> loginCheck(String username, String password);

	/**
	 * 获取用户权限
	 * 
	 * @param  [java.lang.String] username 用户登录名称
	 * 
	 * @param  [java.lang.String] password 用户密码
	 * 
	 * @return [java.util.Map<String,Object>] 
	 * 
     * @author 	[jixingwang]
     * 
     * @Date    [2017-2-27 11:10:11]
	 */
	Map<String, Object> getUserRole(String userId,String menuId);
}
