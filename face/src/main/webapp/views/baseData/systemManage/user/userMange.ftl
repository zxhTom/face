<#assign base=request.contextPath />
<!DOCTYPE HTML>
<html>
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		
		<meta content="" name="description" />
		
		<meta content="" name="author" />
        <title>Metronic | Editable Datatables</title>
        
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
		<!-- END GLOBAL MANDATORY STYLES -->
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
		<!-- END PAGE LEVEL PLUGINS -->
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

		<!-- BEGIN THEME GLOBAL STYLES -->
		<link href="${base}/js/library/metronic-4.7/assets/global/css/components.css" rel="stylesheet" id="style_components" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
		<!-- END THEME GLOBAL STYLES -->
		<!-- BEGIN THEME LAYOUT STYLES -->
		<link href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
		<link href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
		<!-- END THEME LAYOUT STYLES -->
		<!-- 弹出框 -->
		<link href="${base}/js/library/sweetalert/css/sweetalert.css" rel="stylesheet" type="text/css" />
		</head>
    <!-- END HEAD -->
		<body style="background-color:#FFFFFF;" id="userManage">
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<div class="page-toolbar">
					<div id="dashboard-report-range"
						class="pull-right tooltips btn btn-sm" data-container="body"
						data-placement="bottom"
						data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp; <span
							class="thin uppercase hidden-xs"></span>&nbsp; <i
							class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->

			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">
				后台管理 <small>用户管理</small>
			</h3>
			<div class="row">
			    <div class="col-md-12">
			        <!-- BEGIN EXAMPLE TABLE PORTLET-->
			        <div class="portlet light portlet-fit bordered">
			            <div class="portlet-title">
			            	<div style="height:50px">
			            		<div class="col-md-4">
				            		<label class="col-sm-3 control-label">用户名</label>
									<div class="col-sm-6 input-group">
										<input id="userName" type="text" class="form-control"  style="margin-left:31px"/>
									</div>
			            		</div>
			            		<div class="col-md-5">
				            		<label class="col-sm-4 control-label">范围时间</label>
				            		<div class="col-sm-3">
					            		<div class="input-group input-large date-picker input-daterange" style="margin-right:350px;"
												data-date-format="yyyy-mm-dd">
												<input id="start" type="text" class="form-control" readonly /> 
												<span class="input-group-addon">-</span> <input type="text" id="end" class="form-control" readonly />
										</div>	
									</div>
								</div>
			            	</div>
			                <div>
			                	<div class="col-md-3" >
			                    	<label class="control-label col-md-5">实名状态</label>
			                    	<div class="col-sm-2">
				                    	<select id="realName"class='form-control input-sm input-msmall input-inline' data-width='100px'>
										 <option value="">请选择</option>
	                                     <option value="1">已认证</option>
	             	                     <option value="2">未认证</option>
	             	                     <option value="3">待认证</option>
	             	                     <option value="4">已驳回</option>
             	                     </select>
             	                    </div>
         	                    </div>
         	                    <div class="col-md-3" > 
             	              		<label class="col-md-4 control-label">状态</label>
             	              		<div class="col-sm-2">
	             	              		 <select id="status" class='form-control input-sm input-msmall input-inline' data-width='100px'>
										 <option value="">请选择</option>
	                                     <option value="1">有效</option>
	             	                     <option value="0">无效</option>
	             	                     </select>
             	                    </div>
         	                    </div>
         	                    <div class="col-md-3" > 
             	     		        <label class="col-md-5 control-label">所属站点</label>
             	     		        <div class="col-sm-2">
             	     		       	 	 <select id="site" name="querySelect" class='form-control input-sm input-msmall input-inline' data-width='200px'>
									 	 <option value="">请选择</option>
             	                     	 </select>
             	                    </div> 
         	                    </div>
             	                     <button class='btn green'  value="清空" style="float:right;margin-left:15px;" onclick="clear1()">清空</button>
             	                     <button class='btn green'  value="搜索" style="float:right;margin-left:15px;" onclick="queryUserTable()">搜索</button>
             	                     
			                </div>
			            </div>
			            <div class="portlet-body">
			                <div class="table-toolbar">
			                    <div class="row">
			                       
			                    </div>
			                </div>
			                <table class="table table-striped table-hover table-bordered"
								id="userTable">
							</table>	
			            </div>
			        </div>
			        <!-- END EXAMPLE TABLE PORTLET-->
			    </div>
			</div>
			
			<!--查看模态框-->
			<div id="viewModal" class="modal fade" role="dialog"
		        aria-hidden="true">
		        <div class="modal-dialog">
		          <div class="modal-content">
		            <div class="modal-header">
		              <button type="button" class="close" data-dismiss="modal"
		                aria-hidden="true"></button>
		              <h4 class="modal-title">查看信息</h4>
		            </div>
		            <div class="tabbable tabbable-tabdrop">
		              <ul class="nav nav-tabs">
		                <li class="active"><a href="#tab1" data-toggle="tab">个人学习</a>
		                </li>
		                <li><a href="#tab2" data-toggle="tab">课程学习</a></li>
		                <li><a href="#tab3" data-toggle="tab">证书学习</a></li>
		                <li><a href="#tab4" data-toggle="tab">培训学习</a></li>
		              </ul>
		              <div class="tab-content">
		                <div class="tab-pane active" id="tab1">
		                  <div class="modal-body form">
		                    <form action="#" class="form-horizontal form-row-seperated">
		                      <label class="list-group-item bg-default bg-font-default">基础信息</label>
		                      <div>
		                        <p>
		                          <span style="margin-left:80px;width: 220px;display:inline-block;"
		                            id="modal_userinfo_user_name">用户名：</span> <span
		                            style="margin-left:50px;width: 190px;display:inline-block;" id="modal_userinfo_site" >所属站点：</span>
		                        </P>
		                      </div>
		                      <div>
		                        <p>
		                          <span style="margin-left:80px;width: 220px;display:inline-block;" id="modal_userinfo_nick">昵称：</span>
		                          <span style="margin-left:50px;width: 190px;display:inline-block;" id="modal_userinfo_address">所属地区：</span>
		                        </p>
		                      </div>
		                      <label class="list-group-item bg-default bg-font-default">认证信息</label>
		                      <div>
		                        <p>
		                          <span style="margin-left:80px;width: 220px;display:inline-block;" id="modal_userinfo_verify">实名状态：</span>
		                        </p>
		                      </div>
		                      <div>
		                        <p>
		                          <span style="margin-left:80px;width: 220px;display:inline-block;"
		                            id="modal_userinfo_real_name">真实姓名：</span> <span
		                            style="margin-left:50px;width: 190px;display:inline-block;" id="modal_userinfo_sex">性别：</span>
		                        </p>
		                      </div>
		                      <div>
		                        <p>
		                          <span style="margin-left:80px;width: 220px;display:inline-block;" id="modal_userinfo_card">身份证号：</span>
		                          <span style="margin-left:50px;width: 190px;display:inline-block;" id="modal_userinfo_phone">手机号：</span>
		                        </p>
		                      </div>
		                      <div>
		                        <p>
		                          <span style="margin-left:80px;width: 220px;display:inline-block;"
		                            id="modal_userinfo_job_address">工作单位：</span> <span
		                            style="margin-left:50px;width: 190px;display:inline-block;" id="modal_userinfo_email">邮箱：</span>
		                        </p>
		                      </div>
		                      <div>
		                        <p>
		                          <span style="margin-left:80px;width: 220px;display:inline-block;" id="modal_userinfo_edu">学历：</span>
		                          <span style="margin-left:50px;width: 190px;display:inline-block;" id="modal_userinfo_college">大学院校：</span>
		                        </p>
		                      </div>
		                    </form>
		                  </div>
		                </div>
		                <div class="tab-pane" id="tab2">
		                  <p>课程学习</p>
		                </div>
		                <div class="tab-pane" id="tab3">
		                  <p>证书学习</p>
		                </div>
		                <div class="tab-pane" id="tab4">
		                  <p>培训学习</p>
		                </div>
		              </div>
		            </div>
		
		            <div class="modal-footer">
		              <button type="button" class="btn grey-salsa btn-outline"
		                data-dismiss="modal">关闭</button>
		            </div>
		          </div>
		        </div>
		      </div>
			<!-- 模态框 -->
			<div id="editModal" class="modal fade" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">编辑</h4>
						</div>
						<div class="modal-body form">
							<form action="#" class="form-horizontal form-row-seperated" id="editForm">
								<label class="list-group-item bg-default bg-font-default">基础信息</label>
								<div class="form-group">
									<input type="text" name="id" style="display:none" id="editUserId">
									<label class="col-sm-2 control-label">用户名<span class="required"> * </span></label>
									<div class="col-sm-4">
										<div class="input-group">
											<input type="text" name="userName" id="editUserName" class="form-control required" style="width:260px;"/>
											<span id="checkEditUserName" style="display:none"></span>
										</div>
									</div>
									<label class="col-sm-2 control-label">昵称</label>
									<div class="col-sm-4">
										<div class="input-group">
											<input type="text" name="userNick" id="editUserNick" class="form-control" style="width:260px;"/>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">所属站点<span class="required"> * </span></label>
									<div class="input-group">
									<span id="editSiteName"></span>
									<input type="button" class="btn green" value="选择" data-toggle='modal' data-target='#treeModal' onclick="getRoleTree()"/>
									<input type="text" name="siteId" id="editSiteId" class="form-control required "  style="width:10px;visibility:hidden"/>
									</div>
								</div>
								<label class="list-group-item bg-default bg-font-default">实名信息</label>
								<div class="form-group" id="addIdentity">
									<label class="col-sm-2 control-label">实名状态</label>
									<label class="col-sm-2">
	                                    <input type="radio" name="identity" value="1" checked/>已认证</label>
	                                <label class="col-sm-2">
	                                    <input type="radio" name="identity" value="0" />未认证</label>
								</div>
								<div class="form-group" id="editIdentity">
									<label class="col-sm-2 control-label">实名状态 </label>
								 	<select id="editVerifyStatus" name="verifyStatus" style="margin-left:15px" onchange="changeVerifyStatus()" class='form-control input-sm input-msmall input-inline' data-width='200px'>
									 	<option value="">请选择</option>
                                    	<option value="1">已认证</option>
             	                    	<option value="2">未认证</option>
             	                     	<option value="3">待认证</option>
             	                     	<option value="4">已驳回</option>
             	                     	</select>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">实名图片</label>
									<div class="col-md-4" id="editFront" >
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <img src="../image/user/default.jpg" alt="" id="preview_front"/> </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> 选择正面 </span>
                                                    <span class="fileinput-exists"> 更换 </span>
                                                    <input type="file" name="files" id="editFrontFile" accept="image/png,image/jpeg" > </span>
                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> 移除 </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4" id="editBack" >
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <img src="../image/user/default.jpg" alt="" id="preview_back"/> </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> 选择反面 </span>
                                                    <span class="fileinput-exists"> 更换 </span>
                                                    <input type="file" name="files" id="editBackFile" accept="image/png,image/jpeg" > </span>
                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> 移除 </a>
                                            </div>
                                        </div>
                                    </div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">真实姓名<span class="required" id="editRealNameRequired">*</span></label>
									<div class="col-sm-4">
										<div class="input-group">
											<input type="text" name="realName" id="editRealName" class="form-control required" style="width:260px;"/>
										</div>
									</div>
		                                <label class="col-sm-2 control-label">性别</label>
		                                <label>
		                                    <input type="radio" name="sex" value="1" checked/>男</label>
		                                <label>
		                                    <input type="radio" name="sex" value="2" />女</label>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">身份证<span class="required" id="editIdCardRequired">*</span></label>
									<div class="col-sm-4">
										<div class="input-group">
											<input type="text" id="editIdCard" name="idCard" class="form-control required" style="width:260px;"/>
											<span id="checkEditIdCard" style="display:none"></span>
										</div>
									</div>
									<label class="col-sm-2 control-label ">手机<span id="editPhoneRequired" class="required">*</span></label>
									<div class="col-sm-4">
										<div class="input-group">
											<input type="text" id="editPhone" name="phone" class="form-control required" style="width:260px;"/>
											<span id="checkEditUserPhone" style="display:none"></span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">所属地区：</label>
									<div class="col-sm-3">
										<div class="input-group">
											<select name="addrCity" id="editAddrCity" onchange="queryArea(0)"class='form-control input-sm input-msmall input-inline' data-width='200px'>
										 	<option value="">请选择</option>
	             	                     	</select>
										</div>
									</div>
		                                <label>市</label>
		                                <select name="addrArea" id="editAddrArea"class='form-control input-sm input-msmall input-inline' data-width='200px'>
										 	<option value="">请选择</option>
             	                     	</select>县/镇
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">工作单位</label>
									<div class="col-sm-4">
										<div class="input-group">
											<input type="text" id="editJobAddr" name="jobAddr" class="form-control" style="width:260px;"/>
										</div>
									</div>
									<label class="col-sm-2 control-label email">邮箱</label>
									<div class="col-sm-4">
										<div class="input-group">
											<input type="text" id="editEmail" name="email" class="form-control" style="width:260px;"/>
											<span id="checkEditUserEmail" style="display:none"></span>
										</div>
									</div>
								</div>
								<div class="form-group last">
									<label class="col-sm-2 control-label">学历</label>
									<div class="col-sm-4">
										<div class="input-group">
											<select id="editEduDegree" name="eduDegree" class='form-control input-sm input-msmall input-inline' data-width='200px'>
											 	<option value="">请选择</option>
		                                    	<option value="1">其他</option>
											 	<option value="2">小学</option>
		                                    	<option value="3">初中</option>
		             	                    	<option value="4">高中/中专</option>
		             	                     	<option value="5">专科/高职</option>
		             	                     	<option value="6">大学本科</option>
		             	                     	<option value="7">硕士</option>
		             	                     	<option value="8">博士</option>
	             	                     	</select>
             	                     	</div>
         	                     	</div>
									<label class="col-sm-2 control-label">毕业院校</label>
									<div class="col-sm-4">
										<div class="input-group">
											<input type="text" id="editGraduation" name="graduation" class="form-control" style="width:260px;"/>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn grey-salsa btn-outline" data-dismiss="modal">关闭</button>
									<button type="sumbit" class="btn green">
				                    	提交
				                    </button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- 树模态框 -->
			<div id="treeModal" class="modal fade" role="dialog" aria-hidden="true" style="height:800px">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title" >编辑</h4>
						</div>
						<div style="height:200px; overflow-y:scroll">
							<div class="portlet light bordered">
								<div class="portlet-body">
									<div id="tree_3" class="tree-demo"> </div>
								</div>
							</div>
						</div>	
						<div class="modal-footer">
							<button type="button" class="btn grey-salsa btn-outline" data-dismiss="modal">关闭</button>
						</div>
					</div>
				</div>
			</div>
			
			<!-- 审批模态框 -->
			<div id="apprModal" class="modal fade" role="dialog" aria-hidden="true" style="height:800px">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">实名认证</h4>
						</div>
						<div class="modal-body form">
							<form id="apprForm" action="#" class="form-horizontal form-row-seperated">
								<div class="form-group">
									<input type="text" name="id" style="display:none" id="apprId">
									<label class="col-sm-3 control-label">身份证图片：</label>
									<img class="col-sm-3" src="/comminityEducation-manage/image/user/default.jpg"  alt="暂无图片" id="appr_front">
		                            <img class="col-sm-3" src="/comminityEducation-manage/image/user/default.jpg"  alt="暂无图片" id="appr_back">
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">认证审批：</label>
									<label class="col-sm-2">
		                                <input type="radio" name="appr" value="1" checked/>通过</label>
		                            <label class="col-sm-2">
		                                <input type="radio" name="appr" value="4" />驳回</label>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">认证意见：</label>
									<textarea class=" col-sm-8" name="message" rows="6" placeholder="Enter a message ..."></textarea>
								</div>
							</form>	
						</div>
						<div class="modal-footer">
							<button type="button" class="btn grey-salsa btn-outline" data-dismiss="modal">关闭</button>
							<button type="button" class="btn green" onclick="apprUser()">提交</button>
						</div>
					</div>
				</div>
			</div>
			<!-- 导入模态框 -->
			<div id="importModal" class="modal fade" role="dialog" aria-hidden="true" style="height:800px">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">批量上传</h4>
						</div>
						<div class="modal-body form">
							<form id="importForm" action="#" class="form-horizontal form-row-seperated">
								<div class="form-group">
									<label class="col-sm-3 control-label">下载模板：</label>
									<button class='btn green' style='float:left;margin-left:15px;' onclick="downloadTemplate()">下载</button>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">上传文件：</label>
									<div class="col-md-4" id="importFile">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <span class="btn green btn-file">
                                                <span class="fileinput-new"> 上传 </span>
                                                <span class="fileinput-exists"> 更换 </span>
                                                <input type="file" name="file" accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" class="required"> </span>
                                            <span class="fileinput-filename"> </span> &nbsp;
                                            <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
                                        </div>
                                    </div>
								</div>
							</form>	
						</div>
						<div class="modal-footer">
							<button type="button" class="btn grey-salsa btn-outline" data-dismiss="modal">关闭</button>
							<button type="button" class="btn green" onclick="uploadUser()">提交</button>
						</div>
					</div>
				</div>
			</div>
			
		<!-- BEGIN CORE PLUGINS -->
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/moment.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-CN.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/scripts/datatable.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jstree/dist/jstree.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery-validation/js/jquery.validate.js" type="text/javascript"></script>
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="${base}/js/library/metronic-4.7/assets/global/scripts/app.min.js" type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>

	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script src="${base}/js/library/metronic-4.7/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->
    <!-- 弹出框 -->
	<script src="${base}/js/library/sweetalert/js/sweetalert.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/sweetalert/js/showAlert.js" type="text/javascript"></script>  
		<!-- 获取根路径 -->
	<script src="${base}/js/common/util/jqueryUtil.js" type="text/javascript"></script>  
	<script>
	
		//定义全局变量
		var usersId={};//用户列表勾选状态
		var onlyUserName='1';//用户名唯一验证
		var onlyUserPhone='1';//用户电话唯一验证
		var onlyUserEmail='1';//用户邮箱唯一验证
		var onlyUserIdCard='1';//用户身份证唯一验证
		var UserDataTable;//用户列表
		var roles = [];//用户所拥有的按钮权限
		$("#userManage").bind("DOMNodeInserted", function() 
		{
		  window.top.resizeParentHeightFun();
		});
		
		$(document).ready(function()
		{	
		    $("input[name=identity]").click(function(){
		    	var flag=$("input[name='identity']:checked").val();
		    	if(flag =='0')
		    	{
		    		changeStatus(1);
		    	}else
		    	{
		    		changeStatus(0);
		    	}
		    });
		    
			//加载用户列表
			UserDataTable = $('#userTable').DataTable(
			{
			"bSort": true,
			"bProcessing": true,
			"bServerSide": true,
			"bStateSave": false,
			"sAjaxSource": jQuery.getBasePath() + "/userManage/getUserTable.bsh",
			"sDom": '<"top"l<"clear">>rt<"bottom"ip<"clear">>',
			"lengthMenu": [10, 20, 30],
			"pageLength": 10,
			"oLanguage": 
			{
				"sProcessing": "正在加载数据...",
				"sSearch": "搜索:",
				"sLengthMenu": "_MENU_条记录",
				"sZeroRecords": "没有查到记录",
				"sInfo": "第  _START_ 条到第  _END_ 条记录,一共  _TOTAL_ 条记录",
				"sInfoEmpty": "0条记录",
				"oPaginate": 
				{
					"sPrevious": "上一页 ",
					"sNext": " 下一页",
				}
			},
			"aoColumnDefs": [
			{
				"bSearchable": true,
				"bVisible": false,
				"aTargets": [0]
			}],
			"aoColumns": [
			{
				"sTitle": "id",
				"bSortable": false
			}, {
				"sTitle": '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" id="checkAll" onclick="chooseOrNoChoose(this)" /><span></span></label>',
				"bSortable": false,
				"sClass": "text-center",
				"fnCreatedCell":function(nTd, sData, oData, iRow, iCol)
				{
					var value = oData[0];
					var content = '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" class="testCheckbox" value="'+value+'" onclick="recordRoleId(this)"/><span></span></label>';
					$(nTd).html(content);
				}
			},{
				"sTitle": "用户名",
				"bSortable": true,
				"sClass": "text-center",
				"fnCreatedCell":function(nTd, sData, oData, iRow, iCol)
				{
					var content = "<a href='' onclick='showUserInfo(\""+oData[0]+"\")' data-toggle='modal' data-target='#viewModal'>"+sData+"</a>";;
					$(nTd).html(content);
				}
			}, {
				"sTitle": "实名认证",
				"bSortable": false,
				"bAutoWidth": true,
				"sClass": "text-center",
				"fnCreatedCell":function(nTd, sData, oData, iRow, iCol)
				{
					var content;
					if(sData=="1"){
		            	content="已认证";
		            }else if(sData=="2")
		            {
		           		content="未认证";
		            }else if(sData=="3")
		            {
		            	/*if(roles.indexOf('USERMANAGE_APPR')>-1)
		            	{
	           				content="<a href='#' onclick='apprUserModel(\""+oData[8]+"\",\""+oData[9]+"\",\""+oData[0]+"\")' data-toggle='modal' data-target='#apprModal'>待认证</a>";
		            	}else
		            	{
		            		content="<a href='#' disabled onclick='apprUserModel(\""+oData[8]+"\",\""+oData[9]+"\",\""+oData[0]+"\")' data-toggle='modal' data-target='#apprModal'>待认证</a>";
		            	}*/
		            	content="<a href='#' class='apprBtn1' onclick='apprUserModel(\""+oData[8]+"\",\""+oData[9]+"\",\""+oData[0]+"\")' data-toggle='modal' data-target='#apprModal'>待认证</a><span class='apprBtn2'>待认证</span></div>"
		            }else if(sData=="4"){
		           		content="已驳回";
		            }
					$(nTd).html(content);
				}
			}, {
				"sTitle": "所属站点",
				"bSortable": false,
				"sClass": "text-center"
			}, {
				"sTitle": "注册时间",
				"bSortable": true,
				"sClass": "text-center"
			}, {
				"sTitle": "状态",
				"bSortable": false,
				"sClass": "text-center",
				"fnCreatedCell":function(nTd, sData, oData, iRow, iCol)
				{
					var content;
		          	if(sData>0)
		          	{
		            	content ="<button class='btn btn-xs blue statusBtn' disabled onclick='changeStatusById(\""+oData[0]+"\",0)' >有效</a>";
		            }else
		            {
		           		content ="<button class='btn btn-xs blue statusBtn' disabled onclick='changeStatusById(\""+oData[0]+"\",1)' >无效</a>";
		            }
					$(nTd).html(content);
				}
			}, {
				"sTitle": "操作",
				"bSortable": false,
				"sClass": "text-center",
				"fnCreatedCell":function(nTd, sData, oData, iRow, iCol)
				{
					var editBtn = "";
					var resetBtn ="";
					var deleteBtn = "";
          			editBtn =  $("<button class='btn btn-xs blue editBtn' disabled onclick='showModal(\""+oData[0]+"\")'>修改资料</button>&nbsp&nbsp");
          			resetBtn = $("<button class='btn btn-xs red-flamingo resetBtn' disabled onclick='showConfirmModal(\"确定重置密码？\",resetPassword,\""+oData[0]+"\")' >密码重置</button>&nbsp&nbsp");
          			deleteBtn = $("<button class='btn btn-xs yellow-crusta deleteBtn' disabled onclick='showConfirmModal(\"确定删除此角色？\",deleteUserById,\""+oData[0]+"\")' >删除</button>");
					$(nTd).append(editBtn);
					$(nTd).append(resetBtn);
					$(nTd).append(deleteBtn);
				}
			},{
				"sTitle": "身份证前",
				"bSortable": false,
				"bVisible": false
			},{
				"sTitle": "身份证后",
				"bSortable": false,
				"bVisible": false
			}],
			// 服务器端，数据回调处理  
			"fnServerData": function retrieveData(sSource, aoData, fnCallback) 
			{
			    aoData.push({"name":"userName","value":$("#userName").val()});
			    aoData.push({"name":"start","value":$("#start").val()});
			    aoData.push({"name":"end","value":$("#end").val()});
			    aoData.push({"name":"realName","value":$("#realName").val()});
			    aoData.push({"name":"status","value":$("#status").val()});
			    aoData.push({"name":"site","value":$("#site").val()});
				$.ajax(
				{
					"type": "POST",
					"url": sSource,
					"dataType": "json",
					"contentType": "application/json",
					"data": JSON.stringify(aoData),
					"success": function(data) 
					{
						if(data.flag)
						{
							fnCallback(data.data);
							if(roles.length>0)
							{
								if(roles.indexOf('USERMANAGE_STATUS')>-1)
								{
									$(".statusBtn").removeAttr("disabled");
								}
								if(roles.indexOf('USERMANAGE_EDIT')>-1)
								{
									$(".editBtn").removeAttr("disabled");
								}
								if(roles.indexOf('USERMANAGE_RESETCODE')>-1)
								{
									$(".resetBtn").removeAttr("disabled");
								}
								if(roles.indexOf('USERMANAGE_DELETE')>-1)
								{
									$(".deleteBtn").removeAttr("disabled");
								}
								if(roles.indexOf('USERMANAGE_APPR')>-1)
				            	{
				            		$(".apprBtn2").hide();
				            	}else
				            	{
				            		$(".apprBtn1").hide();
				            	}
							}
						}else
						{
							showSuccessOrErrorModal("查询用户出错了","error"); 
						}
						
					}
				});
			}
			});
			queryUserRole();
			querySelect();
			queryCity();
			
		});
		
		
		//表单验证	
		$("#editForm").validate({
		 	submitHandler : function() 
			{
				if(onlyUserName == '0')
				{
					showInfoModal("用户名不合法，请重新填写");
					return;
				}else if(onlyUserPhone == '0'&& $("#editPhone").val()!="")
				{
					showInfoModal("手机号不合法，请重新填写");
					return;
				}else if(onlyUserEmail == '0'&& $("#editEmail").val()!="")
				{
					showInfoModal("邮箱已被使用，请重新填写");
					return;
				}else if(onlyUserIdCard == '0'&& $("#editIdCard").val()!="")
				{
					showInfoModal("身份证已被使用，请重新填写");
					return;
				}else if($("#editPicRequired").hasClass("required"))
				{	
					if(document.getElementById("editFrontFile").value==""||document.getElementById("editFrontFile").value==null
					||document.getElementById("editBackFile").value==null||document.getElementById("editBackFile").value==null)
					{
						showInfoModal("请上传身份证图片");
						return;
					}else
					{
						if($("#editUserId").val()!="")
		    			{
		    				editUser();
		    			}else
		    			{
		    				addUser();
		    			}
						return;
					}
				}else
				{
					if($("#editUserId").val()!="")
					{
						editUser();
					}else
					{
						addUser();
					}
				}
			}
		});	
		//时间插件汉化
		$('.date-picker').datepicker(
		{
			language: 'zh-CN',
			autoclose: true,
			todayHighlight: true
		});
		
		//加载站点树
		var getRoleTree = function () 
		{
	        $.ajax(
	        {
		        type : "POST",  
		        dataType : 'json', 
		        url : jQuery.getBasePath() + "/userManage/getSiteTree.bsh",
		        success : function(data) 
		        {  
		        	if(data.flag)
		        	{
		                $('#tree_3').jstree(
		                {
		                    'plugins': ["wholerow","types"],
		                    'core': 
		                    {
		                        "themes" : 
		                        {
		                            "responsive": false
		                        },    
		                        'data':data.tree   
		                    },
		                    "types" : 
		                    {
		                        "default" : 
		                        {
		                            "icon" : "fa fa-folder icon-state-warning icon-lg"
		                        },
		                        "file" : 
		                        {
		                            "icon" : "fa fa-file icon-state-warning icon-lg"
		                        }
		                    }
		                });
		            }else
		            {
		            	showSuccessOrErrorModal("查询站点树出错","error");
		            }   
	            }, error : function(data) {  
			        	showSuccessOrErrorModal("查询站点树出错","error"); 
		        }
		          
		    });
	    }
			    
		//树节点单击事件
	    $('#tree_3').bind("select_node.jstree", function (obj, data) 
	    {
		    // 处理代码
		    // 获取当前节点
		    var currentNode = data.node;
		    document.getElementById("editSiteName").innerText=currentNode.text;
		    $("#editSiteId").val(currentNode.id);
		    $("#treeModal").modal("hide");
		});
			
		function clear1()
		{
			$("#userName").val("");
			$("#start").val("");
			$("#end").val("");
			$("#realName").val("");
			$("#site").val("");
			$("#status").val("");
		}
		
		
		/*
		//跨域处理
		var sbDonghang = null;
		function domain()
		{	
			window.addEventListener('message', function(e)
			{
			    console.log('foo say: ' + e.data.a);
			    e.source.postMessage({jxw:133323}, '*');
			    sbJXW = e;
			}, false)
		}
		
		window.onload = domain;
		*/
		//清空搜索框
		
		/*
		setTimeout(function()
		{
			console.log(11111);
			sbJXW.source.postMessage({dh:123,jxw1:133325553}, '*');
		},5000);
		*/
		$("#realName").change(function()
		{
			console.log($("#realName").val());	
		});
		
		//条件查询
		function queryUserTable()
		{
			UserDataTable.draw();
		}
		
		//重置密码
		function resetPassword(id)
		{
			$.ajax(
			{
				"type": "POST",
				"url": jQuery.getBasePath() + "/userManage/resetPassword.bsh",
				"dataType": "json",
				data: {"id":id},
				"success": function(data) 
				{
					if(data.flag)
					{
						showSuccessOrErrorModal("重置成功","success"); 
					}else
					{
						showSuccessOrErrorModal("重置失败","error"); 
					}
					
				}, error : function(data) 
				{  
		        	showSuccessOrErrorModal("请求出错了","error"); 
		        }
			});
		}
		
		//根据id删除用户
		function deleteUserById(id)
		{
			$.ajax(
			{
				"type": "POST",
				"url": jQuery.getBasePath() + "/userManage/deleteUserById.bsh",
				"dataType": "json",
				data: {"id":id},
				"success": function(data) 
				{
					if(data.flag)
					{
						showSuccessOrErrorModal("删除成功","success");
						UserDataTable.draw();
					}else
					{
						showSuccessOrErrorModal("删除失败","error");
					}
				}, error : function(data) 
				{  
		        	showSuccessOrErrorModal("请求出错了","error");
		        }
			});
		}
		
		//更改用户状态
		function changeStatusById(id,status)
		{
			$.ajax(
			{
				"type": "POST",
				"url": jQuery.getBasePath() + "/userManage/changeStatusById.bsh",
				"dataType": "json",
				data: {"id":id,"status":status},
				"success": function(data) 
				{
					if(data.flag)
					{
						showSuccessOrErrorModal("更改成功","success");
						UserDataTable.draw();
					}else
					{
						showSuccessOrErrorModal("更改失败","error");
					}
				}, error : function(data) {  
	        		showSuccessOrErrorModal("请求出错了","error"); 
	       		}
			});
		}
		
		//列表子勾选框处理
		function chooseOrNoChoose(obj)
		{
			if($(obj).is(":checked"))
			{
				$(".testCheckbox").prop("checked", true);
				$(".testCheckbox").each(function()
			    {
			      var content=$(this).val();
			      usersId[content]=content;
			      console.log(content);
			    });
			}
			else
			{
				$(".testCheckbox").prop("checked",false);
				usersId={};
			}
		}
		
		//展示实名审核信息
		function apprUserModel(front,back,id)
		{
			if(front!="null")
			{	
				$("#appr_front").attr('src',"../image/user/"+front);
			}else
			{
				$("#appr_front").attr('src',"../image/user/default.jpg");
			}
			if(back!="null")
			{
				$("#appr_back").attr('src',"../image/user/"+back);
			}else
			{
				$("#appr_front").attr('src',"../image/user/default.jpg");
			}	
			$("#apprId").val(id);
		}
		
		//实名审核
		function apprUser()
		{
			var form = new FormData(document.getElementById("apprForm"));
			$.ajax(
			{
                url:jQuery.getBasePath() + "/userManage/apprUserVerify.bsh",
                type:"post",
                data:form,
                dataType:"json",
                processData:false,
                contentType:false,
                success:function(data){
                	if(data.flag)
                	{
	                	$("#apprModal").modal("hide");
	                    UserDataTable.draw();
	                    showSuccessOrErrorModal("认证成功","success");
                    }else
                    {
                    	showSuccessOrErrorModal("认证出错了","error");
                    }
                },
                error:function(e){
                    showSuccessOrErrorModal("请求出错了","error"); 
                }
            });
		}
		
		//新增用户
		function addUser()
		{
			var form = new FormData(document.getElementById("editForm"));
			$.ajax(
			{
                url:jQuery.getBasePath() + "/userManage/addUser.bsh",
                type:"post",
                data:form,
                dataType:"json",
                processData:false,
                contentType:false,
                success:function(data)
                {
                
                	if(data.flag)
                	{
	                	showSuccessOrErrorModal("新增用户成功","success"); 
	                    $("#editModal").modal("hide");  
	                    UserDataTable.draw();
	                    changeStatus(0);
	               }else
	               {
	               		showSuccessOrErrorModal("新增出错了","error");	
	               }         
                },
                error:function(e)
                {
                    showSuccessOrErrorModal("请求出错了","error"); 
                }
            });
		}
		
		//查看用户-展示用户信息
        function showUserInfo(param) 
        {
          //发送Ajax
          $.ajax(
          {
            type : "post",
            dataType : "json",
            url : jQuery.getBasePath()
                + '/userManage/getUserById.bsh',
            data : {
              "id" : param
            },
            success : function(data) 
            {
              if (data.flag) 
              {
                var userDefault = "暂无数据";
                if (data.user.user_name == null
                    || data.user.user_name == 'null'
                    || data.user.user_name == '') 
                {
                  data.user.user_name = userDefault;
                }
                if (data.user.site_name == null
                    || data.user.site_name == 'null'
                    || data.user.site_name == '') 
                {
                  data.user.site_name = userDefault;
                }
                if (data.user.user_nick == null
                    || data.user.user_nick == 'null'
                    || data.user.user_nick == '') 
                {
                  data.user.user_nick = userDefault;
                }
                if (data.user.city == null
                    || data.user.city == 'null'
                    || data.user.city == '') 
                {
                  data.user.city = userDefault;
                }
                if (data.user.area == null
                    || data.user.area == 'null'
                    || data.user.area == '') 
                {
                  data.user.area = userDefault;
                }
                if (data.user.verify_status == null
                    || data.user.verify_status == 'null'
                    || data.user.verify_status == '') 
                {
                  data.user.verify_status = userDefault;
                }
                if (data.user.real_name == null
                    || data.user.real_name == 'null'
                    || data.user.real_name == '') 
                {
                  data.user.real_name = userDefault;
                }
                if (data.user.user_sex == null
                    || data.user_sex == 'null'
                    || data.user_sex == '') 
                {
                  data.user.user_sex = userDefault;
                }
                if (data.user.id_card == null
                    || data.user.id_card == 'null'
                    || data.user.id_card == '') 
                {
                  data.user.id_card = userDefault;
                }
                if (data.user.user_phone == null
                    || data.user.user_phone == 'null'
                    || data.user.user_phone == '') 
                {
                  data.user.user_phone = userDefault;
                }
                if (data.user.job_address == null
                    || data.user.job_address == 'null'
                    || data.user.job_address == '') 
                {
                  data.user.job_address = userDefault;
                }
                if (data.user.user_email == null
                    || data.user.user_email == 'null'
                    || data.user.user_email == '') 
                {
                  data.user.user_email = userDefault;
                }
                if (data.user.eduDegree == null
                    || data.user.eduDegree == 'null'
                    || data.user.eduDegree == '') 
                {
                  data.user.eduDegree = userDefault;
                }
                if (data.user.graduation_college == null
                    || data.user.graduation_college == 'null'
                    || data.user.graduation_college == '') 
                {
                  data.user.graduation_college = userDefault;
                }
                $('#modal_userinfo_user_name').text(
                    "用户名：" + data.user.user_name);
                $('#modal_userinfo_site').text(
                    "所属站点：" + data.user.site_name);
                $('#modal_userinfo_nick').text(
                    "昵称：" + data.user.user_nick);
                $('#modal_userinfo_address').text(
                    "所属地区：" + data.user.city + " "
                        + data.user.area);
                if(data.user.verify_status=="1")
                {
              	  	$('#modal_userinfo_verify').text(
                    "实名状态：已认证");
                }else if(data.user.verify_status=="2")
                {
              	   	$('#modal_userinfo_verify').text(
                    "实名状态：未认证");
                }else if(data.user.verify_status=="3")
                {
                	$('#modal_userinfo_verify').text(
                    "实名状态：待认证");
                }else if(data.user.verify_status=="4")
                {
               	  	$('#modal_userinfo_verify').text(
                    "实名状态：以驳回");
                }
                $('#modal_userinfo_real_name').text(
                    "真实姓名：" + data.user.real_name);
                if(data.user.user_sex=="1")
                {    
                	$('#modal_userinfo_sex').text("性别：男");
               	}else{
	               	$('#modal_userinfo_sex').text("性别：女");
               	}	     
                $('#modal_userinfo_card').text(
                    "身份证号：" + data.user.id_card);
                $('#modal_userinfo_phone').text(
                    "手机号：" + data.user.user_phone);
                $('#modal_userinfo_job_address').text(
                    "工作单位：" + data.user.job_address);
                $('#modal_userinfo_email').text(
                    "邮箱：" + data.user.user_email);
                if(data.user.eduDegree=="1")
                {
                	$('#modal_userinfo_edu').text(
                    "学历：其他");
                }else if(data.user.eduDegree=="2")
                {
                	$('#modal_userinfo_edu').text(
                    "学历：小学");
                }else if(data.user.eduDegree=="3")
                {
                	$('#modal_userinfo_edu').text(
                    "学历：初中");
                }else if(data.user.eduDegree=="4")
                {
                	$('#modal_userinfo_edu').text(
                    "学历：高中/中专");
                }else if(data.user.eduDegree=="5")
                {
                	$('#modal_userinfo_edu').text(
                    "学历：大专/高职");
                }else if(data.user.eduDegree=="6")
                {
                	$('#modal_userinfo_edu').text(
                    "学历：大学本科");
                }else if(data.user.eduDegree=="7")
                {
                	$('#modal_userinfo_edu').text(
                    "学历：硕士");
                }else if(data.user.eduDegree=="8")
                {
                	$('#modal_userinfo_edu').text(
                    "学历：博士");
                }else
                {
                	$('#modal_userinfo_edu').text(
                	"学历："+data.user.eduDegree);
                }
                $('#modal_userinfo_college').text(
                    "大学院校：" + data.user.graduation_college);
              }
            }
          });
          return false;
        }
        
        //修改用户-展示用户信息
        function showEditUserInfo(id) 
        {
			  //发送Ajax
			  $.ajax(
			  {
			    type : "post",
			    dataType : "json",
			    url : jQuery.getBasePath()
			        + '/userManage/getUserById.bsh',
			    data : 
			    {
			      "id" : id
			    },
			    success : function(data) 
			    {
			      if (data.flag) 
			      {
			        var userDefault = "";
			        if (data.user.user_name == null
			            || data.user.user_name == 'null'
			            || data.user.user_name == '') 
			        {
			          data.user.user_name = userDefault;
			        }
			        if (data.user.site_name == null
			            || data.user.site_name == 'null'
			            || data.user.site_name == '') 
			        {
			          data.user.site_name = userDefault;
			        }
			        if (data.user.user_nick == null
			            || data.user.user_nick == 'null'
			            || data.user.user_nick == '') 
			        {
			          data.user.user_nick = userDefault;
			        }
			        if (data.user.addr_city == null
			            || data.user.addr_city == 'null'
			            || data.user.addr_city == '') 
			        {
			          data.user.addr_city = userDefault;
			        }
			        if (data.user.addr_area == null
			            || data.user.addr_area == 'null'
			            || data.user.addr_area == '') 
			        {
			          data.user.addr_area = userDefault;
			        }
			        if (data.user.verify_status == null
			            || data.user.verify_status == 'null'
			            || data.user.verify_status == '') 
			        {
			          data.user.verify_status = userDefault;
			        }
			        if (data.user.real_name == null
			            || data.user.real_name == 'null'
			            || data.user.real_name == '') 
			        {
			          data.user.real_name = userDefault;
			        }
			        if (data.user.user_sex == null
			            || data.user.user_sex == 'null'
			            || data.user.user_sex == '') 
			        {
			          data.user.user_sex = userDefault;
			        }
			        if (data.user.id_card == null
			            || data.user.id_card == 'null'
			            || data.user.id_card == '') 
			        {
			          data.user.id_card = userDefault;
			        }
			        if (data.user.user_phone == null
			            || data.user.user_phone == 'null'
			            || data.user.user_phone == '') 
			        {
			          data.user.user_phone = userDefault;
			        }
			        if (data.user.job_address == null
			            || data.user.job_address == 'null'
			            || data.user.job_address == '') 
			        {
			          data.user.job_address = userDefault;
			        }
			        if (data.user.user_email == null
			            || data.user.user_email == 'null'
			            || data.user.user_email == '') 
			        {
			          data.user.user_email = userDefault;
			        }
			        if (data.user.eduDegree == null
			            || data.user.eduDegree == 'null'
			            || data.user.eduDegree == '') 
			        {
			          data.user.eduDegree = userDefault;
			        }
			        if (data.user.graduation_college == null
			            || data.user.graduation_college == 'null'
			            || data.user.graduation_college == '') 
			        {
			          data.user.graduation_college = userDefault;
			        }
			        if(data.user.verify_img_front_url ==null || data.user.verify_img_front_url == 'null'
			            || data.user.verify_img_front_url == '')
			        {
			        	$("#preview_front").attr('src',"../image/user/default.jpg");
			        }else
			        {
			        	$("#preview_front").attr('src',"../image/user/"+data.user.verify_img_front_url);
			        }
			        if(data.user.verify_img_back_url ==null || data.user.verify_img_back_url == 'null'
			            || data.user.verify_img_back_url == '')
			        {
			        	$("#preview_back").attr('src',"../image/user/default.jpg");
			        }else
			        {
			        	$("#preview_back").attr('src',"../image/user/"+data.user.verify_img_back_url);
			        }
			        $('#editUserId').val(data.user.id);
			        $('#editUserName').val(data.user.user_name);
			        document.getElementById("editSiteName").innerText=data.user.site_name;
			        $('#editSiteId').val(data.user.site_id);
			        $('#editUserNick').val(data.user.user_nick);
			        $('#editAddrCity').val(data.user.addr_city);
			        queryArea(data.user.addr_area);	
			       
			        $('#editVerifyStatus').val(data.user.verify_status);
			        if(data.user.verify_status=='1')
			        {
			        	changeStatus(0);
			        }else
			        {
			        	changeStatus(1);
			        }
			        $('#editRealName').val(data.user.real_name);  
			    	$("input[name='sex']").each(function(index, element) 
			    	{
			            if($(this).val()==data.user.user_sex)
			            {
							$(this).prop("checked",true);
						}
			        });
			        $('#editIdCard').val(data.user.id_card);
			        $('#editPhone').val(data.user.user_phone);
			        $('#editJobAddr').val(data.user.job_address);
			        $('#editEmail').val(data.user.user_email);
			   		$('#editEduDegree').val(data.user.eduDegree);
			        $('#editGraduation').val(data.user.graduation_college);
			        onlyUserName='1';
			        onlyUserPhone='1';
			        onlyUserEmail='1';
			        onlyUserIdCard='1';
			      }
			    }
			  });
          return false;
        }
        
        //修改用户
		function editUser()
		{
			var form = new FormData(document.getElementById("editForm"));
			$.ajax(
			{
                url:jQuery.getBasePath() + "/userManage/eidtUser.bsh",
                type:"post",
                data:form,
                processData:false,
                dataType : "json",
                contentType:false,
                success:function(data)
                {
                	if(data.flag)
                	{
               	 		showSuccessOrErrorModal("修改成功","success"); 
	                    UserDataTable.draw();
	                    $("#editModal").modal("hide");
	                    changeStatus(0);                    
                    }else
                    {
                    	showSuccessOrErrorModal("修改失败","error"); 
                    }
                },
                error:function(e)
                {
                    showSuccessOrErrorModal("请求出错了","error"); 
                }
            });
		}
		
		//实时记录状态
        function recordRoleId(obj) 
        {
          if ($(obj).is(":checked") == true) 
          {
            usersId[obj.value] = obj.value;
          } else {
            usersId[obj.value] = "";
          }
        }
        
        //批量更改用户状态
        function changeUsersStatus(status)
        {	
        	for(var i in usersId)
        	{
        		if(usersId[i]=="")
        		{
        			delete usersId[i];
    			}
        	}	
        	var list = JSON.stringify(usersId);
        	if(list.length>2)
        	{
	        	$.ajax({
	            type : "post",
	            dataType : "json",
	            url : jQuery.getBasePath() + "/userManage/changeUsersStatus.bsh",
	            data : {
	            	"ids" : JSON.stringify(usersId),
	            	"status":status
	            },
	            success : function(data) 
	            {
	            	if(data.flag)
	            	{
		          	 	showSuccessOrErrorModal("批量修改成功","success"); 
		                UserDataTable.draw();
		                usersId={};
	                }else
	                {
	                	showSuccessOrErrorModal("批量修改失败","error"); 
	                }
	            }, error : function(data) 
	            {  
		        	showSuccessOrErrorModal("请求出错了","error");  
		        }
	         	});
          	}else
          	{
          		showInfoModal("请选择用户");
          	}
        }
        
        //初始化管辖站点的下拉框
        function querySelect() 
        {
          $.ajax(
          {
            type : "post",
            dataType : "json",
            url : jQuery.getBasePath() + "/userManage/getSelectList.bsh",
            data : {
            },
            success : function(data)
            {
              if (data.flag) 
              {
                $.each(data.list, function(index, value) 
                {
                  $("select[name='querySelect']").append(
                      "<option value='"+value.site_id+"'>"
                          + value.site_name
                          + "</option>");
                });
              }else
              {
              	showSuccessOrErrorModal("请求管辖范围出错了","error"); 
              }
            },error : function(data) 
            {  
	           showSuccessOrErrorModal("请求出错了","error"); 
	        }
          });
        }
        
        //初始城市的下拉框
        function queryCity() 
        {
          $.ajax(
          {
            type : "post",
            dataType : "json",
            url : jQuery.getBasePath() + "/userManage/getCity.bsh",
            data : {},
            success : function(data) 
            {
                if (data.flag) 
                {
               	 	$.each(data.list, function(index, value) 
               	 	{
                  	$("select[name='addrCity']").append(
                      "<option value='"+value.id+"'>"
                          + value.area_name
               	           + "</option>");
               		})
				}else
				{
					showSuccessOrErrorModal("查询城市出错了","error");
				}
            },error : function(data) 
            {  
		           showSuccessOrErrorModal("请求出错了","error"); 
	        }
          });
        }
        
        //初始区/县的下拉框
        function queryArea(edit) 
        {	
    		var city = $('#editAddrCity option:selected').val();
    		$.ajax({
	            type : "post",
	            dataType : "json",
	            url : jQuery.getBasePath() + "/userManage/getArea.bsh",
	            data : {"city":city},
	            success : function(data) 
	            {
	              if (data.flag) 
	              {
	              	$("#editAddrArea").empty();
	              	$("#editAddrArea").append(
	                    "<option value=''>请选择</option>");
	                $.each(data.list, function(index, value) 
	                {
	                  $("#editAddrArea").append(
	                      "<option value='"+value.id+"'>"
	                          + value.area_name
	                          + "</option>");
	                })
	              }else
	              {
	              	showSuccessOrErrorModal("查询城市出错了","error");
	              }
	              if(edit!="0")
	              {
	              	 $('#editAddrArea').val(edit);
	              }
	            },  
		        error : function(data) 
		        {  
		            showSuccessOrErrorModal("请求出错了","error"); 
		        }
       		});
        }
		
		//用户名唯一验证
		$("#editUserName").blur(function()
		{
        	var userName=$("#editUserName").val();
        	var id = $("#editUserId").val();
        	if($.trim(userName)==""&&userName!="")
        	{
        		$("#checkEditUserName").text("请填写用户名");
              	$("#checkEditUserName").css('display','inline');
              	$("#checkEditUserName").css('color','red');
              	onlyUserName='0';
              	return;
        	}
        	else
        	{
				$.ajax(
				{
			            type : "post",
			            dataType : "json",
			            url : jQuery.getBasePath() + "/userManage/checkUser.bsh",
			            data : {"userName":userName,"id":id},
			            success : function(data)
			             {
			              if (data.flag) 
			              {
				              if(data.isUsing)
				              {	
				              	$("#checkEditUserName").text("用户名可用");
				              	$("#checkEditUserName").css('display','inline');
				              	$("#checkEditUserName").css('color','grey');
				              	onlyUserName='1';
				              }else
				              {
				              	$("#checkEditUserName").text("用户名已存在");
				              	$("#checkEditUserName").css('display','inline');
				              	$("#checkEditUserName").css('color','red');
				              	onlyUserName='0';
				              }
				          }else
				          {
				          	  showSuccessOrErrorModal("检查用户名出错了","error");
				          }   
			            },  
				        error : function(data) {  
				            showSuccessOrErrorModal("请求出错了","error");  
				        }
		       		});
       		}
		});
		
		//用户手机号唯一验证
		$("#editPhone").blur(function()
		{
        	var phone=$("#editPhone").val();
        	var id=$("#editUserId").val();
        	if (!phone.match(/^(((13[0-9]{1})|15[0-9]{1}|18[1-9]{1})+\d{8})$/)&&phone != "") 
        	{
				$("#checkEditUserPhone").text("请填写正确的手机号");
              	$("#checkEditUserPhone").css('display','inline');
              	$("#checkEditUserPhone").css('color','red');
              	onlyUserPhone='0';
				return;
			}else
			{
				$("#checkEditUserPhone").css('display','none');
			}
        	if(phone != "")
        	{
				$.ajax(
				{
			            type : "post",
			            dataType : "json",
			            url : jQuery.getBasePath() + "/userManage/checkUser.bsh",
			            data : {"phone":phone,"id":id},
			            success : function(data) 
			            {
			              if (data.flag)
			              { 
				              if(data.isUsing)
				              {	
				              	$("#checkEditUserPhone").text("手机号可用");
				              	$("#checkEditUserPhone").css('display','inline');
				              	$("#checkEditUserPhone").css('color','grey');
				              	onlyUserPhone='1';
				              }else
				              {
				              	$("#checkEditUserPhone").text("手机号已注册");
				              	$("#checkEditUserPhone").css('display','inline');
				              	$("#checkEditUserPhone").css('color','red');
				              	onlyUserPhone='0';
				              }
				          }else
				          {
				          	  showSuccessOrErrorModal("检查手机号出错了","error");	
				          }    
			            },  
				        error : function(data) 
				        {  
				            showSuccessOrErrorModal("请求出错了","error"); 
				        }
		       		});
       		}
		});
		
		//用户邮箱唯一验证
		$("#editEmail").blur(function()
		{
        	var email=$("#editEmail").val();
        	var id=$("#editUserId").val();
        	if(email !="")
        	{
				$.ajax(
				{
			            type : "post",
			            dataType : "json",
			            url : jQuery.getBasePath() + "/userManage/checkUser.bsh",
			            data : {"email":email,"id":id},
			            success : function(data) 
			            {
			              if (data.flag)
			              { 
				              if(data.isUsing)
				              {	
				              	$("#checkEditUserEmail").text("邮箱可用");
				              	$("#checkEditUserEmail").css('display','inline');
				              	onlyUserEmail='1';
				              }else
				              {
				              	$("#checkEditUserEmail").text("邮箱已注册");
				              	$("#checkEditUserEmail").css('display','inline');
				              	onlyUserEmail='0';
				              }
				          }else
				          {
				          	  showSuccessOrErrorModal("检查邮箱出错了","error"); 
				          }   
			            },  
				        error : function(data) 
				        {  
				            showSuccessOrErrorModal("请求出错了","error"); 
				        }
		       		});
       		}
		});	
		
		//身份证唯一验证
		$("#editIdCard").blur(function(){
			var idCard=$("#editIdCard").val();
			var id=$("#editUserId").val();
			// 身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X  
		  	var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;  
	  		if(reg.test(idCard) === false && $.trim(idCard) != "")  
		   	{  
		    	$("#checkEditIdCard").text("请填写正确的身份证号");
              	$("#checkEditIdCard").css('display','inline');
              	$("#checkEditIdCard").css('color','red');
              	onlyUserIdCard='0';
		    	return ;  
		   	}else
		   	{
		   		$("#checkEditIdCard").css('display','none');
		   	}
		   	if(idCard !="")
        	{
				$.ajax({
			            type : "post",
			            dataType : "json",
			            url : jQuery.getBasePath() + "/userManage/checkUser.bsh",
			            data : {"idCard":idCard,"id":id},
			            success : function(data) 
			            {
			              if (data.flag)
			              { 
				              if(data.isUsing)
				              {	
				              	$("#checkEditIdCard").text("身份证可用");
				              	$("#checkEditIdCard").css('display','inline');
				              	$("#checkEditIdCard").css('color','grey');
				              	onlyUserIdCard='1';
				              }else
				              {
				              	$("#checkEditIdCard").text("身份证已被使用");
				              	$("#checkEditIdCard").css('display','inline');
				              	$("#checkEditIdCard").css('color','red');
				              	onlyUserIdCard='0';
				              }
				          }else
				          {
				          	  showSuccessOrErrorModal("检查身份证号码出错了","error"); 
				          }   
			            },  
				        error : function(data) 
				        {  
				            showSuccessOrErrorModal("请求出错了","error"); 
				        }
		       		});
       		}  
		});
		//下载模板
		function downloadTemplate()
		{
			top.location.href = encodeURI(jQuery.getBasePath() + "/userManage/downloadtemplate.bsh?fileName=用户模板.xls")
		}
		//批量导入
		function uploadUser()
		{	
			var form = new FormData(document.getElementById("importForm"));
			if(document.getElementById("importForm").file.value)
			{
				$.ajax(
				{
	                url:jQuery.getBasePath() + "/userManage/importUserInfo.bsh",
	                type:"post",
	                dataType : "json",
	                data:form,
	                processData:false,
	                contentType:false,
	                success:function(data)
	                {
	                	if(data.msg=='')
	                	{
		                	showSuccessOrErrorModal("批量用户成功","success"); 
		                    UserDataTable.draw();
		                    $("#importModal").modal("hide");
		                    document.getElementById('importFile').innerHTML="<div class='fileinput fileinput-new' data-provides='fileinput'>"
	                                          +  "<span class='btn green btn-file'>"
	                                          +     " <span class='fileinput-new'> 上传 </span>"
	                                          +     " <span class='fileinput-exists'> 更换 </span>"
	                                          +     " <input type='file' name='file' accept='application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' class='required'> </span>"
	                                          +  "<span class='fileinput-filename'> </span> &nbsp;"
	                                          +  "<a href='javascript:;' class='close fileinput-exists' data-dismiss='fileinput'> </a>"
	                                          +  "</div>";
	                    }else
	                    {
	                    	showSuccessOrErrorModal(data.msg,"error"); 
	                    }
	                },
	                error:function(e){
	                    showSuccessOrErrorModal("请求出错了","error"); 
	                }
	            });
            }else
            {
            	showInfoModal("请选择文件");
            }
		}
		
		//新增/修改模态框展示 若有id则为修改用户/若id=0 则为新增框
		function showModal(id)
		{
			if(id=="0")
			{
				$(".input-group label").hide();
				document.getElementById("editSiteName").innerText="";
                resetForm();
                changeStatus(0);
                document.getElementById("editForm").reset();      			
				$("#editIdentity").css('display','none');
				$("#addIdentity").css('display','inline');
				$("#editModal").modal("show");
			}else
			{
				$(".input-group label").hide();
				$("#editIdentity").css('display','inline');
				$("#addIdentity").css('display','none');
				resetForm();
				showEditUserInfo(id);
				$("#editModal").modal("show");
			}
		}
		
		//更改必填选项
		function changeStatus(i)
		{	
			$(".input-group label").hide();
			if(i=='0')
			{
				document.getElementById("editRealNameRequired").innerText="*";
        		//document.getElementById("editPicRequired").innerText="*";
        		document.getElementById("editIdCardRequired").innerText="*";
        		document.getElementById("editPhoneRequired").innerText="*";
        		$("#editRealName").addClass("required");
        		//$("#editPicRequired").addClass("required");
        		$("#editIdCard").addClass("required");
        		$("#editPhone").addClass("required");
			}else
			{
				document.getElementById("editRealNameRequired").innerText="";
        		$("#editRealName").removeClass("required");
        		//document.getElementById("editPicRequired").innerText="";
        		//$("#editPicRequired").removeClass("required");
        		document.getElementById("editIdCardRequired").innerText="";
        		$("#editIdCard").removeClass("required");
        		document.getElementById("editPhoneRequired").innerText="";
        		$("#editPhone").removeClass("required");
			}
		}
		//编辑用户-根据用户认证状态更改必填项
		function changeVerifyStatus()
		{
			if($("#editVerifyStatus").val()=='1')
			{
				changeStatus(0);
			}else
			{
				changeStatus(1);
			}
		}
		
		//查询用户按钮权限
		function queryUserRole()
		{
			$.ajax(
			{
            type : "post",
            dataType : "json",
            url : jQuery.getBasePath() + "/navigation/getMenuPower.bsh",
            data : {resourceId:window.top.currentMenuId},
            success : function(data) 
            {
            	$.each(data.resourceMenus,function(index,value)
            	{
		          roles.push(value.menuCode);
		        });
		          if(roles.indexOf('USERMANAGE_STATUS')>-1)
				{
					$(".statusBtn").removeAttr("disabled");
				}
				if(roles.indexOf('USERMANAGE_EDIT')>-1)
				{
					$(".editBtn").removeAttr("disabled");
				}
				if(roles.indexOf('USERMANAGE_RESETCODE')>-1)
				{
					$(".resetBtn").removeAttr("disabled");
				}
				if(roles.indexOf('USERMANAGE_DELETE')>-1)
				{
					$(".deleteBtn").removeAttr("disabled");
				}
				
				if(roles.indexOf('USERMANAGE_APPR')>-1)
            	{
            		$(".apprBtn2").hide();
            	}else
            	{
            		$(".apprBtn1").hide();
            	}
            	if(roles.indexOf('USERMANAGE_STATUS')>-1){
					$(".dataTables_length").after("<button class='btn green' style='float:left;margin-left:15px;' onclick='changeUsersStatus(0)'>批量无效</button>");
					$(".dataTables_length").after("<button class='btn green' style='float:left;margin-left:15px;' onclick='changeUsersStatus(1)'>批量有效</button>");
				}
				if(roles.indexOf('USERMANAGE_IMPORT')>-1){
					$(".dataTables_length").after("<button class='btn green' style='float:left;margin-left:15px;' data-toggle='modal' data-target='#importModal'>导入</button>");
				}
				if(roles.indexOf('USERMANAGE_ADD')>-1)
				{
					$(".dataTables_length").after("<button id='menuAdd' class='btn green' style='float:left;margin-left:15px;' onclick='showModal(0)'>新增</button>");
				}
            },error : function(data) 
            {  
	           showSuccessOrErrorModal("请求出错了","error"); 
	        }
          });
		}
		
		//重置表单
		function resetForm()
		{
			$("#checkEditUserName").css('display','none');
            $("#checkEditUserEmail").css('display','none');
            $("#checkEditUserPhone").css('display','none');
            $("#checkEditIdCard").css('display','none');
			document.getElementById('editFront').innerHTML=" <div class='fileinput fileinput-new' data-provides='fileinput'>"
                      +  "<div class='fileinput-new thumbnail' style='width: 200px; height: 150px;'>"
                      +     "<img src='../image/user/default.jpg' alt=' ' id='preview_front'/> </div>"
                      +     "<div class='fileinput-preview fileinput-exists thumbnail' style='max-width: 200px; max-height: 150px;' > </div>"
                      +		"<div>"
                      +		"<span class='btn default btn-file'>"
                      +		" <span class='fileinput-new'> 选择正面 </span>"
                      +		"<span class='fileinput-exists'> 更换 </span>"
                      +		"<input type='file' name='files' id='editFrontFile' accept='image/png,image/jpeg' > </span>"
                      +  	"<a href='javascript:;' class='btn red fileinput-exists' data-dismiss='fileinput'>移除</a>"
                      +  	"</div>"
                      +		"</div>"
            document.getElementById('editBack').innerHTML=" <div class='fileinput fileinput-new' data-provides='fileinput'>"
                      +  "<div class='fileinput-new thumbnail' style='width: 200px; height: 150px;'>"
                      +     "<img src='../image/user/default.jpg' alt=' ' / id='preview_back'> </div>"
                      +     "<div class='fileinput-preview fileinput-exists thumbnail' style='max-width: 200px; max-height: 150px;'> </div>"
                      +		"<div>"
                      +		"<span class='btn default btn-file'>"
                      +		" <span class='fileinput-new'> 选择正面 </span>"
                      +		"<span class='fileinput-exists'> 更换 </span>"
                      +		"<input type='file' name='files' id='editBcakFile' accept='image/png,image/jpeg' > </span>"
                      +  	"<a href='javascript:;' class='btn red fileinput-exists' data-dismiss='fileinput'>移除</a>"
                      +  	"</div>"
                      +		"</div>"
                      
          	onlyUserName='1';
	        onlyUserPhone='1';
	        onlyUserEmail='1';
	        onlyUserIdCard='1';
		}
		</script>
		
    </body>

</html>	