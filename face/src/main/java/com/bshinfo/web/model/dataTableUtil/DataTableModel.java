package com.bshinfo.web.model.dataTableUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * datatable返回数据的实体类<br/>
 * 
 * 功能描述: 封装后台查询出的数据返回给前端datatable表格解析的实体<BR/>
 * 
 * @author 	[jixingwang]
 * 
 * @see 	[DataTableModel]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-2-17 16:20:25]
 */
public class DataTableModel implements Serializable
{
	private static final long serialVersionUID = -4134300147853888287L;
	//页面传递过来的操作次数,必须设置的值
	private int sEcho = 0;
	//总数量
    private int iTotalRecords = 0;
    //table展示的数量
    private int iTotalDisplayRecords = 0;
    //页面数据
    private List<List<Object>> aaData = new ArrayList<List<Object>>();
    private double sum=0;
    
	public DataTableModel() 
	{
		super();
	}
	
	public DataTableModel(int sEcho, int iTotalRecords,
			int iTotalDisplayRecords, List<List<Object>> aaData) 
	{
		super();
		this.sEcho = sEcho;
		this.iTotalRecords = iTotalRecords;
		this.iTotalDisplayRecords = iTotalDisplayRecords;
		this.aaData = aaData;
	}
	
	public int getsEcho() {
		return sEcho;
	}
	public void setsEcho(int sEcho) {
		this.sEcho = sEcho;
	}
	public int getiTotalRecords() {
		return iTotalRecords;
	}
	
	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}
	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}
	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}
	public List<List<Object>> getAaData() {
		return aaData;
	}
	public void setAaData(List<List<Object>> aaData) {
		this.aaData = aaData;
	}
	public double getSum() {
		return sum;
	}
	public void setSum(double sum) {
		this.sum = sum;
	}

}

