package com.bshinfo.web.service.serviceI.role;

import java.util.List;
import java.util.Map;

import com.bshinfo.web.model.dataTableUtil.DataTableModel;

/**
 * role业务逻辑层接口 <br/>
 * 
 * 功能描述: 对role的各种业务处理 <BR/>
 * 
 * @author 	[hezhijian]
 * 
 * @see 	[RoleServiceI]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-2-20 11:03:55]
 */
public interface RoleServiceI 
{
	/***
	 * 查询后台管理的所有角色
	 * 
	 * @Param [java.util.Map<String, String>] dataTableMap 前台传过来的数据
	 * 
	 * @return [com.bshinfo.web.model.dataTableUtil.DataTableModel] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-2-20 11:08:47]
	 */
	DataTableModel roleList(Map<String, String> dataTableMap);

	/***
	 * 查询权限树
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-2-21 14:31:40]
	 */
	List<Map<String, Object>> getPowerTree();

	/***
	 * 新增角色
	 * 
	 * @Param [java.util.Map<String, String>] roleMap 新增角色的数据
	 * 
	 * @return [int] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-2-22 09:57:38]
	 */
	int addRole(Map<String, Object> roleMap);

	/***
	 * 检测角色名称是否唯一
	 * 
	 * @Param [java.util.Map<String, Object>] roleMap 角色信息集合
	 * 
	 * @return [int] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-2-22 09:57:38]
	 */
	int checkName(Map<String, Object> roleMap);

	/***
	 * 根据id查询角色信息
	 * 
	 * @Param [java.lang.String] id 角色id
	 * 
	 * @return [java.util.Map<String, Object>] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-2-22 13:18:40]
	 */
	Map<String, Object> selectRoleById(String id);

	/***
	 * 修改角色
	 * 
	 * @Param [java.util.Map<String, String>] roleMap 修改角色的数据
	 * 
	 * @return [int] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-2-22 15:22:02]
	 */
	int editRole(Map<String, Object> roleMap);

	/***
	 * 根据角色id查询角色被哪些人使用
	 * 
	 * @Param [java.lang.String] roleId 角色id
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-2-22 16:05:24]
	 */
	List<Map<String, Object>> checkRoleById(String roleId);

	/***
	 * 根据角色Id删除角色信息
	 * 
	 * @Param [java.lang.String] roleId 角色Id
	 * 
	 * @return [] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-2-22 16:37:34]
	 */
	void deleteRoleById(List<String> roleIdList);

	/***
	 * 根据角色ids查询哪些角色被使用
	 * 
	 * @Param [java.util.List<String>] roleIds 角色ids集合
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-2-22 17:04:58]
	 */
	List<Map<String, Object>> checkRoleByIds(List<String> roleIdList);

	/***
	 * 根据角色id查询该角色拥有的权限
	 * 
	 * @Param [java.lang.String] roleId 角色id
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-2-23 09:37:15]
	 */
	List<Map<String, Object>> getPowerTreeById(String roleId);

	/***
	 * 根据角色id查询拥有该角色用户的id
	 * 
	 * @Param [java.lang.String] roleId 角色id
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-3-6 09:30:39]
	 */
	List<Map<String, Object>> getUserIdById(String roleId);

	/***
	 * 给权限添加上类型
	 * 
	 * @Param [java.util.List<String>] power 权限集合
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-3-6 09:56:15]
	 */
	List<Map<String, Object>> getPowerType(List<String> power);
}
