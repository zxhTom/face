package com.bshinfo.web.dao.navigation;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.bshinfo.web.model.ResourceMenu;

/**
 * ResourceMenu数据访问层接口 <br/>
 * 
 * 功能描述: 对ResourceMenu的原子性操作 <BR/>
 * 
 * @author 	[jixingwang]
 * 
 * @see 	[com.bshinfo.web.model.ResourceMenu]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-3-1 10:54:38]
 */
public interface NavigationMapper 
{
	/**
	 * 查询一二级菜单以及每个页面按钮
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author 	[jixingwang]
     * 
     * @Date    [2017-3-1 11:19:59]
	 */
	List<Map<String, Object>> getPowerTree();
	
	/**
	 * 根据id查询按钮详情
	 * 
	 * @Param [java.lang.String] menuId 按钮id
	 * 
	 * @return [java.util.Map<String, Object>] 
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-1 15:00:27]
	 */
	Map<String, Object> getMenuInfo(String menuId); 
	
	/**
	 * 新增按钮
	 * 
	 * @Param [com.bshinfo.web.model.ResourceMenu] resourceMenu 按钮类
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-2 09:25:17]
	 */
	void addMenu(ResourceMenu resourceMenu);
	
	/**
	 * 修改按钮
	 * 
	 * @Param [com.bshinfo.web.model.ResourceMenu] resourceMenu 按钮类
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-2 11:11:08]
	 */
	void updateMenu(ResourceMenu resourceMenu);
	
	/**
	 * 根据id删除按钮
	 * 
	 * @Param [java.lang.String] menuId 菜单id
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-2 11:46:17]
	 */
	void delMenu(String menuId);
	
	/**
	 * 检验同一父级下菜单编号是否唯一
	 * 
	 * @Param [java.lang.String] parentId 父级菜单id
	 * 
	 * @Param [java.lang.String] menuCode 菜单编号
	 * 
	 * @Param [java.lang.String] menuId 修改时自身的id
	 * 
	 * @return [java.lang.Integer] 
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-3 09:35:51]
	 */
	Integer checkCodeByParentId(@Param("parentId") String parentId, @Param("menuCode") String menuCode, @Param("menuId") String menuId);
	
	/**
	 * 获取页面按钮权限
	 * 
	 * @Param [java.lang.String] resourceId 二级菜单id
	 * 
	 * @Param [java.lang.String] userId 当前用户id
	 * 
	 * @return [java.util.List<Map<String,Object>>] 
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-3 16:45:27]
	 */
	List<Map<String,Object>> getMenuByPower(@Param("resourceId")String resourceId, @Param("userId")String userId);
}
