package com.bshinfo.web.model.logUtil;



/**
 * 系统日志实体类<br/>
 * 
 * 功能描述: 对系统用户操作、异常日志实体<BR/>
 * 
 * @author 	[zhangChao]
 * 
 * @see 	[SystemLog]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2016-12-16 15:21:51]
 */
public class SystemLog implements java.io.Serializable
{
	/**
	 *  java 序列化实现 (实体类必须添加)
	 */
	private static final long serialVersionUID = 2843577813958722219L;
	
	//  用户id 
	private String 	userId; 
	//  日期  
    private String 	createTime;
	//  日志内容  
    private String 	content;
    //	请求方法(包括类名和方法名  Example:ClassName.MethodName)
    private String 	requestMethod;
    //  请求方法描述
    private String  methodDescription;
    //  请求参数数组
    private String  requestParamsArray;
    //	异常序代码
    private String 	excepitonCode;
    // 异常信息
    private String  exceptionDetail;
    //  服务器IP地址
    private String 	ServerIp;
    //  请求用户Ip地址
    private String  requestIp;
    // 日志类型 0-用户操作日志,1-异常日志
    private int logType;
    
    public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getRequestMethod() {
		return requestMethod;
	}
	public void setRequestMethod(String requestMethod) {
		this.requestMethod = requestMethod;
	}
	public String getMethodDescription() {
		return methodDescription;
	}
	public void setMethodDescription(String methodDescription) {
		this.methodDescription = methodDescription;
	}
	public String getRequestParamsArray() {
		return requestParamsArray;
	}
	public void setRequestParamsArray(String requestParamsArray) {
		this.requestParamsArray = requestParamsArray;
	}
	public String getExcepitonCode() {
		return excepitonCode;
	}
	public void setExcepitonCode(String excepitonCode) {
		this.excepitonCode = excepitonCode;
	}
	public String getExceptionDetail() {
		return exceptionDetail;
	}
	public void setExceptionDetail(String exceptionDetail) {
		this.exceptionDetail = exceptionDetail;
	}
	public String getServerIp() {
		return ServerIp;
	}
	public void setServerIp(String serverIp) {
		ServerIp = serverIp;
	}
	public String getRequestIp() {
		return requestIp;
	}
	public void setRequestIp(String requestIp) {
		this.requestIp = requestIp;
	}
	public int getLogType() {
		return logType;
	}
	public void setLogType(int logType) {
		this.logType = logType;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
