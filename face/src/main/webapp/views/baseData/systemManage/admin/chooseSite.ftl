<#assign base=request.contextPath />
<!DOCTYPE HTML>
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

		<link href="${base}/js/library/metronic-4.7/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
		<link href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/sweetalert/css/sweetalert.css" rel="stylesheet" type="text/css" />
	</head>

	<body style="background-color:#FFFFFF; " id="manager" >
		<div class="container1" style="height:350px; overflow-y:scroll;overflow-x:hidden" >
			<div class="row" style="margin: 20px;">
				<form class="form form-horizontal" >
					<div class="form-group form-inline">
						<div class="col-md-5 col-sm-5 col-xs-5 ">
							<label>站点名称 </label>
							<input type="text" style="width: 50%;" id="site_name" name="name" class="form-control" />
						</div>
						<div class="col-md-5 col-sm-5 col-xs-5">
							<label> 站点类型 </label>
							<select id="site_type" style="width: 50%;" class="form-control">
								<option value="">请选择...</option>
								<option value="1">总站</option>
								<option value="2">子站</option>
								<option value="3">联盟网站</option>
							</select>
						</div>
					</div>
					<div class="form-group form-inline">
						<div style="margin-right:30px; float: right;">
							<div class="col-md-4">
								<button type="button" class="btn green" onclick="doSearch()">查询</button>
								<button type="button" class="btn green" onclick="clearSearch()">清空</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="row" style="margin: 20px;">
				<table class="table table-striped table-hover table-bordered" id="ChooseSiteTable">
				</table>
			</div>
			<div class="row modal-footer" >
				<button type="button" class='btn green'  id='select'>选择</button>
			</div>
		</div>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/moment.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-CN.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/scripts/datatable.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jstree/dist/jstree.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/scripts/app.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/ui-tree.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/sweetalert/js/sweetalert.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/sweetalert/js/showAlert.js" type="text/javascript"></script>
		<script src="${base}/js/common/util/jqueryUtil.js" type="text/javascript"></script>
		<script type="text/javascript">
			var siteDataTable = null;
			var siteIds = [];
			var siteNames = [];

			function initSiteTable() {
				siteDataTable = $('#ChooseSiteTable').DataTable({
					"bSort": true,
					"bProcessing": true,
					"bServerSide": true,
					"async": false,
					"bStateSave": false,
					"bAutoWidth": false,
					"sAjaxSource": jQuery.getBasePath() + "/adminManage/getSite.bsh",
					"sDom": '<"top"l<"clear">>rt<"bottom"ip<"clear">>',
					"lengthMenu": [10, 20, 100],
					"pageLength": 10,
					"oLanguage": {
						"sProcessing": "正在加载数据...",
						"sSearch": "搜索:",
						"sLengthMenu": "_MENU_条记录",
						"sZeroRecords": "没有查到记录",
						"sInfo": "第  _START_ 条到第  _END_ 条记录,一共  _TOTAL_ 条记录",
						"sInfoEmpty": "0条记录",
						"oPaginate": {
							"sPrevious": "上一页 ",
							"sNext": " 下一页",
						}
					},
					"aoColumnDefs": [{
						"bSearchable": true,
						"bVisible": false,
						"aTargets": [0]
					}],
					"aoColumns": [{
							"sTitle": "id",
							"bSortable": false
						},
						{
							"sTitle": '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" id="checkAll" onclick="siteChooseOrNoChoose(this)" /><span></span></label>',
							"bSortable": false,
							"sClass": "text-center",
							"width": "178px",
							"fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
								var value = oData[0];
								var content = '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" class="testCheckbox" name="' + oData[2] + '" value="' + value + '" /><span></span></label>';
								$(nTd).html(content);
							}
						}, {
							"sTitle": "站点名称",
							"bSortable": true,
							"sClass": "text-center",
						}, {
							"sTitle": "站点类型",
							"bSortable": true,
							"bAutoWidth": true,
							"sClass": "text-center",
						}
					],
					"fnServerData": function retrieveData(sSource, aoData, fnCallback) {
						aoData.push({ "name": "site_name", "value": $('#site_name').val() });
						aoData.push({ "name": "site_type", "value": $('#site_type').val() });
						$.ajax({
							"type": "POST",
							"url": sSource,
							"dataType": "json",
							"contentType": "application/json",
							"data": JSON.stringify(aoData),
							"success": function(data) {
								fnCallback(data);
							}
						});
					}
				});
			}

			function siteChooseOrNoChoose(obj) {
				if($(obj).is(":checked")) {
					$(".testCheckbox").prop("checked", true);
				} else {
					$(".testCheckbox").prop("checked", false);
				}
			}

			//查询
			function doSearch() {
				siteDataTable.draw();
			}

			//清空查询状态栏
			function clearSearch() {
				$('#site_name').val("");
				$('#site_type').val("");
				siteDataTable.draw();
			}

			$(document).ready(function() {
				initSiteTable();
			});

			//跨域发送信息
			$("#select").click(function() {
				var flag = 0;
				siteIds=[];
				siteNames=[];
				$(".testCheckbox").each(function() {
					if($(this).is(":checked")) {
						siteIds.push($(this).val());
						siteNames.push(this.attributes['name'].nodeValue);
					}
				});
				if(JSON.stringify(siteIds).length == 2) {
					showInfoModal("没有选择任何站点");
					return false;
				} else {
					var success = "success";
					window.parent.postMessage({ siteIds: siteIds.join(","), siteNames: siteNames.join(","), flag: success }, '*');
				}
			});
		</script>
	</body>

</html>