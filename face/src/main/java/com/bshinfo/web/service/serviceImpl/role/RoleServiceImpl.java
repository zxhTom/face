package com.bshinfo.web.service.serviceImpl.role;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.bshinfo.web.base.model.Page;
import com.bshinfo.web.base.util.PaginationUtil;
import com.bshinfo.web.dao.role.RoleMapper;
import com.bshinfo.web.model.dataTableUtil.DataTableModel;
import com.bshinfo.web.service.serviceI.role.RoleServiceI;

/**
 * role业务逻辑层实现类 <br/>
 * 
 * 功能描述: 对role的各种业务处理 <BR/>
 * 
 * @author 	[hezhijian]
 * 
 * @see 	[RoleServiceImpl]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-2-17 16:20:25]
 */
@Service
public class RoleServiceImpl implements RoleServiceI 
{

	@Override
	public DataTableModel roleList(Map<String, String> dataTableMap) 
	{
		Map<String,Object> paramMap = new HashMap<String,Object>();
		
		DataTableModel dataTableModel = new DataTableModel();
		
		List<List<Object>> data=new ArrayList<List<Object>>();
		
		String sEcho = dataTableMap.get("sEcho");
		int start = Integer.parseInt(dataTableMap.get("iDisplayStart"));
		int length = Integer.parseInt(dataTableMap.get("iDisplayLength"));
		int currentPage = start/length + 1;
		
		paramMap.put("roleName", dataTableMap.get("sname").trim());
		paramMap.put("beginDate", dataTableMap.get("beginDate").trim() + ("".equals(dataTableMap.get("beginDate").trim()) || dataTableMap.get("beginDate").trim() == null ? "" : " 00:00:00"));
		paramMap.put("endDate", dataTableMap.get("endDate").trim() + ("".equals(dataTableMap.get("endDate").trim()) || dataTableMap.get("endDate").trim() == null ? "" : " 23:59:59"));
		
		Page<Map<String, Object>> page = PaginationUtil.setPageInfoStart(paramMap,currentPage,length);
		
		List<Map<String, Object>> resList = this.roleMapper.roleList(page);
		
		Integer count = roleMapper.findRoleCount(paramMap);
		
		for (Map<String, Object> map : resList) 
		{
			List<Object> rowList = new ArrayList<Object>();
			rowList.add(map.get("id"));
			rowList.add("");
			rowList.add(map.get("role_name"));
			rowList.add(map.get("role_desc"));
			rowList.add(map.get("user_name"));
			rowList.add(map.get("create_time"));
			rowList.add("");
			data.add(rowList);
		}
		dataTableModel.setiTotalDisplayRecords(count);
		dataTableModel.setiTotalRecords(count);
		dataTableModel.setsEcho(Integer.valueOf(sEcho));
		dataTableModel.setAaData(data);
		
		return dataTableModel;
	}
	
	@Override
	public List<Map<String, Object>> getPowerTree() 
	{
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		
		List<Map<String, Object>> tree = roleMapper.getPowerTree();
		
		if(tree!=null && tree.size() > 0)
		{
			for(Map<String,Object> child : tree)
			{
				if(child.get("children").toString().equals("0"))
				{
					child.put("icon","fa fa-file icon-state-warning icon-lg");
				}
				child.remove("children");
				list.add(child);
			}
		}
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public int addRole(Map<String, Object> roleMap) 
	{
		int i = 0;
		
		i = roleMapper.addRole(roleMap);
		
		//权限List<Map<String, Object>>
		List<Map<String, Object>> powerList = (List<Map<String, Object>>) roleMap.get("powerList");
				
		roleMapper.addRoleAndPower(powerList);
		
		return i;
	}
	

	@Override
	public int checkName(Map<String, Object> roleMap) 
	{
		int i = 0;
		
		i = roleMapper.checkName(roleMap);
		
		return i;
	}
	
	@Override
	public Map<String, Object> selectRoleById(String id) 
	{
		List<Map<String, Object>> roleList = roleMapper.selectRoleById(id);
		
		Map<String, Object> roleMap = new HashMap<String,Object>();
		
		List<String> list = new ArrayList<String>();
		
		for(Map<String, Object> map : roleList)
		{
			roleMap.put("roleId", map.get("id"));
			roleMap.put("roleName", map.get("roleName"));
			roleMap.put("roleDesc", map.get("roleDesc"));
			list.add(map.get("object_id").toString());
		}
		roleMap.put("powerId", list);
		
		return roleMap;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public int editRole(Map<String, Object> roleMap) 
	{
		int i = 0;
		
		i = roleMapper.editRole(roleMap);
		
		List<String> roleIdList = new ArrayList<String>();
		roleIdList.add(roleMap.get("roleId").toString());
		roleMapper.deleteRoleAndPower(roleIdList);
		
		//权限List<Map<String, Object>>
		List<Map<String, Object>> powerList = (List<Map<String, Object>>) roleMap.get("powerList");
		
		roleMapper.addRoleAndPower(powerList);
		return i;
	}
	
	@Override
	public List<Map<String, Object>> checkRoleById(String roleId) 
	{
		List<Map<String, Object>> userList = roleMapper.checkRoleById(roleId);
		return userList;
	}
	
	@Override
	public void deleteRoleById(List<String> roleIdList) 
	{
		//删除角色信息
		roleMapper.deleteRole(roleIdList);
		//删除角色权限关联表
		roleMapper.deleteRoleAndPower(roleIdList);
	}
	
	@Override
	public List<Map<String, Object>> checkRoleByIds(List<String> roleIdList) 
	{
		List<Map<String, Object>> roleList = roleMapper.checkRoleByIds(roleIdList);
		return roleList;
	}
	
	@Override
	public List<Map<String, Object>> getPowerTreeById(String roleId) 
	{
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		
		List<Map<String, Object>> tree = roleMapper.getPowerTreeById(roleId);
		
		if(tree!=null && tree.size() > 0)
		{
			for(Map<String,Object> child : tree)
			{
				if(child.get("children").toString().equals("0"))
				{
					child.put("icon","fa fa-file icon-state-warning icon-lg");
				}
				child.remove("children");
				list.add(child);
			}
		}
		
		return list;
	}
	
	@Override
	public List<Map<String, Object>> getUserIdById(String roleId) {
		List<Map<String, Object>> map = roleMapper.getUserIdById(roleId);
		return map;
	}
	
	@Override
	public List<Map<String, Object>> getPowerType(List<String> power) {
		List<Map<String, Object>> powerList = roleMapper.getPowerType(power);
		return powerList;
	}
	
	@Resource
	private RoleMapper roleMapper;

}
