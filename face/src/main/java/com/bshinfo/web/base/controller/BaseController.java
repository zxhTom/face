package com.bshinfo.web.base.controller;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.bshinfo.web.base.util.DateUtil;
import com.bshinfo.web.base.util.IPAddressUtil;
import com.bshinfo.web.model.logUtil.SystemLog;


@Component
public class BaseController 
{
	
	
	public void exceptionProcessorHandler(HttpServletRequest request,String className,String methodName, String methodDesc, Exception e)
	{
		//*========数据库日志=========*//
    	SystemLog log = new SystemLog();
    	
    	// 请求的IP
        String requestIpAddress = IPAddressUtil.getRemoteIpAddress(request);
        
        // 当前主机的IP地址
        String serverIpAddress = IPAddressUtil.getHostIp(IPAddressUtil.getInetAddress());
    	
    	log.setUserId("zc20161226");
    	log.setCreateTime(DateUtil.getDateString("yyyy-MM-dd HH:mm:ss"));
        log.setContent(e.getMessage());
        log.setExcepitonCode(e.getClass().getName());
        log.setExceptionDetail(getExceptionDetail(e));
        log.setLogType(1);
        log.setMethodDescription(methodDesc);
        log.setRequestMethod((className+ "." + methodName + "()"));
        log.setRequestParamsArray("");
        log.setRequestIp(requestIpAddress);
        log.setServerIp(serverIpAddress);
        
	}
	
	
	private String getExceptionDetail(Exception ex)
	{
		
		String exceptionDetailStr = "";
		
    	StringWriter sw = new StringWriter();
    	
        ex.printStackTrace(new PrintWriter(sw, true));
        
        String [] strs = sw.toString().split("\r\n");
        
        if(strs.length>5)
        {
        	StringBuffer sb = new StringBuffer("");
        	
        	int count = 0;
        	
        	StringBuffer sbOther = new StringBuffer("");
        	
        	for(int i=0;i<strs.length;i++)
        	{
            	 if(i==0)
            	 {
            		 sb.append(strs[i]).append("\r\n");
            	 }
            	 else if(i<=5)
            	 {
            		 if(strs[i].indexOf("com.bshinfo")!=-1)
            		 {
            			 count ++;
            			 sb.append(strs[i]).append("\r\n");
            		 }
            		 else
            		 {
            			 sbOther.append(strs[i]).append("\r\n");
            		 }
            	 }
            	 else
            	 {
            		 if(strs[i].indexOf("com.bshinfo")!=-1)
            		 {
            			 count ++;
            			 sb.append(strs[i]).append("\r\n");
            		 }
            	 }
            }
        	
        	if(count>0)
        	{
        		exceptionDetailStr = sb.toString();
        	}
        	else
        	{
        		exceptionDetailStr = sbOther.toString();
        	}
        }
        else
        {
        	exceptionDetailStr = sw.toString();
        }
        
        // 数据库中异常内容长度 为 2000
        if(exceptionDetailStr.length()>1980)
        {
        	exceptionDetailStr = exceptionDetailStr.substring(0, 1980);
        }
        
        /***
         * 此处需要将字符串中的 $符号进行特殊处理 
         * 
         * replaceAll(regex, replacement)函数，由于第一个参数支持正则表达式，replacement中出现“$”,会按照$1$2的分组模式进行匹配。
		 * 当编译器发现“$”后跟的不是整数的时候，就会抛出"非法的组引用"的异常-Illegal group reference
		 * 
		 * replaceAll 会在sql语句性能的拦截器中使用
         * 
         */
        exceptionDetailStr = java.util.regex.Matcher.quoteReplacement(exceptionDetailStr);
        
    	return exceptionDetailStr;
	}
	
	
}
