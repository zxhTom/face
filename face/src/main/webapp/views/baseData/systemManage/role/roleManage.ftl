<#assign base=request.contextPath />
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		
		<meta content="" name="description" />
		
		<meta content="" name="author" />
		
		<title>JXW|DEMO</title>
		
		<!-- BEGIN GLOBAL MANDATORY STYLES -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
		<!-- END GLOBAL MANDATORY STYLES -->
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
		<!-- END PAGE LEVEL PLUGINS -->
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

		<!-- BEGIN THEME GLOBAL STYLES -->
		<link href="${base}/js/library/metronic-4.7/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
		<!-- END THEME GLOBAL STYLES -->
		<!-- BEGIN THEME LAYOUT STYLES -->
		<link href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
		<link href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
		<!-- END THEME LAYOUT STYLES -->
		
		<!-- 弹出框 -->
		<link href="${base}/js/library/sweetalert/css/sweetalert.css" rel="stylesheet" type="text/css" />
	</head>
	
	<body style="background-color:#FFFFFF;" id="roleManageBody">
		<!-- BEGIN PAGE BAR -->
		<div class="page-bar">
			<div class="page-toolbar">
				<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body"
					data-placement="bottom" data-original-title="Change dashboard date range">
					<i class="icon-calendar"></i>&nbsp; 
					<span class="thin uppercase hidden-xs"></span>&nbsp; 
					<i class="fa fa-angle-down"></i>
				</div>
			</div>
		</div>
		<!-- END PAGE BAR -->

		<!-- BEGIN PAGE TITLE-->
		<h3 class="page-title">
			后台管理 <small>角色管理</small>
		</h3>
		<!-- END PAGE TITLE-->
		<!-- END PAGE HEADER-->
		<div class="row">
			<form class="form-horizontal form-row-seperated">
				<div class="portlet light bordered">
					<div class="form-group">
						<div class="col-md-5" >
							<label class="control-label col-md-3">角色名称：</label>
							<input type="text" id="sName" class="form-control input-inline input-medium" maxlength="20" />
						</div>
						<div class="col-md-5">
							<label class="control-label col-md-3">创建时间：</label>
							<div class="col-md-4">
								<div class="input-group input-large date-picker input-daterange" data-date-format="yyyy-mm-dd">
									<input type="text" id="beginDate" class="form-control" readonly /> 
									<span class="input-group-addon">-</span> 
									<input type="text" id="endDate" class="form-control" readonly />
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-5">
							<label class="control-label col-md-3">创&nbsp;&nbsp;建&nbsp;&nbsp;人：</label>
							<input type="text" id="sUserName" class="form-control input-inline input-medium"/>
						</div>
					</div>
					<div class="form-group">
						<div style="float:right;margin-right:120px;">
							<button class='btn green' type="button" style='float:left;margin-left:15px;' onclick="queryRole()">查询</button>
							<button class='btn green' type="button" style='float:left;margin-left:15px;' onclick="cleanRole()">清空</button>
						</div>
					</div>
				</div>
			</form>
		</div>
		<div class="row">
			<div class="portlet light bordered">
				<table class="table table-striped table-hover table-bordered"
					id="roleTable">
				</table>
		</div>
	</div>
	
	<!-- 查看角色模态框 -->
    <div id="showRole" class="modal fade" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h5 class="modal-title">查看角色</h5>
				</div>
				<div class="modal-body form">
					<form action="javascript:;" class="form-horizontal form-row-seperated">
						<div class="form-group">
							<label class="col-sm-3 control-label">
								角色名称
								<span class="required"> * </span>
							</label>
							<div class="col-sm-8">
								<div class="input-group">
									<input type="text" id="sroleName" name="sroleName" class="form-control required" style="width:300px;" readOnly="true"/>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">
								角色描述
								<span class="required"> * </span>
							</label>
							<div class="col-sm-8">
								<div class="input-group">
									<textarea class="form-control required" id="sdescription" name="sdescription" rows="3" style="margin: 0px -1px 0px 0px;width:300px;" readOnly="true"></textarea>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">
								拥有权限
								<span class="required"> * </span>
							</label>
							<div class="col-sm-8">
								<div class="input-group">
                                    <div id="showTree" class="tree-demo"> </div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn green" data-dismiss="modal">关闭</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<!-- 新增/编辑模态框 -->
	<div id="editRole" class="modal fade" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title" id="modalTitle"></h4>
				</div>
				<div class="modal-body form">
					<form action="javascript:;" id="addRoleInfo" class="form-horizontal form-row-seperated">
						<input type="hidden" id="roleId" name="roleId" value="0">
						<div class="form-group">
							<label class="col-sm-3 control-label">
								角色名称
								<span class="required"> * </span>
							</label>
							<div class="col-sm-8">
								<div class="input-group">
									<input type="text" id="roleName" name="roleName" class="form-control required" maxlength="20" style="width:300px;" />
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">
								角色描述
								<span class="required"> * </span>
							</label>
							<div class="col-sm-8">
								<div class="input-group">
									<textarea class="form-control required" id="description" name="description" maxlength="200" rows="3" style="margin: 0px -1px 0px 0px;width:300px;"></textarea>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">
								分配权限
								<span class="required"> * </span>
							</label>
							<div class="col-sm-8">
								<div class="input-group">
                                    <div id="getTree" class="tree-demo"> </div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn green" data-dismiss="modal">关闭</button>
							<button type="sumbit" class="btn green">提交</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<!-- BEGIN CORE PLUGINS -->
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/moment.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-CN.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<script src="${base}/js/library/metronic-4.7/assets/global/scripts/datatable.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jstree/dist/jstree.min.js" type="text/javascript"></script>
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="${base}/js/library/metronic-4.7/assets/global/scripts/app.min.js" type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/ui-tree.js" type="text/javascript"></script>
	<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>

	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script src="${base}/js/library/metronic-4.7/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
	<!-- END THEME LAYOUT SCRIPTS -->
	
	<!-- BEGIN 表单判空 -->
	<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery-validation/js/jquery.validate.js" type="text/javascript"></script>
	<!-- END 表单判空 -->
	
	<!-- 弹出框 -->
	<script src="${base}/js/library/sweetalert/js/sweetalert.min.js" type="text/javascript"></script>
	<script src="${base}/js/library/sweetalert/js/showAlert.js" type="text/javascript"></script>
	
	<!-- 获取根路径 -->
	<script src="${base}/js/common/util/jqueryUtil.js" type="text/javascript"></script>
	
	<script>
	
		$('.date-picker').datepicker({
			language : 'zh-CN',
			autoclose : true,
			todayHighlight : true
		});
		
		$(document).ready(function()
		{	
			$(".dataTables_length").after("<button class='btn green' style='float:left;margin-left:15px;' onclick='showConfirmModal(\"确定删除所选角色？\",batchDeleteRole)'>批量删除</button>");
			$(".dataTables_length").after("<button class='btn green' data-toggle='modal' data-target='#editRole' style='float:left;margin-left:15px;' onclick='addRole()'>新增</button>");
			
			/* 验证表单 */
			$("#addRoleInfo").validate({
				submitHandler : function() 
				{
					//拿到树的所有节点
					var power = getids("getTree");
					
					if(power.length<1)
					{
						showInfoModal("请选择除了根节点以外的权限！");
						return false;
					}
					
					var roleName = $.trim($("#roleName").val());
					var description = $.trim($("#description").val());
					var roleId = $("#roleId").val();
					
					if(roleName == "" || roleName == null)
					{
						showInfoModal("角色名称不能为空！");
						return false;
					}
					
					if(description == "" || description == null)
					{
						showInfoModal("角色描述不能为空！");
						return false;
					}
					
					$.ajax(
					{
					 	url      : jQuery.getBasePath() + "/role/doAddRole.bsh",
	                 	dataType : "json",
	                 	async    :  true,
	                 	type     : "post",
	                 	traditional: true,//数组格式转换 加上这个就可以了
	                 	data     :  {"roleId":roleId,"roleName":roleName,"description":description,"power":power}, // 参数对象
	                	success:function(data)
	                 	{
	                 		if(data.status == "success")
	                 		{
	                 			showSuccessOrErrorModal(data.msg,"success");
	                     		$("#editRole").modal("hide");
	                     		roleDataTable.draw();
	                 		}else
	                 		{
	                 			showSuccessOrErrorModal(data.msg,"error");
	                 		}
	                     	
	                 	},
	                 	error:  function(data)
	                 	{
	                     	showSuccessOrErrorModal(data.msg,"error");

	                 	}
					});
					
				}
			});
			
		});
			
		$("#roleManageBody").bind("DOMNodeInserted",function()
		{
			window.top.resizeParentHeightFun();
		});
		
		var sname = "";
		var beginDate = "";
		var endDate = "";
		
		var roleDataTable = $('#roleTable').DataTable({
		
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"bStateSave": false,
		"sAjaxSource": jQuery.getBasePath() + "/role/doGetRoleList.bsh",
		"sDom": '<"top"l<"clear">>rt<"bottom"ip<"clear">>',
		"lengthMenu": [10,20,30],
		"pageLength": 10,
		"oLanguage": {
			"sProcessing": "正在加载数据...",
			"sLengthMenu": "_MENU_条记录",
			"sZeroRecords": "没有查到记录",
			"sInfo": "第  _START_ 条到第  _END_ 条记录，一共  _TOTAL_ 条记录",
			"sInfoEmpty": "0条记录",
			"oPaginate": {
				"sPrevious": "上一页 ",
				"sNext": " 下一页",
			}
		},
		"aoColumnDefs": [{
			"bSearchable": true,
			"bVisible": false,
			"aTargets": [0]
		} ],
		"aoColumns": [{
			"sTitle": "id",
			"bSortable": false
		}, {
			"sTitle": '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" id="checkAll" onclick="chooseOrNoChoose(this)" /><span></span></label>',
			"bSortable": false,
			"sWidth":"5%",
			"sClass": "text-center",
			"fnCreatedCell":function(nTd, sData, oData, iRow, iCol)
			{
				var value = oData[0];
				var content = '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" class="roleCheckbox" value="'+value+'" /><span></span></label>';
				$(nTd).html(content);
			}
		}, {
			"sTitle": "角色名称",
			"bSortable": false,
			"sWidth":"15%",
			"sClass": "text-center",
			"fnCreatedCell":function(nTd, sData, oData, iRow, iCol)
			{
				var id = oData[0];
				var value = oData[2];
				var content = "<a href='javascript:;' data-toggle='modal' data-target='#showRole' onclick='showRole(\""+ id +"\")'> " + value + " </a> ";
				$(nTd).html(content);
			}
		}, {
			"sTitle": "描述",
			"bSortable": false,
			"sWidth":"25%",
			"sClass": "text-center",
			"fnCreatedCell":function(nTd, sData, oData, iRow, iCol)
			{
				var value = oData[3];
				var content="";
				if(value.length >= 18)
				{
					content = '<span title="'+value+'">'+ value.substring(0,18) +'...</span>';
				}else
				{
					content = '<span title="'+value+'">'+ value +'</span>';
				}
				
				$(nTd).html(content);
			}
		}, {
			"sTitle": "创建人",
			"bSortable": false,
			"sClass": "text-center"
		}, {
			"sTitle": "创建时间",
			"bSortable": false,
			"sClass": "text-center"
		}, {
			"sTitle": "操作",
			"bSortable": false,
			"sClass": "text-center",
			"fnCreatedCell":function(nTd, sData, oData, iRow, iCol)
			{
				var value = oData[0];
				var content = "<button class='btn btn-xs blue' data-toggle='modal' data-target='#editRole' onclick='editRole(\""+ value +"\")'> 修改 </button> <button  class='btn btn-xs yellow-crusta' onclick='showConfirmModal(\"确定删除此角色？\",delRole,\""+ value +"\")'> 删除 </button>";
				$(nTd).html(content);
			}
		}],
		// 服务器端，数据回调处理  
		"fnServerData": function retrieveData(sSource, aoData, fnCallback) 
		{
				aoData.push({"name":"sname","value":sname});
				aoData.push({"name":"beginDate","value":beginDate});
				aoData.push({"name":"endDate","value":endDate});
				$.ajax(
				{
					"type": "POST",
					"url": sSource,
					"dataType": "json",
					"contentType": "application/json",
					"data": JSON.stringify(aoData),
					"success": function(data) 
					{
						$("#checkAll").prop("checked",false);
						fnCallback(data);
					}
				});
			}
		}); 
		
		//全选
		function chooseOrNoChoose(obj)
		{
			if($(obj).is(":checked"))
			{
				$(".roleCheckbox").prop("checked", true);
			}
			else
			{
				$(".roleCheckbox").prop("checked",false);
			}
		}
		
		//查询按钮
		function queryRole()
		{
			sname = $("#sName").val();
			beginDate = $("#beginDate").val();
			endDate = $("#endDate").val();
			roleDataTable.draw();
		}
		
		//清空按钮
		function cleanRole()
		{
			$("#sName").val("");
			$("#beginDate").val("");
			$("#endDate").val("");
			$("#sUserName").val("");
			sname = "";
			beginDate = "";
			endDate = "";
			roleDataTable.draw();
		}
		
		//删除
		function delRole(id)
		{
			$.ajax(
			{
				url      : jQuery.getBasePath() + "/role/doDeleteRoleById.bsh",
		        dataType : "json",
		        async    :  true,
		        type     : "post",
		        data     :  {"id":id}, // 参数对象
		        success:function(data)
		        {
		            if(data.status == "success")
		            {
		            	showSuccessOrErrorModal(data.msg,"success");
		            	roleDataTable.draw();
		            }else
		            {
		            	showSuccessOrErrorModal(data.userInfo,"error");
		            }
		        },
		        error:  function(data)
		        {
		            showSuccessOrErrorModal(data.msg,"error");
		        }
			});
		}
		
		//修改按钮
		function editRole(id)
		{
			$(".input-group label").hide();
			$("#modalTitle").text("修改");
			//清空树 和deselect_all()方法要联用
			$("#getTree").data('jstree', false).empty();
			$.ajax(
			{
				url      : jQuery.getBasePath() + "/role/doGetRoleById.bsh",
	            dataType : "json",
	            async    :  false,
	            type     : "post",
	            data     :  {"id":id}, // 参数对象
	            success:function(data)
	            {
	            	$("#roleId").val(data.role.roleId);
					$("#roleName").val(data.role.roleName);
					$("#description").val(data.role.roleDesc);
					var roleTree = data.role.powerId;
					//让已经有的权限显示出来
					$('#getTree').on('loaded.jstree', function(e,data) 
					{ 
						var inst = data.instance;
						//删除所有勾选状态
						inst.deselect_all(); 
						//让树闭合
						inst.close_all(); 
						for(var i = 0;i < roleTree.length;i++)
						{
							//根据id让对应的节点变成被选中状态  
							inst.select_node(roleTree[i]);
						}
				    });
	            },
	            error:  function(data)
	            {
	            	showSuccessOrErrorModal(data.msg,"error");
	            }
			});
			showPowerTree();
		}
		
		//新增按钮
		function addRole()
		{
			$(".input-group label").hide();
			$("#modalTitle").text("新增");
			$("#roleId").val("0");
			$("#roleName").val("");
			$("#description").val("");
			//清空树 和deselect_all()方法要联用
			$("#getTree").data('jstree', false).empty();
			$('#getTree').on('loaded.jstree', function(e,data) 
			{ 
				var inst = data.instance;
				//删除所有勾选状态
				inst.deselect_all();
				//让树闭合
				inst.close_all();
			});
			showPowerTree();
		}
		
		//带有复选框的权限分配树
		function showPowerTree() {   
    		$.ajax({
        		type 	 : "POST",
        		async    :  false,  
        		dataType : 'json',  
		        url : jQuery.getBasePath() + "/role/doGetPowerTree.bsh",  
		        error : function(data) 
		        {  
		        	showSuccessOrErrorModal("出错了！！:" + data,"error");
		        },  
		        success : function(data) 
		        {  
		        	$('#getTree').jstree(
		        	{
		        		'plugins': ["wholerow","checkbox", "types"],
		        		'core': 
		        		{
		        			"themes" : 
		        			{
		        				"responsive": false
		        			},
		        			'data':data.tree
		        		},
		        		"checkbox": 
		        		{
		        			"keep_selected_style": false,//是否默认选中
		        			"three_state": false,//父子级别级联选择
		        			"tie_selection": true,
		        			"cascade":"undetermined"
		        		},
		        		"types" :
		        		{
		        			"default" : 
		        			{
		        				"icon" : "fa fa-folder icon-state-warning icon-lg"
		        			},
		        			"file" : 
		        			{
		        				"icon" : "fa fa-file icon-state-warning icon-lg"
		        			}
		        		}
		        	});
		        }
		    });
		}
		
		//批量删除
		function batchDeleteRole()
		{
			var manyRole = [];
			$(".roleCheckbox:checked").each(function(){
				manyRole.push($(this).val());
			});
			
			if(manyRole.length<1)
			{
				showInfoModal("请选择要删除的角色！");
				return false;
			}
			
			$.ajax(
			{
				url      : jQuery.getBasePath() + "/role/doBatchDeleteRoleById.bsh",
		        dataType : "json",
		        async    :  true,
		        type     : "post",
		        traditional: true,//数组格式转换 加上这个就可以了
		        data     :  {"roleIds":manyRole}, // 参数对象
		        success:function(data)
		        {
		        	if(data.status == "success")
		            {
		            	showSuccessOrErrorModal(data.msg,"success");
		            	roleDataTable.draw();
		            }else
		            {
		            	showSuccessOrErrorModal(data.roleInfo,"error");
		            }
		        },
		        error:  function(data)
		        {
		            showSuccessOrErrorModal(data.msg,"error");
		        }
			});
		}
		
		//查看角色		
		function showRole(id)
		{
			$.ajax(
			{
				url      : jQuery.getBasePath() + "/role/doGetRoleById.bsh",
	            dataType : "json",
	            async    :  true,
	            type     : "post",
	            cache:false,
	            data     :  {"id":id}, // 参数对象
	            success:function(data)
	            {
					$("#sroleName").val(data.role.roleName);
					$("#sdescription").val(data.role.roleDesc);
					showRoleTree(data);
					//重新加载有变动的树
					$('#showTree').jstree(true).settings.core.data=data.tree;
					$('#showTree').jstree(true).refresh();
	            },
	            error:  function(data)
	            {
	            	showSuccessOrErrorModal(data.msg,"error");
	            }
			});
		}
		
		function showRoleTree(data)
		{
			$('#showTree').jstree({
				'data' : {   
                    type : "json",//类型为json  
                    async : true,//动态操作
                },
            	'plugins': ["wholerow","types"],
                'core': {
                	"themes" : {
                    	"responsive": false
                     },    
                	"data":data.tree   
                },
                "types" : {
                	"default" : {
                		"icon" : "fa fa-folder icon-state-warning icon-lg"
                	},
                	"file" : {
                		"icon" : "fa fa-file icon-state-warning icon-lg"
                	}
                }
            });
		}
		
	</script>
  </body>
</html>