<#assign base=request.contextPath />
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta content="width=device-width, initial-scale=1" name="viewport" />

		<meta content="" name="description" />

		<meta content="" name="author" />

		<title>后台管理</title>

		<!-- BEGIN GLOBAL MANDATORY STYLES -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
		<!-- END GLOBAL MANDATORY STYLES -->
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
		<!-- END PAGE LEVEL PLUGINS -->
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

		<!-- BEGIN THEME GLOBAL STYLES -->
		<link href="${base}/js/library/metronic-4.7/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
		<!-- END THEME GLOBAL STYLES -->
		<!-- BEGIN THEME LAYOUT STYLES -->
		<link href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
		<link href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
		
		<!-- 弹出框 -->
		<link href="${base}/js/library/sweetalert/css/sweetalert.css" rel="stylesheet" type="text/css" />
		<!-- END THEME LAYOUT STYLES -->

	</head>

	<body id="siteAdminBody" style="background-color:#FFFFFF;">
		<!-- BEGIN PAGE BAR -->
		<div class="page-bar">
			<div class="page-toolbar">
				<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body"
					data-placement="bottom" data-original-title="Change dashboard date range">
					<i class="icon-calendar"></i>&nbsp; 
					<span class="thin uppercase hidden-xs"></span>&nbsp; 
					<i class="fa fa-angle-down"></i>
				</div>
			</div>
		</div>
		<!-- END PAGE BAR -->

		<!-- BEGIN PAGE TITLE-->
		<h3 class="page-title">
			后台管理 <small>站点管理</small>
		</h3>
		<!-- END PAGE TITLE-->
		<!-- END PAGE HEADER-->
		<div class="row ">
			<div class="col-md-3 ">
				<div class="portlet light bordered" >
					<div style="height:550px;overflow-y:scroll;">
						<div id="areaTree" class="tree-demo" > </div>
					</div>
				</div>
			</div>
			<div class="col-md-9 ">
				<div class="portlet light bordered">
					<div class="row">
						<form class="form-horizontal form-row-seperated">
							<div class="form-group">
								<div class="col-md-5" >
									<label class="control-label col-md-4">站点名称：</label>
									<input id="searchSiteName" type="text" class="form-control input-inline input-medium" />
								</div>
								<div class="col-md-5" >
									<label class="control-label col-md-4">站点简称：</label>
									<input id="searchSiteAbbreviation" type="text" class="form-control input-inline input-medium" />
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-5" >
									<label class="control-label col-md-4">站点域名：</label>
									<input id="searchSiteUrl" type="text" class="form-control input-inline input-medium" />
								</div>
								<div class="col-md-5">
									<label class="control-label col-md-4">站点类型：</label>
									<select id="searchSiteType" class="form-control input-medium input-inline" >
										<option value="0">全部</option>
										<option value="2">子站</option>
										<option value="3">联盟网站</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<div style="float:right;margin-right:120px;">
									<button id="searchSiteList" class='btn green' type="button" style='float:left;margin-left:15px;'>查询</button>
									<button id="clearSearchCondition" class='btn green' type="button" style='float:left;margin-left:15px;'>清空</button>
								</div>
							</div>
						</form>
					</div>
					<div class="row">
						<table class="table table-striped table-hover table-bordered" id="dataTable">
					
						</table>
					</div>
				</div>
			</div>
		</div>			
	
	<!-- 模态框 -->
	<div id="windownAdd" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">新增站点</h4>
				</div>
				<div class="modal-body form">
					<form action="javascript:;" id="modalWin1" class="form-horizontal form-row-seperated">
						<div class="form-group">
							<div class="col-md-6" >
								<label class="control-label col-md-4">
									站点名称
									<span class="required"> * </span>：
								</label>
								<input id="addSiteName" type="text" class="form-control input-inline input-medium required" />
							</div>
							<div class="col-md-6" >
								<label class="control-label col-md-4">
									站点简称
									<span class="required"> * </span>：
								</label>
								<input id="addSiteAbbreviation" type="text" class="form-control input-inline input-medium required" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6" >
								<label class="control-label col-md-4">
									覆盖区域
									<span class="required"> * </span>：
								</label>
								<input id="coverArea" readOnly type="text" class="form-control input-inline input-medium" />
							</div>
							<div class="col-md-6" >
								<label class="control-label col-md-4">
									站点类型
									<span class="required"> * </span>：
								</label>
								<select id="selectSiteType" class="form-control input-medium input-inline" >
									<option value="2">子站</option>
									<option value="3">联盟网站</option>
								</select>
							</div>
						</div>
						<div id="windowSelectArea" style="display: none; flex-direction: column; height: 150px;background-color: gray;">
							<div style="display: flex; flex: 1;background-color: #;">
								<div style="display: flex;flex: 7;display: flex;align-items: center;justify-content: center;">
									选择当前新增站点所覆盖的行政区域（可多选）
								</div>
								<div style="display: flex;flex: 3;align-items: center;justify-content: flex-end;">
									<button id="confirmCoverDistrictSure" style="height: 40px;width: 60px;margin-right: 20px;">确定</button>
									<button id="cancelSelectArea" style="height: 40px;width: 60px; margin-right: 20px;">取消</button>
								</div>
							</div>
							<div id="containerDistricts" style="display: flex; flex: 1;background-color: #;align-items: center;">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6" >
								<label class="control-label col-md-4">
									站点LOGO
									<span class="required"> * </span>：
								</label>
								<input id="" type="text" class="form-control input-inline input-medium" />
							</div>
							<div class="col-md-6" >
								<button type="button" class="btn btn-default">选择图片</button>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6" >
								<label class="col-sm-4 control-label">
									站点描述
									<span> &nbsp;&nbsp; </span>：
								</label>
								<div class="col-sm-6">
									<div class="input-group">
										<textarea class="form-control" id="addSiteDesc" maxlength="200" rows="5" style="margin: 0px -1px 0px 0px;width:600px;"></textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6">
								<label class="control-label col-md-4">
									站点网址
									<span> &nbsp;&nbsp; </span>：
								</label>
								<div id="selectChildrenSiteGenerateTypeRadio" style="margin-top:7px;">
									<input type="radio" name="addSelectUrlType" checked="checked" value="1" />系统生成
									<input type="radio" name="addSelectUrlType" value="2" />手动输入
									<input id="addChildrenSiteInputUrl" style="margin-left: 140px; display: none;" class="form-control input-inline input-medium" />
								</div>
								<div>
									<input id="addUnionSiteInput" style="display: none;" class="form-control input-inline input-medium" />
								</div>
							</div>
						</div>
						
						<div style="display: flex; background-color: #;height: 50px;align-items: center;justify-content: center;">
							<button id="nextBtn" style="display: block;" type="button" class="btn btn-primary margin-right-10">下一步</button>
							<button id="addUnionSite" style="display:none" type="button" class="btn btn-primary margin-right-10">确定</button>
							<button type="button" class="btn btn-primary" data-dismiss="modal">取消</button>
						</div>
					</form>
					
					<form action="javascript:;" id="modalWin2" style="display: none;" class="form-horizontal form-row-seperated">
						<div class="form-group">
							<div class="col-md-6" >
								<label class="control-label col-md-4">
									站点模板：
								</label>
								<div class="col-md-6" style="margin-top:7px;" >
									<input type="radio" name="siteTemplate" checked="checked" />默认 
									<input type="radio" name="siteTemplate" />选择新模板
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6" >
								<label class="control-label col-md-4">
									模块配置：
								</label>
								<label class="control-label">
									(<font color="red">请勾选子站需要使用的功能模块</font>)
								</label>
							</div>
							<div class="col-md-5" style="margin-top:7px;">
								<input type="checkbox" />课程学习 
								<input type="checkbox" />培训活动 
								<input type="checkbox" />证书学习
								<input type="checkbox" />学习圈
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6" >
								<label class="control-label col-md-4">
									是否启动：
								</label>
								<div class="col-md-6" style="margin-top:7px;" >
									<input type="radio" name="isChildrenSiteStart" checked="checked" value="1" />立即启动 
									<input type="radio" name="isChildrenSiteStart" value="0" />暂不启用
								</div>
							</div>
						</div>
						
						<div style="display: flex; background-color: #;height: 50px;align-items: center;justify-content: center;">
							<button id="preBtn" style="display: block;" type="button" class="btn btn-primary margin-right-10">上一步</button>
							<button id="addChildrenSite" style="display:block" type="button" class="btn btn-primary margin-right-10">确定</button>
							<button type="button" class="btn btn-primary" data-dismiss="modal">取消</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- 站点修改模态框 -->
	<div id="windownModify" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">编辑站点</h4>
				</div>
				<div class="modal-body form">
					<form action="javascript:;" class="form-horizontal form-row-seperated">
						<div class="form-group">
							<div class="col-md-6" >
								<label class="control-label col-md-4">
									站点名称
									<span class="required"> * </span>
								</label>
								<input id="modifySiteName" type="text" class="form-control input-inline input-medium required" />
							</div>
							<div class="col-md-6" >
								<label class="control-label col-md-4">
									站点简称
									<span class="required"> * </span>
								</label>
								<input id="modifySiteAbbreviation" type="text" class="form-control input-inline input-medium required" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6" >
								<label class="control-label col-md-4">
									覆盖区域
									<span class="required"> * </span>
								</label>
								<input id="modifycoverAreaDisplay" readOnly type="text" class="form-control input-inline input-medium" />
							</div>
							<div class="col-md-6" >
								<label class="control-label col-md-4">
									站点类型
									<span class="required"> * </span>
								</label>
								<label class="control-label"><span id="modifyDisplaySiteType" ></span></label>
							</div>
						</div>
						
						<div id="windowModifySelectArea" style="display: none; flex-direction: column; height: 150px;background-color: gray;">
							<div style="display: flex; flex: 1;background-color: #;">
								<div style="display: flex;flex: 7;display: flex;align-items: center;justify-content: center;">
									选择当前新增站点所覆盖的行政区域（可多选）
								</div>
								<div style="display: flex;flex: 3;align-items: center;justify-content: flex-end;">
									<button id="modifyConfirmCoverDistrictSure" style="height: 40px;width: 60px;margin-right: 20px;">确定</button>
									<button id="modifyCancelSelectArea" style="height: 40px;width: 60px; margin-right: 20px;">取消</button>
								</div>
							</div>
							<div id="modifyContainerDistricts" style="display: flex; flex: 1;background-color: #;align-items: center;">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6" >
								<label class="control-label col-md-4">
									站点LOGO
									<span class="required"> * </span>
								</label>
								<input id="" type="text" class="form-control input-inline input-medium" />
							</div>
							<div class="col-md-6" >
								<button type="button" class="btn btn-default">选择图片</button>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6" >
								<label class="col-sm-4 control-label">
									站点描述
									<span> &nbsp;&nbsp; </span>
								</label>
								<div class="col-sm-6">
									<div class="input-group">
										<textarea class="form-control" id="modifySiteDesc" maxlength="200" rows="5" style="margin: 0px -1px 0px 0px;width:600px;"></textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6">
								<label class="control-label col-md-4">
									站点网址
									<span class="required"> * </span>
								</label>
								<input id="modifyChildrenSiteUrlInput" type="text" class="form-control input-inline input-medium required" />
							</div>
						</div>
						
						<div style="display: flex; background-color: #;height: 50px;align-items: center;justify-content: center;">
							<button id="modifyDoneBtn" style="display:block" type="button" class="btn btn-primary margin-right-10">确定</button>
							<button type="button" class="btn btn-primary" data-dismiss="modal">取消</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
		<!-- BEGIN CORE PLUGINS -->
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
		<!-- END CORE PLUGINS -->
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/moment.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-CN.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL PLUGINS -->
		<script src="${base}/js/library/metronic-4.7/assets/global/scripts/datatable.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jstree/dist/jstree.min.js" type="text/javascript"></script>
		<!-- BEGIN THEME GLOBAL SCRIPTS -->
		<script src="${base}/js/library/metronic-4.7/assets/global/scripts/app.min.js" type="text/javascript"></script>
		<!-- END THEME GLOBAL SCRIPTS -->
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/ui-tree.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>

		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->
		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->
		<!-- BEGIN THEME LAYOUT SCRIPTS -->
		<script src="${base}/js/library/metronic-4.7/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>

		<!-- BEGIN 表单判空 -->
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery-validation/js/jquery.validate.js" type="text/javascript"></script>
		
		<!-- 弹出框 -->
		<script src="${base}/js/library/sweetalert/js/sweetalert.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/sweetalert/js/showAlert.js" type="text/javascript"></script>
		
		<!-- 获取根路径 -->
		<script src="${base}/js/common/util/jqueryUtil.js" type="text/javascript"></script>
	
	<script>
		//该用户在该页面不具备的权限
		var menus = [];
		
		//最终的选中的区域树的节点-对应的是数据库
		var targetSelectTree = null;

		//搜索的五个条件
		var searchAreaNodeId = "";
		var searchSiteName = "";
		var searchSiteAbbr = "";
		var searchSiteDomain = "";
		var searchSiteType = ""

		//编辑条件
		var isCoverAreaModify = false;

		function initPage() {
			$(document).ajaxError(function() {
				showSuccessOrErrorModal("网络错误！","error");
			});
			//获取区域树
			$.ajax({
        		type 	 : "POST",
        		async    :  false,  
        		dataType : 'json',  
		        url : jQuery.getBasePath() + "/siteManage/getSiteAreaJsonTree.bsh",  
		        error : function(data) 
		        {  
		        	showSuccessOrErrorModal("出错了！！:" + data,"error");
		        },  
		        success : function(data) 
		        { 
		        	data.state = {
						opened: true
					};
		        	$('#areaTree').jstree(
		        	{
		        		'plugins': ["wholerow", "types"],
		        		'core': 
		        		{
		        			"themes" : 
		        			{
		        				"responsive": false
		        			},
		        			'data':data
		        		},
		        		"types" :
		        		{
		        			"default" : 
		        			{
		        				"icon" : "fa fa-folder icon-state-warning icon-lg"
		        			},
		        			"file" : 
		        			{
		        				"icon" : "fa fa-file icon-state-warning icon-lg"
		        			}
		        		}
		        	}).bind("select_node.jstree", function(e, data) {
						var areaEntity = data.node.original.attributes;
						targetSelectTree = areaEntity;
						
						//设置搜索条件
						searchAreaNodeId = areaEntity.id;
						//alert(areaEntity.areaType);
						if(areaEntity.areaType == -1){
							//表示选中了总站
							searchAreaNodeId = "";
						}
						positionDataTable.draw();
					});
		        }
		    });
			//获取所有的站点
			positionDataTable = $('#dataTable').DataTable({
				"bSort": true,
				"bProcessing": true,
				"bServerSide": true,
				"bStateSave": false,
				"sAjaxSource": jQuery.getBasePath() + "/siteManage/selectSiteListByCondition.bsh",
				"sDom": '<"top"l<"clear">>rt<"bottom"ip<"clear">>',
				"lengthMenu": [10, 20, 30],
				"pageLength":10,
				"oLanguage": {
					"sProcessing": "正在加载数据...",
					"sSearch": "搜索:",
					"sLengthMenu": "_MENU_条记录",
					"sZeroRecords": "没有查到记录",
					"sInfo": "第  _START_ 条到第  _END_ 条记录,一共  _TOTAL_ 条记录",
					"sInfoEmpty": "0条记录",
					"oPaginate": {
						"sPrevious": "上一页 ",
						"sNext": " 下一页",
					}
				},
				"aoColumnDefs": [{
					"bSearchable": true,
					"bVisible": false,
					"aTargets": [0],
				}, ],
				"aoColumns": [{
					"sTitle": "id",
					"bSortable": false
				}, 
				{
					"sTitle": "站点名称",
					"bSortable": false,
					"bAutoWidth": true,
					"sClass": "text-center"
				}, 
				{
					"sTitle": "网址",
					"bSortable": false,
					"sClass": "text-center"
				}, 
				{
					"sTitle": "覆盖区域",
					"bSortable": false,
					"sClass": "text-center",
					"fnCreatedCell": function(nTd, sData, oData, iRow, iCol) 
					{
						var list = JSON.parse(oData[iCol]);
						var showName = "";
						var content = "";
						for(var i = 0; i < list.length; i++) 
						{
							if(i == 0) 
							{
								showName += (list[i].city_name + "-" + list[i].area_name);
							} 
							else 
							{
								showName += "|" + (list[i].city_name + "-" + list[i].area_name);
							}
						}
						
						if(showName.length > 26)
						{
							content = "<span title='"+showName+"'>"+showName.substring(0,26)+"...</span>";
						}
						else
						{
							content = "<span title='"+showName+"'>"+showName+"</span>";
						}
						
						$(nTd).html(content);
					}

				}, 
				{
					"sTitle": "站点类型",
					"bSortable": false,
					"sClass": "text-center",
					"fnCreatedCell": function(nTd, sData, oData, iRow, iCol) 
					{
						if(oData[iCol] == 1) 
						{
							$(nTd).text('总站');
						} 
						else if(oData[iCol] == 2) 
						{
							$(nTd).text('子站');
						} 
						else 
						{
							$(nTd).text('联盟网站');
						}
					}
				}, 
				{
					"sTitle": "启用",
					"bSortable": false,
					"sClass": "text-center",
					"fnCreatedCell": function(nTd, sData, oData, iRow, iCol) 
					{
						var id = oData[0];
						var isEnable = oData[5];
						if(isEnable == 1) 
						{
							var pauseBtn = $("<button class='btn btn-xs red enableTimeOut' disabled attr='pause'>暂停</button>");
							pauseBtn.click(function() 
							{
								$.getJSON(jQuery.getBasePath() + "/siteManage/pauseSite.bsh", 
								{
									siteId: id
								}, function(data, textStatus) 
								{
									if(data == "pauseSitetDone") 
									{
										showSuccessOrErrorModal("暂停成功！","success");
										positionDataTable.draw();
									} 
									else 
									{
										showSuccessOrErrorModal("暂停失败！","error");
									}
								});
							});
							$(nTd).html(pauseBtn);
						} 
						else 
						{
							var startBtn = $("<button class='btn btn-xs blue enableTimeOut' disabled attr='pause'>启用</button>");
							startBtn.click(function() 
							{
								$.getJSON(jQuery.getBasePath() + "/siteManage/startSite.bsh", 
								{
									siteId: id
								}, function(data, textStatus) 
									{
									if(data == "starSitetDone")
									{
										showSuccessOrErrorModal("启用成功！","success");
										positionDataTable.draw();
									} 
									else 
									{
										showSuccessOrErrorModal("启用失败！","error");
									}
								});
							});
							$(nTd).html(startBtn);
						}

					}
				}, 
				{
					"sTitle": "操作",
					"bSortable": false,
					"sWidth":"15%",
					"sClass": "text-center",
					"fnCreatedCell": function(nTd, sData, oData, iRow, iCol) 
					{
						var id = oData[0];
						var isEnable = oData[5];
						//先清空
						$(nTd).html("");
						var modifyBtn = $("<button class='btn btn-xs blue modiBtn' data-toggle='modal' disabled data-target='#myModal'> 修改 </button>");
						modifyBtn.click(function() 
						{
							//初始化数据
							$.getJSON(jQuery.getBasePath() + "/siteManage/selecOneSiteInfo.bsh", 
							{
								siteId: id
							}, function(data, textStatus) 
							{
								var siteInfo = data;
								initModifyWindow(siteInfo);
							});
						});
						var deleteBtn = $("<button onclick='showConfirmModal(\"确认删除站点吗？\",delSite,\""+isEnable+"\",\""+id+"\")' disabled class='btn btn-xs yellow-crusta delBtn'> 删除 </button>");
						$(nTd).append(modifyBtn);
						$(nTd).append(deleteBtn);
					}
				}],
				// 服务器端，数据回调处理  
				"fnServerData": function retrieveData(sSource, aoData, fnCallback) {
					aoData.push({"name": "searchAreaNodeId","value": searchAreaNodeId});
					aoData.push({"name": "searchSiteName","value": searchSiteName});
					aoData.push({"name": "searchSiteAbbr","value": searchSiteAbbr});
					aoData.push({"name": "searchSiteDomain","value": searchSiteDomain});
					aoData.push({"name": "searchSiteType","value": searchSiteType});
					$.ajax({
						"type": "POST",
						"url": sSource,
						"dataType": "json",
						"contentType": "application/json",
						"data": JSON.stringify(aoData),
						"success": function(data) {
							fnCallback(data);
							if(menus.length > 0)
							{
								for(var j = 0; j < menus.length; j++)
								{
									$("." + menus[j].menuCode).removeAttr("disabled");
								}
							}
						}
					});
				},
			});
			
			$(".dataTables_length").after("<button class='btn green addSite' type='button' id='showAddSiteWindow' data-toggle='modal' data-target='#windownAdd' style='float:left;margin-left:15px; display:none;'>新增</button>");
		}
		
		//用查出来的站点信息初始化编辑层
		function initModifyWindow(siteInfo) 
		{
			$("#modifyDoneBtn").attr("currentModifySiteId", siteInfo.id);
			isCoverAreaModify = false;
			$("#modifySiteName").val(siteInfo.site_name);
			$("#modifySiteAbbreviation").val(siteInfo.site_abbreviation);
			$("#modifySiteDesc").val(siteInfo.site_desc);
			if(siteInfo.site_type == 1) 
			{
				$("#modifyDisplaySiteType").text('总站');
			} 
			else if(siteInfo.site_type == 2) 
			{
				$("#modifyDisplaySiteType").text('子站');
			} 
			else 
			{
				$("#modifyDisplaySiteType").text('联盟网站');
			}
			$("#modifyChildrenSiteUrlInput").val(siteInfo.site_url);
			//覆盖区域初始化
			$("#modifycoverAreaDisplay").val("");
			for(var i = 0; i < siteInfo.districts.length; i++) 
			{
				if(i == 0) 
				{
					$("#modifycoverAreaDisplay").val(siteInfo.districts[i].area_name);
				} 
				else 
				{
					$("#modifycoverAreaDisplay").val($("#modifycoverAreaDisplay").val() + "|" + siteInfo.districts[i].area_name);
				}
			}
			//覆盖区域DIV初始化
			/*获取当前市节点,市节点包含了所有的子节点*/
			var cityNode = null;
			//判断当前选择的是市还是区 1省 2市 3区
			if(siteInfo.site_type == 1) 
			{
				$("#modifycoverAreaDisplay").val("总站");
				$("#modifycoverAreaDisplay").attr('disabled', 'disabled');
				$('#windownModify').modal('toggle');
				return;
			} 
			else 
			{
				$("#modifycoverAreaDisplay").removeAttr('disabled');
			}
			var firstDistrict = siteInfo.districts[0];
			if(firstDistrict.area_type == 2) 
			{
				cityNode = $('#areaTree').jstree("get_node", firstDistrict.id);
			} 
			else if(firstDistrict.area_type == 3) 
			{
				//查找它的父节点 即市节点
				var cityNode = $('#areaTree').jstree("get_node", firstDistrict.fatherId);
			}
			//初始化区域覆盖
			$("#modifyContainerDistricts").html("");
			//首先插入全市
			var city = $('<div class="modifyCityItem" style="height: 50px;width: 50px;border: 1px solid black;display: flex;align-items: center;justify-content: center;"></div>');
			city.text('全市');
			city.attr('id', cityNode.id);
			city.attr('select-status', 'no');
			//全市节点添加
			$("#modifyContainerDistricts").append(city);
			//绑定全市点击事件
			city.click(function() {
				var cityId = $(this).attr('id');
				//首先换颜色增加选中标识
				$(this).css({
					backgroundColor: 'yellow'
				});
				$(this).attr('select-status', "yes");
				//取消所有的districts选中状态
				$(".modifyDistrictItem").css({
					backgroundColor: 'grey'
				});
				$(".modifyDistrictItem").attr('select-status', "no");
			});
			for(var i = 0; i < cityNode.children.length; i++) 
			{
				var district = $('<div class="modifyDistrictItem" style="height: 50px;width: 50px;border: 1px solid black;display: flex;align-items: center;justify-content: center;"></div>');
				var districtNode = $('#areaTree').jstree("get_node", cityNode.children[i]);
				district.text(districtNode.text);
				district.attr("id", districtNode.id);
				district.attr('select-status', 'no');
				$("#modifyContainerDistricts").append(district);
			}
			//绑定区域点击事件
			$(".modifyDistrictItem").click(function() {
				var id = $(this).attr('id');
				var selectStatus = $(this).attr('select-status');
				if(selectStatus == 'yes') 
				{
					$(this).attr('select-status', 'no');
					$(this).css({
						backgroundColor: 'grey',
					});
				} 
				else if(selectStatus == 'no') 
				{
					//取消全市选中逻辑
					$(".modifyCityItem").css({
						backgroundColor: 'grey',
					});
					$(".modifyCityItem").attr('select-status', 'no');
					$(this).attr('select-status', 'yes');
					$(this).css({
						backgroundColor: 'yellow',
					});
				}
			});
			$('#windownModify').modal('toggle');
		}

		function setListener() 
		{
			//显示区域选择框
			$("#coverArea").focus(function() {
				$("#windowSelectArea").css({
					"display": "flex",
				});
			});
			//取消 区域选择
			$("#cancelSelectArea").click(function() {
				$("#windowSelectArea").css({
					"display": "none",
				});
			});

			//站点类型下拉框事件
			$("#selectSiteType").change(function() {
				var index = $("#selectSiteType").get(0).selectedIndex;
				if(index == 0) 
				{
					//当选择站点类型为 子站
					$("#nextBtn").show();
					$("#addUnionSite").hide();
					$("#addUnionSiteInput").hide();
					$("#selectChildrenSiteGenerateTypeRadio").show();
				} 
				else if(index == 1) 
				{
					//当选择站点类型为 联盟网站
					$("#addUnionSite").show();
					$("#nextBtn").hide();
					$("#addUnionSiteInput").show();
					$("#selectChildrenSiteGenerateTypeRadio").hide();

				}
			});
			//下一步
			$("#nextBtn").click(function() {
				//展示子站下一步
				$("#modalWin1").hide();
				$("#modalWin2").show();
			});
			//上一步
			$("#preBtn").click(function() {
				$("#modalWin1").show();
				$("#modalWin2").hide();
			});
			//新增-按钮
			$("#showAddSiteWindow").click(function(e) {
				if(!targetSelectTree) 
				{
					showInfoModal("请选择某个市或者某个区！");
					//阻止事件传播，即不显示弹窗了
					e.stopPropagation();
				} 
				else if(targetSelectTree.areaType == 1)
				{
					showInfoModal("根节点下无法新增！");
					e.stopPropagation();
				}
				else
				{
					//清空表单
					$("#addSiteName").val("");
					$("#addSiteAbbreviation").val("");
					$("#coverArea").val("");
					$("#selectSiteType").val("2");
					$("#addSiteDesc").val("");
					$("#addUnionSiteInput").val("");
					$("#addChildrenSiteInputUrl").val("");
					
					window.targetSelectCoverDistrict = {
						isAll: "",
						cityId: "",
						listArea: []
					};
					/*获取当前市节点,市节点包含了所有的子节点*/
					var cityNode = null;
					//判断当前选择的是市还是区 1省 2市 3区
					if(targetSelectTree.areaType == 2) 
					{
						cityNode = $('#areaTree').jstree("get_node", targetSelectTree.id);
					} 
					else if(targetSelectTree.areaType == 3) 
					{
						//查找它的父节点 即市节点
						var cityNode = $('#areaTree').jstree("get_node", targetSelectTree.parentId);
					}
					//初始化区域覆盖
					$("#containerDistricts").html("");
					//首先插入全市
					var city = $('<div class="cityItem" style="height: 50px;width: 50px;border: 1px solid black;display: flex;align-items: center;justify-content: center;"></div>');
					city.text('全市');
					city.attr('id', cityNode.id);
					city.attr('select-status', 'no');
					$("#containerDistricts").append(city);
					//绑定全市点击事件
					$(".cityItem").click(function() 
					{
						var cityId = $(this).attr('id');
						//首先换颜色增加选中标识
						$(this).css({
							backgroundColor: 'yellow'
						});
						$(this).attr('select-status', "yes");
						//取消所有的districts选中状态
						$(".districtItem").css({
							backgroundColor: 'grey'
						});
						$(".districtItem").attr('select-status', "no");
					});
					for(var i = 0; i < cityNode.children.length; i++) 
					{
						var district = $('<div class="districtItem" style="height: 50px;width: 50px;border: 1px solid black;display: flex;align-items: center;justify-content: center;"></div>');
						var districtNode = $('#areaTree').jstree("get_node", cityNode.children[i]);
						district.text(districtNode.text);
						district.attr("id", districtNode.id);
						district.attr('select-status', 'no');
						$("#containerDistricts").append(district);
					}
					//绑定区域点击事件
					$(".districtItem").click(function() 
					{
						var id = $(this).attr('id');
						var selectStatus = $(this).attr('select-status');
						if(selectStatus == 'yes') 
						{
							$(this).attr('select-status', 'no');
							$(this).css({
								backgroundColor: 'grey',
							});
						} 
						else if(selectStatus == 'no') 
						{
							//取消全市选中逻辑
							$(".cityItem").css({
								backgroundColor: 'grey',
							});
							$(".cityItem").attr('select-status', 'no');
							$(this).attr('select-status', 'yes');
							$(this).css({
								backgroundColor: 'yellow',
							});
						}
					});
				}
			});
			//新增站点里面的选择好覆盖区域确定按钮
			$("#confirmCoverDistrictSure").click(function() 
			{
				$("#coverArea").val("");
				//选中覆盖区域逻辑
				window.targetSelectCoverDistrict = {
					isAll: "",
					cityId: "",
					listArea: []
				};
				if($(".cityItem").attr('select-status') == 'yes') 
				{
					targetSelectCoverDistrict.isAll = 'yes';
					targetSelectCoverDistrict.cityId = $(".cityItem").attr('id');
					$("#coverArea").val("全市");
				} 
				else 
				{
					targetSelectCoverDistrict.isAll = 'no';
					$(".districtItem[select-status='yes']").each(function(index) 
					{
						console.log($(this).attr('id') + $(this).text());
						targetSelectCoverDistrict.listArea.push($(this).attr('id'));
						if(index == 0) 
						{
							$("#coverArea").val($("#coverArea").val() + $(this).text());
						} 
						else 
						{
							$("#coverArea").val($("#coverArea").val() + "|" + $(this).text());
						}

					});
				}
				if(targetSelectCoverDistrict.isAll == "no" && targetSelectCoverDistrict.listArea.length == 0) 
				{
					showSuccessOrErrorModal("必须选择覆盖区域！","error");
				} 
				else 
				{
					//隐藏选择覆盖区域框
					$("#windowSelectArea").hide();
				}
			});
			//编辑站点里面的选择好覆盖区域 确定按钮
			$("#modifyConfirmCoverDistrictSure").click(function() 
			{
				isCoverAreaModify = true;
				window.targetSelectCoverDistrict = {
					isAll: "",
					cityId: "",
					listArea: []
				};
				$("#modifycoverAreaDisplay").val("");
				//选中覆盖区域逻辑
				if($(".modifyCityItem").attr('select-status') == 'yes') 
				{
					targetSelectCoverDistrict.isAll = 'yes';
					targetSelectCoverDistrict.cityId = $(".modifyCityItem").attr('id');
					$("#modifycoverAreaDisplay").val("全市");
				} 
				else 
				{
					targetSelectCoverDistrict.isAll = 'no';
					$(".modifyDistrictItem[select-status='yes']").each(function(index) 
					{
						targetSelectCoverDistrict.listArea.push($(this).attr('id'));
						if(index == 0) 
						{
							$("#modifycoverAreaDisplay").val($("#modifycoverAreaDisplay").val() + $(this).text());
						} 
						else 
						{
							$("#modifycoverAreaDisplay").val($("#modifycoverAreaDisplay").val() + "|" + $(this).text());
						}

					});
				}
				if(targetSelectCoverDistrict.isAll == "no" && targetSelectCoverDistrict.listArea.length == 0) 
				{
					showSuccessOrErrorModal("必须选择覆盖区域！","error");
				} 
				else 
				{
					//隐藏选择覆盖区域框
					$("#windowModifySelectArea").hide();
				}
			});
			//编辑层 选中覆盖区域 取消按钮
			$("#modifyCancelSelectArea").click(function() 
			{
				isCoverAreaModify = false;
				$("#windowModifySelectArea").hide();
			});
			//模态框的关闭事件
			$('#windownAdd').on('hidden.bs.modal', function(e) 
			{
				$("#modalWin1").show();
				$("#modalWin2").hide();
			});
			//单选radio变化事件监听 - 新增的时候选择站点网址是 系统生成 | 手动输入 1代表系统生成 2代表手动输入
			$("#selectChildrenSiteGenerateTypeRadio input").change(function() 
			{
				var childrenSiteType = $('#selectChildrenSiteGenerateTypeRadio input[name="addSelectUrlType"]:checked ').val();
				if(childrenSiteType == "1") 
				{
					$("#addChildrenSiteInputUrl").hide();
				} 
				else 
				{
					$("#addChildrenSiteInputUrl").show();
				}

			});
			//新增联盟网站-按钮
			$("#addUnionSite").click(function() 
			{
				//数据验证
				if(!$.trim($("#addSiteName").val())) 
				{
					showInfoModal("站点名称必填！");
					return;
				}
				if(!$.trim($("#addSiteAbbreviation").val())) 
				{
					showInfoModal("站点简称必填！");
					return;
				}
				if(!window.targetSelectCoverDistrict || targetSelectCoverDistrict.isAll == "" || (targetSelectCoverDistrict.isAll == "no" && targetSelectCoverDistrict.listArea.length == 0)) 
				{
					showInfoModal("必须选择覆盖区域！");
					return;
				} 
				if(!$.trim($("#addUnionSiteInput").val())) 
				{
					showInfoModal("联盟网站网址必填！");
					return;
				}
				//站点类型 需要+2 才能对应
				var siteTypeIndex = $("#selectSiteType").get(0).selectedIndex;
				var baseInfo = {
					siteName: $("#addSiteName").val(),
					siteAbbreviation: $("#addSiteAbbreviation").val(),
					siteType: siteTypeIndex + 2,
					siteLogoUrl: "www.moren.com.pic",
					siteDesc: $("#addSiteDesc").val(),
					siteTemplateId: 1,
					siteUrl: $("#addUnionSiteInput").val(),
				};
				$.getJSON(jQuery.getBasePath() + "/siteManage/addNewSite.bsh", 
				{
					info: JSON.stringify(baseInfo),
					isAll: targetSelectCoverDistrict.isAll,
					cityId: targetSelectCoverDistrict.cityId,
					listArea: JSON.stringify(targetSelectCoverDistrict.listArea)
				}, 
				function(data, textStatus) 
				{
					if(data == "addSuccess") 
					{
						showSuccessOrErrorModal("添加联盟网站成功！","success");
						$('#windownAdd').modal('toggle');
						positionDataTable.draw();
					}
				});
			});
			//新增子站-按钮
			$("#addChildrenSite").click(function() 
			{
				//数据验证
				if(!$.trim($("#addSiteName").val())) 
				{
					showInfoModal("站点名称必填！");
					return;
				}
				if(!$.trim($("#addSiteAbbreviation").val())) 
				{
					showInfoModal("站点简称必填！");
					return;
				}
				if(!window.targetSelectCoverDistrict || targetSelectCoverDistrict.isAll == "" || (targetSelectCoverDistrict.isAll == "no" && targetSelectCoverDistrict.listArea.length == 0)) 
				{
					showInfoModal("必须选择覆盖区域！");
					return;
				} 
				var childrenSiteUrl;
				//判断站点网址生成类型
				var childrenSiteType = $('#selectChildrenSiteGenerateTypeRadio input[name="addSelectUrlType"]:checked ').val();
				if(childrenSiteType == "1") 
				{
					//系统生成
					childrenSiteUrl = "-1";
				} 
				else 
				{
					//手动输入
					if(!$.trim($("#addChildrenSiteInputUrl").val())) 
					{
						showInfoModal("子站网址不可为空！");
						return;
					} 
					else 
					{
						childrenSiteUrl = $("#addChildrenSiteInputUrl").val();
					}
				}
				//站点类型 需要+2 才能对应
				var siteTypeIndex = $("#selectSiteType").get(0).selectedIndex;
				var baseInfo = {
					siteName: $("#addSiteName").val(),
					siteAbbreviation: $("#addSiteAbbreviation").val(),
					siteType: siteTypeIndex + 2,
					siteLogoUrl: "www.default.com.pic",
					siteDesc: $("#addSiteDesc").val(),
					siteTemplateId: 1,
					siteUrl: childrenSiteUrl,
					isEnabled: $("input[name='isChildrenSiteStart']:checked").val()
				};
				$.getJSON(jQuery.getBasePath() + "/siteManage/addNewSite.bsh", 
				{
					info: JSON.stringify(baseInfo),
					isAll: targetSelectCoverDistrict.isAll,
					cityId: targetSelectCoverDistrict.cityId,
					listArea: JSON.stringify(targetSelectCoverDistrict.listArea)
				}, 
				function(data, textStatus) 
				{
					if(data == "addSuccess") 
					{
						showSuccessOrErrorModal("添加子站成功！","success");
						$('#windownAdd').modal('toggle');
						positionDataTable.draw();
					}
				});
			});
			//编辑层 确定按钮
			$("#modifyDoneBtn").click(function() 
			{
				//数据验证
				if(!$.trim($("#modifySiteName").val())) 
				{
					showInfoModal("站点名称必填！");
					return;
				}
				if(!$.trim($("#modifySiteAbbreviation").val())) 
				{
					showInfoModal("站点简称必填！");
					return;
				}
				if(isCoverAreaModify && (!$.trim($("#modifycoverAreaDisplay").val()) || !window.targetSelectCoverDistrict || (targetSelectCoverDistrict.isAll == "no" && targetSelectCoverDistrict.listArea.length == 0))) 
				{
					showInfoModal("必须选择覆盖区域！");
					return;
				}
				if(!$.trim($("#modifyChildrenSiteUrlInput").val())) 
				{
					showInfoModal("站点网址不能为空！");
					return;
				}
				//发起编辑请求
				$.getJSON(jQuery.getBasePath() + "/siteManage/modifySite.bsh", 
				{
					isCoverAreaModify: isCoverAreaModify,
					info: JSON.stringify({
						siteId: $("#modifyDoneBtn").attr("currentModifySiteId"),
						modifySiteName: $("#modifySiteName").val(),
						modifySiteAbbreviation: $("#modifySiteAbbreviation").val(),
						modifySiteUrl: $("#modifyChildrenSiteUrlInput").val(),
						modifySiteDesc: $("#modifySiteDesc").val()
					}),
					isAll: isCoverAreaModify ? targetSelectCoverDistrict.isAll : "none",
					cityId: isCoverAreaModify ? targetSelectCoverDistrict.cityId : 'none',
					listArea: isCoverAreaModify ? JSON.stringify(targetSelectCoverDistrict.listArea) : 'none'
				}, 
				function(data, textStatus) 
				{
					if(data == "modifySuccess") 
					{
						showSuccessOrErrorModal("修改站点信息成功！","success");
						$('#windownModify').modal('toggle');
						positionDataTable.draw();
					}
				});
			});
			//查询按钮
			$("#searchSiteList").click(function() 
			{
				searchSiteName = $("#searchSiteName").val();
				searchSiteAbbr = $("#searchSiteAbbreviation").val();
				searchSiteDomain = $("#searchSiteUrl").val();
				if($("#searchSiteType").val() == 0) 
				{
					searchSiteType = "";
				} 
				else 
				{
					searchSiteType = $("#searchSiteType").val();
				}
				positionDataTable.draw();
			});
			//清空查询条件
			$("#clearSearchCondition").click(function() 
			{
				$("#searchSiteName").val("");
				$("#searchSiteAbbreviation").val("");
				$("#searchSiteUrl").val("");
				//区域节点条件置空
				searchAreaNodeId = "";
				$("#searchSiteType").val("0");
			});
			//在编辑层 点击覆盖区域 输入框
			$("#modifycoverAreaDisplay").focus(function() 
			{
				$("#windowModifySelectArea").show();
			});
		}
		
		//删除站点
		function delSite(isEnable,id) 
		{
			if(isEnable == 1) 
			{
				showSuccessOrErrorModal("开启状态下的子站不能被删除！", "error");
				return;
			}
			$.getJSON(jQuery.getBasePath() + "/siteManage/deleteSite.bsh", 
			{
				siteId: id
			}, 
			function(data, textStatus) 
			{
				if(data == "deleteSitetDone") 
				{
					showSuccessOrErrorModal("删除成功！", "success");
					positionDataTable.draw();
				} 
				else 
				{
					showSuccessOrErrorModal("删除失败,原因为:" + data + "！", "error");
				}
			});
		  }

		$(function() 
		{
			initPage();
			setListener();
			getMenuPower();
		});
		
		function getMenuPower()
		{
			$.ajax(
			{
				type 	 : "POST",
        		dataType : 'json',  
		    	url      : jQuery.getBasePath() + "/navigation/getMenuPower.bsh", 
		    	data     : {"resourceId":window.top.currentMenuId},
		    	success  : function(data) 
		       	{
		       		if(data.success)
		       		{
		       			if(data.resourceMenus.length > 0)
		       			{
		       				for(var i = 0; i < data.resourceMenus.length; i++)
		       				{
		       					if(data.resourceMenus[i].menuCode == "addSite")
		       					{
		       						$(".addSite").show();
		       					}
		       					else
		       					{
		       						menus.push(data.resourceMenus[i]);
		       					}
		       				}
		       			}
		       			if(menus.length > 0)
		       			{	
		       				for(var i = 0; i < menus.length; i++)
		       				{
		       					$("." + menus[i].menuCode).removeAttr("disabled");
		       				}
		       			}
		       		}
		       		else
		       		{
		       			showSuccessOrErrorModal(data.msg, "error");
		       		}
		       	}
		     });
		}
		
		
		$("#siteAdminBody").bind("DOMNodeInserted",function()
		{
			window.top.resizeParentHeightFun();
		});
	</script>
	</body>
</html>