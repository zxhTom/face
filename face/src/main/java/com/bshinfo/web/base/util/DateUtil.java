package com.bshinfo.web.base.util;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * 类描述：	时间格式处理工具类<br/>
 * 
 * 功能描述: 根据要求的指定格式和时间处理并返回<br/>
 * 
 * @author 	[zhangChao]
 * 
 * @see 	[java.text.SimpleDateFormat,java.util.Date]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-2-21 11:53:21]
 */
public class DateUtil 
{
	
	/**
	 * 返回系统当前时间的指定格式
	 * 
	 * @param dateStyle	[String]
	 * 
	 * @return Str		[String]
	 */
	public static String getDateString(String dateStyle)
	{
		if(null==dateStyle||("".equals(dateStyle)))
		{
			dateStyle = "yyyy-MM-dd HH:mm:ss";
		}
		
		SimpleDateFormat format = new SimpleDateFormat(dateStyle);
		
		return format.format(new Date());
	}
	
}
