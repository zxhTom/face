package com.bshinfo.web.base.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 * 返回结果集中Key值转换工具类<br/>
 * 
 * 功能描述: 对查询数据库返回的List<Map<String,Object>> 结果接进行key值驼峰格式转换处理<BR/>
 * 
 * @author 	[zhangChao]
 * 
 * @see 	[java.util.List,java.util.Map]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-2-21 11:53:21]
 */
public class MapUtil
{
	
	/***
	 * 将DB返回的List集合中存放Map中的key转换成驼峰格式
	 * Map中的key统一转化为小写，下划线后的字母大写，并去除下划线
	 * 
	 * @param 	list	[java.util.List]	参数结果集List
	 * 
	 * @return	list	[java.util.List]	处理后的结果List(remark:异常时返回空的List)
	 * 
	 * @Example [userInDept=USER_IN_DEPT, userId=USER_ID, userName=USER_NAME, password=PASSWORD]
	 * 
	 */
	public static List<Map<String, Object>> convertDBReturnList(List<Map<String, Object>> list)
	{
		
		List<Map<String,Object>> newList = null;
		
		if((null != list) && (!list.isEmpty()))
		{
			
			newList = new ArrayList<Map<String,Object>>();
			
			for(Map<String,Object> m : list)
			{
				try 
				{
					newList.add(convertMapKey(m));
				}
				catch (IOException e) 
				{
					return new ArrayList<Map<String,Object>>();
				}
			}
			
			return newList;
		}
		return new ArrayList<Map<String,Object>>();
	}
	
	

	/**
	 * 将Map中key值大写转小写，下划线后的字母大写不变，并去除下划线
	 * 
	 * @param 	m 		[java.util.Map]	Map集合
	 * 
	 * @return	resM	[java.util.Map]	处理后的Map集合，若参数Map为空则直接返回-null
	 * 
	 * @throws IOException
	 */
	public static Map<String, Object> convertMapKey(Map<String, Object> m) throws IOException
	{
		
		Map<String, Object> newMap = null;
		
		String key = null;
		
		Object  value = null;
		
		// 若map为空直接返回null
		if((null != m) && (!m.isEmpty()))
		{
			// 初始化，处理后的新Map
			newMap = new HashMap<String, Object>();
				
			Iterator<String> it = m.keySet().iterator();

			while(it.hasNext())
			{
				String strKey = it.next().toString();
				// 组装新的Map
				key = replaceAndConvertString(strKey,"_");
				
				value = m.get(strKey);
				
				newMap.put(key, value);
			}
			
			return newMap;
		}
		
		return null;
	}
	
	
	/**
	 * 将字符串中首字母大写其余小写
	 * 
	 * @param  s	[String]	参数字符串
	 * 
	 * @return s	[String]	处理后的字符串
	 */
	public static String firstCharacterToUpper(String s)
	{
		StringBuffer sb = new StringBuffer(s.toLowerCase());
		
		return sb.toString().substring(0,1).toUpperCase() + sb.toString().substring(1);
	}
	
	
	/**
	 * 替换字符串并让它的下一个字母为大写
	 * 
	 * @param str	[String]	参数字符串
	 * 
	 * @param org	[String]	指定字符
	 * 
	 * @return str  [String]	处理后的字符串
	 */
	public static String replaceAndConvertString(String str,String org) 
	{
		str = str.toLowerCase();
		
		String newString = "";
		
		int first = 0;
		
		while (str.indexOf(org) != -1) 
		{
			first = str.indexOf(org);
			
			if (first != str.length())
			{
				newString = newString + str.substring(0, first);
				
				str = str.substring(first + org.length(), str.length());
				
				str = firstCharacterToUpper(str);
			}
		}
		
		newString = newString + str;
		
		return newString;
		
	}
}
