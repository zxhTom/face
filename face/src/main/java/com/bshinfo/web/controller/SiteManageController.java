package com.bshinfo.web.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bshinfo.web.base.controller.BaseController;
import com.bshinfo.web.base.util.DatatableUtil;
import com.bshinfo.web.model.dataTableUtil.DataTableModel;
import com.bshinfo.web.model.dataTableUtil.DataTableParam;
import com.bshinfo.web.service.serviceI.site.SiteManageI;

/**
 * SiteManageController控制层 <br/>
 * 
 * 功能描述: 对 站点管理 做数据获取封装与页面控制跳转 <BR/>
 * 
 * @author [wudongwei]
 * 
 * @see [SiteManageController]
 * 
 * @since [JDK 1.7]
 * 
 * @version [1.0]
 * 
 * @Date [2017-2-17 16:41:55]
 */
@Controller
@RequestMapping("/siteManage")
public class SiteManageController extends BaseController
{
	public static Logger logger = Logger.getLogger(SiteManageController.class);

	/**
	 * 跳转至站点管理页面
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [String]
	 * 
	 * @author [wudongwei]
	 * 
	 * @Date [2017-2-17 16:29:26]
	 */
	@RequestMapping("/siteAdmin.bsh")
	public String siteAdminPage(HttpServletRequest request, HttpServletResponse response)
	{
		return "baseData/siteManage/site/siteMangeHomePage.ftl";
	}

	/**
	 * 请求区域树 响应返回json字符串
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [String]
	 * 
	 * @author [wudongwei]
	 * @throws IOException
	 * 
	 * @Date [2017-2-17 16:29:26]
	 */
	@RequestMapping("/getSiteAreaJsonTree.bsh")
	public void siteAreaJsonTree(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		String jsonR = siteManageI.selectAreaTree();
		response.getWriter().write(jsonR);
	}

	/**
	 * 新增站点 子站|联盟网站
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [String]
	 * 
	 * @author [wudongwei]
	 * @throws IOException
	 * 
	 * @Date [2017-2-17 16:29:26]
	 */
	@RequestMapping("/addNewSite.bsh")
	@ResponseBody
	public String addNewSite(String info, String isAll, String cityId, String listArea, HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		Boolean result = siteManageI.addNewSite(info, isAll, cityId, listArea);
		if (result)
		{
			return "addSuccess";
		}
		else
		{
			return "addFailed";
		}
	}
	@RequestMapping("/modifySite.bsh")
	@ResponseBody
	public String modifySite(String info,Boolean isCoverAreaModify, String isAll, String cityId, String listArea, HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		Boolean result = siteManageI.modifySiteInfoByInfo(info,isCoverAreaModify, isAll, cityId, listArea);
		if (result)
		{
			return "modifySuccess";
		}
		else
		{
			return "modifyFailed";
		}
	}
	/**
	 * 请求站点列表
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [String]
	 * 
	 * @author [wudongwei]
	 * @throws IOException
	 * 
	 * @Date [2017-2-17 16:29:26]
	 */
	@RequestMapping("/selectSiteListByCondition.bsh")
	@ResponseBody
	public Object selectSiteListByCondition(@RequestBody DataTableParam[] dataTableParams, HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		Map<String, String> dataTableMap = DatatableUtil.convertToMap(dataTableParams);
		DataTableModel dataTableModel = siteManageI.selectAllSiteByCondition(dataTableMap);
		return dataTableModel;
	}
	/**
	 * 开启站点
	 * @param siteId
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/startSite.bsh")
	@ResponseBody
	public Object startSite(String siteId, HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		if (siteManageI.startSiteById(siteId))
		{
			return "starSitetDone";
		}
		else
		{
			return "startSiteFailed";
		}
	}
	/**
	 * 暂停站点
	 * @param siteId
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/pauseSite.bsh")
	@ResponseBody
	public Object pauseSite(String siteId, HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		if (siteManageI.pauseSiteById(siteId))
		{
			return "pauseSitetDone";
		}
		else
		{
			return "pauseSiteFailed";
		}
	}
	/**
	 * 删除站点-逻辑删
	 * @param siteId
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/deleteSite.bsh")
	@ResponseBody
	public Object deleteSite(String siteId, HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		return siteManageI.virtualDeleteSiteById(siteId);
	}
	/**
	 * 查询某个站点的信息带覆盖区域
	 * @param siteId
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/selecOneSiteInfo.bsh")
	@ResponseBody
	public Object selecOneSiteInfo(String siteId, HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		return siteManageI.selecOneSiteInfo(siteId);
	}
	/**
	 * 将JSON转为Map 仅支持1级
	 * 
	 * @param jsonString
	 * @return
	 */
	public static Map<String, String> json2Map(String jsonString)
	{
		net.sf.json.JSONObject jsonObject = net.sf.json.JSONObject.fromObject(jsonString);
		Map<String, String> map = new HashMap<String, String>();
		for (Iterator<?> iter = jsonObject.keys(); iter.hasNext();)
		{
			String key = (String) iter.next();
			map.put(key, String.valueOf(jsonObject.get(key)));
		}
		return map;
	}
	
	@Resource
	private SiteManageI siteManageI;
}
