package com.bshinfo.web.service.serviceI.adminManage;

import java.util.List;
import java.util.Map;

import com.bshinfo.web.model.dataTableUtil.DataTableModel;

/**
 * adminManage业务逻辑层接口 <br/>
 * 
 * 功能描述: 对adminManage的各种业务处理 <BR/>
 * 
 * @author 	[zhangxinhua]
 * 
 * @see 	[AdminManageServiceI]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017年2月20日10:48:08]
 */
public interface AdminManageServiceI 
{
	/***
	 * 查询后台管理中管理员的信息
	 * 
	 * @return [DataTableModel] 
	 * 
     * @author 	[zhangxinhua]
     * 
     * @Date    [2017年2月20日10:49:42]
	 */
	DataTableModel selectAdminInfo(Map<String, String> paramMap,String nowUserId);
	
	/**
	 * 通过用户ID查询管辖站点
	 * 
	 * @author [zhangxinhua]
	 * 
	 * @param userId
	 * 
	 * @return List<Map<String, Object>>
	 * 
	 * #Date   [2017年2月20日17:03:05]
	 */
	List<Map<String, Object>> getSelectList(String userId);
	
	/***
	 * 查询后台管理的所有角色
	 * 
	 * @Param [java.util.Map<String, String>] dataTableMap 前台传过来的数据
	 * 
	 * @return [com.bshinfo.web.model.dataTableUtil.DataTableModel] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月21日16:41:03]
	 */
	DataTableModel roleList(Map<String, String> dataTableMap,String nowUserId);
	
	/***
	 * 批量添加用户和角色的关联关系
	 * 
	 * @Param [List<Map<String, Object>>] list 前台传过来经controller处理过的的数据
	 * 
	 * @return [Interger] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月22日09:40:18]
	 */
	Integer addUserRoleBatch(String userids,String roleids,String nowUserId);
	
	/***
	 * 批量添加用户和站点的关联关系
	 * 
	 * @Param [List<Map<String, Object>>] list 前台传过来经controller处理过的的数据
	 * 
	 * @return [Interger] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月22日09:40:18]
	 */
	Integer addUserSiteBatch(String userids,String siteids,String nowUserId);
	
	/***
	 * 查询角色
	 * 
	 * @Param [Map<String, Object>] paramMap 前台的数据
	 * 
	 * @return [List<Map<String, Object>>] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月22日10:53:19]
	 */
	List<Map<String, Object>> getRoleByUserid(Map<String, Object> paramMap);
	
	/***
	 * 批量更新用户和角色的关联关系
	 * 
	 * @Param [List<Map<String, Object>>] list 前台传过来经controller处理过的的数据
	 * 
	 * @return [Interger] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月22日09:40:18]
	 */
	Integer updateUserRoleBatch(String userids,String roleids,String nowUserId);
	
	/***
	 * 批量更新用户和站点的关联关系
	 * 
	 * @Param [List<Map<String, Object>>] list 前台传过来经controller处理过的的数据
	 * 
	 * @return [Interger] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月22日09:40:18]
	 */
	Integer updateUserSiteBatch(String userids,String siteids,String nowUserId);
	
	/***
	 * 通过ID获取该用户创建的用户，将这些用户查询出来
	 * 
	 * @Param [List<Map<String, Object>>] list 前台传过来经controller处理过的的数据
	 * 
	 * @return [List<Map<String, Object>>] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月22日13:53:26]
	 */
	List<Map<String, Object>> getManageUsersByUserid(String userId);
	
	/***
	 * 通过ID删除用户的角色
	 * 
	 * @Param [List<Map<String, Object>>] list 前台传过来经controller处理过的的数据
	 * 
	 * @return [boolean] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月22日13:53:26]
	 */
	boolean deleteUserBatch(String userids,String nowUserId);
	
	/***
	 * 获取树
	 * 
	 * @Param [List<Map<String, Object>>] list 前台传过来经controller处理过的的数据
	 * 
	 * @return [List<Map<String, Object>>] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月22日13:53:26]
	 */
	List<Map<String, Object>> getGroupTree(String nowUserId);
	
	/***
     * 获取角色权限
     * 
     * @Param [List<Map<String, Object>>] list 前台传过来经controller处理过的的数据
     * 
     * @return [List<Map<String, Object>>] 
     * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月28日10:54:35]
     */
	List<Map<String, Object>> roleSourceAndMenu(String roleId);
	
	/***
     * 获取站点表格数据
     * 
     * @Param [List<Map<String, Object>>] list 前台传过来经controller处理过的的数据
     * 
     * @return [DataTableModel] 
     * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月28日10:54:35]
     */
	DataTableModel getSite(Map<String, String> dataTableMap,String nowUserId);
	
}
