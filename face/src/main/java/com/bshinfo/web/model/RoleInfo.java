package com.bshinfo.web.model;

/**
 * 角色实体类<br/>
 * 
 * 功能描述: 显示角色实体<BR/>
 * 
 * @author 	[hezhijian]
 * 
 * @see 	[roleInfo]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-2-17 13:21:31]
 */
public class RoleInfo implements java.io.Serializable
{
	/** 
	 * java 序列化实现 (实体类必须添加)
	 */ 
	private static final long serialVersionUID = 2541591973992973292L;
	
	// 主键id
	private String id;
	// 角色名称
	private String role_name;
	// 角色编码
	private String role_code;
	// 角色描述
	private String role_desc;
	// 创建时间
	private String create_time;
	// 更新时间
	private String update_time;
	// 创建人ID：bsh_base_user_info.id
	private String create_user_id;
	// 是否删除标识：0-未删除，1-已删除,：默认未删除 0
	private Integer isDel;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRole_name() {
		return role_name;
	}
	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}
	public String getRole_code() {
		return role_code;
	}
	public void setRole_code(String role_code) {
		this.role_code = role_code;
	}
	public String getRole_desc() {
		return role_desc;
	}
	public void setRole_desc(String role_desc) {
		this.role_desc = role_desc;
	}
	public String getCreate_time() {
		return create_time;
	}
	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}
	public String getUpdate_time() {
		return update_time;
	}
	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}
	public String getCreate_user_id() {
		return create_user_id;
	}
	public void setCreate_user_id(String create_user_id) {
		this.create_user_id = create_user_id;
	}
	public Integer getIsDel() {
		return isDel;
	}
	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}
	
}
