package com.bshinfo.web.model;

/**
 * 菜单实体类<br/>
 * 
 * 功能描述: 显示后台管理端左侧菜单的实体<BR/>
 * 
 * @author 	[jixingwang]
 * 
 * @see 	[MainMenu]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-2-17 13:21:31]
 */
public class MainMenu implements java.io.Serializable
{
	/** 
	 * java 序列化实现 (实体类必须添加)
	 */ 
	private static final long serialVersionUID = -4284311863506142277L;
	
	// 主键id
	private String id;
	// 资源名称
	private String resourceName;
	// 资源编码
	private String resourceCode;
	// 资源描述
	private String resourceDesc;
	// 资源链接地址
	private String resourceUrlAddress;
	// 资源图标
	private String resourceIcon;
	// 资源排序字段
	private Integer resourceOrder;
	// 资源父节点ID
	private String parentId;
	// 创建时间
	private String createTime;
	// 更新时间
	private String updateTime;
	// 是否删除标识：0-未删除，1-已删除,：默认未删除 0
	private Integer isDel;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getResourceName() {
		return resourceName;
	}
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}
	public String getResourceCode() {
		return resourceCode;
	}
	public void setResourceCode(String resourceCode) {
		this.resourceCode = resourceCode;
	}
	public String getResourceDesc() {
		return resourceDesc;
	}
	public void setResourceDesc(String resourceDesc) {
		this.resourceDesc = resourceDesc;
	}
	public String getResourceUrlAddress() {
		return resourceUrlAddress;
	}
	public void setResourceUrlAddress(String resourceUrlAddress) {
		this.resourceUrlAddress = resourceUrlAddress;
	}
	public String getResourceIcon() {
		return resourceIcon;
	}
	public void setResourceIcon(String resourceIcon) {
		this.resourceIcon = resourceIcon;
	}
	public Integer getResourceOrder() {
		return resourceOrder;
	}
	public void setResourceOrder(Integer resourceOrder) {
		this.resourceOrder = resourceOrder;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getIsDel() {
		return isDel;
	}
	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}
}
