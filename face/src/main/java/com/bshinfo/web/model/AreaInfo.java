package com.bshinfo.web.model;

/**
 * 区域实体类<br/>
 * 
 * 功能描述: 显示区域实体<BR/>
 * 
 * @author 	[hezhijian]
 * 
 * @see 	[AreaInfo]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-3-1 11:24:27]
 */
public class AreaInfo implements java.io.Serializable
{
	/** 
	 * java 序列化实现 (实体类必须添加)
	 */ 
	private static final long serialVersionUID = 8791773157131594744L;
	
	// 主键id
	private String id;
	// 区域名称：市、县、区的名称
	private String area_name;
	// 区域类型：1-省，2-市，3-区或者县
	private String area_type;
	// 区域编码
	private String area_code;
	// 区域顺序：根据国家规定的行政区划排定顺序
	private Integer area_order;
	// 区域邮编
	private String area_zip_code;
	// 所属父级区域ID
	private String parent_id;
	// bsh_base_user_info.id
	private String create_user_id;
	// 创建时间
	private String create_time;
	// 更新时间
	private String update_time;
	// 是否删除标识：0-未删除，1-已删除,：默认未删除 0
	private Integer isDel;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getArea_name() {
		return area_name;
	}
	public void setArea_name(String area_name) {
		this.area_name = area_name;
	}
	public String getArea_type() {
		return area_type;
	}
	public void setArea_type(String area_type) {
		this.area_type = area_type;
	}
	public String getArea_code() {
		return area_code;
	}
	public void setArea_code(String area_code) {
		this.area_code = area_code;
	}
	public Integer getArea_order() {
		return area_order;
	}
	public void setArea_order(Integer area_order) {
		this.area_order = area_order;
	}
	public String getArea_zip_code() {
		return area_zip_code;
	}
	public void setArea_zip_code(String area_zip_code) {
		this.area_zip_code = area_zip_code;
	}
	public String getParent_id() {
		return parent_id;
	}
	public void setParent_id(String parent_id) {
		this.parent_id = parent_id;
	}
	public String getCreate_user_id() {
		return create_user_id;
	}
	public void setCreate_user_id(String create_user_id) {
		this.create_user_id = create_user_id;
	}
	public String getCreate_time() {
		return create_time;
	}
	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}
	public String getUpdate_time() {
		return update_time;
	}
	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}
	public Integer getIsDel() {
		return isDel;
	}
	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}
	
}
