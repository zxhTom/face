package com.bshinfo.web.dao.adminManage;

import java.util.List;
import java.util.Map;


import com.bshinfo.web.base.model.Page;

/**
 * adminManage数据访问层接口 <br/>
 * 
 * 功能描述: 对adminManage的原子性操作 <BR/>
 * 
 * @author 	[zhangxinhua]
 * 
 * @see 	[com.bshinfo.web.model.UserInfo]
 * 
 * @see 	[com.bshinfo.web.model.RoleInfo]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017年2月20日10:34:12]
 */
public interface AdminManageMapper 
{
	/***
	 * 查询数量
	 * 
	 * @return [List<Map<String, Object>>] 
	 * 
     * @author 	[zhangxinhua]
     * 
     * @Date    [2017年2月20日10:49:42]
	 */
	List<Map<String, Object>> getCount(Map<String, Object> param);
	/***
	 * 查询后台管理中管理员的信息
	 * 
	 * @return [List<Map<String, Object>>] 
	 * 
     * @author 	[zhangxinhua]
     * 
     * @Date    [2017年2月20日10:49:42]
	 */
	List<Map<String, Object>> selectAdminInfo(Page<Map<String, Object>> page);
	
	/**
	 * 通过用户ID查询管辖站点
	 * 
	 * @author [zhangxinhua]
	 * 
	 * @param userId
	 * 
	 * @return List<Map<String, Object>>
	 * 
	 * #Date   [2017年2月20日17:03:05]
	 */
	List<Map<String, Object>> getSiteByUserId(Map<String, Object> param);
	
	/**
     * 通过用户ID查询管辖站点
     * 
     * @author [zhangxinhua]
     * 
     * @param userId
     * 
     * @return List<Map<String, Object>>
     * 
     * #Date   [2017年2月20日17:03:05]
     */
    List<Map<String, Object>> getAllSite();
	
	
	/**
	 * 查询所有角色
	 * 
	 * @param [com.bshinfo.web.base.model.Page<Map<String, Object>>] page 前台传过来的参数
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author 	[zhangxinhua]
     * 
     * @Date    [2017年2月21日16:42:51]
	 */
	List<Map<String, Object>> roleList(Page<Map<String, Object>> page);
	/**
     * 查询所有角色
     * 
     * @param [com.bshinfo.web.base.model.Page<Map<String, Object>>] page 前台传过来的参数
     * 
     * @return [java.util.List<Map<String, Object>>] 
     * 
     * @author  [zhangxinhua]
     * 
     * @Date    [2017年2月21日16:42:51]
     */
    List<Map<String, Object>> allRoleList(Page<Map<String, Object>> page);
	
	/**
	 * 查询角色总数
	 * 
	 * @param [java.util.Map<String, Object>] param 前台传过来的参数
	 * 
	 * @return [java.lang.Integer] 
	 * 
     * @author 	[zhangxinhua]
     * 
     * @Date    [2017年2月21日16:42:47]
	 */
	Integer findRoleCount(Map<String, Object> param);
	
	/**
     * 查询角色总数
     * 
     * @param [java.util.Map<String, Object>] param 前台传过来的参数
     * 
     * @return [java.lang.Integer] 
     * 
     * @author  [zhangxinhua]
     * 
     * @Date    [2017年2月21日16:42:47]
     */
    Integer findAddRoleCount(Map<String, Object> param);
	
	/**
	 * 批量添加用户角色关联关系
	 * 
	 * @param [List<Map<String, Object>>] list 前台传过来的参数
	 * 
	 * @return [java.lang.Integer] 
	 * 
     * @author 	[zhangxinhua]
     * 
     * @Date    [2017年2月22日09:42:02]
	 */
	Integer addUserRoleBatch(List<Map<String, Object>> list);
	
	/***
	 * 批量添加用户和站点的关联关系
	 * 
	 * @Param [List<Map<String, Object>>] list 前台传过来经controller处理过的的数据
	 * 
	 * @return [java.lang.Interger] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月22日09:40:18]
	 */
	Integer addUserSiteBatch(List<Map<String, Object>> list);
	
	/***
	 * 查询角色
	 * 
	 * @Param [Map<String, Object>] paramMap 前台的数据
	 * 
	 * @return [List<Map<String, Object>>] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月22日10:53:19]
	 */
	List<Map<String, Object>> getRoleByUserid(Map<String, Object> paramMap);
	
	/***
	 * 删除用户角色对应关系
	 * 
	 * @Param [Map<String, Object>] paramMap 前台的数据
	 * 
	 * @return [java.lang.Integer] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月22日13:08:08]
	 */
	Integer deleteUserRoleByUserIds(List<Map<String, Object>> list);
	
	/***
	 * 删除用户站点对应关系
	 * 
	 * @Param [Map<String, Object>] paramMap 前台的数据
	 * 
	 * @return [java.lang.Integer] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月22日13:08:13]
	 */
	Integer deleteUserSiteByUserIds(List<Map<String, Object>> list);
	
	/***
	 * 通过ID获取该用户创建的用户，将这些用户查询出来
	 * 
	 * @Param [Map<String, Object>] paramMap 前台传过来经controller处理过的的数据
	 * 
	 * @return [List<Map<String, Object>>] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月22日13:56:11]
	 */
	List<Map<String, Object>> getManageUsersByUserConf(Map<String, Object> paramMap);
	
	/***
	 * 将修改创建人
	 * 
	 * @Param [Map<String, Object>] paramMap 前台传过来经controller处理过的的数据
	 * 
	 * @return [java.lang.Integer] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月22日13:56:11]
	 */
	Integer updateCreateUserId(Map<String, Object> paramMap);
	
	/***
	 * 将修改site创建人
	 * 
	 * @Param [Map<String, Object>] paramMap 前台传过来经controller处理过的的数据
	 * 
	 * @return [java.lang.Integer] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月22日13:56:11]
	 */
	Integer updateSiteCreateUserId(Map<String, Object> paramMap);
	
	/***
	 * 删除用户和角色的对应关系
	 * 
	 * @Param [Map<String, Object>] paramMap 前台传过来经controller处理过的的数据
	 * 
	 * @return [java.lang.Integer] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月22日13:56:11]
	 */
	Integer deleteUser(String user_id);
	
	/***
	 * 删除用户和站点的对应关系
	 * 
	 * @Param [Map<String, Object>] paramMap 前台传过来经controller处理过的的数据
	 * 
	 * @return [java.lang.Integer] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月22日13:56:11]
	 */
	Integer deleteSiteUser(String user_id);
	
	/***
	 * 获取树
	 * 
	 * @Param [Map<String, Object>] paramMap 前台传过来经controller处理过的的数据
	 * 
	 * @return [List<Map<String, Object>>] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月22日13:56:11]
	 */
	List<Map<String, Object>> getTree(Map<String, Object> paramMap);
	/***
     * 管理员获取树
     * 
     * @Param [Map<String, Object>] paramMap 前台传过来经controller处理过的的数据
     * 
     * @return [List<Map<String, Object>>] 
     * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月22日13:56:11]
     */
    List<Map<String, Object>> adminGetTree();
	
	/***
	 * 判断用户是否已经拥有该权限了
	 * 
	 * @Param [Map<String, Object>] paramMap 前台传过来经controller处理过的的数据
	 * 
	 * @return [java.lang.Integer] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月22日13:56:11]
	 */
	Integer isExiteByUserAndRole(Map<String, Object> paramMap);

	/***
	 * 判断用户是否已经管辖该站点了
	 * 
	 * @Param [Map<String, Object>] paramMap 前台传过来经controller处理过的的数据
	 * 
	 * @return [java.lang.Integer] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月22日13:56:11]
	 */
	Integer isExiteByUserAndSite(Map<String, Object> paramMap);
	
	/***
     * 通过角色获取角色权限的菜单及资源
     * 
     * @Param [Map<String, Object>] paramMap 前台传过来经controller处理过的的数据
     * 
     * @return [List<Map<String, Object>>] 
     * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月28日10:47:21]
     */
	List<Map<String, Object>> getSourceOrSourceMenu(Map<String, Object> paramMap);
	
	/***
     * 获取资源
     * 
     * @Param [List<Map<String, Object>>] list 前台传过来经controller处理过的的数据
     * 
     * @return [List<Map<String, Object>>] 
     * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月28日10:47:21]
     */
	List<Map<String, Object>> getSource(List<Map<String, Object>> list);
	
	/***
     * 获取资源
     * 
     * @Param [List<Map<String, Object>>] list 前台传过来经controller处理过的的数据
     * 
     * @return [List<Map<String, Object>>] 
     * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月28日10:47:21]
     */
    List<Map<String, Object>> getAllSource();
	/***
     * 获取资源菜单
     * 
     * @Param [List<Map<String, Object>>] list 前台传过来经controller处理过的的数据
     * 
     * @return [List<Map<String, Object>>] 
     * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月28日10:47:21]
     */
    List<Map<String, Object>> getMenu(List<Map<String, Object>> list);
    
    /***
     * 获取资源菜单
     * 
     * @Param [List<Map<String, Object>>] list 前台传过来经controller处理过的的数据
     * 
     * @return [List<Map<String, Object>>] 
     * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年2月28日10:47:21]
     */
    List<Map<String, Object>> getAllMenu();
    /***
     * 获取站点表格数据
     * 
     * @Param [Map<String, Object>] paramMap 前台传过来经controller处理过的的数据
     * 
     * @return [List<Map<String, Object>>] 
     * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年3月2日10:57:43]
     */
    List<Map<String, Object>> getTableSite(Page<Map<String, Object>> page);
    /***
     * 获取站点表格数据数量
     * 
     * @Param [Map<String, Object>] paramMap 前台传过来经controller处理过的的数据
     * 
     * @return [Integer] 
     * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年3月2日10:57:43]
     */
    Integer getTableSiteCount(Map<String, Object> paramMap);
    /***
     * 获取所有站点表格数据
     * 
     * @Param [Map<String, Object>] paramMap 前台传过来经controller处理过的的数据
     * 
     * @return [List<Map<String, Object>>] 
     * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年3月2日10:57:43]
     */
    List<Map<String, Object>> getAllTableSite(Page<Map<String, Object>> page);
    
    /***
     * 获取所有站点表格数据数量
     * 
     * @Param [Map<String, Object>] paramMap 前台传过来经controller处理过的的数据
     * 
     * @return [Integer] 
     * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年3月2日10:57:43]
     */
    Integer getAllTableSiteCount(Map<String, Object> paramMap);
    /***
     * 通过用户ID获取用户的站点相关数据
     * 
     * @Param [Map<String, Object>] paramMap 前台传过来经controller处理过的的数据
     * 
     * @return [map] 
     * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年3月2日10:57:43]
     */
    Map<String, Object> getSitesByUserId(Map<String, Object> paramMap);
    
    /***
     * 通过用户ID获取用户的角色相关数据
     * 
     * @Param [Map<String, Object>] paramMap 前台传过来经controller处理过的的数据
     * 
     * @return [map] 
     * 
     * @author [zhangxinhua]
     * 
     * @Date    [2017年3月2日10:57:43]
     */
    Map<String, Object> getRolesByUserId(Map<String, Object> paramMap);
}
