package com.bshinfo.web.dao.main;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.bshinfo.web.model.MainMenu;

/**
 * MainMenu数据访问层接口 <br/>
 * 
 * 功能描述: 对MainMenu的原子性操作 <BR/>
 * 
 * @author 	[jixingwang]
 * 
 * @see 	[com.bshinfo.web.model.MainMenu]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-2-17 16:20:25]
 */
public interface MainMenuMapper 
{
	/**
	 * 查询一级菜单
	 * 
	 * @Param [java.lang.String] userId 当前用户的id
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author 	[jixingwang]
     * 
     * @Date    [2017-2-17 16:29:26]
	 */
	List<Map<String, Object>> selectParentMenu(String userId);
	/**
	 * 查询二级菜单
	 * 
	 * @Param [List<String>] parentIds 一级菜单的id的集合
	 * 
	 * @Param [java.lang.String] userId 当前用户的id
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author 	[jixingwang]
     * 
     * @Date    [2017-2-17 16:29:26]
	 */
	List<Map<String, Object>> selectSubMenu(@Param("parentIds") List<String> parentIds,@Param("userId")String userId);
	
	/**
	 * 查询一级或二级菜单的详情
	 * 
	 * @Param [java.lang.String] menuId 菜单id
	 * 
	 * @return [java.util.Map<String, Object>] 
	 * 
     * @author 	[jixingwang]
     * 
     * @Date    [2017-3-1 15:16:00]
	 */
	Map<String, Object> getMenuInfo(String menuId);
	
	/**
	 * 新增一二级菜单
	 * 
	 * @Param [com.bshinfo.web.model.MainMenu] mainMenu 一二级菜单类
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-2 09:25:17]
	 */
	void addMenu(MainMenu mainMenu);
	
	/**
	 * 修改一二级菜单
	 * 
	 * @Param [com.bshinfo.web.model.MainMenu] mainMenu 一二级菜单类
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-2 11:15:33]
	 */
	void updateMenu(MainMenu mainMenu);
	
	/**
	 * 根据id删除一二级菜单
	 * 
	 * @Param [java.lang.String] menuId 菜单id
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-2 11:46:17]
	 */
	void delMenu(String menuId);
	
	/**
	 * 检验同一父级下菜单编号是否唯一
	 * 
	 * @Param [java.lang.String] parentId 父级菜单id
	 * 
	 * @Param [java.lang.String] menuCode 菜单编号
	 * 
	 * @Param [java.lang.String] menuId 修改时自身的id
	 * 
	 * @return [java.lang.Integer] 
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-3 09:35:51]
	 */
	Integer checkCodeByParentId(@Param("parentId") String parentId, @Param("menuCode") String menuCode, @Param("menuId") String menuId);
}
