package com.bshinfo.web.base.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.BoundListOperations;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

/***
 * 类 <code>{RedisCacheTemplate}</code> Redis缓存操作类
 * 
 * @author zhangChao
 * 
 * @since JDK1.7
 * 
 * @version 1.0
 * 
 * @date 2016-12-9 12:19:44
 * 
 */
@Component(value = "redisCacheTemplate")
public class RedisCacheTemplate {
	/***
	 * 日志工具
	 */
	public static Logger logger = Logger.getLogger(RedisCacheTemplate.class);

	/**
	 * 
	 * @param key
	 *            存储的Key值
	 * @param value
	 *            存储的Value值
	 */
	public void put(String key, String value) 
	{
		init();
		ValueOperations<String, Object> ops = clusterRedisTemplate.opsForValue();
		ops.set(key, value.toString());
	}
	
	/**
	 * 加入map集合
	 * @param key
	 * @param map
	 */
	public void put(String key , Map<String, String> map)
	{
	    init();
	    BoundHashOperations<String, String, String> ops = clusterRedisTemplate.boundHashOps(key);
	    for(Map.Entry<String, String> entry : map.entrySet())
	    {
	        ops.put(entry.getKey().toString(), entry.getValue().toString());
	    }
	}
	
	/**
	 * 加入list集合
	 * @param key
	 * @param list
	 */
	public void put(String key,List<Object> list)
    {
        init();
        clusterRedisTemplate.opsForList().leftPushAll(key, list);
    }
	
	/**
     * 向Map中添加元素，覆盖式添加
     * @param key
     * @param value
     */
    public void putAsMapValue(String key,String mapKey , String value)
    {
        init();
        BoundHashOperations<String, String, String> ops = clusterRedisTemplate.boundHashOps(key);
        ops.put(mapKey, value);
    }
	/**
	 * 向list中添加元素，覆盖式添加
	 * @param key
	 * @param value
	 */
	public void putAsListValue(String key,String value)
    {
        init();
        ListOperations<String, Object> opsForList = clusterRedisTemplate.opsForList();
        List<Object> data = getDataFromList(key);
        boolean flag=false;
        for (Object object : data)
        {
            if(value.equals(object.toString()))
            {
                flag=true;
            }
        }
        if(!flag)
        {
            opsForList.leftPush(key, value);
        }
    }
	/**
	 * 删除key
	 */
	 public long del(final String... keys) 
	 {
		 init();
		 for (int i = 0; i < keys.length; i++) 
         {
		     clusterRedisTemplate.delete(keys[i]);
         }
		 return 1l;
	    }
	 
	 /**
	  * 批量删除key
	  */
	 public long del(final List<String> list) 
     {
         init();
         for (int i = 0; i < list.size(); i++) 
         {
             clusterRedisTemplate.delete(list.get(i));
         }
         return 1l;
     }
	 /**
	  * 删除map中指定的数据
	  * @return
	  */
	 public long delDataFromMap(String tableName,final String... keys)
	 {
	     init();
	     BoundHashOperations<String, String, String> ops = clusterRedisTemplate.boundHashOps(tableName);
	     for (int i = 0; i < keys.length; i++) 
         {
	         System.out.println(ops.entries().get(keys[i]));
	         if(ops.hasKey(keys[i]))
	         {
	             ops.delete(keys[i]);
	         }
         }
	     return 1l;
	 }
	 /**
      * 删除map中指定的数据
      * @return
      */
     public long delDataFromList(String key,final String... values)
     {
         init();
         ListOperations<String, Object> opsForList = clusterRedisTemplate.opsForList();
         for (String value : values)
         {
             opsForList.remove(key, 1l, value);
         }
         return 1l;
     }
	/**
	 * Redis获取对象的方法
	 * 
	 * @param key
	 *            获取对象对应的Key值
	 * 
	 * @return {Object}
	 */
	public Object get(String key) 
	{
		init();
		ValueOperations<String, Object> ops = clusterRedisTemplate.opsForValue();
		Object object=ops.get(key);   
		return object;
	}

	public Map<String, String> getMapData(String tableName)
	{
	    init();
	    BoundHashOperations<String, String, String> ops = clusterRedisTemplate.boundHashOps(tableName);
	    Map<String, String> resultMap=new HashMap<String,String>();
	    resultMap=ops.entries();
	    return resultMap;
	}
	public List<Object> getDataFromList(String key)
    {
//      return clusterRedisTemplate.opsForList().rightPop(key);
	    init();
        BoundListOperations<String,Object> listOps = clusterRedisTemplate.boundListOps(key);
        List<Object> list = listOps.range(0, listOps.size());
        return list;
    }
	/** Redis模板注入 */
	@Resource
	private RedisTemplate<String, Object> clusterRedisTemplate;
	@Resource
	private RedisClusterConfiguration redisClusterConfig;
	private JedisConnectionFactory redis4CacheConnectionFactory;
	//重构连接池
	private void init()
	{
		redis4CacheConnectionFactory=new JedisConnectionFactory(redisClusterConfig);
		redis4CacheConnectionFactory.afterPropertiesSet();
		clusterRedisTemplate.setConnectionFactory(redis4CacheConnectionFactory);
	}
}
