package com.bshinfo.web.service.serviceI.area;

import java.util.List;
import java.util.Map;

/**
 * area业务逻辑层接口 <br/>
 * 
 * 功能描述: 对area的各种业务处理 <BR/>
 * 
 * @author 	[hezhijian]
 * 
 * @see 	[AreaServiceI]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-3-1 11:32:59]
 */
public interface AreaServiceI 
{

	/***
	 * 查询区域树
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-3-1 11:38:23]
	 */
	List<Map<String, Object>> getAreaTree();

	/***
	 * 获取区域名称和编码信息
	 * 
	 * @param [java.lang.String] areaId 区域id
	 * 
	 * @return [java.util.Map<String, Object>] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-3-1 14:15:58]
	 */
	Map<String, Object> getAreaInfoByAreaId(String areaId);
	
	/***
	 * 获取区域所有信息
	 * 
	 * @param [java.lang.String] areaId 区域id
	 * 
	 * @return [java.util.Map<String, Object>] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-3-1 15:32:12]
	 */
	Map<String, Object> getAreaAllInfoByAreaId(String areaId);

	/***
	 * 新增的时候检测名字是否重复
	 * 
	 * @param [java.lang.String] areaName 区域名称
	 * 
	 * @param [java.lang.String] parentId 父级Id
	 * 
	 * @return [java.lang.Integer] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-3-1 16:24:22]
	 */
	Integer checkNameAdd(String areaName, String parentId);

	/***
	 * 新增子区域
	 * 
	 * @param [java.util.Map<String, Object>] areaMap 区域信息的集合
	 * 
	 * @return [] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-3-1 16:33:22]
	 */
	void addArea(Map<String, Object> areaMap);

	/***
	 * 修改的时候检测名字是否重复
	 * 
	 * @param [java.lang.String] areaName 区域名称
	 * 
	 * @param [java.lang.String] areaId 区域id
	 * 
	 * @return [java.lang.Integer] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-3-1 16:40:22]
	 */
	Integer checkNameEdit(String areaName, String areaId);

	/***
	 * 修改子区域
	 * 
	 * @param [java.util.Map<String, Object>] areaMap 区域信息的集合
	 * 
	 * @return [] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-3-1 17:05:07]
	 */
	void editArea(Map<String, Object> areaMap);

	/***
	 * 检测是否含有子区域
	 * 
	 * @param [java.lang.String] areaId 区域id
	 * 
	 * @return [java.lang.Integer] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-3-2 09:41:42]
	 */
	Integer checkAreaChild(String areaId);

	/***
	 * 修改子区域
	 * 
	 * @param [java.lang.String] areaId 区域id
	 * 
	 * @return [] 
	 * 
     * @author [hezhijian]
     * 
     * @Date    [2017-3-2 09:52:26]
	 */
	void deleteArea(String areaId);

}
