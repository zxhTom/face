package com.bshinfo.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bshinfo.web.base.cache.RedisCacheTemplate;
import com.bshinfo.web.base.controller.BaseController;
import com.bshinfo.web.model.UserInfo;
import com.bshinfo.web.service.serviceI.main.MainServiceI;

/**
 * MainMenu控制层 <br/>
 * 
 * 功能描述: 对MainMenu做数据获取封装与页面控制跳转 <BR/>
 * 
 * @author 	[jixingwang]
 * 
 * @see 	[MainController]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-2-17 16:41:55]
 */
@Controller
@RequestMapping("/main")
public class MainController extends BaseController
{
	public static Logger logger = Logger.getLogger(MainController.class);

	/**
	 * 获取菜单数据并跳转到主页面main.ftl
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpSession] session 会话对象
	 * 
	 * @return [String] 
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-2-17 16:29:26]
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/menuList.bsh")
	public String memuList(HttpServletRequest request,HttpSession session) 
	{
		List<Map<String, Object>> menu = null;
		
		JSONObject json = new JSONObject();
		
		UserInfo userInfo = (UserInfo) session.getAttribute("userInfo");
		
		//从缓存redis中取数据
		String cacheMenu = (String) redisCacheTemplate.get(userInfo.getId() + "_resource");
		
		//如果缓存中没有就需要取数据库中查出数据放入缓存
		if(cacheMenu == null || "".equals(cacheMenu))
		{
			menu = mainServiceI.selectMenu(userInfo.getId());
			if(menu == null)
			{
				return "baseData/common/empty.jsp";
			}
			json.put("resource", menu);
			redisCacheTemplate.put(userInfo.getId() + "_resource", json.toString());
		}
		else
		{
			JSONObject jsonObj = JSONObject.fromObject(cacheMenu);
			menu = (List<Map<String, Object>>) jsonObj.get("resource");
		}
		
		request.setAttribute("menus", menu);
		return "baseData/common/main.ftl";
	}
	
	/**
	 * 获取菜单数据
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpSession] session 
	 * 
	 * @return [java.util.Map<String, Object>] 
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-1 13:55:29]
	 */
	@ResponseBody
	@RequestMapping("/getMemuJson.bsh")
	public Map<String,Object> getMemuJson(HttpServletRequest request,HttpSession session) 
	{
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		List<Map<String, Object>> menu = mainServiceI.selectMenu(request.getParameter("userId"));
		jsonMap.put("menus", menu);
		return jsonMap;
	}
	
	@Resource
	private MainServiceI mainServiceI;
	@Resource
	private RedisCacheTemplate redisCacheTemplate;
}
