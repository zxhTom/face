package com.bshinfo.web.dao.userManage;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.bshinfo.web.base.model.Page;

/**
 * UserManage数据访问层接口 <br/>
 * 
 * 功能描述: 对UserManage的原子性操作 <BR/>
 * 
 * @author 	[donghang]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-2-20]
 */
public interface UserManageMapper {
	
	/**
	 * 查询用户列表
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-20]
	 */
	List<Map<String,Object>> getUserTable(Page<Map<String, Object>> page);
	
	/**
	 * 根据id查询用户信息
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-20]
	 */
	Map<String, Object> getUserById(String id);
	
	/**
	 * 查询用户总数
	 * 
	 * @return [int] 
	 * 
     * @author 	[donghang]
	 * @param paramMap 
     * 
     * @Date    [2017-2-20]
	 */
	int getUserTableCount(Map<String, Object> paramMap);

	/**
	 * 重置用户密码
	 * 
	 * @return [void] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-20]
	 */
	void resetPassword(String id);

	/**
	 * 根据id删除用户
	 * 
	 * @return [void] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-21]
	 */
	void deleteUserById(String id);
	
	/**
	 * 根据id更改用户状态
	 * 
	 * @return [void] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-21]
	 */
	void changeStatusById(Map<String, Object> paramMap);

	/**
	 * 根据id更改用户实名认证状态
	 * 
	 * @return [void] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-21]
	 */
	void apprUserVerifyById(Map<String, Object> paramMap);

	/**
	 * 根据id更改用户实名状态
	 * 
	 * @return [void] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-21]
	 */
	void changeUserVerifyById(Map<String, Object> paramMap);

	/**
	 * 将用户新增至表user_info
	 * 
	 * @return [void] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-21]
	 */
	void addUserToInfo(Map<String, Object> paramMap);

	/**
	 * 将用户新增至表user_attr
	 * 
	 * @return [void] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-21]
	 */
	void addUserToAttr(Map<String, Object> paramMap);

	/**
	 * 将用户新增至表user_login
	 * 
	 * @return [void] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-21]
	 */
	void addUserToLogin(Map<String, Object> paramMap);

	/**
	 * 修改用户，表user_Info
	 * 
	 * @return [void] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-22]
	 */
	void editUserToInfo(Map<String, Object> paramMap);

	/**
	 * 修改用户，表user_Attr
	 * 
	 * @return [void] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-22]
	 */
	void editUserToAttr(Map<String, Object> paramMap);

	/**
	 * 修改用户，表user_Login
	 * 
	 * @return [void] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-22]
	 */
	void editUserToLogin(Map<String, Object> paramMap);

	/**
	 * 批量修改用户状态
	 * 
	 * @return [void] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-22]
	 */
	void changeUsersStatus(Map<String,Object> paramMap);

	/**
	 * 查询用户管辖站点
	 * 
	 * @return [List<Map<String, Object>>] 
	 * 
     * @author 	[donghang]
     * 
     * @Date    [2017-2-23]
	 */
	List<Map<String, Object>> getRuleSite(String id);

	/**
	 * 获取城市列表
	 * 
	 * @return [List<Map<String, Object>>] 
	 * 
     * @author 	[donghang]
     * 
	 * @param paramMap 
     * 
     * @Date    [2017-2-23]
	 */
	List<Map<String, Object>> getCity(Map<String, Object> paramMap);

	/**
	 * 获取站点树
	 * 
	 * @return [List<Map<String, Object>>] 
	 * 
     * @author 	[donghang]
     * 
	 * @param paramMap 
     * 
     * @Date    [2017-2-23]
	 */
	List<Map<String, Object>> getSiteTree(Map<String, Object> paramMap);

	/**
	 * 查询用户名，手机号，邮箱是否重复
	 * 
	 * @return [int] 
	 * 
     * @author 	[donghang]
     * 
	 * @param paramMap 
     * 
     * @Date    [2017-2-23]
	 */
	int checkUser(Map<String, Object> map);

	/**
	 * 根据站名查找站点id
	 * 
	 * @return [int] 
	 * 
     * @author 	[donghang]
     * 
	 * @param paramMap 
     * 
     * @Date    [2017-2-23]
	 */
	Map<String,Object> querySiteByName(String siteName);
	
	/**
	 * 登录验证
	 * 
	 * @param  [java.lang.String] username 用户登录名称
	 * 
	 * @param  [java.lang.String] password 用户密码
	 * 
	 * @return [java.util.Map<String,Object>] 
	 * 
     * @author 	[jixingwang]
     * 
     * @Date    [2017-2-27 11:10:11]
	 */
	Map<String,Object> loginCheck(@Param("username")String username, @Param("password")String password);

	/**
	 * 删除用户-根据id删除用户关联表的信息
	 * 
	 * @return [int] 
	 * 
     * @author 	[donghang]
     * 
	 * @param paramMap 
     * 
     * @Date    [2017-2-27]
	 */
	void deleteUserRelation(String id);
	
	/**
	 * 根据id获取用户角色权限
	 * 
	 * @return [int] 
	 * 
     * @author 	[donghang]
     * 
	 * @param paramMap 
     * 
     * @Date    [2017-2-27]
	 */
	List<Map<String, Object>> getUserRole(Map<String, Object> map);
}
