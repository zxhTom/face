package com.bshinfo.web.controller;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bshinfo.web.base.cache.RedisCacheTemplate;
import com.bshinfo.web.base.controller.BaseController;
import com.bshinfo.web.base.util.ConstantUtil;
import com.bshinfo.web.base.util.DatatableUtil;
import com.bshinfo.web.model.UserInfo;
import com.bshinfo.web.model.dataTableUtil.DataTableModel;
import com.bshinfo.web.model.dataTableUtil.DataTableParam;
import com.bshinfo.web.service.serviceI.adminManage.AdminManageServiceI;

/**
 * AdminManageMenu控制层 <br/>
 * 
 * 功能描述: 对AdminManageMenu做数据获取封装与页面控制跳转 <BR/>
 * 
 * @author 	[zhangxinhua]
 * 
 * @see 	[AdminManageController]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-2-20 9:40:55]
 */
@Controller
@RequestMapping("/adminManage")
public class AdminManageController extends BaseController
{
	public static Logger logger = Logger.getLogger(AdminManageController.class);

	/**
	 * 获取菜单数据并跳转到主页面adminManage.ftl
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [String] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date   [2017-2-20 10:20:17]
	 */
	@RequestMapping("/adminManageDisplay.bsh")
	public String secondDisplay(HttpServletRequest request,HttpServletResponse response) 
	{
		return "baseData/systemManage/admin/adminManage.ftl";
	}
	
	/**
	 * 主界面数据获取
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [DataTableModel] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date   [2017-2-20 10:20:17]
	 */
	@RequestMapping("/tableDisplay.bsh")
	@ResponseBody
	public Object tableDisplay(HttpServletRequest request,HttpServletResponse response, @RequestBody DataTableParam[] dataTableParams) 
	{
		Map<String, String> dataTableMap = DatatableUtil.convertToMap(dataTableParams);
		//String id=request.getSession().getAttribute("userId").toString()==""?ConstantUtil.ADMIN_ID:request.getSession().getAttribute("userId").toString();
		UserInfo user = (UserInfo) request.getSession().getAttribute("userInfo");
		DataTableModel dataTableModel = adminManageServiceI.selectAdminInfo(dataTableMap,user.getId());
		
		return dataTableModel;
	}
	
	/**
	 * 界面中的站点数据初始化
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [Object] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date   [2017-2-20 10:20:17]
	 */
	@RequestMapping("/getSelectList.bsh")
	@ResponseBody
	public Object getSelectList(HttpServletRequest request,HttpServletResponse response)
	{
		//获取登录中的用户的ID
//	    String id=request.getSession().getAttribute("userId").toString()==""?ConstantUtil.ADMIN_ID:request.getSession().getAttribute("userId").toString();
	    UserInfo user = (UserInfo) request.getSession().getAttribute("userInfo");
		//通过ID查询该用户管辖的站点
		List<Map<String, Object>> selectList = adminManageServiceI.getSelectList(user.getId());
		return selectList;
	}
	
	/**
	 * 处理界面中新增操作，用户  + 角色 + 站点
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [Object] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date   [2017-2-20 10:20:17]
	 */
	@RequestMapping("/addUsersAndRoles.bsh")
	@ResponseBody
	public Object addUsersAndRoles(HttpServletRequest request,HttpServletResponse response)
	{
		Map<String, Object> infoMap=new HashMap<String,Object>();
//		String id=request.getSession().getAttribute("userId").toString()==""?ConstantUtil.ADMIN_ID:request.getSession().getAttribute("userId").toString();
		UserInfo user = (UserInfo) request.getSession().getAttribute("userInfo");
		//获取登录中的用户的ID
		String ids = request.getParameter("ids");
		String Roleids = request.getParameter("Roleids");
		String Siteids = request.getParameter("Siteids");
		Integer userRoleFlag = adminManageServiceI.addUserRoleBatch(ids,Roleids,user.getId());
		Integer userSiteFlag = adminManageServiceI.addUserSiteBatch(ids,Siteids,user.getId());
		infoMap.put("role", userRoleFlag);
		infoMap.put("site", userSiteFlag);
		return infoMap;
	}
	
	/**
	 * 获取菜单数据并跳转到主页面roleList.ftl
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @Param [com.bshinfo.web.model.dataTableUtil.DataTableParam[]] dataTableParams 前台传过来的数组
	 * 
	 * @return [String] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date   [2017年2月21日16:42:20]
	 */
	@RequestMapping("/roleList.bsh")
	@ResponseBody
	public DataTableModel roleList(HttpServletRequest request,HttpServletResponse response, @RequestBody DataTableParam[] dataTableParams) 
	{
		Map<String, String> dataTableMap = DatatableUtil.convertToMap(dataTableParams);
//		String id=request.getSession().getAttribute("userId").toString()==""?ConstantUtil.ADMIN_ID:request.getSession().getAttribute("userId").toString();
		UserInfo user = (UserInfo) request.getSession().getAttribute("userInfo");
		DataTableModel dataTableModel = adminManageServiceI.roleList(dataTableMap,user.getId());
		
		return dataTableModel;
	}
	
	/**
	 * 通过用户ID获取该用户的角色  adminManage.ftl
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @Param [com.bshinfo.web.model.dataTableUtil.DataTableParam[]] dataTableParams 前台传过来的数组
	 * 
	 * @return [String] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date   [2017年2月22日10:49:46]
	 */
	@RequestMapping("/getRoleByUserid.bsh")
	@ResponseBody
	public Object getRoleByUserid(HttpServletRequest request,HttpServletResponse response) 
	{
		Map<String, Object> paramMap=new HashMap<String,Object>();
		//获取用户ID
		String userId=request.getParameter("id");
		paramMap.put("user_id", userId);
		List<Map<String, Object>> roleList = adminManageServiceI.getRoleByUserid(paramMap);
		return roleList;
	}
	
	/**
	 * 更新用户角色关系  adminManage.ftl
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @Param [com.bshinfo.web.model.dataTableUtil.DataTableParam[]] dataTableParams 前台传过来的数组
	 * 
	 * @return [String] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date   [2017年2月22日11:59:28]
	 */
	@RequestMapping("/updateUsersAndRoles.bsh")
	@ResponseBody
	public Object updateUsersAndRoles(HttpServletRequest request,HttpServletResponse response) 
	{
		Map<String, Object> infoMap=new HashMap<String,Object>();
//		String id=request.getSession().getAttribute("userId").toString()==""?ConstantUtil.ADMIN_ID:request.getSession().getAttribute("userId").toString();
		UserInfo user  =(UserInfo) request.getSession().getAttribute("userInfo");
		//获取登录中的用户的ID
		String ids = request.getParameter("ids");
		String Roleids = request.getParameter("Roleids");
		String Siteids = request.getParameter("Siteids");
		String resourceId = request.getParameter("resourceId");
		Integer userRoleFlag = adminManageServiceI.updateUserRoleBatch(ids,Roleids,user.getId());
		infoMap.put("role", userRoleFlag);
		if(!"null".equals(Siteids))
		{
			Integer userSiteFlag = adminManageServiceI.updateUserSiteBatch(ids,Siteids,user.getId());
			JSONObject userIds = JSONObject.fromObject(ids);
	        if(userIds!=null)
	        {
	            Iterator<?> iterator = userIds.keys();
	            while(iterator.hasNext())
	            {
	                String key = (String) iterator.next();
	                String value = userIds.getString(key);
	                redisCacheTemplate.del(value+"_menu_"+resourceId);
	                redisCacheTemplate.del(value+"_resource");
	            }
	        }
			infoMap.put("site", userSiteFlag);
		}else 
		{
			infoMap.put("site", 1);
		}
		return infoMap;
	}
	
	/**
	 * 通过用户的ID获取u信息
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [Object] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date   [2017年2月22日11:59:28]
	 */
	@RequestMapping("/getManageUsersByUserid.bsh")
	@ResponseBody
	public Object getManageUsersByUserid(HttpServletRequest request,HttpServletResponse response) 
	{
		String userId=request.getParameter("id");
		List<Map<String,Object>> userList = adminManageServiceI.getManageUsersByUserid(userId);
		return userList;
	}
	
	/**
	 * 删除操作
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [Object] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date   [2017年2月22日11:59:28]
	 */
	@RequestMapping("/deleteUsersAndRoles.bsh")
	@ResponseBody
	public Object deleteUsersAndRoles(HttpServletRequest request,HttpServletResponse response) 
	{
		Map<String, Object> infoMap=new HashMap<>();
//		String id=request.getSession().getAttribute("userId").toString()==""?ConstantUtil.ADMIN_ID:request.getSession().getAttribute("userId").toString();
		UserInfo user = (UserInfo) request.getSession().getAttribute("userInfo");
		String userIds=request.getParameter("ids");
		boolean flag = adminManageServiceI.deleteUserBatch(userIds,user.getId());
		infoMap.put("flag", flag);
		return infoMap;
	}
	
	/**
	 * 左侧站点树
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [Object] 
	 * 
     * @author [zhangxinhua]
     * 
     * @Date   [2017年2月22日11:59:28]
	 */
	@RequestMapping("/getGroupTree.bsh")
	@ResponseBody
	public Object getGroupTree(HttpServletRequest request,HttpServletResponse response) 
	{
//	    String id=request.getSession().getAttribute("userId").toString()==""?ConstantUtil.ADMIN_ID:request.getSession().getAttribute("userId").toString();
	    UserInfo user = (UserInfo) request.getSession().getAttribute("userInfo");
		return adminManageServiceI.getGroupTree(user.getId());
	}
	
	/**
     * 查看角色权限
     * 
     * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
     * 
     * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
     * 
     * @return [Object] 
     * 
     * @author [zhangxinhua]
     * 
     * @Date   [2017年2月28日10:53:30]
     */
    @RequestMapping("/roleSourceAndMenu.bsh")
    @ResponseBody
    public Object roleSourceAndMenu(HttpServletRequest request,HttpServletResponse response) 
    {
        //获取ID
        String roleId = request.getParameter("roleId");
        return adminManageServiceI.roleSourceAndMenu(roleId);
    }
    
    /**
     * 获取站点数据并进行显示并返回chooseSite.ftl页面
     * 
     * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
     * 
     * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
     * 
     * @return [String] 
     * 
     * @author [zhangxinhua]
     * 
     * @Date   [2017年3月2日10:11:38]
     */
    @RequestMapping("/chooseSite.bsh")
    public String chooseSite(HttpServletRequest request,HttpServletResponse response) 
    {
        return "baseData/systemManage/admin/chooseSite.ftl";
    }
    
    /**
     * 获取站点数据显示
     * 
     * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
     * 
     * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
     * 
     * @return [Object] 
     * 
     * @author [zhangxinhua]
     * 
     * @Date   [2017年3月2日10:11:38]
     */
    @RequestMapping("/getSite.bsh")
    @ResponseBody
    public Object getSite(HttpServletRequest request,HttpServletResponse response,@RequestBody DataTableParam[] dataTableParams) 
    {
//        String id=request.getSession().getAttribute("userId").toString()==""?ConstantUtil.ADMIN_ID:request.getSession().getAttribute("userId").toString();
//        UserInfo user = (UserInfo) request.getSession().getAttribute("userInfo");
        Map<String, String> dataTableMap = DatatableUtil.convertToMap(dataTableParams);
        DataTableModel model = adminManageServiceI.getSite(dataTableMap,ConstantUtil.ADMIN_ID);
        return model;
    }
    
    @RequestMapping("/test.bsh")
    public String test(HttpServletRequest request,HttpServletResponse response) 
    {
        return "baseData/systemManage/admin/test.ftl";
    }
	@Resource
	private AdminManageServiceI adminManageServiceI;
	@Resource
    private RedisCacheTemplate redisCacheTemplate;
}
