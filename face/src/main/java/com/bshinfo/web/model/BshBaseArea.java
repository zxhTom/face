package com.bshinfo.web.model;

import java.io.Serializable;

/**
 * 区域实体类<br/>
 * 
 * 功能描述: 显示在站点列表的左侧区域表<BR/>
 * 
 * @author [wudongwei]
 * 
 * @see [MainMenu]
 * 
 * @since [JDK 1.7]
 * 
 * @version [1.0]
 * 
 * @Date [2017-2-17 13:21:31]
 */
public class BshBaseArea implements Serializable
{
	/**
	 *  java 序列化实现 (实体类必须添加)
	 */
	private static final long serialVersionUID = 423952517482468804L;

	// 主键
	private String id;

	// 区域名称：市、县、区的名称
	private String areaName;

	// 区域类型：1-省，2-市，3-区或者县
	private short areaType;

	// 区域编码
	private String areaCode;

	// 区域顺序：根据国家规定的行政区划排定顺序
	private short areaOrder;

	// 区域邮编
	private String areaZipCode;

	// 所属父级区域ID
	private String parentId;

	// 创建人ID：bsh_base_user_info.id
	private String createUserId;

	// 创建时间
	private java.util.Date createTime;

	// 更新时间
	private java.util.Date updateTime;

	// 是否删除标识:0-未删除，1-已删除,默认0
	private short isDel;

	public String getId()
	{

		return this.id;
	}

	public void setId(String id)
	{

		this.id = id;
	}

	public String getAreaName()
	{

		return this.areaName;
	}

	public void setAreaName(String areaName)
	{

		this.areaName = areaName;
	}

	public short getAreaType()
	{

		return this.areaType;
	}

	public void setAreaType(short areaType)
	{

		this.areaType = areaType;
	}

	public String getAreaCode()
	{

		return this.areaCode;
	}

	public void setAreaCode(String areaCode)
	{

		this.areaCode = areaCode;
	}

	public short getAreaOrder()
	{

		return this.areaOrder;
	}

	public void setAreaOrder(short areaOrder)
	{

		this.areaOrder = areaOrder;
	}

	public String getAreaZipCode()
	{

		return this.areaZipCode;
	}

	public void setAreaZipCode(String areaZipCode)
	{

		this.areaZipCode = areaZipCode;
	}

	public String getParentId()
	{

		return this.parentId;
	}

	public void setParentId(String parentId)
	{

		this.parentId = parentId;
	}

	public String getCreateUserId()
	{

		return this.createUserId;
	}

	public void setCreateUserId(String createUserId)
	{

		this.createUserId = createUserId;
	}

	public java.util.Date getCreateTime()
	{

		return this.createTime;
	}

	public void setCreateTime(java.util.Date createTime)
	{

		this.createTime = createTime;
	}

	public java.util.Date getUpdateTime()
	{

		return this.updateTime;
	}

	public void setUpdateTime(java.util.Date updateTime)
	{

		this.updateTime = updateTime;
	}

	public short getIsDel()
	{

		return this.isDel;
	}

	public void setIsDel(short isDel)
	{

		this.isDel = isDel;
	}
}
