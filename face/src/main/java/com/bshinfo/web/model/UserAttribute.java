package com.bshinfo.web.model;

/**
 * 用户属性信息类<br/>
 * 
 * 功能描述: 后台管理用户属性信息<BR/>
 * 
 * @author 	[donghang]
 * 
 * @see 	[UserAttribute]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-2-20]
 */
public class UserAttribute implements java.io.Serializable
{
	/** 
	 * java 序列化实现 (实体类必须添加)
	 */
	private static final long serialVersionUID = 6855323597058084797L;
	 
	// 主键id
	private String id;
	// 邮箱
	private String userEmail;
	// 电话
	private Integer userPhone;
	// 身份证
	private String idCard;
	// 所在地区：市
	private String addrCity;
	// 所在地区：区/县
	private String addrArea;
	// 所在地区：街道
	private String addrStreet;
	// 学历
	private String eduDegree;
	// 工作单位
	private String jobAddress;
	// 毕业院校
	private String graduationCollege;
	// 身份证正面信息
	private String verifyImgFrontUrl;
	// 身份证反面信息
	private String verifyImgBackUrl;
	// 认证意见
	private String verifyOpinion;
	// 认证状态
	private Integer varifyStatus;
	// 更新时间
	private String updateTime;
	// 创建时间
	private String createTime;
	// 是否删除标识：0-未删除，1-已删除,：默认未删除 0
	private Integer isDel;
	//创建人id
	private String createUserId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public Integer getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(Integer userPhone) {
		this.userPhone = userPhone;
	}
	public String getIdCard() {
		return idCard;
	}
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	public String getAddrCity() {
		return addrCity;
	}
	public void setAddrCity(String addrCity) {
		this.addrCity = addrCity;
	}
	public String getAddrArea() {
		return addrArea;
	}
	public void setAddrArea(String addrArea) {
		this.addrArea = addrArea;
	}
	public String getAddrStreet() {
		return addrStreet;
	}
	public void setAddrStreet(String addrStreet) {
		this.addrStreet = addrStreet;
	}
	public String getEduDegree() {
		return eduDegree;
	}
	public void setEduDegree(String eduDegree) {
		this.eduDegree = eduDegree;
	}
	public String getJobAddress() {
		return jobAddress;
	}
	public void setJobAddress(String jobAddress) {
		this.jobAddress = jobAddress;
	}
	public String getGraduationCollege() {
		return graduationCollege;
	}
	public void setGraduationCollege(String graduationCollege) {
		this.graduationCollege = graduationCollege;
	}
	public String getVerifyImgFrontUrl() {
		return verifyImgFrontUrl;
	}
	public void setVerifyImgFrontUrl(String verifyImgFrontUrl) {
		this.verifyImgFrontUrl = verifyImgFrontUrl;
	}
	public String getVerifyImgBackUrl() {
		return verifyImgBackUrl;
	}
	public void setVerifyImgBackUrl(String verifyImgBackUrl) {
		this.verifyImgBackUrl = verifyImgBackUrl;
	}
	public String getVerifyOpinion() {
		return verifyOpinion;
	}
	public void setVerifyOpinion(String verifyOpinion) {
		this.verifyOpinion = verifyOpinion;
	}
	public Integer getVarifyStatus() {
		return varifyStatus;
	}
	public void setVarifyStatus(Integer varifyStatus) {
		this.varifyStatus = varifyStatus;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public Integer getIsDel() {
		return isDel;
	}
	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
}
