package com.bshinfo.web.base.LogUtil.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/** 
 * 
 * <p>
 * 		Description:<br/>
 * 		
 * 			表示对标记有XXX注解的类,做代理 注解@Retention可以用来修饰注解,是注解的注解,称为元注解。 <br/>
 * 		Retention注解有一个属性value,是RetentionPolicy类型的,Enum RetentionPolicy是一个枚举类型,<br/>
 * 		这个枚举决定了Retention注解应该如何去保持,也可理解为Rentention 搭配 RententionPolicy使用。<br/>
 * 		RetentionPolicy有3个值：CLASS RUNTIME SOURCE <br/>
 * 		用@Retention(RetentionPolicy.CLASS)修饰的注解,表示注解的信息被保留在class文件(字节码文件)中当程序编译时，但不会被虚拟机读取在运行的时候; <br/>
 * 		用@Retention(RetentionPolicy.SOURCE )修饰的注解,表示注解的信息会被编译器抛弃，不会留在class文件中，注解的信息只会留在源文件中; <br/>
 *	  	用@Retention(RetentionPolicy.RUNTIME )修饰的注解,表示注解的信息被保留在class文件(字节码文件)中当程序编译时，会被虚拟机保留在运行时,<br/>
 *		所以他们可以用反射的方式读取。<br/>
 *		RetentionPolicy.RUNTIME 可以让你从JVM中读取Annotation注解的信息,以便在分析程序的时候使用.<br/>
 * </p>	
 * 
 * <p>
 * 			类和方法的annotation缺省情况下是不出现在javadoc中的,为了加入这个性质我们用@Documented <br/>
 *  	java用  @interface Annotation{ } 定义一个注解 @Annotation,一个注解是一个类。 @interface是一个关键字,<br/>
 *  	在设计annotations的时候必须把一个类型定义为@interface，而不能用class或interface关键字  <br/>
 * </p>
 * 
 * @author 	zhangChao
 * 
 * @see     java.lang.annotation(page)
 * 
 * @since  	JDk 1.7
 * 
 * @version 1.0 
 * 
 * @Date    2016-12-16 15:14:33
 *  
 */  
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SystemControllerLog 
{
	
	/**
     * 操作方法描述
     * @return
     */
    String methodDesc() default "";
    
    /**
     * 模块编码
     * @return
     */
    String moudleCode() default "commonEdu-web";

    /**
     * 模块名称
     * @return
     */
    String moudleName() default "社会化学习-web端";

}
