<#assign base=request.contextPath />
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta content="width=device-width, initial-scale=1" name="viewport" />

		<meta content="" name="description" />

		<meta content="" name="author" />

		<title>后台管理</title>

		<!-- BEGIN GLOBAL MANDATORY STYLES -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
		<!-- END GLOBAL MANDATORY STYLES -->
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
		<!-- END PAGE LEVEL PLUGINS -->
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

		<!-- BEGIN THEME GLOBAL STYLES -->
		<link href="${base}/js/library/metronic-4.7/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
		<!-- END THEME GLOBAL STYLES -->
		<!-- BEGIN THEME LAYOUT STYLES -->
		<link href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
		<link href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
		<link href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
		
		<!-- 弹出框 -->
		<link href="${base}/js/library/sweetalert/css/sweetalert.css" rel="stylesheet" type="text/css" />
		<!-- END THEME LAYOUT STYLES -->

	</head>
	
	<body id="navigationManageBody" style="background-color:#FFFFFF;">
		<!-- BEGIN PAGE BAR -->
		<div class="page-bar">
			<div class="page-toolbar">
				<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body"
					data-placement="bottom" data-original-title="Change dashboard date range">
					<i class="icon-calendar"></i>&nbsp; 
					<span class="thin uppercase hidden-xs"></span>&nbsp; 
					<i class="fa fa-angle-down"></i>
				</div>
			</div>
		</div>
		<!-- END PAGE BAR -->

		<!-- BEGIN PAGE TITLE-->
		<h3 class="page-title">
			后台管理 <small>导航菜单管理</small>
		</h3>
		<!-- END PAGE TITLE-->
		<!-- END PAGE HEADER-->
		<div class="row ">
			<div class="col-md-3 ">
				<div class="portlet light bordered" >
					<div style="height:500px;overflow-y:scroll;">
						<div id="getTree" class="tree-demo"> </div>
					</div>
				</div>
			</div>
			<div class="col-md-9 ">
				<div class="portlet light bordered" >
					<div class="row">
						<table class="table table-striped table-hover table-bordered" style="text-align:center">
							<tr>
								<td style="width:50%">菜单编号</td>
								<td id="menuCode">江苏开放大学</td>
							</tr>
							<tr>
								<td style="width:50%">菜单名称</td>
								<td id="menuName">江苏开放大学</td>
							</tr>
							<tr>
								<td style="width:50%">菜单图标</td>
								<td id="menuIcon">江苏开放大学</td>
							</tr>
							<tr>
								<td style="width:50%">菜单类型</td>
								<td id="menuTpye">江苏开放大学</td>
							</tr>
							<tr>
								<td style="width:50%">URL地址</td>
								<td id="menuURL">江苏开放大学</td>
							</tr>
							<tr>
								<td style="width:50%">顺序</td>
								<td id="menuOrder">江苏开放大学</td>
							</tr>
							<tr>
								<td style="width:50%">说明</td>
								<td id="menuDesc">江苏开放大学</td>
							</tr>
						</table>
						<button class="btn green" type="button" style="float:right;margin-right:15px;" onclick="showConfirmModal('确认删除此节点？',delMenu)">删除</button>
            			<button id="updateMenuBtn" class="btn green" type="button" style="float:right;margin-right:15px;" onclick="updateMenu(event)" data-toggle="modal" data-target="#addOrUpdateMenu">修改</button>
            			<button	id="addMenuBtn" class="btn green" type="button" style="float:right;margin-right:15px;" onclick="addMenu(event)" data-toggle="modal" data-target="#addOrUpdateMenu">新增</button>
            		</div>
				</div>
			</div>
		</div>
		<!-- 模态框 -->
		<div id="addOrUpdateMenu" class="modal fade" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title" id="modalTitle"></h4>
					</div>
					<div class="modal-body form">
						<form action="javascript:;" id="addOrEditeRoleInfo" class="form-horizontal form-row-seperated">
							<div class="form-group">
								<label class="col-sm-3 control-label">
									菜单编号
									<span class="required"> * </span>：
								</label>
								<div class="col-sm-8">
									<div class="input-group">
										<input type="text" id="menuCodeModal" name="menuCodeModal" class="form-control input-inline input-medium required" maxlength="20" />
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">
									菜单名称
									<span class="required"> * </span>：
								</label>
								<div class="col-sm-8">
									<div class="input-group">
										<input type="text" id="menuNameModal" name="menuNameModal" class="form-control input-inline input-medium required" maxlength="20" />
									</div>
								</div>
							</div>
							<div id="menuIconDiv" class="form-group">
								<label class="col-sm-3 control-label">
									菜单图标
									<span class="required"> * </span>：
								</label>
								<div class="col-sm-8">
									<div class="input-group">
										<input type="text" id="menuIconModal" name="menuIconModal" class="form-control input-inline input-medium required" maxlength="20" />
									</div>
								</div>
							</div>
							<div id="menuURLDiv" class="form-group">
								<label class="col-sm-3 control-label">
									URL地址
									<span class="required"> * </span>：
								</label>
								<div class="col-sm-8">
									<div class="input-group">
										<input type="text" id="menuURLModal" name="menuURLModal" class="form-control input-inline input-medium required" maxlength="200" />
									</div>
								</div>
							</div>
							<div id="menuOrderDiv" class="form-group">
								<label class="col-sm-3 control-label">
									顺序
									<span class="required"> * </span>：
								</label>
								<div class="col-sm-8">
									<div class="input-group">
										<input type="text" id="menuOrderModal" name="menuOrderModal" class="form-control input-inline input-medium required" maxlength="20" />
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">
									说明
									<span class="required"> * </span>：
								</label>
								<div class="col-sm-8">
									<div class="input-group">
										<textarea class="form-control required" id="menuDescModal" maxlength="200" rows="4" style="margin: 0px -1px 0px 0px;width:300px;"></textarea>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn green" data-dismiss="modal">关闭</button>
								<button type="sumbit" class="btn green">提交</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		
		
		
		<!-- BEGIN CORE PLUGINS -->
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
		<!-- END CORE PLUGINS -->
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/moment.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-CN.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL PLUGINS -->
		<script src="${base}/js/library/metronic-4.7/assets/global/scripts/datatable.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jstree/dist/jstree.min.js" type="text/javascript"></script>
		<!-- BEGIN THEME GLOBAL SCRIPTS -->
		<script src="${base}/js/library/metronic-4.7/assets/global/scripts/app.min.js" type="text/javascript"></script>
		<!-- END THEME GLOBAL SCRIPTS -->
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>

		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->
		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
		<!-- END PAGE LEVEL SCRIPTS -->
		<!-- BEGIN THEME LAYOUT SCRIPTS -->
		<script src="${base}/js/library/metronic-4.7/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>

		<!-- BEGIN 表单判空 -->
		<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery-validation/js/jquery.validate.js" type="text/javascript"></script>
		
		<!-- 弹出框 -->
		<script src="${base}/js/library/sweetalert/js/sweetalert.min.js" type="text/javascript"></script>
		<script src="${base}/js/library/sweetalert/js/showAlert.js" type="text/javascript"></script>
		
		<!-- 获取根路径 -->
		<script src="${base}/js/common/util/jqueryUtil.js" type="text/javascript"></script>
		
		<script>
			//获取菜单id
			var resourceId = "";
			//获取菜单类型
			var resourceType = "";
			//新增或是修改的标记
			var mark = "";
			//被选的节点
			var selectedNode = null;
			
			function showPowerTree() 
			{   
    			$.ajax({
        			type 	 : "POST",
        			dataType : 'json',  
		       	 	url : jQuery.getBasePath() + "/navigation/getPowerTree.bsh",  
		        	success : function(data) 
		        	{  
		        		$('#getTree').jstree(
		        		{
		        			'plugins': ["wholerow", "types"],
		        			'core': 
		        			{
		        				"themes" : 
		        				{
		        					"responsive": false
		        				},
		        				'data':data.tree
		        			},
		        			"types" :
		        			{
		        				"default" : 
		        				{
		        					"icon" : "fa fa-folder icon-state-warning icon-lg"
		        				},
		        				"file" : 
		        				{
		        					"icon" : "fa fa-file icon-state-warning icon-lg"
		        				}
		        			}
		        		}).on("loaded.jstree", function(e, data) 
		        		{
                			var inst = data.instance;
                			var obj = inst.get_node(e.target.firstChild.firstChild.lastChild); 
                			inst.open_node(obj);  
         				}).bind("select_node.jstree", function(e, data) 
         				{
							selectedNode = data.node;
							var searchNodeId = data.node.original.id;
							var searchNodeType = data.node.original.type;
							resourceId = searchNodeId;
							resourceType = searchNodeType;
							
							if(resourceType == -1)
							{
								$("#updateMenuBtn").hide();
							}
							else
							{
								$("#updateMenuBtn").show();
							}
							if(resourceType == 3)
							{
								$("#addMenuBtn").hide();
							}
							else
							{
								$("#addMenuBtn").show();
							}
							if(searchNodeId == -1)
							{
								$("#menuCode").text("江苏开放大学");
								$("#menuName").text("江苏开放大学");
								$("#menuIcon").text("江苏开放大学");
								$("#menuTpye").text("江苏开放大学");
								$("#menuURL").text("江苏开放大学");
								$("#menuOrder").text("江苏开放大学");
								$("#menuDesc").text("江苏开放大学");
							}
							else
							{
								$.ajax(
								{
									type 	 : "POST",
        							dataType : 'json',  
		       	 					url      : jQuery.getBasePath() + "/navigation/getMenuInfo.bsh", 
		       	 					data     : {"menuId":searchNodeId,"menuType":searchNodeType},
		       	 					success  : function(data) 
		       	 					{
		       	 						if(data.success)
		       	 						{
		       	 							$("#menuCode").text(data.menuCode);
											$("#menuName").text(data.menuName);
											$("#menuIcon").text(data.menuIcon);
											$("#menuTpye").text(data.menuTpye);
											$("#menuURL").text(data.menuURL);
											$("#menuOrder").text(data.menuOrder);
											$("#menuDesc").text(data.menuDesc);
		       	 						}
		       	 					},
		       	 					error    : function()
		       	 					{
		       	 						showSuccessOrErrorModal("网络错误！","error");
		       	 					}
								});
							}
						});
		       		 }
		    	});
			}
			
			//点击新增
			function addMenu(event)
			{
				$("#modalTitle").html("新增");
				mark = 1;
				$(".input-group label").hide();
				//置空模态框
				$("#menuCodeModal").val("");
		       	$("#menuNameModal").val("");
		       	$("#menuIconModal").val("");
		       	$("#menuURLModal").val("");
		       	$("#menuOrderModal").val("");
		       	$("#menuDescModal").val("");
				if(!resourceType)
				{
					showInfoModal("请选择左侧节点！");
					event.stopPropagation();
					return;
				}
				if(resourceType == 2)
				{
					$("#menuIconDiv").hide();
					$("#menuURLDiv").hide();
					$("#menuOrderDiv").hide();
				}
				else
				{
					$("#menuIconDiv").show();
					$("#menuURLDiv").show();
					$("#menuOrderDiv").show();
				}
			}
			
			//点击修改
			function updateMenu(event)
			{
				$("#modalTitle").html("修改");
				mark = 2;
				$(".input-group label").hide();
				if(!resourceType)
				{
					showInfoModal("请选择左侧根节点外的节点！");
					event.stopPropagation();
					return;
				}
				
				$.ajax(
				{
					type 	 : "POST",
        			dataType : 'json',  
		       	 	url      : jQuery.getBasePath() + "/navigation/getMenuInfo.bsh", 
		       	 	data     : {"menuType":resourceType,"menuId":resourceId},
		       	 	success  : function(data)
		       	 	{
		       	 		if(data.success)
		       	 		{
		       	 			if(resourceType == 3)
							{
								$("#menuIconDiv").hide();
								$("#menuURLDiv").hide();
								$("#menuOrderDiv").hide();
								$("#menuCodeModal").val(data.menuCode);
		       					$("#menuNameModal").val(data.menuName);
		       					$("#menuDescModal").val(data.menuDesc);
							}
							else
							{
								$("#menuIconDiv").show();
								$("#menuURLDiv").show();
								$("#menuOrderDiv").show();
								$("#menuCodeModal").val(data.menuCode);
		       					$("#menuNameModal").val(data.menuName);
		       					$("#menuIconModal").val(data.menuIcon);
		       					$("#menuURLModal").val(data.menuURL);
		       					$("#menuOrderModal").val(data.menuOrder);
		       					$("#menuDescModal").val(data.menuDesc);
							}
		       	 		}
		       	 		else
		       	 		{
		       	 			showSuccessOrErrorModal(data.msg,"error");
		       	 			event.stopPropagation();
		       	 		}
		       	 	},
		       	 	error    : function()
		       	 	{
		       	 		showSuccessOrErrorModal("网络错误！","error");
		       	 		event.stopPropagation();
		       	 	}
				});
			}
			
			//点击删除
			function delMenu()
			{
				if(!resourceType)
				{
					showSuccessOrErrorModal("请选择左侧根节点外的节点！","error");
					return;
				}
				if(resourceType == -1)
				{
					showSuccessOrErrorModal("根节点无法删除！","error");
					return;
				}
				else
				{
					if(selectedNode.children.length > 0)
					{
						showSuccessOrErrorModal("请先删除该节点的子节点！","error");
						return;
					}
					else
					{
						$.ajax(
						{
							type 	 : "POST",
        					dataType : 'json',  
		       	 			url      : jQuery.getBasePath() + "/navigation/delMenu.bsh", 
		       	 			data     : {"menuType":resourceType,"menuId":resourceId},
		       	 			success  : function(data)
		       	 			{
		       	 				if(data.success)
		       	 				{
		       	 					$('#getTree').jstree(true).settings.core.data=data.tree;
                           			$('#getTree').jstree(true).refresh();
		       	 					showSuccessOrErrorModal(data.msg,"success");
		       	 				}
		       	 				else
		       	 				{
		       	 					showSuccessOrErrorModal(data.msg,"error");
		       	 				}
		       	 			},
		       	 			error    : function()
		       	 			{
		       	 				showSuccessOrErrorModal("网络错误！","error");
		       	 			}
		       	 		});
					}
				}
			}
			
			$(document).ready(function()
			{	
				//初始化树
				showPowerTree();
				//表单提交
				$("#addOrEditeRoleInfo").validate(
				{
					submitHandler : function()
					{
						var targetUrl = "";
						if(mark == 1)
						{
							targetUrl = jQuery.getBasePath() + "/navigation/addMenu.bsh";
						}
						else
						{
							targetUrl = jQuery.getBasePath() + "/navigation/updateMenu.bsh";
						}
						//parentId只在修改时接收
						$.ajax(
						{
							type 	 : "POST",
        					dataType : 'json',  
		       	 			url      : targetUrl, 
		       	 			data     : {
		       	 						"menuCode":$("#menuCodeModal").val(),
		       	 						"menuName":$("#menuNameModal").val(),
		       	 						"menuIcon":$("#menuIconModal").val(),
		       	 						"menuType":resourceType,
		       	 						"menuURL":$("#menuURLModal").val(),
		       	 						"menuOrder":$("#menuOrderModal").val(),
		       	 						"menuDesc":$("#menuDescModal").val(),
		       	 						"resourceId":resourceId,
		       	 						"parentId":selectedNode.parent
		       	 						},
		       	 			success  : function(data)
		       	 			{
		       	 				if(data.success)
		       	 				{
		       	 					showSuccessOrErrorModal(data.msg,"success");
		       	 					$("#addOrUpdateMenu").modal("hide");
		       	 					$('#getTree').jstree(true).settings.core.data=data.tree;
                           			$('#getTree').jstree(true).refresh();
		       	 				}
		       	 				else
		       	 				{
		       	 					showSuccessOrErrorModal(data.msg,"error");
		       	 				}
		       	 			},
		       	 			error    : function()
		       	 			{
		       	 				showSuccessOrErrorModal("网络错误！","error");
		       	 			}	
						});
					}
				});
			});
			$("#navigationManageBody").bind("DOMNodeInserted",function()
			{
				window.top.resizeParentHeightFun();
			});
		</script>
	</body>
</html>