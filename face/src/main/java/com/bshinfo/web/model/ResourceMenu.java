package com.bshinfo.web.model;

/**
 * 按钮实体类<br/>
 * 
 * 功能描述: 页面按钮的实体<BR/>
 * 
 * @author 	[jixingwang]
 * 
 * @see 	[ResourceMenu]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-3-1 18:05:54]
 */
public class ResourceMenu implements java.io.Serializable 
{
	private static final long serialVersionUID = 7881436328993778777L;
	
	// 主键id
	private String id;
	// 页面按钮名称
	private String menuName;
	// 按钮编码
	private String menuCode;
	// 按钮描述
	private String menuDesc;
	// 按钮所属的页面ID，关联表字段为：bsh_base_resource.id
	private String resourceId;
	// 创建时间
	private String createTime;
	// 更新时间
	private String updateTime;
	// 是否删除标识：0-未删除，1-已删除
	private Integer isDel;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public String getMenuCode() {
		return menuCode;
	}
	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}
	public String getMenuDesc() {
		return menuDesc;
	}
	public void setMenuDesc(String menuDesc) {
		this.menuDesc = menuDesc;
	}
	public String getResourceId() {
		return resourceId;
	}
	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getIsDel() {
		return isDel;
	}
	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}
	
}
