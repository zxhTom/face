package com.bshinfo.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bshinfo.web.base.cache.RedisCacheTemplate;
import com.bshinfo.web.base.controller.BaseController;
import com.bshinfo.web.base.util.DateUtil;
import com.bshinfo.web.base.util.UUIDTool;
import com.bshinfo.web.model.MainMenu;
import com.bshinfo.web.model.ResourceMenu;
import com.bshinfo.web.model.UserInfo;
import com.bshinfo.web.service.serviceI.navigation.NavigationServiceI;

/**
 * 导航菜单管理的控制层 <br/>
 * 
 * 功能描述: 对导航菜单管理模块的数据处理与页面跳转  <BR/>
 * 
 * @author 	[jixingwang]
 * 
 * @see 	[NavigationController]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-3-1 10:20:50]
 */
@Controller
@RequestMapping("/navigation")
public class NavigationController extends BaseController
{
	public static Logger logger = Logger.getLogger(NavigationController.class);
	
	/**
	 * 跳转到页面navigationManage.ftl
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [java.lang.String] 
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-1 13:03:34]
	 */
	@RequestMapping("/navigationManage.bsh")
	public String navigationManage(HttpServletRequest request,HttpServletResponse response) 
	{
		return "baseData/systemManage/navigation/navigationManage.ftl";
	}
	
	/**
	 * 查询一二级菜单以及每个页面按钮并转化成树结构
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [java.lang.Object] 
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-1 11:42:07]
	 */
	@RequestMapping("/getPowerTree.bsh")
	@ResponseBody
	public Object getPowerTree(HttpServletRequest request,HttpServletResponse response) 
	{
		List<Map<String,Object>> tree = null;
		
		Map<String,Object> data = new HashMap<String,Object>();
		
		try
		{
			tree = navigationServiceI.getPowerTree();
			data.put("tree", tree);
			data.put("status", "success");
			data.put("msg", "请求数据成功!");
		}
		catch(Exception e)
		{
			logger.error("[请求权限树异常-ErrorMsg:]", e);
			data.put("status", "error");
			data.put("msg", "请求数据异常!");
		}
		
		return data;
	}
	
	/**
	 * 查询一二级菜单或按钮详情
	 * 
	 * @Param [java.lang.String] menuId 菜单id
	 * 
	 * @Param [java.lang.String] menuType 菜单类型    -1.页面根节点    1.一级菜单   2.二级菜单   3.按钮
	 * 
	 * @return [java.lang.Object] 
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-1 15:00:27]
	 */
	@RequestMapping("/getMenuInfo.bsh")
	@ResponseBody
	public Object getMenuInfo(String menuId,String menuType) 
	{
		Map<String,Object> data = null;
		
		try
		{
			data = navigationServiceI.getMenuInfo(menuId, menuType);
			data.put("success", true);
			data.put("msg", "请求数据成功！");
		}
		catch(Exception e)
		{
			logger.error("[请求权限树异常-ErrorMsg:]", e);
			data.put("success", false);
			data.put("msg", "请求数据异常！");
		}
		
		return data;
	}
	
	/**
	 * 新增一二级菜单或按钮
	 * 
	 * @Param [java.lang.String] menuCode 菜单编号
	 * 
	 * @Param [java.lang.String] menuName 菜单名称
	 * 
	 * @Param [java.lang.String] menuIcon 菜单图标
	 * 
	 * @Param [java.lang.String] menuType 父级类型    -1.页面根节点    1.一级菜单   2.二级菜单   3.按钮
	 * 
	 * @Param [java.lang.String] menuURL 菜单地址
	 * 
	 * @Param [java.lang.String] menuOrder 菜单顺序
	 * 
	 * @Param [java.lang.String] menuDesc 菜单描述
	 * 
	 * @Param [java.lang.String] resourceId 父级id
	 * 
	 * @return [java.util.Map<String, Object>] 
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-1 17:08:00]
	 */
	@RequestMapping("/addMenu.bsh")
	@ResponseBody
	public Map<String,Object> addMenu(String menuCode, String menuName, String menuIcon, String menuType, String menuURL, String menuOrder, String menuDesc, String resourceId) 
	{
		Map<String,Object> data = new HashMap<String,Object>();
		//菜单id
		String menuId = "";
		try
		{
			menuId = UUIDTool.randomUUID();
			//此时代表新增的是按钮
			if("2".equals(menuType))
			{
				//验证标号是否可用
				if(navigationServiceI.checkCodeByParentId(resourceId, menuCode, 1, null) == 0)
				{
					ResourceMenu resourceMenu = new ResourceMenu();
					resourceMenu.setId(menuId);
					resourceMenu.setMenuCode(menuCode);
					resourceMenu.setMenuName(menuName);
					resourceMenu.setMenuDesc(menuDesc);
					resourceMenu.setResourceId(resourceId);
					resourceMenu.setCreateTime(DateUtil.getDateString(""));
					resourceMenu.setUpdateTime(DateUtil.getDateString(""));
					resourceMenu.setIsDel(0);
					navigationServiceI.addMenu(null, resourceMenu);
					data.put("success", true);
					data.put("msg", "新增数据成功！");
				}
				else
				{
					data.put("success", false);
					data.put("msg", "编号已存在！");
				}
			}
			else
			{
				//验证标号是否可用
				if(navigationServiceI.checkCodeByParentId(resourceId, menuCode, 2, null) == 0)
				{
					MainMenu mainMenu = new MainMenu();
					mainMenu.setId(menuId);
					mainMenu.setResourceCode(menuCode);
					mainMenu.setResourceName(menuName);
					mainMenu.setResourceIcon(menuIcon);
					mainMenu.setResourceUrlAddress(menuURL);
					mainMenu.setResourceOrder(Integer.valueOf(menuOrder));
					mainMenu.setResourceDesc(menuDesc);
					mainMenu.setCreateTime(DateUtil.getDateString(""));
					mainMenu.setUpdateTime(DateUtil.getDateString(""));
					mainMenu.setIsDel(0);
					//此时代表新增一级菜单
					if("-1".equals(menuType))
					{
						mainMenu.setParentId("-1");
					}
					else
					{
						//此时代表新增二级菜单
						mainMenu.setParentId(resourceId);
					}
					navigationServiceI.addMenu(mainMenu, null);
					data.put("success", true);
					data.put("msg", "新增数据成功！");
				}
				else
				{
					data.put("success", false);
					data.put("msg", "编号已存在！");
				}
			}
			
			//刷新页面左侧树
			List<Map<String, Object>> tree = navigationServiceI.getPowerTree();
			data.put("tree", tree);
			
		}
		catch(Exception e)
		{
			logger.error("[请求权限树异常-ErrorMsg:]", e);
			data.put("success", false);
			data.put("msg", "新增数据失败！");
		}
		
		return data;
	}
	
	/**
	 * 修改一二级菜单或按钮
	 * 
	 * @Param [java.lang.String] menuCode 菜单编号
	 * 
	 * @Param [java.lang.String] menuName 菜单名称
	 * 
	 * @Param [java.lang.String] menuIcon 菜单图标
	 * 
	 * @Param [java.lang.String] menuType 菜单类型     -1.页面根节点    1.一级菜单   2.二级菜单   3.按钮
	 * 
	 * @Param [java.lang.String] menuURL 菜单地址
	 * 
	 * @Param [java.lang.String] menuOrder 菜单顺序
	 * 
	 * @Param [java.lang.String] menuDesc 菜单描述
	 * 
	 * @Param [java.lang.String] resourceId 菜单id
	 * 
	 * @Param [java.lang.String] parentId 父级id
	 * 
	 * @return [java.util.Map<String, Object>] 
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-2 11:03:35]
	 */
	@RequestMapping("/updateMenu.bsh")
	@ResponseBody
	public Map<String,Object> updateMenu(String menuCode, String menuName, String menuIcon, String menuType, String menuURL, String menuOrder, String menuDesc, String resourceId,String parentId) 
	{
		Map<String,Object> data = new HashMap<String,Object>();
		try
		{
			//此时代表修改的是按钮
			if("3".equals(menuType))
			{
				//检验编号是否唯一
				if(navigationServiceI.checkCodeByParentId(parentId, menuCode, 1, resourceId) == 0)
				{
					ResourceMenu resourceMenu = new ResourceMenu();
					resourceMenu.setId(resourceId);
					resourceMenu.setMenuCode(menuCode);
					resourceMenu.setMenuName(menuName);
					resourceMenu.setMenuDesc(menuDesc);
					resourceMenu.setUpdateTime(DateUtil.getDateString(""));
					navigationServiceI.updateMenu(null, resourceMenu);
					data.put("success", true);
					data.put("msg", "修改数据成功！");
				}
				else
				{
					data.put("success", false);
					data.put("msg", "编号已存在！");
				}
				
			}
			else
			{	
				if(navigationServiceI.checkCodeByParentId(parentId, menuCode, 2, resourceId) == 0)
				{
					//此时代表修改一二级菜单
					MainMenu mainMenu = new MainMenu();
					mainMenu.setId(resourceId);
					mainMenu.setResourceCode(menuCode);
					mainMenu.setResourceName(menuName);
					mainMenu.setResourceIcon(menuIcon);
					mainMenu.setResourceUrlAddress(menuURL);
					mainMenu.setResourceOrder(Integer.valueOf(menuOrder));
					mainMenu.setResourceDesc(menuDesc);
					mainMenu.setUpdateTime(DateUtil.getDateString(""));
					navigationServiceI.updateMenu(mainMenu, null);
					data.put("success", true);
					data.put("msg", "修改数据成功！");
				}
				else
				{
					data.put("success", false);
					data.put("msg", "编号已存在！");
				}
			}
			
			//刷新页面左侧树
			List<Map<String, Object>> tree = navigationServiceI.getPowerTree();
			data.put("tree", tree);
			
		}
		catch(Exception e)
		{
			logger.error("[请求权限树异常-ErrorMsg:]", e);
			data.put("success", false);
			data.put("msg", "修改数据失败！");
		}
		
		return data;
	}
	
	/**
	 * 删除一二级菜单或按钮
	 * 
	 * @Param [java.lang.String] menuId 菜单id
	 * 
	 * @Param [java.lang.String] menuType 菜单类型   -1.页面根节点    1.一级菜单   2.二级菜单   3.按钮
	 * 
	 * @return [java.lang.Object] 
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-2 11:42:52]
	 */
	@RequestMapping("/delMenu.bsh")
	@ResponseBody
	public Map<String,Object> delMenu(String menuId,String menuType) 
	{
		Map<String,Object> data = new HashMap<String,Object>();
		
		try
		{
			navigationServiceI.delMenu(menuId, menuType);
			//刷新页面左侧树
			List<Map<String, Object>> tree = navigationServiceI.getPowerTree();
			data.put("tree", tree);
			data.put("success", true);
			data.put("msg", "删除数据成功！");
		}
		catch(Exception e)
		{
			logger.error("[请求权限树异常-ErrorMsg:]", e);
			data.put("success", false);
			data.put("msg", "删除数据异常！");
		}
		
		return data;
	}
	
	/**
	 * 查询一二级菜单或按钮详情
	 * 
	 * @Param [java.lang.String] resourceId 菜单页面id
	 * 
	 * @Param [javax.servlet.http.HttpSession] session 会话对象
	 * 
	 * @return [java.lang.Object] 
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-3 15:59:13]
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/getMenuPower.bsh")
	@ResponseBody
	public Object getMenuPower(String resourceId, HttpSession session) 
	{
		Map<String,Object> data = new HashMap<String,Object>();
		UserInfo userInfo = null;
		JSONObject json = null;
		List<Map<String, Object>> resourceMenus = null;
		try
		{
			userInfo = (UserInfo) session.getAttribute("userInfo");
			//从缓存redis中取数据
			String menuStr = (String) redisCacheTemplate.get(userInfo.getId() + "_menu_" + resourceId);
			json = new JSONObject();
			//如果缓存中没有就需要取数据库中查出数据放入缓存
			if(menuStr == null || "".equals(menuStr))
			{
				resourceMenus = navigationServiceI.getMenuByPower(resourceId, userInfo.getId());
				json.put("menu", resourceMenus);
				redisCacheTemplate.put(userInfo.getId() + "_menu_" + resourceId, json.toString());
			}
			else
			{
				JSONObject jsonObj = JSONObject.fromObject(menuStr);
				resourceMenus = (List<Map<String, Object>>) jsonObj.get("menu");
			}
			
			data.put("resourceMenus", resourceMenus);
			data.put("success", true);
			data.put("msg", "请求数据成功！");
		}
		catch(Exception e)
		{
			logger.error("[请求权限树异常-ErrorMsg:]", e);
			data.put("success", false);
			data.put("msg", "请求数据异常！");
		}
		
		return data;
	}
	
	@Resource
	private NavigationServiceI navigationServiceI;
	@Resource
	private RedisCacheTemplate redisCacheTemplate;
}
