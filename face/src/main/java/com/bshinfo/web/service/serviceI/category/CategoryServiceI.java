package com.bshinfo.web.service.serviceI.category;

import java.util.List;
import java.util.Map;

public interface CategoryServiceI 
{
	/**
	 * 查询所有的课程分类信息
	 * 
	 * @param []
	 * 
	 * @return [java.util.List<Map<String, Object>>]
	 * 
	 * @author [tuxiaokang]
	 * 
	 * @Date [2017-2-28 09:43:12]
	 */
	public List<Map<String, Object>> getAllCategory();

	/**
	 * 根据当前分类ID查询出上级所有分类
	 * 
	 * @param [java.lang.String] id 分类ID
	 * 
	 * @return [java.util.List<Map<String, Object>>]
	 * 
	 * @author [tuxiaokang]
	 * 
	 * @Date [2017-2-28 09:43:12]
	 */
	public List<Map<String, Object>> getUpCategory(String id);

	/**
	 * 根据当前分类ID插入对应的下级分类信息
	 * 
	 * @param [java.util.Map<String, Object>]
	 * 
	 * @return [int]
	 * 
	 * @author [tuxiaokang]
	 * 
	 * @Date [2017-2-28 09:43:12]
	 */
	public int insertAddCategory(Map<String, Object> insertAddSort);

	/**
	 * 根据当前分类ID删除对应的分类信息
	 * 
	 * @param [java.util.String]
	 * 
	 * @return [int]
	 * 
	 * @author [tuxiaokang]
	 * 
	 * @Date [2017-2-28 09:43:12]
	 */
	public Integer delCategory(String id);

	/**
	 * 编辑当前分类
	 * 
	 * @param [java.util.Map<String, Object>]
	 * 
	 * @return [int]
	 * 
	 * @author [tuxiaokang]
	 * 
	 * @Date [2017-2-28 09:43:12]
	 */
	public int editCategory(Map<String, Object> editSort);

	/**
	 * 根据当前分类ID是否有下级分类
	 * 
	 * @param [java.util.Map<String, Object>]
	 * 
	 * @return [int]
	 * 
	 * @author [tuxiaokang]
	 * 
	 * @Date [2017-2-28 09:43:12]
	 */
	public Integer selectupId(Map<String, Object> selectupId);

	/**
	 * 根据当前分类父ID删除对应的下级分类信息
	 * 
	 * @param [java.util.Map<String, Object>]
	 * 
	 * @return [int]
	 * 
	 * @author [tuxiaokang]
	 * 
	 * @Date [2017-2-28 09:43:12]
	 */
	public int delAllupCategory(Map<String, Object> selectupId);

	
	
	
	/**
	 * 查询是否重名
	 * 
	 * @param [java.util.Map<String, Object>]
	 * 
	 * @return [int]
	 * 
	 * @author [tuxiaokang]
	 * 
	 * @Date [2017-2-28 09:43:12]
	 */
	public int ischeck(Map<String, Object> idcheck);
	
	/**
	 * 查询出最大序号
	 * 
	 * @param [java.util.Map<String, Object>]
	 * 
	 * @return [int]
	 * 
	 * @author [tuxiaokang]
	 * 
	 * @Date [2017-2-28 09:43:12]
	 */
	public int querySeq();
	
	/**
	 * 查询根id
	 * 
	 * @param [java.util.Map<String, Object>]
	 * 
	 * @return [int]
	 * 
	 * @author [tuxiaokang]
	 * 
	 * @Date [2017-2-28 09:43:12]
	 */
	public String queryId();
	
	
}
