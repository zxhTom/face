package com.bshinfo.web.service.serviceI.main;

import java.util.List;
import java.util.Map;

/**
 * MainMenu业务逻辑层接口 <br/>
 * 
 * 功能描述: 对MainMenu的各种业务处理 <BR/>
 * 
 * @author 	[jixingwang]
 * 
 * @see 	[MainServiceI]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-2-17 16:20:25]
 */
public interface MainServiceI 
{
	/***
	 * 查询后台管理的左侧所有菜单（即一级菜单与二级菜单）
	 * 
	 * @Param [java.lang.String] userId 当前用户的id
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author 	[jixingwang]
     * 
     * @Date    [2017-2-17 16:29:26]
	 */
	List<Map<String, Object>> selectMenu(String userId);
}
