package com.bshinfo.web.service.serviceI.site;

import java.util.Map;

import com.bshinfo.web.model.dataTableUtil.DataTableModel;

/**
 * 站点管理业务逻辑层接口 <br/>
 * 
 * 功能描述: 对MainMenu的各种业务处理 <BR/>
 * 
 * @author 	[SiteManageI]
 * 
 * @see 	[MainServiceI]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-2-17 16:20:25]
 */
public interface SiteManageI
{
	/***
	 * 查询后台管理的左侧的区域树（即江苏-江苏在线学习-各省-各区）
	 * 
	 * @return [java.lang.String] 
	 * 
     * @author 	[wudongwei]
     * 
     * @Date    [2017-2-17 16:29:26]
	 */
	String selectAreaTree();
	
	/***
	 * 分页带条件查询所有的站点
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author 	[wudongwei]
     * 
     * @Date    [2017-2-17 16:29:26]
	 */
	DataTableModel selectAllSiteByCondition(Map<String,String> conditionMap);
	
	/**
	 * 查询某个站点的信息
	 * @param siteId
	 * @return
	 */
	Object selecOneSiteInfo(String siteId);
	/***
	 * 新增站点（即江苏-江苏在线学习-各省-各区）
	 * 
	 * @return [boolean] 
	 * 
     * @author 	[wudongwei]
     * 
     * @Date    [2017-2-17 16:29:26]
	 */
	Boolean addNewSite(String siteInfo,String isAll,String cityId,String listArea);
	
	/***
	 * 编辑站点信息
	 * 
	 * @return [boolean] 
	 * 
     * @author 	[wudongwei]
     * 
     * @Date    [2017-2-17 16:29:26]
	 */
	Boolean modifySiteInfoByInfo(String info,Boolean isCoverAreaModify, String isAll, String cityId, String listArea);
	/***
	 * 启用 某站点
	 * 
	 * @return [boolean] 
	 * 
     * @author 	[wudongwei]
     * 
     * @Date    [2017-2-17 16:29:26]
	 */
	Boolean startSiteById(String uuid);
	
	/***
	 * 暂停 某站点
	 * 
	 * @return [boolean] 
	 * 
     * @author 	[wudongwei]
     * 
     * @Date    [2017-2-17 16:29:26]
	 */
	Boolean pauseSiteById(String uuid);
	
	/***
	 * 逻辑删除站点
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author 	[wudongwei]
     * 
     * @Date    [2017-2-17 16:29:26]
	 */
	String virtualDeleteSiteById(String uuid);
}
