package com.bshinfo.web.service.serviceI.navigation;

import java.util.List;
import java.util.Map;

import com.bshinfo.web.model.MainMenu;
import com.bshinfo.web.model.ResourceMenu;

/**
 * 导航菜单管理的业务逻辑层接口 <br/>
 * 
 * 功能描述: 对导航菜单管理模块的各种业务处理 <BR/>
 * 
 * @author 	[jixingwang]
 * 
 * @see 	[NavigationServiceI]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-3-1 10:46:40]
 */
public interface NavigationServiceI 
{
	/**
	 * 查询一二级菜单以及每个页面按钮
	 * 
	 * @return [java.util.List<Map<String, Object>>] 
	 * 
     * @author 	[jixingwang]
     * 
     * @Date    [2017-3-1 11:19:59]
	 */
	List<Map<String, Object>> getPowerTree();
	
	/**
	 * 查询一二级菜单或按钮详情
	 * 
	 * @Param [java.lang.String] menuId 菜单id
	 * 
	 * @Param [java.lang.String] menuType 菜单类型
	 * 
	 * @return [java.util.Map<String, Object>] 
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-1 15:00:27]
	 */
	Map<String, Object> getMenuInfo(String menuId,String menuType);
	
	/**
	 * 新增页面菜单或者按钮 
	 * 
	 * @Param [com.bshinfo.web.model.MainMenu] mainMenu 一二级菜单类
	 * 
	 * @Param [com.bshinfo.web.model.ResourceMenu] resourceMenu 按钮类
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-2 09:24:06]
	 */
	void addMenu(MainMenu mainMenu,ResourceMenu resourceMenu);
	
	/**
	 * 修改页面菜单或者按钮 
	 * 
	 * @Param [com.bshinfo.web.model.MainMenu] mainMenu 一二级菜单类
	 * 
	 * @Param [com.bshinfo.web.model.ResourceMenu] resourceMenu 按钮类
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-2 09:24:06]
	 */
	void updateMenu(MainMenu mainMenu,ResourceMenu resourceMenu);
	
	/**
	 * 根据id删除菜单或按钮
	 * 
	 * @Param [java.lang.String] menuId 菜单id
	 * 
	 * @Param [java.lang.String] menuType 菜单类型
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-2 11:46:17]
	 */
	void delMenu(String menuId,String menuType);
	
	/**
	 * 检验同一父级下菜单编号是否唯一
	 * 
	 * @Param [java.lang.String] parentId 父级菜单id
	 * 
	 * @Param [java.lang.String] menuId 修改时本身的id
	 * 
	 * @Param [int] menuType 菜单类型   1.按钮    2.一二级菜单
	 * 
	 * @Param [java.lang.String] menuCode 菜单编号
	 * 
	 * @return [java.lang.Integer] 
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-3 09:35:51]
	 */
	Integer checkCodeByParentId(String parentId,String menuCode,int menuType, String menuId);
	
	/**
	 * 获取页面按钮权限
	 * 
	 * @Param [java.lang.String] resourceId 二级菜单id
	 * 
	 * @Param [java.lang.String] userId 当前用户id
	 * 
	 * @return [java.util.List<Map<String,Object>>] 
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-3 16:45:27]
	 */
	List<Map<String,Object>> getMenuByPower(String resourceId, String userId);
}
