package com.bshinfo.web.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bshinfo.web.base.controller.BaseController;
import com.bshinfo.web.model.UserInfo;
import com.bshinfo.web.service.serviceI.userManage.UserManageService;

/**
 * 登录功能的控制层 <br/>
 * 
 * 功能描述: 登录功能的后台验证与页面跳转  <BR/>
 * 
 * @author 	[jixingwang]
 * 
 * @see 	[LoginController]
 * 
 * @since 	[JDK 1.7] 
 * 
 * @version [1.0]
 * 
 * @Date    [2017-2-27 10:44:04]
 */
@Controller
@RequestMapping("/login")
public class LoginController extends BaseController
{
	public static Logger logger = Logger.getLogger(LoginController.class);
	
	/**
	 * 跳转到页面login.ftl
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpServletResponse] response 响应对象
	 * 
	 * @return [java.lang.String] 
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-2-27 10:48:23]
	 */
	@RequestMapping("/loginJump.bsh")
	public String loginJump(HttpServletRequest request,HttpServletResponse response) 
	{
		return "baseData/common/login.ftl";
	}
	
	/**
	 * 接收页面login.ftl发送的ajax请求，并做用户名/密码查询验证
	 * 
	 * @Param [java.lang.String] username 用户名  
	 * 
	 * @Param [java.lang.String] password 密码
	 * 
	 * @Param [javax.servlet.http.HttpSession] session 会话对象
	 * 
	 * @return [java.lang.Object] 
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-2-27 13:11:10]
	 */
	@ResponseBody
	@RequestMapping("/loginCheck.bsh")
	public Object loginCheck(String username, String password, HttpSession session) 
	{
		Map<String, Object> userMap = userManageService.loginCheck(username, password);
		
		if(userMap != null && userMap.size() > 0)
		{
			if("0".equals(userMap.get("userStatus").toString()))
			{
				userMap.put("msg", "该账号无效！");
				userMap.put("success", false);
			}
			if("1".equals(userMap.get("userStatus").toString()))
			{
				userMap.put("success", true);
				Map<String, Object> map = userManageService.getUserById(userMap.get("id").toString());
				UserInfo userInfo = new UserInfo();
				userInfo.setId(map.get("id").toString());
				if(map.get("real_name") == null)
				{
					map.put("real_name", "");
				}
				if(map.get("user_nick") == null)
				{
					map.put("user_nick", "");
				}
				userInfo.setRealName(map.get("real_name").toString());
				userInfo.setUserNick(map.get("user_nick").toString());
				userInfo.setUserSex(Integer.valueOf(map.get("user_sex").toString()));
				userInfo.setSiteId(map.get("site_id").toString());
				session.setAttribute("userInfo", userInfo);
				session.setAttribute("username", username);
			}
		}
		else
		{
			userMap = new HashMap<String, Object>();
			userMap.put("msg", "用户名/密码错误！");
			userMap.put("success", false);
		}
		return userMap;
	}
	
	/**
	 * 注销后跳转到登录页面login.ftl
	 * 
	 * @Param [javax.servlet.http.HttpServletRequest] request 请求对象  
	 * 
	 * @Param [javax.servlet.http.HttpSession] session 会话对象
	 * 
	 * @return [java.lang.String] 
	 * 
     * @author [jixingwang]
     * 
     * @Date   [2017-3-3 09:11:20]
	 */
	@RequestMapping("/layOut.bsh")
	public String layOut(HttpServletRequest request,HttpSession session) 
	{
		session.removeAttribute("userInfo");
		session.removeAttribute("username");
		return "baseData/common/login.ftl";
	}
	
	@Resource
	private UserManageService userManageService;
}
