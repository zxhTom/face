<#assign base=request.contextPath />
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta content="width=device-width, initial-scale=1" name="viewport" />

<meta content="" name="description" />

<meta content="" name="author" />

<title>管理员管理</title>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/select2/css/select2.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/select2/css/select2-bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/morris/morris.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/fullcalendar/fullcalendar.min.css"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/jstree/dist/themes/default/style.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/datatables.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css"
	rel="stylesheet" type="text/css" />

<!-- BEGIN THEME GLOBAL STYLES -->
<link
	href="${base}/js/library/metronic-4.7/assets/global/css/components.min.css"
	rel="stylesheet" id="style_components" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/global/css/plugins.min.css"
	rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link
	href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/layout.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/themes/darkblue.min.css"
	rel="stylesheet" type="text/css" id="style_color" />
<link
	href="${base}/js/library/metronic-4.7/assets/layouts/layout/css/custom.min.css"
	rel="stylesheet" type="text/css" />
<!-- 弹出框 -->
<link href="${base}/js/library/sweetalert/css/sweetalert.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
</head>

<body style="background-color:#FFFFFF;" id="manager">
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
	<div class="page-toolbar">
		<div id="dashboard-report-range"
			class="pull-right tooltips btn btn-sm" data-container="body"
			data-placement="bottom"
			data-original-title="Change dashboard date range">
			<i class="icon-calendar"></i>&nbsp; 
			<span class="thin uppercase hidden-xs"></span>&nbsp; 
			<i class="fa fa-angle-down"></i>
		</div>
	</div>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
		<h3 class="page-title">
			后台管理 <small>管理员管理</small>
		</h3>
		<!-- END PAGE TITLE-->
	<div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label col-md-3"> 用户名 </label>
						<div class="col-md-9">
							<input type="text" id="user_name" name="name"
								class="form-control" />
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label col-md-3"> 姓名 </label>
						<div class="col-md-9">
							<input type="text" id="real_name" name="name"
								class="form-control" />
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label col-md-3"> 授权者 </label>
						<div class="col-md-9">
							<input type="text" id="create_user_name" name="name"
								class="form-control" />
						</div>
					</div>
				</div>
			</div>
			<div class="row" style="margin-top: 30px;margin-bottom: 30px;">
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label col-md-5"> 管辖站点 </label>
						<div class="col-md-7">
							<select class="form-control" name="select" id="site_id">
								<option value="">Select...</option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-8">
					<label class="control-label col-md-4"> 创建时间： </label>
					<div
						class="input-group input-large date-picker input-daterange"
						data-date-format="yyyy-mm-dd">
						<input type="text" id="fromTime" class="form-control" readonly />
						<span class="input-group-addon">-</span> <input type="text"
							id="toTime" class="form-control" readonly />
					</div>
				</div>
				<div class="row"
					style="margin-top:20px; margin-right:30px; float: right;">
					<div class="col-md-6">
						<button type="button" class="btn green" onclick="adminSearch()">查询</button>
					</div>
					<div class="col-md-6">
						<button type="button" class="btn green" onclick="clearSearch()">清空</button>
					</div>
				</div>
			</div>
			<table class="table table-striped table-hover table-bordered"
				id="adminTable">
			</table>

			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row"></div>
				</div>
			</div>

			<!-- 模态框 -->
			<div id="myModal" class="modal fade" role="dialog" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true"></button>
							<h4 class="modal-title">新增</h4>
						</div>
						<div class="modal-body form">
							<form action="#" class="form-horizontal form-row-seperated" id="addUserRoleForm">
								<div class="form-group">
									<label class="col-sm-3 control-label">管理员 ： </label>
									<div class="col-sm-8">
										<div class="input-group">
											<a href="javascript:;" id="selectUserShow" onclick="initTree()">选择人员</a>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">分配角色 ： </label>
									<div class="col-sm-8">
										<div class="input-group">
											<a href="javascript:;"  id="selectRoleShow" onclick="reloadRoleTable()">分配</a>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">管辖站点 ： </label>
									<div class="col-sm-8">
										<div class="input-group">
											<div id="reDraw">
											<select class="form-control select2-multiple" multiple
												name="select" id="selectSiteShow">
												<optgroup label="">
													<!-- <option value="test">test</option> -->
												</optgroup>
											</select>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn grey-salsa btn-outline"
								data-dismiss="modal">关闭</button>
							<button type="button" class="btn green" onclick="lastAdd()">提交</button>
						</div>
					</div>
				</div>
			</div>
			<!--新增模态框结束 -->
			<!-- 编辑模态框开始 -->
			<div id="myModalUpdate" class="modal fade" role="dialog"
				aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true"></button>
							<h4 class="modal-title">编辑</h4>
						</div>
						<div class="modal-body form">
							<form action="#" class="form-horizontal form-row-seperated">
								<div class="form-group">
									<label class="col-sm-3 control-label">管理员 ： </label>
									<div class="col-sm-8">
										<div class="input-group">
											<label id="selectedUserShow">选择人员</label>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">分配角色 ： </label>
									<div class="col-sm-8">
										<div class="input-group">
											<a href="javascript:;" id="selectedRoleShow" onclick="reloadRoleTable()">分配</a>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">管辖站点 ： </label>
									<div class="col-sm-8">
										<div class="input-group">
											<select class="form-control select2-multiple" multiple
												name="select" id="selectedSiteShow">
												<optgroup label="">
													<!-- <option value="test">test</option> -->
												</optgroup>
											</select>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn grey-salsa btn-outline"
								data-dismiss="modal">关闭</button>
							<button type="button" class="btn green" onclick="lastupdate()">更新</button>
						</div>
					</div>
				</div>
			</div>
			<!-- 编辑模态框结束 -->
			<!-- 嵌套模态框 -->
			<!-- 模态框（Modal） -->
			<div id="selectUser" class="modal fade" role="dialog"
				aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true"></button>
							<h4 class="modal-title">选择人员</h4>
						</div>
						<div class="modal-body">
							<div class="row" style="height:400px; ">
								<div class="col-md-3" style="height:400px;">
									<!-- 树 -->
									<div id="tree" class="tree-demo">
										
									</div>
									<input type="hidden" id="site">
								</div>
								<div class="col-md-9">
									<div style="height:100px;">
										<!-- 搜索栏 -->
										<div style="height:50px;">
											<div class="form-group">
												<label class="col-sm-3 control-label">用户名 ： </label>
												<div class="col-sm-8">
													<div class="input-group">
														<input type="text" id="selectUserName"
															class="form-control">
													</div>
												</div>
											</div>
										</div>
										<div style="height:50px; float: right;">
											<div class="form-group">
												<button type="button" class="btn green"
													onclick="selectUserSearch()">查询</button>
												<button type="button" class="btn green"
													onclick="selectUserClear()">清空</button>
											</div>
										</div>
									</div>
									<div style="height:300px;">
										<table class="table table-striped table-hover table-bordered"
											id="selectUserTable">
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn green"
								onclick="getSelectedUserId()">选择</button>
							<button type="button" class="btn grey-salsa btn-outline"
								data-dismiss="modal">关闭</button>
						</div>
					</div>
				</div>
			</div>
			<!-- /.modal -->
			<!-- 角色模态框开始 -->
			<div id="selectRole" class="modal fade" role="dialog"
				aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true"></button>
							<h4 class="modal-title">选择角色</h4>
						</div>
						<div class="modal-body">
							<div class="row" style="height:400px; ">
								<div class="col-md-3" style="height:400px;">1</div>
								<div class="col-md-9">
									<div style="height:100px;">
										<!-- 搜索栏 -->
										<div style="height:50px;">
											<div class="form-group">
												<label class="col-sm-3 control-label">角色名称 ： </label>
												<div class="col-sm-8">
													<div class="input-group">
														<input type="text" id="selectRoleName"
															class="form-control">
													</div>
												</div>
											</div>
										</div>
										<div style="height:50px; float: right;">
											<div class="form-group">
												<button type="button" class="btn green"
													onclick="selectRoleSearch()">查询</button>
												<button type="button" class="btn green"
													onclick="selectRoleClear()">清空</button>
											</div>
										</div>
									</div>
									<div style="height:300px;">
										<table class="table table-striped table-hover table-bordered"
											id="selectRoleTable">
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn green"
								onclick="getSelectedRoleId()">选择</button>
							<button type="button" class="btn grey-salsa btn-outline"
								data-dismiss="modal">关闭</button>
						</div>
					</div>
				</div>
			</div>
			<!-- 角色模态框结束 -->
			<!-- 角色权限列表模态框 -->
			<div class="modal fade" id="roleListModal" tabindex="-1" role="dialog"
				aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">×</button>
							<h4 class="modal-title">权限列表</h4>
						</div>
						<div class="modal-body">
							<div class="row" id="sourceAndMenuList">
								
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">关闭</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- 角色权限列表模态框结束 -->
			<!-- 删除提示模态框 -->
			<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog"
				aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">×</button>
							<h4 class="modal-title" id="myModalLabel">删除 提示</h4>
						</div>
						<div class="modal-body">
							<div class="row">确定要删除<label><font color="red" id="deleteRoleName"></font>角色吗？以下人员将被您拥有</label></div>
							<div id="deleteUsers"></div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">关闭</button>
							<button type="button" class="btn btn-primary" onclick="deleteUser()">确定</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- 删除模态框结束 -->
			<!-- 模态框 -->
			<div id="viewModal" class="modal fade" role="dialog"
				aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true"></button>
							<h4 class="modal-title">查看用户信息</h4>
						</div>
						<div class="tabbable tabbable-tabdrop">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tab1" data-toggle="tab">个人学习</a>
								</li>
								<li><a href="#tab2" data-toggle="tab">课程学习</a></li>
								<li><a href="#tab3" data-toggle="tab">证书学习</a></li>
								<li><a href="#tab4" data-toggle="tab">培训学习</a></li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="tab1">
									<div class="modal-body form">
										<form action="#" class="form-horizontal form-row-seperated">
											<label class="list-group-item bg-default bg-font-default">基础信息</label>
											<div>
												<p>
													<span style="margin-left:80px;width: 220px;display:inline-block;" 
														id="modal_userinfo_user_name">用户名：Lisa Wong</span>
													 <span style="margin-left:50px;width: 190px;display:inline-block;" 
														id="modal_userinfo_site">所属站点：江苏开放大学</span>
												</P>
											</div>
											<div>
												<p>
													<span style="margin-left:80px;width: 220px;display:inline-block;"  id="modal_userinfo_nick">昵称：烟花散尽13141</span>
													<span style="margin-left:50px;width: 190px;display:inline-block;"  id="modal_userinfo_address">所属地区：烟花散尽13141</span>
												</p>
											</div>
											<label class="list-group-item bg-default bg-font-default">认证信息</label>
											<div>
												<p>
													<span style="margin-left:80px;width: 220px;display:inline-block;" id="modal_userinfo_verify">实名状态：被驳回</span>
												</p>
											</div>
											<div>
												<p>
													<span style="margin-left:80px;width: 220px;display:inline-block;"
														id="modal_userinfo_real_name">真实姓名：李念</span> <span
														style="margin-left:50px;width: 190px;display:inline-block;" id="modal_userinfo_sex">性别：男</span>
												</p>
											</div>
											<div>
												<p>
													<span style="margin-left:80px;width: 220px;display:inline-block;" id="modal_userinfo_card">身份证号：320706XXXXXXXXXXXX</span>
													<span style="margin-left:50px;width: 190px;display:inline-block;" id="modal_userinfo_phone">手机号：139XXXXXXXX</span>
												</p>
											</div>
											<div>
												<p>
													<span style="margin-left:80px;width: 220px;display:inline-block;"
														id="modal_userinfo_job_address">工作单位：XXXXXXXXXXXX</span> <span
														style="margin-left:50px;width: 190px;display:inline-block;" id="modal_userinfo_email">邮箱：139XXXXXXXX@qq.com</span>
												</p>
											</div>
											<div>
												<p>
													<span style="margin-left:80px;width: 220px;display:inline-block;" id="modal_userinfo_edu">学历：本科</span>
													<span style="margin-left:50px;width: 190px;display:inline-block;" id="modal_userinfo_college">大学院校：中国药科大学</span>
												</p>
											</div>
										</form>
									</div>
								</div>
								<div class="tab-pane" id="tab2">
									<p>课程学习</p>
								</div>
								<div class="tab-pane" id="tab3">
									<p>证书学习</p>
								</div>
								<div class="tab-pane" id="tab4">
									<p>培训学习</p>
								</div>
							</div>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn grey-salsa btn-outline"
								data-dismiss="modal">关闭</button>
							<button type="button" class="btn green" data-toggle="modal"
								data-target="#viewModal">提交</button>
						</div>
					</div>
				</div>
			</div>
<!-- BEGIN CORE PLUGINS -->
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-CN.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="${base}/js/library/metronic-4.7/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/jstree/dist/jstree.min.js" type="text/javascript"></script>
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="${base}/js/library/metronic-4.7/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/global/scripts/app.min.js" type="text/javascript"></script>

<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/ui-tree.js" type="text/javascript"></script>
<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="${base}/js/library/metronic-4.7/assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="${base}/js/library/metronic-4.7/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<!-- 弹出框 -->
<script src="${base}/js/library/sweetalert/js/sweetalert.min.js" type="text/javascript"></script>
<script src="${base}/js/library/sweetalert/js/showAlert.js" type="text/javascript"></script>
<!-- 获取根路径 -->
<script src="${base}/js/common/util/jqueryUtil.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script type="text/javascript">
var userTimes=0;
var roleTimes=0;
var positionDataTable = null;
var UserDataTable = null;
var RoleDataTable = null;
var userIds = {};
var userNameShows = {};
var roleIds = {};
var roleNameShows = {};
var authCode=[];

/* 表格结束 */
$(document).ready(function() {
	initSelect();
	auth();
	initTable();
	//initSelectTable();
	//initRoleTable();
	authTitle();
});

initDataPacker();
//权限问题
function auth()
{
	//obj.attributes['site'].nodeValue
	//console.log($(".adminAdd").text());
	//发请求
	$.ajax({
		type : "post",
		dataType : "json",
		"async" : false,
		url : jQuery.getBasePath()+'/navigation/getMenuPower.bsh',
		data : 
		{
			"resourceId" : window.top.currentMenuId
		},
		success : function(data) 
		{
			if (data != "")
			{
				$.each(data.resourceMenus,function(index,value){
					//console.log("sdf"+value.menuCode);
					authCode.push(value.menuCode);
					/* if(value.menuCode=='deleteAll')
					{
						$('.deleteAll').show();
					}
					if(value.menuCode=='adminAdd')
					{
						$('.adminAdd').show();
					}
					$('.'+value.menuCode).attr('disabled',false); */
				});
			}
			else
			{
				showSuccessOrErrorModal("获取权限失败","error");
			}
		},
		error : function(data)
		{
			
		}
	});
}
//表头部分的权限
function authTitle()
{
	if(authCode.indexOf('adminAdd')>-1)
	{
		$("#adminTable").before("<button  class='btn green adminAdd' id='code' code='1' style='float:left;margin-left:15px;' onclick='mainAdd()'>新增</button>");
	}
	if(authCode.indexOf('deleteAll')>-1)
	{
		$("#adminTable").before("<button  class='btn green deleteAll' style='float:left;margin-left:15px;' onclick='showConfirmModal(\"确定要进行批量删除吗？\",deleteUser)' >权限收回</button>");
	}
}
function initDataPacker()
{
	//时间插件汉化
    $('.date-picker').datepicker({
      language: 'zh-CN',
      autoclose: true,
      todayHighlight: true
    });
}
function initTable() 
{
	positionDataTable = $('#adminTable').DataTable(
			{
				"bSort" : true,
				"bProcessing" : true,
				"bServerSide" : true,
				//"async" : false,
				"bAutoWidth" : false,
				//"retrieve" : true,//保证只有一个table实例  
				"bStateSave" : false,
				"sAjaxSource" : jQuery.getBasePath()
						+ "/adminManage/tableDisplay.bsh",
				"sDom" : '<"top"l<"clear">>rt<"bottom"ip<"clear">>',
				"lengthMenu" : [ 3, 25, 75, 100 ],
				"pageLength" : 3,
				"oLanguage" : 
				{
					"sProcessing" : "正在加载数据...",
					"sSearch" : "搜索:",
					"sLengthMenu" : "_MENU_条记录",
					"sZeroRecords" : "没有查到记录",
					"sInfo" : "第  _START_ 条到第  _END_ 条记录,一共  _TOTAL_ 条记录",
					"sInfoEmpty" : "0条记录",
					"oPaginate" : 
					{
						"sPrevious" : "上一页 ",
						"sNext" : " 下一页",
					}
				},
				"aoColumnDefs" : [
						{
							"bSearchable" : true,
							"bVisible" : false,
							"aTargets" : [ 0 ]
						},
						{
							"aTargets" : [ 3 ],
							"mRender" : function(data,type, full) 
							{
								var value=full[0];
								if(data==null||data=='')
								{
									data=full[2];
								}
								return "<a href='' onclick='showUserInfo(\""+value+"\")' data-toggle='modal' data-target='#viewModal'>"+ data+ "</a>";
							}
						},
						{
							"aTargets" : [ 4 ],
							"mRender" : function(data,type, full) 
							{
								if(data==null)
								{
									data="@@@";
								}
								var arr = data.split("@@@");
								return "<a href='' data-toggle='modal' data-target='#viewModal' onclick='showUserInfo(\""+ arr[0]+ "\")' >"+ arr[1]+ "</a>";
							}
						},
						{
							"aTargets" : [ 8 ],
							"mRender" : function(data,type, full) {
								//console.log("124324234234"+authCode.indexOf('adminDelete'));
								//return "<button disabled class='btn btn-xs blue adminUpdate' onclick='adminUpdate(this)' site='"+ full[6]+ "' id='"+ full[0]+ "' name='"+ full[2]+ "' >修改</button>     <button disabled class='btn btn-xs yellow-crusta adminDelete'  onclick='return adminDelete(this)' id='"+full[0]+"' role='"+ full[5]+ "'>删除</button>";
								if(authCode.indexOf('adminUpdate')==-1&&authCode.indexOf('adminDelete')==-1)
								{
									return "<button disabled class='btn btn-xs blue'  onclick='adminUpdate(this)' site='"+ full[6]+ "' id='"+ full[0]+ "' name='"+ full[2]+ "' >修改</button>      <button disabled class='btn btn-xs yellow-crusta'  onclick='return adminDelete(this)' id='"+full[0]+"' role='"+ full[5]+ "'>删除</button>";
								}
								else if(authCode.indexOf('adminUpdate')==-1&&authCode.indexOf('adminDelete')>-1)
								{
									return "<button disabled class='btn btn-xs blue'  onclick='adminUpdate(this)' site='"+ full[6]+ "' id='"+ full[0]+ "' name='"+ full[2]+ "' >修改</button>     <button class='btn btn-xs yellow-crusta'  onclick='return adminDelete(this)' id='"+full[0]+"' role='"+ full[5]+ "'>删除</button>";
								}
								else if(authCode.indexOf('adminUpdate')>-1&&authCode.indexOf('adminDelete')==-1)
								{
									return "<button class='btn btn-xs blue'  class='label label-sm label-info' onclick='adminUpdate(this)' site='"+ full[6]+ "' id='"+ full[0]+ "' name='"+ full[2]+ "' >修改</button>     <button disabled class='btn btn-xs yellow-crusta'  onclick='return adminDelete(this)' id='"+full[0]+"' role='"+ full[5]+ "'>删除</button>";
								}
								else if(authCode.indexOf('adminUpdate')>-1&&authCode.indexOf('adminDelete')>-1)
								{
									return "<button class='btn btn-xs blue' onclick='adminUpdate(this)' site='"+ full[6]+ "' id='"+ full[0]+ "' name='"+ full[2]+ "' >修改</button>     <button class='btn btn-xs yellow-crusta'  onclick='return adminDelete(this)' id='"+full[0]+"' role='"+ full[5]+ "'>删除</button>";
									//return "<button disabled class='btn btn-xs blue'  onclick='adminUpdate(this)' site='"+ full[6]+ "' id='"+ full[0]+ "' name='"+ full[2]+ "' >修改</button>     <button class='btn btn-xs yellow-crusta'  onclick='return adminDelete(this)' id='"+full[0]+"' role='"+ full[5]+ "'>删除</button>";
								}
							}
						} ],
				"aoColumns" : [
						{
							"sTitle" : "id",
							"bSortable" : false
						},
						{
							"sTitle" : '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" id="checkAll" onclick="adminChooseOrNoChoose(this)" /><span></span></label>',
							"bSortable" : false,
							"sClass" : "text-center",
							"fnCreatedCell" : function(nTd, sData, oData,iRow, iCol) 
							{
								var value = oData[0];
								var content = '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" class="testCheckbox" onclick="recordMainId(this)" value="'+value+'" /><span></span></label>';
								$(nTd).html(content);
							}
						}, {
							"sTitle" : "用户名",
							"bSortable" : true,
							"sClass" : "text-center"
						}, {
							"sTitle" : "姓名",
							"bSortable" : true,
							"bAutoWidth" : true,
							"sClass" : "text-center"
						}, {
							"sTitle" : "授权人",
							"bSortable" : false,
							"sClass" : "text-center"
						}, {
							"sTitle" : "已分配角色",
							"bSortable" : true,
							"sClass" : "text-center"
						}, {
							"sTitle" : "管辖站点",
							"bSortable" : true,
							"sClass" : "text-center"
						}, {
							"sTitle" : "创建时间",
							"bSortable" : true,
							"sClass" : "text-center"
						}, {
							"sTitle" : "操作",
							"bSortable" : true,
							"sClass" : "text-center"
						} ],
				// 服务器端，数据回调处理  
				"fnServerData" : function retrieveData(sSource, aoData, fnCallback) 
				{
					aoData.push({
						"name" : "user_name",
						"value" : $('#user_name').val()
					});
					aoData.push({
						"name" : "real_name",
						"value" : $('#real_name').val()
					});
					aoData.push({
						"name" : "create_user_name",
						"value" : $(
								'#create_user_name').val()
					});
					aoData.push({
						"name" : "site_id",
						"value" : $('#site_id').val()
					});
					aoData.push({
						"name" : "fromTime",
						"value" : $('#fromTime').val()
					});
					aoData.push({
						"name" : "toTime",
						"value" : $('#toTime').val()
					});
					$.ajax({
						"type" : "POST",
						"url" : sSource,
						"dataType" : "json",
						"contentType" : "application/json",
						"data" : JSON.stringify(aoData),
						"success" : function(data) 
						{
							fnCallback(data);
						}
					});
				}
			});
}

function initSelectTable() 
{
	UserDataTable = $('#selectUserTable').DataTable(
	{
		"bSort" : true,
		"bProcessing" : true,
		"bServerSide" : true,
		"bStateSave" : false,
		/* "sScrollX": "100%",
		"sScrollXInner": "100%",
		"bScrollCollapse": true, */
		"bAutoWidth" : false,
		"sAjaxSource" : jQuery.getBasePath()+ "/userManage/getUserTable.bsh",
		"sDom" : '<"top"l<"clear">>rt<"bottom"ip<"clear">>',
		"lengthMenu" : [ 3, 20, 30 ],
		"pageLength" : 3,
		"oLanguage" : {
		"sProcessing" : "正在加载数据...",
		"sSearch" : "搜索:",
		"sLengthMenu" : "_MENU_条记录",
		"sZeroRecords" : "没有查到记录",
		"sInfo" : "第  _START_ 条到第  _END_ 条记录,一共  _TOTAL_ 条记录",
		"sInfoEmpty" : "0条记录",
		"oPaginate" : 
		{
			"sPrevious" : "上一页 ",
			"sNext" : " 下一页",
		}
	},
	"aoColumnDefs" : [
		{
			"bSearchable" : true,
			"bVisible" : false,
			"aTargets" : [ 0 ]
		},
		{
			"aTargets" : [ 1 ],
			"mRender" : function(data,type, full) 
			{
				return "<a href='' onclick='viewUser("+ full[0]+ ")' data-toggle='modal' data-target='#viewModal'>"+ data + "</a>";
			}
		},
		{
            "aTargets":[2],
            "mRender":function(data,type,full)
            {
              return "<a href='' onclick='showUserInfo(\""+full[0]+"\")' data-toggle='modal' data-target='#viewModal'>"+data+"</a>";
            } 
        },
		{
			"aTargets" : [ 3 ],
			"mRender" : function(data,type, full) 
			{
				if(data=="1")
				{
                	return "已认证";
                }
				else if(data=="2")
				{
               		return "未认证";
                }
				else if(data=="3")
                {
               		return "待认证";
                }
				else if(data=="4")
				{
               		return "已驳回";
                }
			}
		},
		{
			"aTargets" : [ 4 ],
			"mRender" : function(data,type, full) 
			{
				if (data > 0) 
				{
					return "关闭";
				} 
				else 
				{
					return "启用";
				}
			}
		} ],
	"aoColumns" : [
		{
			"sTitle" : "id",
			"bSortable" : false
		},
		{
			"sTitle" : '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" id="checkAll" onclick="chooseOrNoChoose(this)" /><span></span></label>',
			"bSortable" : false,
			"sClass" : "text-center",
			"fnCreatedCell" : function(nTd, sData, oData,iRow, iCol) 
							{
								var value = oData[0]+"@@@"+oData[2];
								var content = '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" class="userCheckbox" onchange="recordUserId(this)" name="'+ oData[2]+ '" value="'+ value+ '" /><span></span></label>';
								$(nTd).html(content);
							}
		}, 
		{
			"sTitle" : "用户名",
			"bSortable" : true,
			"sWidth" : "120px",
			"sClass" : "text-center"
		}, 
		{
			"sTitle" : "实名认证",
			"sWidth" : "90px",
			"bSortable" : true,
			"sClass" : "text-center"
		}, 
		{
			"sTitle" : "状态",
			"sWidth" : "90px",
			"bSortable" : false,
			"sClass" : "text-center"
		}, 
		{
			"sTitle" : "所属站点",
			"bSortable" : false,
			"sWidth" : "180px",
			"bAutoWidth" : true,
			"sClass" : "text-center"
		} ],
	// 服务器端，数据回调处理  
	"fnServerData" : function retrieveData(sSource, aoData, fnCallback) 
	{
		aoData.push({"name" : "userName","value" : $("#selectUserName").val()});
		aoData.push({"name":"site","value":$("#site").val()});
		aoData.push({"name":"end","value":""});
		aoData.push({"name":"start","value":""});
		/*aoData.push({"name":"realName","value":$("#realName").val()});
		aoData.push({"name":"status","value":$("#status").val()});
		aoData.push({"name":"site","value":$("#site").val()}) */
		$.ajax({
			"type" : "POST",
			"url" : sSource,
			"dataType" : "json",
			"contentType" : "application/json",
			"data" : JSON.stringify(aoData),
			"success" : function(data) 
			{
				if(data.flag)
				{
					fnCallback(data.data);
				}else
				{
					showSuccessOrErrorModal("查询用户出错了","error");
				}
				
			}
		});
	}
	});			
}

function initRoleTable() 
{
	RoleDataTable = $('#selectRoleTable').DataTable(
	{
		"bSort" : true,
		"bProcessing" : true,
		"bServerSide" : true,
		"bStateSave" : false,
		/* "sScrollX": "100%",
		"sScrollXInner": "100%",
    	"bScrollCollapse": true, */
		"bAutoWidth" : false,
		"sAjaxSource" : jQuery.getBasePath()+ "/adminManage/roleList.bsh",
		"sDom" : '<"top"l<"clear">>rt<"bottom"ip<"clear">>',
		"lengthMenu" : [ 3, 20, 30 ],
		"pageLength" : 3,
		"oLanguage" : {
		"sProcessing" : "正在加载数据...",
		"sSearch" : "搜索:",
		"sLengthMenu" : "_MENU_条记录",
		"sZeroRecords" : "没有查到记录",
		"sInfo" : "第  _START_ 条到第  _END_ 条记录,一共  _TOTAL_ 条记录",
		"sInfoEmpty" : "0条记录",
		"oPaginate" : 
		{
			"sPrevious" : "上一页 ",
			"sNext" : " 下一页",
		}
	},
	"aoColumnDefs" : [
	{
		"bSearchable" : true,
		"bVisible" : false,
		"aTargets" : [ 0 ]
	},
	{
		"aTargets" : [ 1 ],
		"mRender" : function(data,type, full) 
		{
			return "<a href='' onclick='viewUser("+ full[0]+ ")' data-toggle='modal' data-target='#viewModal'>"+ data + "</a>";
		}
	} ],
	"aoColumns" : [
	{
		"sTitle" : "id",
		"bSortable" : false
	},
	{
		"sTitle" : '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" id="checkAll" onclick="roleChooseOrNoChoose(this)" /><span></span></label>',
		"bSortable" : false,
		"sWidth" : "70px",
		"sClass" : "text-center",
		"fnCreatedCell" : function(nTd, sData, oData,iRow, iCol) 
		{
			var value = oData[0]+"@@@"+oData[2];
			var content = '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" class="roleCheckbox" onchange="recordRoleId(this)" name="'+ oData[2]+ '" value="'+ value+ '" /><span></span></label>';
			$(nTd).html(content);
		}
	}, 
	{
		"sTitle" : "角色名称",
		"bSortable" : true,
		"sWidth" : "120px",
		"sClass" : "text-center"
	}, 
	{
		"sTitle" : "权限",
		"bSortable" : true,
		"sWidth" : "120px",
		"sClass" : "text-center",
		"fnCreatedCell" : function(nTd, sData, oData,iRow, iCol) 
		{
			var content="<a href='javascript:;' id='"+oData[0]+"' data-toggle='modal' data-target='#roleListModal' onclick='roleSource(this)'>查看</a>";
			$(nTd).html(content);
		}
	},
	{
		"sTitle" : "创建时间",
		"bSortable" : false,
		"sWidth" : "180px",
		"bAutoWidth" : true,
		"sClass" : "text-center"
	} ],
	// 服务器端，数据回调处理  
	"fnServerData" : function retrieveData(sSource, aoData, fnCallback) 
	{
		aoData.push({"name" : "sname","value" : $('#selectRoleName').val()});
		/*aoData.push({"name":"start","value":$("#start").val()});
		aoData.push({"name":"end","value":$("#end").val()});
		aoData.push({"name":"realName","value":$("#realName").val()});
		aoData.push({"name":"status","value":$("#status").val()});
		aoData.push({"name":"site","value":$("#site").val()}) */
		$.ajax({
			"type" : "POST",
			"url" : sSource,
			"dataType" : "json",
			"contentType" : "application/json",
			"data" : JSON.stringify(aoData),
			"success" : function(data) 
			{
				fnCallback(data);
			}
		});
	}
	});					
}
$("#manager").bind("DOMNodeInserted", function() 
{
	window.top.resizeParentHeightFun();
});

//展示用户信息
function showUserInfo(param) 
{
	//console.log("个人信息："+JSON.stringify(param));
	//发送Ajax
	$.ajax({
		type : "post",
		dataType : "json",
		url : jQuery.getBasePath()+ '/userManage/getUserById.bsh',
		data : 
		{
			"id" : param
		},
		success : function(data) 
		{
			if (data.flag) 
			{
				var userDefault = "暂无数据";
				if (data.user.user_name == null|| data.user.user_name == 'null'|| data.user.user_name == '') 
				{
					data.user.user_name = userDefault;
				}
				if (data.user.site_name == null|| data.user.site_name == 'null'|| data.user.site_name == '') 
				{
					data.user.site_name = userDefault;
				}
				if (data.user.user_nick == null|| data.user.user_nick == 'null'|| data.user.user_nick == '') 
				{
					data.user.user_nick = userDefault;
				}
				if (data.user.city == null|| data.user.city == 'null'|| data.user.city == '') 
				{
					data.user.city = userDefault;
				}
				if (data.user.area == null|| data.user.area == 'null'|| data.user.area == '') 
				{
					data.user.area = userDefault;
				}
				if (data.user.verify_status == null|| data.user.verify_status == 'null'|| data.user.verify_status == '') 
				{
					data.user.verify_status = userDefault;
				}
				if(data.user.verify_status==1)
				{
					data.user.verify_status="已认证";
				}
				if(data.user.verify_status==2)
				{
					data.user.verify_status="未认证";
				}
				if(data.user.verify_status==3)
				{
					data.user.verify_status="待认证";
				}
				if(data.user.verify_status==4)
				{
					data.user.verify_status="已驳回";
				}
				if(data.user.user_sex==1)
				{
					data.user.user_sex="男";
				}
				if(data.user.user_sex==2)
				{
					data.user.user_sex="女";
				}
				if(data.user.eduDegree==1)
				{
					data.user.eduDegree="其他";
				}
				if(data.user.eduDegree==2)
				{
					data.user.eduDegree="小学";
				}
				if(data.user.eduDegree==3)
				{
					data.user.eduDegree="初中";
				}
				if(data.user.eduDegree==4)
				{
					data.user.eduDegree="高中/中专";
				}
				if(data.user.eduDegree==5)
				{
					data.user.eduDegree="专科/高职";
				}
				if(data.user.eduDegree==6)
				{
					data.user.eduDegree="大学本科<";
				}
				if(data.user.eduDegree==7)
				{
					data.user.eduDegree="硕士";
				}
				if(data.user.eduDegree==8)
				{
					data.user.eduDegree="博士";
				}
				if (data.user.real_name == null|| data.user.real_name == 'null'|| data.user.real_name == '') 
				{
					data.user.real_name = userDefault;
				}
				if (data.user.user_sex == null|| data.user.user_sex == 'null'|| data.user.user_sex == '') 
				{
					data.user.user_sex = userDefault;
				}
				if (data.user.id_card == null|| data.user.id_card == 'null'|| data.user.id_card == '') 
				{
					data.user.id_card = userDefault;
				}
				if (data.user.user_phone == null|| data.user.user_phone == 'null'|| data.user.user_phone == '') 
				{
					data.user.user_phone = userDefault;
				}
				if (data.user.job_address == null|| data.user.job_address == 'null'|| data.user.job_address == '') 
				{
					data.user.job_address = userDefault;
				}
				if (data.user.user_email == null|| data.user.user_email == 'null'|| data.user.user_email == '') 
				{
					data.user.user_email = userDefault;
				}
				if (data.user.eduDegree == null|| data.user.eduDegree == 'null'|| data.user.eduDegree == '') 
				{
					data.user.eduDegree = userDefault;
				}
				if (data.user.graduation_college == null|| data.user.graduation_college == 'null'|| data.user.graduation_college == '') 
				{
					data.user.graduation_college = userDefault;
				}
				$('#modal_userinfo_user_name').text("用户名：" + data.user.user_name);
				$('#modal_userinfo_site').text("所属站点：" + data.user.site_name);
				$('#modal_userinfo_nick').text("昵称：" + data.user.user_nick);
				$('#modal_userinfo_address').text("所属地区：" + data.user.city + " "+ data.user.area);
				$('#modal_userinfo_verify').text("实名状态：" + data.user.verify_status);
				$('#modal_userinfo_real_name').text("真实姓名：" + data.user.real_name);
				$('#modal_userinfo_sex').text("性别：" + data.user.user_sex);
				$('#modal_userinfo_card').text("身份证号：" + data.user.id_card);
				$('#modal_userinfo_phone').text("手机号：" + data.user.user_phone);
				$('#modal_userinfo_job_address').text("工作单位：" + data.user.job_address);
				$('#modal_userinfo_email').text("邮箱：" + data.user.user_email);
				$('#modal_userinfo_edu').text("学历：" + data.user.eduDegree);
				$('#modal_userinfo_college').text("大学院校：" + data.user.graduation_college);
			}
		}
	});
	return false;
}
//修改
function adminUpdate(obj) {
	roleNameShows={};
	//console.log(obj.attributes['site'].nodeValue);
	$("#selectedSiteShow").select2({
		placeholder : obj.attributes['site'].nodeValue,
		width : null
	});
	/* var arr=obj.attributes['site'].nodeValue.split(",");
	$("span.selection ul").empty();  
	for(var v in arr)
	{
		$("span.selection ul ").append("<li class='select2-selection__choice' title='"+arr[v]+"'>"+arr[v]+"</li>");	
	} */
	//$("span.selection ul ").append("<li class='select2-selection__choice' title='电风扇'><span class='select2-selection__choice__remove' role='presentation'>×</span>水电费1</li>");
	//$("span.selection ul ").append("<li class='select2-selection__choice' title='电风扇'><span class='select2-selection__choice__remove' role='presentation'>×</span>水电费2</li>");
	//$("span.selection ul li:first").after("<li class='select2-search select2-search--inline'><input class='select2-search__field' type='search' tabindex='0' autocomplete='off' autocorrect='off' autocapitalize='off' spellcheck='false' role='textbox' aria-autocomplete='list' placeholder style='width: 0.75em;'></li>");
	//先清空之前数据
	userIds = {};
	roleIds = {};
	$('#selectedUserShow').text(obj.name);
	userIds[obj.id] = obj.id;
	$.ajax({
		type : "post",
		dataType : "json",
		url : 'getRoleByUserid.bsh',
		data : 
		{
			"id" : obj.id
		},
		success : function(data) 
		{
			if (data != "") 
			{
				var roleName = "";
				$.each(data, function(index, value) 
				{
					//console.log(index + "@" + value + "@"+ JSON.stringify(data));
					roleName += value.role_name + ",";
					//roleIds[value.id] = value.id;
				})
				roleName = roleName.substring(0,roleName.length - 1);
				$('#selectedRoleShow').text(roleName);
				//数据准备好 弹出框展示
				$('#myModalUpdate').modal("show");
			}
		}
	});
}
//删除
function adminDelete(obj) 
{
	userIds={};
	var roleName = obj.attributes['role'].nodeValue
	$('#deleteRoleName').text(roleName);
	userIds[obj.id]=obj.id;
	$.ajax({
		type : "post",
		dataType : "json",
		url : 'getManageUsersByUserid.bsh',
		data : 
		{
			"id" : obj.id
		},
		success : function(data) 
		{
			if (data != "") 
			{
				//赋值前清空
				$('#deleteUsers').text("");
				$.each(data,function(index,value)
				{
					$('#deleteUsers').append("<div>"+value.real_name+"</div>");
				});
			}
			if($('#deleteUsers').text()=="")
			{
				$('#deleteUsers').append("<div><font color=red>暂时还没有管理的人员！！！</font></div>");
			}
			//数据准备好 弹出层开始
			$('#deleteModal').modal("show");
		}
	});
	return false;
}
//查询
function adminSearch() 
{
	positionDataTable.draw();
}
//用户选择界面查询
function selectUserSearch() 
{
	UserDataTable.draw();
}
//角色选择界面查询
function selectRoleSearch() 
{
	RoleDataTable.draw();
}
//初始化管辖站点的下拉框
function initSelect() {
	$.ajax({
		type : "post",
		dataType : "json",
		url : 'getSelectList.bsh',
		data : 
		{
			"id" : "1"
		},
		success : function(data) 
		{
			if (data != "")
			{
				$.each(data, function(index, value) 
				{
					//console.log(index + "@" + value + "@"+ JSON.stringify(data));
					$("select[name='select']").append("<option value='"+value.id+"'>"+ value.site_name+ "</option>");
				})
			}
		}
	});
}
//主界面新增
function mainAdd() 
{
	userIds={};
	userNameShows={};
	roleIds={};
	roleNameShows={};
	$('#selectUserShow').text("选择人员");
	$('#selectRoleShow').text("分配");
	/* $('#selectSiteShow').val("");
	$('#addUserRoleForm').reset(); */
	$('#myModal').modal("show");
}
//清空查询状态栏数据
function clearSearch() 
{
	$('#user_name').val("");
	$('#real_name').val("");
	$('#create_user_name').val("");
	$('#site_id').val("");
	$('#fromTime').val("");
	$('#toTime').val("");
	positionDataTable.draw();
}
//主界面全选
function adminChooseOrNoChoose(obj) {
	if ($(obj).is(":checked")) 
	{
		$(".testCheckbox").prop("checked", true);
		$(".testCheckbox").each(function()
		{
		var content=$(this).val();
		userIds[content]=content;
		userNameShows[content]=content;
		});
	} 
	else 
	{
		$(".testCheckbox").prop("checked", false);
		userIds={};
		userNameShows={};
	}
}
//用户选择界面全选反选
function chooseOrNoChoose(obj) 
{
	if ($(obj).is(":checked")) 
	{
		$(".userCheckbox").prop("checked", true);
		$(".userCheckbox").each(function()
		{
			var content=$(this).val();
			var idAndName=content.split("@@@");
			userIds[idAndName[0]]=idAndName[0];
			userNameShows[idAndName[1]]=idAndName[1];
		});
	} 
	else 
	{
		$(".userCheckbox").prop("checked", false);
		userIds={};
		userNameShows={};
		userNameShows[0]="选择人员";
	}
}
//用户选择界面全选反选
function roleChooseOrNoChoose(obj) 
{
	if ($(obj).is(":checked")) 
	{
		$(".roleCheckbox").prop("checked", true);
		$(".roleCheckbox").each(function()
		{
			var content=$(this).val();
			var idAndName=content.split("@@@");
			roleIds[idAndName[0]]=idAndName[0];
			roleNameShows[idAndName[1]]=idAndName[1];
		});
	} 
	else 
	{
		$(".roleCheckbox").prop("checked", false);
		roleIds={};
		roleNameShows={};
		roleNameShows[0]="分配";
	}
}
//获取用户选择界面选择的用户ID
function getSelectedUserId() {
	var nametext = "";
	for ( var o in userNameShows) 
	{
		nametext = nametext + userNameShows[o] + ","
	}
	nametext = nametext.substring(0, nametext.lastIndexOf(","));
	$('#selectUserShow').text(nametext);
	if (nametext == "") 
	{
		showInfoModal("您没有选择任何用户，请重新选择");
		return;
	} 
	else
	{
		$("#selectUser").modal("hide");
	}
}
//实时记录状态
function recordMainId(obj) 
{
	//console.log(obj.value);
	obj.value=obj.value.split("@@@")[0];
	if ($(obj).is(":checked") == true) 
	{
		userIds[obj.value] = obj.value;
		userNameShows[obj.value] = obj.name;
	} else 
	{
		delete userIds[obj.value];
		delete userNameShows[obj.value];
	}
}
//实时记录状态
function recordUserId(obj) 
{
	//console.log(obj);
	obj.value=obj.value.split("@@@")[0];
	if ($(obj).is(":checked") == true) 
	{
		userIds[obj.value] = obj.value;
		userNameShows[obj.value] = obj.name;
	} else 
	{
		delete userIds[obj.value];
		delete userNameShows[obj.value];
	}
}

//获取用户选择界面选择的用户ID
function getSelectedRoleId() 
{
	var nametext = "";
	for ( var o in roleNameShows)
	{
		nametext = nametext + roleNameShows[o] + ","
	}
	nametext = nametext.substring(0, nametext.lastIndexOf(","));
	$('#selectRoleShow').text(nametext);
	$('#selectedRoleShow').text(nametext);
	if (nametext == "") 
	{
		showInfoModal("您没有选择任何角色，请重新选择");
		return;
	}
	else 
	{
		$("#selectRole").modal("hide");
	}
}
//实时记录状态
function recordRoleId(obj) 
{
	//console.log(obj);
	obj.value=obj.value.split("@@@")[0];
	if ($(obj).is(":checked") == true) 
	{
		roleIds[obj.value] = obj.value;
		roleNameShows[obj.value] = obj.name;
	} 
	else 
	{
		delete roleIds[obj.value];
		delete roleNameShows[obj.value];			
	}
}
//将选择的用户和角色进行匹配添加
function lastAdd() 
{
	if ($('#selectUserShow').text() == "选择人员"|| $('#selectUserShow').text() == "null"|| $('#selectUserShow').text() == ""|| $('#selectUserShow').text() == null)
	{
		showInfoModal("请先选择用户");
		return false;
	}
	if ($('#selectRoleShow').text() == "分配"|| $('#selectRoleShow').text() == "null"|| $('#selectRoleShow').text() == ""|| $('#selectRoleShow').text() == null) 
	{
		showInfoModal("请先选择角色");
		return false;
	}
	if ($('#selectSiteShow').val() == "null"|| $('#selectSiteShow').val() == ""|| $('#selectSiteShow').val() == null) 
	{
		showInfoModal("请先选择站点");
		return false;
	}

	//发请求
	$.ajax({
		type : "post",
		dataType : "json",
		url : 'addUsersAndRoles.bsh',
		data : 
		{
			"ids" : JSON.stringify(userIds),
			"Roleids" : JSON.stringify(roleIds),
			"Siteids" : JSON.stringify($('#selectSiteShow').val())
		},
		success : function(data) 
		{
			if (data != "") 
			{
				if(data.role>0&&data.site>0)
				{
					showSuccessOrErrorModal("添加成功","success");
					$("#myModal").modal("hide");
					//清空select2被选项
					$(".select2-selection__choice").remove();
					positionDataTable.draw();
				}
				else
				{
					showSuccessOrErrorModal("添加失败","error");
				}
			}
			else
			{
				showSuccessOrErrorModal("添加失败","error");
			}
		},
		error:  function(data)
     	{
         	showSuccessOrErrorModal("添加失败","error");
     	}
	});
}

//更新
function lastupdate() {
	
	//console.log($('#selectedSiteShow').val());
	//发请求
	$.ajax({
		type : "post",
		dataType : "json",
		url : 'updateUsersAndRoles.bsh',
		data : 
		{
			"ids" : JSON.stringify(userIds),
			"Roleids" : JSON.stringify(roleIds),
			"Siteids" : JSON.stringify($('#selectedSiteShow').val()),
			"resourceId" : window.top.currentMenuId
		},
		success : function(data) 
		{
			if (data != "") 
			{
				if(data.site>0&&data.role>0)
				{
					showSuccessOrErrorModal("更新成功","success");
					$("#myModalUpdate").modal("hide");
				    positionDataTable.draw();
				}
				else
				{
					showSuccessOrErrorModal("更新失败","error");
				}
			}
			else
			{
				showSuccessOrErrorModal("更新失败","error");
			}
		},
		error:  function(data)
     	{
         	showSuccessOrErrorModal("更新失败","error");
     	}
	});
}
//删除用户的角色
function deleteUser()
{
	//console.log(JSON.stringify(userIds));
	for(var j in userIds){
		if(userIds[j]==""){
			delete userIds[j]; 
		}
	}
	
	if(JSON.stringify(userIds).length==2)
	{
		showInfoModal("您未选择任何对象！！！")
		return ;
	}
	//发请求
	$.ajax({
		type : "post",
		dataType : "json",
		url : 'deleteUsersAndRoles.bsh',
		data : 
		{
			"ids" : JSON.stringify(userIds)
		},
		success : function(data) 
		{
			positionDataTable.draw();
			if (data != "")
			{
				if(data.flag==true)
				{
					showSuccessOrErrorModal("删除成功","success");
					$('#deleteModal').modal("hide");
					positionDataTable.draw();
				}
				else
				{
					showSuccessOrErrorModal("删除失败","error");
				}
			}
			else
			{
				showSuccessOrErrorModal("删除失败","error");
			}
		},
		error : function(data)
		{
			showSuccessOrErrorModal("删除失败","error");
		}
	});
}
				
function initTree(){
	//刷新人员选择列表
	if(userTimes==0)
	{
		initSelectTable();
		userTimes++;
	}
	else
	{
		UserDataTable.draw();
	}
	$.ajax({
	  	type : "POST",  
	    dataType : 'json',  
		url : jQuery.getBasePath() + "/adminManage/getGroupTree.bsh",  
		error : function(data) 
		{  
			showSuccessOrErrorModal("出错了！！:" + data,"error");
		},  
		success : function(data) 
		{  
			$('#tree').jstree({
				       'plugins': ["wholerow", "types"],
				        'core': {
				        "themes" : {
				                     "responsive": false
				                   },    
				        			'data':data   
				                   },
				         "types" : 
				         {
					          "default" : {
					                         "icon" : "fa fa-folder icon-state-warning icon-lg"
					                      },
					          "file" : 
					                      {
					                         "icon" : "fa fa-file icon-state-warning icon-lg"
					                      }
				         }
				                });
				            }
				    });   
	$('#selectUser').modal("show");
				}
	//单击事件
	$('#tree').bind("select_node.jstree", function (obj, data) {
	// 处理代码
	// 获取当前节点
	var currentNode = data.node;
	//console.log(currentNode.id+"@@@@@@@"+currentNode.text);
	$('#site').val(currentNode.id);
	 UserDataTable.draw();
});
function selectone()
{
	var id=$('#tree').jstree().get_checked();
	var parentid=$('#tree').jstree().get_parent(id);
	//console.log(id);
}
function reloadRoleTable()
{
	if(roleTimes==0)
	{
		initRoleTable();
		roleTimes++;
	}
	else
	{
		RoleDataTable.draw();
	}
	$('#selectRole').modal("show");
}
function selectUserClear()
{
	$('#selectUserName').val("");
	UserDataTable.draw();
}
function selectRoleClear()
{
	$('#selectRoleName').val("");
	RoleDataTable.draw();
}
//批量删除确认
function confirmDelete()
{
	var r=confirm("确定要批量删除吗？")
	if (r==true)
    {
	   deleteUser();
	}
}
//查看权限
function roleSource(obj)
{
	$('#sourceAndMenuList').empty();
	//发请求
	$.ajax({
		type : "post",
		dataType : "json",
		url : 'roleSourceAndMenu.bsh',
		data : 
		{
			"roleId" : obj.id
		},
		success : function(data) 
		{
			if(data!=null)
			{
				var content="";
				$.each(data,function(index,value){
					//console.log(index+"@@"+value.name);
					//<span style="margin-left:80px;width: 185px;display:inline-block;" id="modal_userinfo_verify">实名状态：被驳回</span>
					content=content+"<span style='margin-left:80px;width: 185px;display:inline-block;'>"+value.name+"</span>";
					if(index%2==0)
					{
						//console.log(content);
						$('#sourceAndMenuList').append("<div><p>"+content+"</p></div>");
						content="";
					}
				});
			}
		},
		error : function(data)
		{
		}
	});
}
</script>			
</body>
</html>